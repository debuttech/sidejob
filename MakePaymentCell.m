//
//  MakePaymentCell.m
//  SideJobsMileston2
//
//  Created by Debut Mac 4 on 07/09/15.
//  Copyright (c) 2015 debut.infotech. All rights reserved.
//

#import "MakePaymentCell.h"

@implementation MakePaymentCell

- (void)awakeFromNib
{
    
}
- (void)setSelected:(BOOL)selected animated:(BOOL)animated
{
    [super setSelected:selected animated:animated];
    CGFloat borderWidth = 0.3f;
    view_makepayment.frame = CGRectInset(view_makepayment.frame, -borderWidth, -borderWidth);
    view_makepayment.layer.borderColor = [UIColor lightGrayColor].CGColor;
    view_makepayment.layer.borderWidth = borderWidth;
    
    _lbl_Amount.layer.masksToBounds=YES;
    _lbl_Amount.layer.cornerRadius=_lbl_Amount.bounds.size.width/2;
    _img_user.layer.masksToBounds=YES;
    _img_user.layer.cornerRadius=_img_user.bounds.size.width/2;
    
}

@end
