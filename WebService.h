//
//  WebService.h
//  Barter
//
//  Created by Debut Infotech on 13/12/13.
//  Copyright (c) 2013 Debut Infotech. All rights reserved.
//

#import <Foundation/Foundation.h>
@interface WebService : NSObject

//+ (NSMutableURLRequest *) webServices :(NSMutableDictionary *)  webDict :(NSString *) addUrl;//Hitting Web Services Meth

+ (NSMutableArray *)signup :(NSMutableDictionary *)  webDict :(NSString *) addUrl;
+(NSMutableArray *)GetwithoutDict : (NSString *)serviceType;
+(NSMutableArray *)deleteData : (NSString *)serviceType;
+(NSMutableArray *)POSTwithoutDict : (NSString *)serviceType;
//+ (NSMutableArray *)signupT :(NSMutableDictionary *)  webDict :(NSString *) addUrl;
@end
