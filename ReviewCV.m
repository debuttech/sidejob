//
//  ReviewCV.m
//  WorkerEarning
//
//  Created by Debut Mac 4 on 25/08/15.
//  Copyright (c) 2015 debut.infotech. All rights reserved.
//

#import "ReviewCV.h"
#import "JSON.h"


@interface ReviewCV ()
{
    NSUserDefaults *defaults;
    NSString *rating;
}

@end

@implementation ReviewCV
@synthesize buttons;

- (void)viewDidLoad
{
    [super viewDidLoad];
    defaults=[NSUserDefaults standardUserDefaults];
    
    [defaults setObject:@"yes" forKey:@"pushjd1"];
    [defaults removeObjectForKey:@"pushjd"];
    [defaults synchronize];
    
    rating=[[NSString alloc]init];
   
    img_User.layer.masksToBounds=YES;
    img_User.layer.cornerRadius=img_User.bounds.size.width/2;
    lbl_Amount.layer.masksToBounds=YES;
    lbl_Amount.layer.cornerRadius=lbl_Amount.bounds.size.width/2;
    self.navigationItem.hidesBackButton=YES;
    self.navigationItem.title=@"Give Rating";
    
    [self showUserDetails];
    NSLog(@"%@",self.dict);
    
    UIButton *button=[UIButton buttonWithType:UIButtonTypeCustom];
    [button setFrame:CGRectMake(0, 0, 23, 27)];
    [button setImage:[UIImage imageNamed:@"arrow"] forState:UIControlStateNormal];
    [button addTarget:self action:@selector(leftbutton:) forControlEvents:UIControlEventTouchUpInside];
    self.navigationItem.leftBarButtonItem=[[UIBarButtonItem alloc]initWithCustomView:button];
    
    
    CGFloat borderWidth = 0.3f;
    _view_Review.frame = CGRectInset(_view_Review.frame, -borderWidth, -borderWidth);
    _view_Review.layer.borderColor = [UIColor lightGrayColor].CGColor;
    _view_Review.layer.borderWidth = borderWidth;
    textView_obj.layer.borderColor = [UIColor lightGrayColor].CGColor;
    textView_obj.layer.borderWidth = borderWidth;
    
    
    buttons = [[NSMutableArray alloc] init];
    for (int i = 0; i < 5; i++)
    {
        UIButton *button1 = [[UIButton alloc]init];
        button1.frame = CGRectMake((i * (36)), 5, 23, 21);
        button1.tag = i;
        [button1 setImage:[UIImage imageNamed:@"not_selected_star.png"] forState:UIControlStateNormal];
        [button1 addTarget:self action:@selector(ratingButtonTapped:) forControlEvents:UIControlEventTouchUpInside];
        [buttons addObject:button1];
        [stars_view addSubview: button1];
    }
}
-(void)showUserDetails
{
    lbl_Title.text=[self.dict objectForKey:@"title"];
    lbl_UserName.text=[[[[self.dict objectForKey:@"job_awarded"]objectAtIndex:0]objectForKey:@"user"]objectForKey:@"name"];
    lbl_Amount.text=[NSString stringWithFormat:@"$%@",[[[self.dict objectForKey:@"job_awarded"]objectAtIndex:0]objectForKey:@"proposed_amount"] ];
    
    NSString *d1=[[[self.dict objectForKey:@"job_awarded"]objectAtIndex:0]objectForKey:@"created_at"];
    NSArray *ad=[d1 componentsSeparatedByString:@" "];
    NSString *Sd=[ad objectAtIndex:0];
    lbl_StartTime.text=Sd;
    
    NSString *d2=[[[self.dict objectForKey:@"job_applications"]objectAtIndex:0]objectForKey:@"updated_at"];
    NSArray *d=[d2 componentsSeparatedByString:@" "];
    NSString *ed=[d objectAtIndex:0];
    lbl_CompletedTime.text=ed;
    
    NSString *img=[[[[self.dict objectForKey:@"job_awarded"]objectAtIndex:0]objectForKey:@"user"]objectForKey:@"profile_image"];
    if (img==nil||[img isEqualToString:@""])
    {
        img_User.image=[UIImage imageNamed:@"user.png"];
    }
    else
    {
    NSString*  imgProfile=[NSString stringWithFormat:@"%@%@",serviceLink,img];
    
    dispatch_async(dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_BACKGROUND, 0), ^{
        NSData * imageData = [NSData dataWithContentsOfURL:[NSURL URLWithString:imgProfile]];
        UIImage *image = [UIImage imageWithData:imageData];
        
        dispatch_sync(dispatch_get_main_queue(), ^{ //in main thread update the image
            img_User.image=image;
        });
    });
    }

    
    
}
-(IBAction)leftbutton:(id)sender
{
    [self.navigationController popViewControllerAnimated:YES];
}
-(IBAction)ratingButtonTapped:(UIButton *)button
{
    rating=[NSString stringWithFormat:@"%ld",(long)button.tag];
    for(int i = 0; i <= button.tag; ++i)
    {
        [[buttons objectAtIndex:i ] setImage:[UIImage imageNamed:@"selected_star.png"] forState:UIControlStateNormal];
    }
        
    for (int j = (int)button.tag + 1 ;j < buttons.count; ++j)
    {

        [[buttons objectAtIndex:j] setImage:[UIImage imageNamed:@"not_selected_star.png"] forState:UIControlStateNormal];
        
    }
}

- (void) viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:YES];
    Scroll_View.contentSize = CGSizeMake(200, 400);
}



- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];

}

- (IBAction)btnSubmitReview_Action:(id)sender
{
    NSString *token=[defaults objectForKey:@"user_token"];
    NSString *jobId=[[[self.dict objectForKey:@"job_awarded"]objectAtIndex:0]objectForKey:@"id"];
    NSString *userId=[[[[self.dict objectForKey:@"job_awarded"]objectAtIndex:0]objectForKey:@"user"] objectForKey:@"id"];
    NSInteger count;
    
    count=[rating integerValue];
    count+=1;
    
    
    NSMutableDictionary *dict=[[NSMutableDictionary alloc]init];
    [dict setObject:jobId forKey:@"id"];
    [dict setObject:userId forKey:@"applicant_id"];
    [dict setObject:[NSString stringWithFormat:@"%ld",(long)count] forKey:@"rating"];
    
    NSString *url=[NSString stringWithFormat:@"%@/rate-user?token=%@",serviceLink,token];
    id json = [WebService signup:dict:url];
    NSLog(@"%@",json);

    if ([[json objectForKey:@"message"]isEqualToString:@"Success"])
    {
        UIAlertView *alert=[[UIAlertView alloc]initWithTitle:@"" message:@"You have successfully given rating." delegate:self cancelButtonTitle:@"OK" otherButtonTitles:nil];
        [alert show];
        alert.tag=301;
           }
    else
    {
        UIAlertView *alert=[[UIAlertView alloc]initWithTitle:@"" message:@"Some error occure to give rating." delegate:nil cancelButtonTitle:@"OK" otherButtonTitles:nil];
        [alert show];
    }
   
}

- (void)alertView:(UIAlertView *)alertView clickedButtonAtIndex:(NSInteger)buttonIndex
{
    if (buttonIndex==0)
    {
        if (alertView.tag==301)
        {
            [self.navigationController popViewControllerAnimated:YES];

        }
    }
    
}

@end
