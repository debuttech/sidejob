//
//  ReviewCV.h
//  WorkerEarning
//
//  Created by Debut Mac 4 on 25/08/15.
//  Copyright (c) 2015 debut.infotech. All rights reserved.
//

#import <UIKit/UIKit.h>


@interface ReviewCV : UIViewController <UITextFieldDelegate>
{
    
    IBOutlet UIView *stars_view;
    IBOutlet UITextView *textView_obj;
    IBOutlet UIScrollView *Scroll_View;
    IBOutlet UIButton *btn_StbmitReview;
    IBOutlet UILabel *lbl_Title;
    IBOutlet UILabel *lbl_UserName;
    IBOutlet UILabel *lbl_Category;
    IBOutlet UILabel *lbl_StartTime;
    IBOutlet UILabel *lbl_CompletedTime;
    IBOutlet UIImageView *img_User;
    IBOutlet UIImageView *img_Category;
    IBOutlet UILabel *lbl_Amount;
}


@property (nonatomic, retain) IBOutletCollection(UIButton) NSMutableArray *buttons;
@property (strong, nonatomic) IBOutlet UIView *view_Review;
@property (strong, nonatomic) IBOutlet UITextView *textView_Field;
@property(strong,nonatomic)NSMutableDictionary *dict;
- (IBAction)btnSubmitReview_Action:(id)sender;


@end
