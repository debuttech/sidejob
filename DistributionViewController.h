//
//  DistributionViewController.h
//  sideJobs
//
//  Created by DebutMac4 on 20/08/15.
//  Copyright (c) 2015 DebutMac4. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface DistributionViewController : NSObject

-(BOOL)isValidEmail:(NSString *)str;
-(BOOL) validateAlphabets: (NSString *)alpha;
-(BOOL) isPasswordValid:(NSString *)pwd;

@end
