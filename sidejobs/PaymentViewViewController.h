//
//  PaymentViewViewController.h
//  sidejobs
//
//  Created by DebutMac4 on 29/10/15.
//  Copyright © 2015 DebutMac4. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "RESideMenu.h"

@interface PaymentViewViewController : UIViewController<UITableViewDataSource,UITableViewDelegate>
{
    
  
    IBOutlet UILabel *lbl_noREcord;
}
@property (strong, nonatomic) IBOutlet UIButton *btn_paymentRecieved;
@property (strong, nonatomic) IBOutlet UIButton *btn_PaymentDue;
@property (strong, nonatomic) IBOutlet UITableView *table_Payment;

- (IBAction)btnPayRecievedAction:(id)sender;
- (IBAction)btn_PayDueAction:(id)sender;

@end
