//
//  WorkerSearchTableViewCell.h
//  sideJobs
//
//  Created by DebutMac4 on 18/08/15.
//  Copyright (c) 2015 DebutMac4. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface WorkerSearchTableViewCell : UITableViewCell
@property (strong, nonatomic) IBOutlet UIView *view_tablebackground_object;
@property (strong, nonatomic) IBOutlet UILabel *label_CategoryTitle;
@property (strong, nonatomic) IBOutlet UILabel *lbl_category;
@property (strong, nonatomic) IBOutlet UILabel *lbl_address;
@property (strong, nonatomic) IBOutlet UILabel *lbl_date;
@property (strong, nonatomic) IBOutlet UILabel *lbl_time;
@property (strong, nonatomic) IBOutlet UIImageView *imageView_category;
@property (strong, nonatomic) IBOutlet UIButton *btnStatusJobApplied;
@property (strong, nonatomic) IBOutlet UILabel *lbl_Status;


@end
