//
//  SideJobsWorkersCell.h
//  WorkerEarning
//
//  Created by Debut Mac 4 on 19/08/15.
//  Copyright (c) 2015 debut.infotech. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface SideJobsWorkersCell : UITableViewCell
{
    
}
@property (strong, nonatomic) IBOutlet UILabel *lbl_HeadingName;
@property (strong, nonatomic) IBOutlet UILabel *lbl_CategoryName;
@property (strong, nonatomic) IBOutlet UILabel *lbl_PostedDate;
@property (strong, nonatomic) IBOutlet UIImageView *image_OfCategory;
@property (strong, nonatomic) IBOutlet UIView *view_AppliedJob;
@property (strong, nonatomic) IBOutlet UILabel *lbl_Applicant;
@property (strong, nonatomic) IBOutlet UIImageView *img_1;
@property (strong, nonatomic) IBOutlet UIImageView *img_2;
@property (strong, nonatomic) IBOutlet UIImageView *img_3;
@property (strong, nonatomic) IBOutlet UIImageView *img_4;
@property (strong, nonatomic) IBOutlet UIView *view_Status;

@end
