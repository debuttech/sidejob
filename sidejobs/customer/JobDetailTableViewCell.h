//
//  JobDetailTableViewCell.h
//  sidejobs
//
//  Created by DebutMac4 on 10/09/15.
//  Copyright (c) 2015 DebutMac4. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "ASStarRatingView.h"

@interface JobDetailTableViewCell : UITableViewCell

@property (strong, nonatomic) IBOutlet UIImageView *img_applicant;
@property (strong, nonatomic) IBOutlet UILabel *lbl_name;
@property (strong, nonatomic) IBOutlet UILabel *lbl_time;
@property (strong, nonatomic) IBOutlet UIButton *btn_AcceptObj;
@property (strong, nonatomic) IBOutlet UIButton *btn_RejectObj;
@property (strong, nonatomic) IBOutlet UIButton *btn_CancelToInvities;
@property (strong, nonatomic) IBOutlet UIView *view_background;
@property (strong, nonatomic) IBOutlet UILabel *lbl_NoPhoto;
@property (strong, nonatomic) IBOutlet UILabel *lbl_Amount;
@property (strong, nonatomic) IBOutlet UIButton *btnComment;
@property (strong, nonatomic) IBOutlet ASStarRatingView *staticStarRatingView;


@end
