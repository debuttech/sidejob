//
//  BeacomeServiceRepViewController.m
//  sidejobs
//
//  Created by DebutMac4 on 16/03/16.
//  Copyright © 2016 DebutMac4. All rights reserved.
//

#import "BeacomeServiceRepViewController.h"
#import <Helper.h>

@interface BeacomeServiceRepViewController ()
{
    NSString *strint_TC;
}

@end

@implementation BeacomeServiceRepViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    defaults = [NSUserDefaults standardUserDefaults];
    self.navigationItem.title=@"Help";
    
    self.navigationItem.hidesBackButton = true;
    UIButton *button=[UIButton buttonWithType:UIButtonTypeCustom];
    [button setFrame:CGRectMake(0, 0, 23, 21)];
    [button addTarget:self action:@selector(backBarButtonItem:) forControlEvents:UIControlEventTouchUpInside];
    [button setImage:[UIImage imageNamed:@"arrow.png"] forState:UIControlStateNormal];
    self.navigationItem.leftBarButtonItem=[[UIBarButtonItem alloc]initWithCustomView:button];
    
    UIButton *Right_button=[UIButton buttonWithType:UIButtonTypeCustom];
    [Right_button setFrame:CGRectMake(0, 0, 23, 21)];
    [Right_button setImage:[UIImage imageNamed:@"icon.png"] forState:UIControlStateNormal];
    Right_button.userInteractionEnabled = false;
    self.navigationItem.rightBarButtonItem=[[UIBarButtonItem alloc]initWithCustomView:Right_button];
    [SVProgressHUD show];
    [self performSelector:@selector(ServiceRep) withObject:nil afterDelay:0.3f];
}

-(void)backBarButtonItem:(id)sender
{
    [self.navigationController popViewControllerAnimated:YES];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

-(void)ServiceRep
{
    id json;
    NSString *url=[NSString stringWithFormat:@"%@pagesContent",serviceLink];
    
    if ([Helper isConnectedToInternet])
    {
        json = [WebService GetwithoutDict:url];
        if ([[json objectForKey:@"message" ]isEqualToString:@"Success"])
        {
            NSArray *arr = [json objectForKey:@"response"];
            [SVProgressHUD dismiss];
            
            for (int i=0; i < arr.count; i++)
            {
                NSString *ID = [NSString stringWithFormat:@"%@",[[arr objectAtIndex:i]objectForKey:@"id"] ];
                if ([ID isEqualToString:@"6"])
                {
                    strint_TC = [[arr objectAtIndex:i] objectForKey:@"content"];
                    NSAttributedString *attributedString = [[NSAttributedString alloc] initWithData:[strint_TC dataUsingEncoding:NSUnicodeStringEncoding] options:@{ NSDocumentTypeDocumentAttribute: NSHTMLTextDocumentType } documentAttributes:nil error:nil];
                    _textView_ServiceRep.attributedText = attributedString;
                }
            }
            
        }
        
        
    }
    else
    {
        UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"No Internet" message:@"Please check your internet connection." delegate:nil cancelButtonTitle:@"OK" otherButtonTitles:nil];
        [alert show];
        [SVProgressHUD dismiss];
        
    }
    
    
    [SVProgressHUD dismiss];
}


- (IBAction)button_send:(id)sender
{
//    indicator = [[UIActivityIndicatorView alloc] initWithFrame:CGRectMake(0.0f, 0.0f, 100.0f, 100.0f)];
//    [indicator setCenter:CGPointMake(160.0f, 208.0f)];
//    [indicator setActivityIndicatorViewStyle:UIActivityIndicatorViewStyleGray];
//    indicator.center=switchview.center;
//    [switchview addSubview:indicator];
    
    
    NSString *userstatus=[defaults objectForKey:@"user_status"];
    if ([userstatus isEqualToString:@"customer"])
    {
        if ([[defaults objectForKey:@"worker_approval"] isEqualToString:@"approval"])
        {
            UIAlertView *alert=[[UIAlertView alloc]initWithTitle:@"" message:@"Request is pending for approval." delegate:nil cancelButtonTitle:@"OK" otherButtonTitles:nil];
            [alert show];
            
        }
        else if ([[defaults objectForKey:@"worker_approval"] isEqualToString:@"approved"])
        {
            [defaults setObject:@"worker" forKey:@"user_status"];
            [defaults synchronize];
           // [self switched];
        }
        else
        {
            [SVProgressHUD show];
            [self performSelector:@selector(changeUserType) withObject:nil afterDelay:5.0];
        }
    }
    else if ([userstatus isEqualToString:@"worker"])
    {
        [defaults setObject:@"customer" forKey:@"user_status"];
        [defaults synchronize];
       // [self switched];
    }
}

-(void)switched
{
    UIViewController *root = [[UIStoryboard storyboardWithName:@"Main" bundle:NULL] instantiateViewControllerWithIdentifier:@"rootController"];
    UIWindow *wnd = [[[UIApplication sharedApplication] delegate] window];
    wnd.rootViewController = root;
    [wnd makeKeyAndVisible];
}



-(void)changeUserType
{
    NSString *token=[NSString stringWithFormat:@"%@",[defaults objectForKey:@"user_token"]];
    NSString *url=[NSString stringWithFormat:@"%@workersApproval/%@?token=%@",serviceLink,[defaults objectForKey:@"user_id"],token];
    
    if ([Helper isConnectedToInternet])
    {
        id  json = [WebService GetwithoutDict:url];
        if ([[json objectForKey:@"message"]isEqualToString:@"Success"])
        {
            [SVProgressHUD dismiss];
            
            UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"" message:@"You have been requested for switch as worker." delegate:nil cancelButtonTitle:@"OK" otherButtonTitles:nil];
            [alert show];
            
            
        }
        else
        {
            UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"" message:@"Some error occured to switch as worker." delegate:nil cancelButtonTitle:@"OK" otherButtonTitles:nil];
            [alert show];
        }
    }
    else
    {
        UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"No Internet" message:@"Please check your internet connection." delegate:nil cancelButtonTitle:@"OK" otherButtonTitles:nil];
        [alert show];
        [SVProgressHUD dismiss];

    }
    [SVProgressHUD dismiss];
}

@end
