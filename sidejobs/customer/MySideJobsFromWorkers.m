//
//  MySideJobs.m
//  WorkerEarning
//
//  Created by Debut Mac 4 on 19/08/15.
//  Copyright (c) 2015 debut.infotech. All rights reserved.

// this is customer side class 


#import "MySideJobsFromWorkers.h"
#import "SideJobsWorkersCell.h"
#import "TPKeyboardAvoidingScrollView.h"
#import "JobDetails.h"
#import "OnGoingCustomerTableViewCell.h"
#import "Helper.h"
#import "CompleteJobCustomerTableViewCell.h"
#import "MakePaymentVC.h"
#import "ReviewCV.h"
#import "JDIndividualCustomer.h"

#import <UIImageView+WebCache.h>

#import "Helper.h"
#import "Braintree.h"


@interface MySideJobsFromWorkers ()
{
    NSUserDefaults *defaults;
    NSMutableArray *jobsArr,*fromSrchArr,*arr_onGoingJob,*arr_CompleteJob;
    NSMutableArray *arrCount;
    NSMutableArray *arrCountF;
    AppDelegate *delegate;
    NSString *Jobstatus;
    NSInteger indexConfirmJob;
    
    Braintree *braintree;
    NSString *paymentToken;
    NSMutableArray *cardDetails;
    
    
    NSString *transationId,*jobId,*PaidTo,*paidBy,*amount,*remark,*string_acceptJobDate;
    
    NSString *status_WorkerCard;

}
@end

@implementation MySideJobsFromWorkers
-(void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:YES];
    noRecord_lbl.hidden = TRUE;
    NSLog(@"%@",[defaults objectForKey:@"pushjd1"]);

    [tableView_AppliedJobsWorker reloadData];
    if ([[defaults objectForKey:@"pushjd"]isEqualToString:@"yes"])
    {
        
    }
    
    else if ([[defaults objectForKey:@"pushjd1"]isEqualToString:@"yes"])
    {
        [SVProgressHUD setDefaultMaskType:SVProgressHUDMaskTypeClear];
        [SVProgressHUD show];
    
        
        [self performSelector:@selector(onGoningJob) withObject:nil afterDelay:0.3f];

        
    }
    else
    {
         [SVProgressHUD setDefaultMaskType:SVProgressHUDMaskTypeClear];
        [SVProgressHUD showWithStatus:@"loading..."];
        [self performSelector:@selector(getMySidejobs) withObject:nil afterDelay:0.3f];
    }
   }

- (void)viewDidLoad
{
    [super viewDidLoad];
    [self createUserExperience];
    
   
}
-(void)createUserExperience
{
    NSLog(@"%@",[defaults objectForKey:@"pushjd1"]);
    
    defaults=[NSUserDefaults standardUserDefaults];
    // [self confirmJobStatus];
//    [SVProgressHUD setDefaultMaskType:SVProgressHUDMaskTypeClear];
//    [SVProgressHUD showWithStatus:@"loading..."];
//    [self performSelector:@selector(getMySidejobs) withObject:nil afterDelay:0.3f];
    
    searchBar_obj.layer.borderWidth = 0.3f;
    searchBar_obj.layer.borderColor = [[UIColor orangeColor] CGColor];
    
    
    delegate =(AppDelegate*)[[UIApplication sharedApplication]delegate];
    self.navigationController.navigationBar.hidden = NO;
    self.navigationItem.title=@"My SideJobs";
    self.navigationItem.hidesBackButton=YES;
    Jobstatus=@"1";
    
    UIButton *button=[UIButton buttonWithType:UIButtonTypeCustom];
    [button setFrame:CGRectMake(0, 0, 23, 21)];
    [button setImage:[UIImage imageNamed:@"arrow.png"] forState:UIControlStateNormal];
    [button addTarget:self action:@selector(leftBarButton:) forControlEvents:UIControlEventTouchUpInside];
    self.navigationItem.leftBarButtonItem=[[UIBarButtonItem alloc]initWithCustomView:button];
    
    
    
    
    
    searchBar_obj.layer.borderWidth = 0.0f;
    searchBar_obj.layer.borderColor = [[UIColor colorWithRed:242.0f green:148.0f blue:29 alpha:1.0f] CGColor];
    
    
    
    
    UITapGestureRecognizer *tap1 = [[UITapGestureRecognizer alloc]
                                    initWithTarget:self
                                    action:@selector(dismissKeyboard)];
    
    [tableView_AppliedJobsWorker addGestureRecognizer:tap1];
    [tableView_OngoingWorkers addGestureRecognizer:tap1];
}

- (void) dismissKeyboard
{
    [searchBar_obj resignFirstResponder];
}
- (BOOL)searchBarShouldBeginEditing:(UISearchBar *)searchBar
{
    UITextField *searchBarTextField = nil;
    for (UIView *subView in searchBar_obj.subviews)
    {
        for (UIView *sndSubView in subView.subviews)
        {
            if ([sndSubView isKindOfClass:[UITextField class]])
            {
                searchBarTextField = (UITextField *)sndSubView;
                break;
            }
        }
    }
    searchBarTextField.enablesReturnKeyAutomatically = NO;
    return YES;
}

-(void)viewWillDisappear:(BOOL)animated
{
    [super viewWillDisappear:YES];
    [searchBar_obj resignFirstResponder];
    
}
-(void)leftBarButton:(UIButton*)sender
{
    [self.navigationController popViewControllerAnimated:YES];
}
-(void)viewDidAppear:(BOOL)animated
{
    [super viewDidAppear:YES];
}
#pragma mark
#pragma mark-Get My side jobs
-(void)getMySidejobs{
    NSString *token=[NSString stringWithFormat:@"%@",[defaults objectForKey:@"user_token"]];
    
    NSString *url=[NSString stringWithFormat:@"%@read-job/%@?token=%@",serviceLink,[defaults valueForKey:@"user_id"],token];
    
    jobsArr=[[NSMutableArray alloc]init];
    if ([Helper isConnectedToInternet])
    {
    id json=[WebService GetwithoutDict:url];
        NSLog(@"read-job %@",[json JSONRepresentation]);
    arrCount=[[NSMutableArray alloc]init];
    
    if ([[json objectForKey:@"message"]isEqualToString:@"Success"])
    {
      
         jobsArr = [json valueForKey:@"response"];
        tableView_AppliedJobsWorker.dataSource=self;
        tableView_AppliedJobsWorker.delegate=self;
        [tableView_AppliedJobsWorker reloadData];
    }

    else if ([[json objectForKey:@"error"]isEqualToString:@"token_expired"])
    {
        [SVProgressHUD dismiss];
        UIAlertView *alert=[[UIAlertView alloc]initWithTitle:@"" message:@"Session Expired Please login agian" delegate:nil cancelButtonTitle:@"OK" otherButtonTitles: nil];
        [alert show];
        [defaults setBool:NO forKey:@"logined"];
        [defaults synchronize];
        [[NSUserDefaults standardUserDefaults]synchronize];
        [delegate loginStatus];
    }
    
        if (jobsArr.count>0)
        {
            lbl_NoRecordSeg.hidden = YES;
        }else{
            lbl_NoRecordSeg.hidden=NO;
            
        }
    }

    
    else
    {
    UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"No Internet" message:@"Please check your internet connection." delegate:nil cancelButtonTitle:@"OK" otherButtonTitles:nil];
        [alert show];
    }
    NSLog(@"%@",jobsArr);
        
    [SVProgressHUD dismiss];
}
-(void)onGoningJob
{
    NSString *token=[NSString stringWithFormat:@"%@",[defaults objectForKey:@"user_token"]];
    NSString *url=[NSString stringWithFormat:@"%@customer-ongoing?token=%@",serviceLink,token];
    NSMutableDictionary *dict=[[NSMutableDictionary alloc]init];
    [dict setValue:[defaults objectForKey:@"user_id"] forKey:@"id"];
    id json = [WebService signup:dict :url];
    if ([[json objectForKey:@"message"]isEqualToString:@"Success"])
    {
        arr_onGoingJob=[[NSMutableArray alloc]init];
        NSMutableArray *arrResponse=[[NSMutableArray alloc]init];
        arrResponse=[json objectForKey:@"response"];
        
        for (int i=0;i<arrResponse.count; i++)
        {
             NSArray *ar=[[arrResponse objectAtIndex:i]objectForKey:@"job_applications"];
            
            if (ar.count)
            {
                
//            for (int j=0;j<ar.count; j++)
//            {

                NSString *st=[[[[arrResponse objectAtIndex:i]objectForKey:@"job_applications"]objectAtIndex:0] objectForKey:@"applicant_job_status"];
                if([st  isEqualToString:@"completed"] || [st  isEqualToString:@"accepted"]) //
                {
                [arr_onGoingJob addObject:[arrResponse objectAtIndex:i]];
                }
           // }
                
            }
            else
            {    
                [arr_onGoingJob addObject:[arrResponse objectAtIndex:i]];
       
            }
        }
            
        tableView_OngoingWorkers.dataSource=self;
        tableView_OngoingWorkers.delegate=self;
        [tableView_OngoingWorkers reloadData];
        
    }
    else if ([[json objectForKey:@"error"]isEqualToString:@"token_expired"])
    {
        [SVProgressHUD dismiss];
        UIAlertView *alert=[[UIAlertView alloc]initWithTitle:@"" message:@"Session Expired Please login agian" delegate:nil cancelButtonTitle:@"OK" otherButtonTitles: nil];
        [alert show];
    
        [defaults setBool:NO forKey:@"logined"];
        [defaults synchronize];
        [[NSUserDefaults standardUserDefaults]synchronize];
        [delegate loginStatus];
    }
    
    if ([Jobstatus isEqualToString:@"1"])
    {
        if (jobsArr.count)
        {
            lbl_NoRecordSeg.hidden = YES;
        }
        
        else{
            // [tableView_AppliedJobsWorker reloadData];
            lbl_NoRecordSeg.hidden=NO;
            
        }
 
    }
    if ([Jobstatus isEqualToString:@"2"])
    {
        if (arr_onGoingJob.count)
        {
            lbl_NoRecordSeg.hidden = YES;
        }
        
        else{
            // [tableView_AppliedJobsWorker reloadData];
            lbl_NoRecordSeg.hidden=NO;
            
        }
       
    }
        

    //[self CheckWorkerCard];
   
    NSLog(@"%@",arr_onGoingJob);
    [SVProgressHUD dismiss];
}
#pragma mark
#pragma mark- searchbar delegate method
- (void)searchBar:(UISearchBar *)searchBar textDidChange:(NSString *)searchText;
{
    fromSrchArr=[[NSMutableArray alloc] init];
    NSMutableArray *arr_filter=[[NSMutableArray alloc]init];
  
    if ([Jobstatus isEqualToString:@"1"])
    {
        arr_filter=[NSMutableArray arrayWithArray:jobsArr];
    }
    if ([Jobstatus isEqualToString:@"2"])
    {
        arr_filter=[NSMutableArray arrayWithArray:arr_onGoingJob];
    }
    if ([Jobstatus isEqualToString:@"3"])
    {
        arr_filter=[NSMutableArray arrayWithArray:arr_CompleteJob];
    }

    
    if(searchText.length > 0)
    {
        for (int i=0; i<arr_filter.count; i++)
        {
            if ([[[arr_filter objectAtIndex:i] valueForKey:@"title"] rangeOfString:searchText options:NSCaseInsensitiveSearch].location != NSNotFound)
            {
                [fromSrchArr addObject:[arr_filter objectAtIndex:i]];
             
                
            }
        }
    }
    if (!fromSrchArr.count && searchBar.text.length)
    {
         noRecord_lbl.hidden=NO;
        [tableView_AppliedJobsWorker setHidden:YES];
    }
    else if (fromSrchArr.count == 0 && searchBar.text.length == 0)
    {
        
        noRecord_lbl.hidden=YES;
        [tableView_AppliedJobsWorker setHidden:NO];
    }
    else
    {
    
        [tableView_AppliedJobsWorker setHidden:NO];
       // noRecord_lbl.hidden=YES;
    }
    
    if ([Jobstatus isEqualToString:@"1"])
    {
       [tableView_AppliedJobsWorker reloadData];
    }
    if ([Jobstatus isEqualToString:@"2"])
    {
        [tableView_OngoingWorkers reloadData];
    }
    if ([Jobstatus isEqualToString:@"3"])
    {
      [tableView_JobCompletedWorker reloadData];
    }
   
}
- (void)searchBarTextDidEndEditing:(UISearchBar *)searchBar
{
    if ([searchBar.text isEqualToString:@""] || !searchBar.text.length)
    {
        fromSrchArr=[[NSMutableArray alloc] init];
    }
    [tableView_AppliedJobsWorker reloadData];
    [tableView_OngoingWorkers reloadData];
    [tableView_JobCompletedWorker reloadData];

}
-(void)searchBarSearchButtonClicked:(UISearchBar *)searchBar
{
    [self.view endEditing:YES];
}

- (UIImage *)imageWithColor:(UIColor *)color {
    CGRect rect = CGRectMake(0.0f, 0.0f, 1.0f, 1.0f);
    
    UIGraphicsBeginImageContext(rect.size);
    CGContextRef context = UIGraphicsGetCurrentContext();
    CGContextSetFillColorWithColor(context, [color CGColor]);
    CGContextFillRect(context, rect);
    
    UIImage *image = UIGraphicsGetImageFromCurrentImageContext();
    UIGraphicsEndImageContext();
    
    return image;
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
}
#pragma mark
#pragma mark- tableView Delegate

-(NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    return 1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section;
{
    if (tableView== tableView_AppliedJobsWorker)
    {
        if (fromSrchArr.count)
        {
            return fromSrchArr.count;
        }
    else
        return jobsArr.count;
    }
    
    else if (tableView== tableView_OngoingWorkers)
    {
        if (fromSrchArr.count)
        {
            return fromSrchArr.count;
        }
        else
            return arr_onGoingJob.count;
    }
    else
        if (fromSrchArr.count)
        {
            return fromSrchArr.count;

        }
    else
        return arr_CompleteJob.count;
}
- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
  if (tableView== tableView_AppliedJobsWorker)
    {
    static NSString *customIdentifierForSideJobsFromWorkers = @"sideJobsWorkersCell";
        SideJobsWorkersCell *cell = [tableView dequeueReusableCellWithIdentifier:customIdentifierForSideJobsFromWorkers];
        if (cell == nil)
        {
            cell = [[SideJobsWorkersCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:customIdentifierForSideJobsFromWorkers];
        }
        
    NSMutableArray *arrData;
    NSMutableArray *arrCountState;
    if (jobsArr.count)
    {
        arrData=[[NSMutableArray alloc]init];
        arrCountState=[[NSMutableArray alloc]init];
        arrData=[NSMutableArray arrayWithArray:jobsArr];
        NSArray *arr1;
        
        for (NSMutableDictionary *dict in [jobsArr self])
        {
         arr1=[dict objectForKey:@"job_applications"];
            
         [arrCountState addObject:[NSString stringWithFormat:@"%lu",(unsigned long)[arr1 count] ]];
        }
         arrCount=arrCountState;
    }
    if (fromSrchArr.count)
    {
        arrData=[[NSMutableArray alloc]init];
        arrData=[NSMutableArray arrayWithArray:fromSrchArr];
          
        arrCountState=[[NSMutableArray alloc]init];
        for (NSMutableDictionary *dict in [fromSrchArr self])
        {
                NSArray* arr=[dict objectForKey:@"job_applications"];
    
                [arrCountState addObject:[NSString stringWithFormat:@"%lu",(unsigned long)[arr count] ]];
        }
        
        arrCount=arrCountState;
    }
        NSString *catId=[NSString stringWithFormat:@"%@",[[arrData valueForKey:@"category_id"] objectAtIndex:indexPath.row] ];
        cell.lbl_HeadingName.text = [[arrData valueForKey:@"title"] objectAtIndex:indexPath.row];
        cell.lbl_PostedDate.text = [[arrData valueForKey:@"created_at"] objectAtIndex:indexPath.row];
        cell.lbl_Applicant.text=[NSString stringWithFormat:@"%@ Applicants",[arrCountState objectAtIndex:indexPath.row]];
        //publish or not
        if ([[[arrData objectAtIndex:indexPath.row]valueForKey:@"published"]isEqualToString:@"1"])
        {
             cell.view_Status.backgroundColor=[UIColor orangeColor];
        }
        else
        {
            cell.view_Status.backgroundColor=[UIColor grayColor];
        }
        // set applicant img start
        NSArray *ArrImg=[[arrData objectAtIndex:indexPath.row]objectForKey:@"job_applications"];
        if ([ArrImg count]>=1)
        {
            if (ArrImg.count==1)
            {
                if ([ArrImg count]==1) {
                    NSString *im=[NSString stringWithFormat:@"%@",[[[ArrImg objectAtIndex:0]objectForKey:@"user"]objectForKey:@"profile_image"]];
                    if (im==nil||[im isEqualToString:@""]||[im isEqual:nil])
                    {
                        cell.img_3.image=[UIImage imageNamed:@"user.png"];
                    }
                    else
                    {
                    NSString *img1=[NSString stringWithFormat:@"%@%@",serviceLink,[[[ArrImg objectAtIndex:0]objectForKey:@"user"]objectForKey:@"profile_image"] ];
                        
                        
                        SDWebImageManager *manager = [SDWebImageManager sharedManager];
                        [manager downloadImageWithURL:[NSURL URLWithString:img1]
                                              options:0
                                             progress:^(NSInteger receivedSize, NSInteger expectedSize) {
                                                 // progression tracking code
                                             }
                                            completed:^(UIImage *image, NSError *error, SDImageCacheType cacheType, BOOL finished, NSURL *imageURL) {
                                                
                                                if ([img1 isEqualToString:@"http://sidejobapp.co:8000/"])
                                                {
                                                    cell.img_3.image = [UIImage imageNamed:@"defuser.png"];
                                                }
                                                else
                                                {
                                                    cell.img_3.image = image;
                                                    
                                                }
                                                
                                            }];
                        
                        
                    
//                    dispatch_async(dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_BACKGROUND, 0), ^{
//                        NSData * imageData = [NSData dataWithContentsOfURL:[NSURL URLWithString:img1]];
//                        UIImage *image = [UIImage imageWithData:imageData];
//                        
//                        dispatch_sync(dispatch_get_main_queue(), ^{ //in main thread update the image
//                            cell.img_3.image = image;
//                            cell.textLabel.text = @""; //add this update will reflect the changes
//                        });
//                    });
                    }
            }
            cell.img_1.hidden=YES;
            cell.img_2.hidden=YES;
            cell.img_3.hidden=NO;
            

            }
           else if  ([ArrImg count]==2)
            {
               // set image first
                NSString *im=[NSString stringWithFormat:@"%@",[[[ArrImg objectAtIndex:0]objectForKey:@"user"]objectForKey:@"profile_image"]];
                if (im==nil||[im isEqualToString:@""]||[im isEqual:nil])
                {
                    cell.img_2.image=[UIImage imageNamed:@"user.png"];
                }
                else
                {
               NSString *img1=[NSString stringWithFormat:@"%@%@",serviceLink,[[[ArrImg objectAtIndex:0]objectForKey:@"user"]objectForKey:@"profile_image"] ];
                    
                    
                    
                    SDWebImageManager *manager = [SDWebImageManager sharedManager];
                    [manager downloadImageWithURL:[NSURL URLWithString:img1]
                                          options:0
                                         progress:^(NSInteger receivedSize, NSInteger expectedSize) {
                                             // progression tracking code
                                         }
                                        completed:^(UIImage *image, NSError *error, SDImageCacheType cacheType, BOOL finished, NSURL *imageURL) {
                                            
                                            if ([img1 isEqualToString:@"http://sidejobapp.co:8000/"])
                                            {
                                                cell.img_2.image = [UIImage imageNamed:@"defuser.png"];
                                            }
                                            else
                                            {
                                                cell.img_2.image = image;
                                                
                                            }
                                            
                                        }];
                    
        
//                dispatch_async(dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_BACKGROUND, 0), ^{
//                    NSData * imageData = [NSData dataWithContentsOfURL:[NSURL URLWithString:img1]];
//                    UIImage *image = [UIImage imageWithData:imageData];
//                    
//                    dispatch_sync(dispatch_get_main_queue(), ^{ //in main thread update the image
//                        cell.img_2.image = image;
//                        cell.textLabel.text = @""; //add this update will reflect the changes
//                    });
//                });
                    
                    
                }

            
                //set image second
                NSString *im1=[NSString stringWithFormat:@"%@",[[[ArrImg objectAtIndex:1]objectForKey:@"user"]objectForKey:@"profile_image"]];
                if (im1==nil||[im1 isEqualToString:@""]||[im1 isEqual:nil])
                {
                    cell.img_3.image=[UIImage imageNamed:@"user.png"];
                }
                else
                {
              NSString *img2=[NSString stringWithFormat:@"%@%@",serviceLink,[[[ArrImg objectAtIndex:1]objectForKey:@"user"]objectForKey:@"profile_image"] ];
                    
                    
                    SDWebImageManager *manager = [SDWebImageManager sharedManager];
                    [manager downloadImageWithURL:[NSURL URLWithString:img2]
                                          options:0
                                         progress:^(NSInteger receivedSize, NSInteger expectedSize) {
                                             // progression tracking code
                                         }
                                        completed:^(UIImage *image, NSError *error, SDImageCacheType cacheType, BOOL finished, NSURL *imageURL) {
                                            
                                            if ([img2 isEqualToString:@"http://sidejobapp.co:8000/"])
                                            {
                                                cell.img_3.image = [UIImage imageNamed:@"defuser.png"];
                                            }
                                            else
                                            {
                                                cell.img_3.image = image;
                                                
                                            }
                                            
                                        }];
 
                    
                    
                    
//        
//                dispatch_async(dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_BACKGROUND, 0), ^{
//                    NSData * imageData = [NSData dataWithContentsOfURL:[NSURL URLWithString:img2]];
//                    UIImage *image = [UIImage imageWithData:imageData];
//                    
//                    dispatch_sync(dispatch_get_main_queue(), ^{ //in main thread update the image
//                        cell.img_3.image = image;
//                        cell.textLabel.text = @""; //add this update will reflect the changes
//                    });
//                });
                    
                    
                }

                cell.img_1.hidden=YES;
                cell.img_2.hidden=NO;
                cell.img_3.hidden=NO;
                
            
            }
            else if ([ArrImg count]>2)
            {
                // set image first
                NSString *im=[NSString stringWithFormat:@"%@",[[[ArrImg objectAtIndex:0]objectForKey:@"user"]objectForKey:@"profile_image"]];
                if (im==nil||[im isEqualToString:@""]||[im isEqual:nil])
                {
                    cell.img_1.image=[UIImage imageNamed:@"user.png"];
                }
                else
                {
                    NSString *img1=[NSString stringWithFormat:@"%@%@",serviceLink,[[[ArrImg objectAtIndex:0]objectForKey:@"user"]objectForKey:@"profile_image"] ];
                    
                    
                    
                    SDWebImageManager *manager = [SDWebImageManager sharedManager];
                    [manager downloadImageWithURL:[NSURL URLWithString:img1]
                                          options:0
                                         progress:^(NSInteger receivedSize, NSInteger expectedSize) {
                                             // progression tracking code
                                         }
                                        completed:^(UIImage *image, NSError *error, SDImageCacheType cacheType, BOOL finished, NSURL *imageURL) {
                                            
                                            if ([img1 isEqualToString:@"http://sidejobapp.co:8000/"])
                                            {
                                                cell.img_1.image = [UIImage imageNamed:@"defuser.png"];
                                            }
                                            else
                                            {
                                                cell.img_1.image = image;
                                                
                                            }
                                            
                                        }];
                    
                    
                    
                    
//                    dispatch_async(dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_BACKGROUND, 0), ^{
//                        NSData * imageData = [NSData dataWithContentsOfURL:[NSURL URLWithString:img1]];
//                        UIImage *image = [UIImage imageWithData:imageData];
//                        
//                        dispatch_sync(dispatch_get_main_queue(), ^{ //in main thread update the image
//                            cell.img_1.image = image;
//                            cell.textLabel.text = @""; //add this update will reflect the changes
//                        });
//                    });
                }
                
                
                //set image second
                NSString *im1=[NSString stringWithFormat:@"%@",[[[ArrImg objectAtIndex:1]objectForKey:@"user"]objectForKey:@"profile_image"]];
                if (im1==nil||[im1 isEqualToString:@""]||[im1 isEqual:nil])
                {
                    cell.img_2.image=[UIImage imageNamed:@"user.png"];
                }
                else
                {
                    NSString *img2=[NSString stringWithFormat:@"%@%@",serviceLink,[[[ArrImg objectAtIndex:1]objectForKey:@"user"]objectForKey:@"profile_image"] ];
                    
                    
                    
                    
                    SDWebImageManager *manager = [SDWebImageManager sharedManager];
                    [manager downloadImageWithURL:[NSURL URLWithString:img2]
                                          options:0
                                         progress:^(NSInteger receivedSize, NSInteger expectedSize) {
                                             // progression tracking code
                                         }
                                        completed:^(UIImage *image, NSError *error, SDImageCacheType cacheType, BOOL finished, NSURL *imageURL) {
                                            
                                            if ([img2 isEqualToString:@"http://sidejobapp.co:8000/"])
                                            {
                                                cell.img_2.image = [UIImage imageNamed:@"defuser.png"];
                                            }
                                            else
                                            {
                                                cell.img_2.image = image;
                                                
                                            }
                                            
                                        }];
                    
                    
                    
//                    dispatch_async(dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_BACKGROUND, 0), ^{
//                        NSData * imageData = [NSData dataWithContentsOfURL:[NSURL URLWithString:img2]];
//                        UIImage *image = [UIImage imageWithData:imageData];
//                        
//                        dispatch_sync(dispatch_get_main_queue(), ^{ //in main thread update the image
//                            cell.img_2.image = image;
//                            cell.textLabel.text = @""; //add this update will reflect the changes
//                        });
//                    });
                }
                
                cell.img_1.hidden=NO;
                cell.img_2.hidden=NO;
                cell.img_3.hidden=NO;
                cell.img_3.image=[UIImage imageNamed:@"circle"];
            }

            }
          else
           {
              cell.img_1.hidden=YES;
              cell.img_2.hidden=YES;
              cell.img_3.hidden=YES;
            }
          // set applicant img end
        
        
        if ([catId isEqualToString:@"1"]) {
            cell.lbl_CategoryName.text = @"Home" ;
            cell.image_OfCategory.image=[UIImage imageNamed:@"cat_home"];
            
        }else if([catId isEqualToString:@"2"])
        {
            cell.lbl_CategoryName.text = @"Auto" ;
            cell.image_OfCategory.image=[UIImage imageNamed:@"car_icon"];
            
        }else if([catId isEqualToString:@"3"])
        {
            cell.lbl_CategoryName.text =@"Electronic" ;
            cell.image_OfCategory.image=[UIImage imageNamed:@"cat_elc"];
        }
        return cell;
        
    }
   else if(tableView== tableView_OngoingWorkers)
    {
       // noRecord_lbl.hidden=YES;
        static NSString *customIdentifierForSideJobsFromWorkers = @"onGoingCustomerCell";
        OnGoingCustomerTableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:customIdentifierForSideJobsFromWorkers];
        if (cell == nil)
        {
            cell = [[OnGoingCustomerTableViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:customIdentifierForSideJobsFromWorkers];
        }
        
        NSMutableArray *arrData;
        arrData=[[NSMutableArray alloc]init];
        if (arr_onGoingJob.count)
        {
           arrData=[NSMutableArray arrayWithArray:arr_onGoingJob];
        }
        if (fromSrchArr.count)
        {
           arrData=[NSMutableArray arrayWithArray:fromSrchArr];
        }

        NSMutableArray *arr=[[arrData objectAtIndex:indexPath.row]objectForKey:@"job_awarded"];
        NSMutableDictionary *dict=[[NSMutableDictionary alloc]init];
        dict=[arr objectAtIndex:0];
        [cell.btn_amount setTitle:[NSString stringWithFormat:@"$%@",[dict objectForKey:@"proposed_amount"]] forState:UIControlStateNormal];
        NSString *url=[[dict objectForKey:@"user"]objectForKey:@"profile_image"];
        NSURL *imageProfile=[NSURL URLWithString:[NSString stringWithFormat:@"%@%@",serviceLink,url]];
        if (url.length<1)
        {
            cell.img_worker.image=[UIImage imageNamed:@"user.png"];
        }
        else
        {
       
            dispatch_async(dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_BACKGROUND, 0), ^{
                NSData * imageData = [NSData dataWithContentsOfURL:imageProfile];
                UIImage *image = [UIImage imageWithData:imageData];
                
                dispatch_sync(dispatch_get_main_queue(), ^{ //in main thread update the image
                    cell.img_worker.image = image;
                    cell.textLabel.text = @""; //add this update will reflect the changes
                });
            });

            
        }
        
        cell.button_Refund.tag = indexPath.row;
        
         [cell.button_Refund addTarget:self action:@selector(refund:) forControlEvents:UIControlEventTouchUpInside];
        
        
        cell.btn_ConfirmJob.tag=indexPath.row;
       // cell.btn_GiveReview.tag=indexPath.row;
        
        [cell.btn_ConfirmJob addTarget:self action:@selector(confirmJobFromC:) forControlEvents:UIControlEventTouchUpInside];
        
       // [cell.btn_GiveReview addTarget:self action:@selector(GiveReviewFromC:) forControlEvents:UIControlEventTouchUpInside];
        
        //Seprated the time
        //start job time
        
        
        NSString *startTime=[[[[arrData objectAtIndex:indexPath.row]objectForKey:@"job_awarded"] objectAtIndex:0]objectForKey:@"created_at"];
        NSArray *ArrStart=[startTime componentsSeparatedByString:@" "];
        NSString* seprateStarttime=[ArrStart objectAtIndex:0];
        cell.lbl_startdate.text=seprateStarttime;
        //show and hide givereview and confirmjob button
        NSArray *arrJAStatu=[[arrData objectAtIndex:indexPath.row]objectForKey:@"job_applications"];
        
        cell.btn_Rating.tag=indexPath.row;
        [cell.btn_Rating addTarget:self action:@selector(GiveReviewFromC:) forControlEvents:UIControlEventTouchUpInside];

        
        
        cell.btn_ConfirmJob.enabled=NO;
        cell.btn_ConfirmJob.backgroundColor=[UIColor grayColor];
        
        cell.btn_Rating.backgroundColor=[UIColor grayColor];
        cell.btn_Rating.enabled=NO;
        
        cell.button_Refund.backgroundColor=[UIColor orangeColor];
        cell.button_Refund.enabled = YES;
        
        
        
        
        
        
        if (arrJAStatu.count)
        {
            
//            for (int j=0;j<arrJAStatu.count; j++)
//            {
                NSString *st=[[[[arrData objectAtIndex:indexPath.row]objectForKey:@"job_applications"]objectAtIndex:0] objectForKey:@"applicant_job_status"];
                
                if ([st isEqualToString:@"completed"])
                {
                    cell.btn_ConfirmJob.enabled=YES;
                    cell.btn_ConfirmJob.backgroundColor=[UIColor orangeColor];
                    
                    cell.btn_Rating.backgroundColor=[UIColor orangeColor];
                    cell.btn_Rating.enabled=YES;
                    
                    cell.button_Refund.backgroundColor =[UIColor grayColor];
                    cell.button_Refund.enabled = NO;
                    
                }
                else
                {
                    
                }
         //   }

            
           
           
           // cell.btn_GiveReview.enabled=YES;
        }
        else
        {
//            cell.btn_ConfirmJob.enabled=NO;
//            cell.btn_ConfirmJob.backgroundColor=[UIColor grayColor];
//            
//            cell.btn_Rating.backgroundColor=[UIColor grayColor];
//            cell.btn_Rating.enabled=NO;
            
           // cell.btn_ConfirmJob.backgroundColor=[UIColor grayColor];
          // cell.btn_GiveReview.enabled=NO;
        }
        
        
     
        
    
        
        cell.lbl_nameWorker.text=[[dict objectForKey:@"user"]objectForKey:@"name"];
        cell.lbl_title.text=[[arrData objectAtIndex:indexPath.row]objectForKey:@"title"];
        NSString *catId=[NSString stringWithFormat:@"%@",[[arrData objectAtIndex:indexPath.row]objectForKey:@"category_id"] ];
        
        if ([catId isEqualToString:@"1"]) {
            cell.lbl_category.text = @"Home" ;
            cell.img_category.image=[UIImage imageNamed:@"cat_home"];
            
        }else if([catId isEqualToString:@"2"])
        {
            cell.lbl_category.text = @"Auto" ;
            cell.img_category.image=[UIImage imageNamed:@"car_icon"];
            
        }else if([catId isEqualToString:@"3"])
        {
            cell.lbl_category.text =@"Electronic" ;
            cell.img_category.image=[UIImage imageNamed:@"cat_elc"];
        }
    return cell;
    }
    else
    {
        static NSString *customIdentifierForSideJobsFromWorkers = @"jobCompletedCustomer";
        CompleteJobCustomerTableViewCell  *cell = [tableView dequeueReusableCellWithIdentifier:customIdentifierForSideJobsFromWorkers];
        if (cell == nil)
        {
            cell = [[CompleteJobCustomerTableViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:customIdentifierForSideJobsFromWorkers];
        }
        
        
        NSMutableArray *arrData;
        arrData=[[NSMutableArray alloc]init];
        if (arr_CompleteJob.count)
        {
            arrData=[NSMutableArray arrayWithArray:arr_CompleteJob];
        }
        if (fromSrchArr.count)
        {
            arrData=[NSMutableArray arrayWithArray:fromSrchArr];
        }
        
        
        
        
        NSString *catId=[NSString stringWithFormat:@"%@",[[arrData objectAtIndex:indexPath.row]objectForKey:@"category_id"] ];
        
        NSString *strImg=[[[[[arrData objectAtIndex:indexPath.row]objectForKey:@"job_awarded"]objectAtIndex:0]objectForKey:@"user"]objectForKey:@"profile_image"];
        
        
        if (strImg==nil||[strImg isEqualToString:@""])
        {
            cell.img_User.image = [UIImage imageNamed:@"user.png"];
        }
        else
        {
            NSString *imgProfile=[NSString stringWithFormat:@"%@%@",serviceLink,strImg];
            dispatch_async(dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_BACKGROUND, 0), ^{
                NSData * imageData = [NSData dataWithContentsOfURL:[NSURL URLWithString:imgProfile]];
                UIImage *image = [UIImage imageWithData:imageData];
                
                dispatch_sync(dispatch_get_main_queue(), ^{
                    cell.img_User.image = image;
                    
                });
            });
        }
      
        
        cell.lbl_Title.text=[[arrData objectAtIndex:indexPath.row]objectForKey:@"title"];
        cell.lbl_UserNmae.text=[[[[[arrData objectAtIndex:indexPath.row]objectForKey:@"job_awarded"]objectAtIndex:0]objectForKey:@"user"]objectForKey:@"name"];
        
        //complete job time
        NSString *completeTime=[[[[arrData objectAtIndex:indexPath.row]objectForKey:@"job_applications"] objectAtIndex:0]objectForKey:@"updated_at"];
        NSArray *Arrtime=[completeTime componentsSeparatedByString:@" "];
        NSString* seprateCompleteTime=[Arrtime objectAtIndex:0];
        cell.lbl_CompleteTime.text=seprateCompleteTime;

        //start job time
        NSString *startTime=[[[[arrData objectAtIndex:indexPath.row]objectForKey:@"job_awarded"] objectAtIndex:0]objectForKey:@"created_at"];
        NSArray *ArrStart=[startTime componentsSeparatedByString:@" "];
        NSString* seprateStarttime=[ArrStart objectAtIndex:0];
        cell.lbl_StartTime.text=seprateStarttime;
        
        cell.lbl_Amount.hidden=NO;
        cell.lbl_Amount.text=[NSString stringWithFormat:@"$%@",[[[[arrData objectAtIndex:indexPath.row]objectForKey:@"job_awarded"]objectAtIndex:0]objectForKey:@"proposed_amount"] ];
        
        
        if ([[[[[arrData objectAtIndex:indexPath.row]objectForKey:@"job_applications"]objectAtIndex:0]objectForKey:@"applicant_job_status"]isEqualToString:@"confirmed"])
        {
            cell.label_status.text = @"Completed";
            cell.label_status.textColor = [UIColor grayColor];
        }
        if ([[[[[arrData objectAtIndex:indexPath.row]objectForKey:@"job_applications"]objectAtIndex:0]objectForKey:@"applicant_job_status"]isEqualToString:@"refunded"])
        {
            cell.label_status.text = @"Refunded";
            cell.label_status.textColor = [UIColor redColor];
        }
        
        
        if ([catId isEqualToString:@"1"]) {
            cell.lbl_categoryName.text = @"Home" ;
            cell.img_Category.image=[UIImage imageNamed:@"cat_home"];
            
        }else if([catId isEqualToString:@"2"])
        {
            cell.lbl_categoryName.text = @"Auto" ;
            cell.img_Category.image=[UIImage imageNamed:@"car_icon"];
            
        }else if([catId isEqualToString:@"3"])
        {
            cell.lbl_categoryName.text =@"Electronic" ;
            cell.img_Category.image=[UIImage imageNamed:@"cat_elc"];
        }

        return cell;
    }
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    
    
    if ([Jobstatus isEqualToString:@"1"])
    {
        JobDetails *jb=[self.storyboard instantiateViewControllerWithIdentifier:@"JobDetails"];
        if (fromSrchArr.count)
        {
            jb.jobDict=[fromSrchArr objectAtIndex:indexPath.row];
            delegate.applicantCount=[arrCount objectAtIndex:indexPath.row];
        }
        else
        {
            jb.jobDict=[jobsArr objectAtIndex:indexPath.row];
            delegate.applicantCount=[arrCount objectAtIndex:indexPath.row];
            
        }
        [self.navigationController pushViewController:jb animated:NO];
    }
    else if  ([Jobstatus isEqualToString:@"3"])
    {
         JDIndividualCustomer *jd=[self.storyboard instantiateViewControllerWithIdentifier:@"JDIndividualCustomer"];
        if (fromSrchArr.count)
        {
            jd.dict=[fromSrchArr objectAtIndex:indexPath.row];
        }
        else
        {
            jd.dict=[arr_CompleteJob objectAtIndex:indexPath.row];
            delegate.applicantCount=[arr_CompleteJob objectAtIndex:indexPath.row];
            
        }

        [self.navigationController pushViewController:jd animated:NO];
        
    }
    
    

}

-(UIView *)tableView:(UITableView *)tableView viewForFooterInSection:(NSInteger)section
{
    UIView *view = [[UIView alloc] initWithFrame:CGRectMake(0, 0, 414, 0)];
    return view;
}


#pragma mark
#pragma mark - Refund Payment

-(void)refund:(id)sender
{
    UIButton *button=(UIButton*)sender;
   // NSInteger t=button.tag;
    
    indexConfirmJob=button.tag;
    
    UIAlertView * alert = [[UIAlertView alloc]initWithTitle:@"" message:@"Do you want refund this payment for this job" delegate:self cancelButtonTitle:@"OK" otherButtonTitles:@"Cancel",nil];
    alert.tag = 2;
    
    [alert show];
}


#pragma mark
#pragma mark - func for refund Payment

-(void)RefundPaymentFunc
{
    
    
    NSMutableArray *arr=[[NSMutableArray alloc]init];
    if (fromSrchArr.count)
    {
        arr=[NSMutableArray arrayWithArray:fromSrchArr];
    }
    else
    {
        arr=[NSMutableArray arrayWithArray:arr_onGoingJob];
    }
    
    transationId=[[[[arr objectAtIndex:indexConfirmJob]objectForKey:@"job_transaction"] objectAtIndex:0]objectForKey:@"transaction_id"];
    
    jobId=[[[[arr objectAtIndex:indexConfirmJob]objectForKey:@"job_transaction"] objectAtIndex:0]objectForKey:@"job_id"];
    
    PaidTo=[[[[arr objectAtIndex:indexConfirmJob]objectForKey:@"job_transaction"] objectAtIndex:0]objectForKey:@"paid_to"];
    
    paidBy=[[[[arr objectAtIndex:indexConfirmJob]objectForKey:@"job_transaction"] objectAtIndex:0]objectForKey:@"paid_by"];
    
    amount=[[[[arr objectAtIndex:indexConfirmJob]objectForKey:@"job_transaction"] objectAtIndex:0]objectForKey:@"amount"];
    
    remark=[[[[arr objectAtIndex:indexConfirmJob]objectForKey:@"job_transaction"] objectAtIndex:0]objectForKey:@"remarks"];

    
    
    
    NSString *token=[NSString stringWithFormat:@"%@",[defaults objectForKey:@"user_token"]];
    
    NSString *url=[NSString stringWithFormat:@"%@refundPayment?token=%@",serviceLink,token];
    NSMutableDictionary *jsonDict=[[NSMutableDictionary alloc]init];
    
    [jsonDict setValue:transationId forKey:@"transaction_id"] ;
    [jsonDict setValue:jobId forKey:@"job_id"] ;
    [jsonDict setValue:PaidTo forKey:@"paid_to"] ;
    [jsonDict setValue:paidBy forKey:@"paid_by"] ;
    [jsonDict setValue:amount forKey:@"amount"] ;
    [jsonDict setValue:remark forKey:@"remark"] ;
    
    
    if ([Helper isConnectedToInternet])
    {
        
        id json=[WebService signup:jsonDict :url];
        
        if ([[json objectForKey:@"message"]isEqualToString:@"Success"])
        {
            [self markFromRefundStatus];
            
            //[SVProgressHUD dismiss];
           
           
        }
        else
        {
            UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"Alert!" message:@"Warning! Some error is encountered to refund payment." delegate:nil cancelButtonTitle:@"OK" otherButtonTitles:nil];
            [alert show];
            [SVProgressHUD dismiss];
            
        }
        
    }
    else{
        UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"No Internet" message:@"Please check your internet connection." delegate:nil cancelButtonTitle:@"OK" otherButtonTitles:nil];
        
        [alert show];
        [SVProgressHUD dismiss];
        
    }

}

#pragma mark
#pragma mark - after refund job as complted



-(void)markFromRefundStatus
{
    NSMutableArray *arr=[[NSMutableArray alloc]init];
    if (fromSrchArr.count)
    {
        arr=[NSMutableArray arrayWithArray:fromSrchArr];
    }
    else
    {
        arr=[NSMutableArray arrayWithArray:arr_onGoingJob];
    }
    
    NSString *id_job=  [[[[arr  objectAtIndex:indexConfirmJob ]objectForKey:@"job_applications"]objectAtIndex:0]objectForKey:@"id"];
    NSString *proposedAmount=[[[[arr objectAtIndex:indexConfirmJob]objectForKey:@"job_awarded"] objectAtIndex:0]objectForKey:@"proposed_amount"];
    
    
    NSString *token=[NSString stringWithFormat:@"%@",[defaults objectForKey:@"user_token"]];
    
    NSString *url=[NSString stringWithFormat:@"%@job-status?token=%@",serviceLink,token];
    NSMutableDictionary *jsonDict=[[NSMutableDictionary alloc]init];
    
    [jsonDict setValue:id_job forKey:@"job_application_id"] ;
    [jsonDict setValue:@"refunded" forKey:@"applicant_job_status"];
    [jsonDict setValue:proposedAmount forKey:@"proposed_amount"];
    [jsonDict setValue:@""forKey:@"comment"];
    
    //change to Notification
    
    [jsonDict setValue:[[[[arr objectAtIndex:indexConfirmJob] objectForKey:@"job_awarded"] objectAtIndex:0]objectForKey:@"job_id"] forKey:@"job_id"];
    
    [jsonDict setValue:[defaults objectForKey:@"user_id"] forKey:@"job_user_id"];
    
    [jsonDict setValue:[defaults objectForKey:@"user_id"] forKey:@"sender_id"];
    
    [jsonDict setValue:[[[[[arr objectAtIndex:indexConfirmJob] objectForKey:@"job_awarded"] objectAtIndex:0]objectForKey:@"user"] objectForKey:@"id"] forKey:@"receiver_id"];
    
    [jsonDict setValue:[defaults objectForKey:@"p_name"] forKey:@"sender_name"];
    
    
    id json=[WebService signup:jsonDict :url];
    if ([[json objectForKey:@"message"]isEqualToString:@"Success"])
    {
        UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"" message:@"Payment has been refund for this job." delegate:nil cancelButtonTitle:@"OK" otherButtonTitles:nil];
        [alert show];
        // UIAlertView *alert=[[UIAlertView alloc]initWithTitle:@"" message:@"You have confirmed this job." delegate:nil cancelButtonTitle:@"OK" otherButtonTitles: nil];
        [self onGoningJob];
        [alert show];
    }
    else
    {
        UIAlertView *alert=[[UIAlertView alloc]initWithTitle:@"Alert!" message:@"Payment has been refund but Some error is encountered while going for confirm job." delegate:nil cancelButtonTitle:@"OK" otherButtonTitles: nil];
        [alert show];
        [SVProgressHUD dismiss];
    }
    
    [SVProgressHUD dismiss];
    
}









#pragma mark 
#pragma mark-complete job Section Confirm Job
-(void)GiveReviewFromC:(id)sender
{
    UIButton *button=(UIButton*)sender;
    NSInteger t=button.tag;
    ReviewCV *rv=[self.storyboard instantiateViewControllerWithIdentifier:@"reviewCV"];
    
    NSString *rating;
    if (fromSrchArr.count)
    {
        rv.dict=[fromSrchArr objectAtIndex:t];
        
         rating=[[[[fromSrchArr objectAtIndex:t]objectForKey:@"job_awarded"]objectAtIndex:0]objectForKey:@"rating"];
    }
    else
    {
        rv.dict=[arr_onGoingJob objectAtIndex:t];
        
         rating=[[[[arr_onGoingJob objectAtIndex:t]objectForKey:@"job_awarded"]objectAtIndex:0]objectForKey:@"rating"];
    }
    
   
    if ([rating isEqualToString:@""])
    {
        [self.navigationController pushViewController:rv animated:YES];
    }
    else
    {
        UIAlertView *alert=[[UIAlertView alloc]initWithTitle:@"" message:@"You have already given the rating for this job." delegate:nil cancelButtonTitle:@"OK" otherButtonTitles:nil];
        [alert show];
        
    }
    
}
-(IBAction)confirmJobFromC:(id)sender
{
    UIButton *button=(UIButton*)sender;
    indexConfirmJob=button.tag;
    NSLog(@"%ld",(long)button.tag);
    
    // [self CheckWorkerCard];
    
   // [SVProgressHUD show];
   // [self performSelector:@selector(CheckWorkerCard) withObject:nil afterDelay:0.3f];
   
   // [self performSelectorInBackground:@selector(CheckWorkerCard) withObject:nil];
    

      NSString *rating;
    if (fromSrchArr.count)
    {
        
        rating=[[[[fromSrchArr objectAtIndex:indexConfirmJob]objectForKey:@"job_awarded"]objectAtIndex:0]objectForKey:@"rating"];
    }
    else
    {
        
        
        rating=[[[[arr_onGoingJob objectAtIndex:indexConfirmJob]objectForKey:@"job_awarded"]objectAtIndex:0]objectForKey:@"rating"];
    }
    
    
    if ([rating isEqualToString:@""])
    {
        UIAlertView *alert=[[UIAlertView alloc]initWithTitle:@"" message:@"Please give rating before proceeding" delegate:nil cancelButtonTitle:@"OK" otherButtonTitles:nil];
        [alert show];
    }
    else
    {
        
        UIAlertView *alert=[[UIAlertView alloc]initWithTitle:@"" message:@"Do you want to confirm this job and make payment?" delegate:self cancelButtonTitle:@"OK" otherButtonTitles:@"Cancel", nil];
        alert.tag=1;
        [alert show];
    }
        
 

}
-(void)confirmJobStatus
{
    
    NSMutableArray *arr=[[NSMutableArray alloc]init];
    if (fromSrchArr.count)
    {
        arr=[NSMutableArray arrayWithArray:fromSrchArr];
    }
    else
    {
        arr=[NSMutableArray arrayWithArray:arr_onGoingJob];
    }
    
    transationId=[[[[arr objectAtIndex:indexConfirmJob]objectForKey:@"job_transaction"] objectAtIndex:0]objectForKey:@"transaction_id"];
    
    jobId=[[[[arr objectAtIndex:indexConfirmJob]objectForKey:@"job_transaction"] objectAtIndex:0]objectForKey:@"job_id"];
    
    PaidTo=[[[[arr objectAtIndex:indexConfirmJob]objectForKey:@"job_transaction"] objectAtIndex:0]objectForKey:@"paid_to"];
    
    paidBy=[[[[arr objectAtIndex:indexConfirmJob]objectForKey:@"job_transaction"] objectAtIndex:0]objectForKey:@"paid_by"];
    
    amount=[[[[arr objectAtIndex:indexConfirmJob]objectForKey:@"job_transaction"] objectAtIndex:0]objectForKey:@"amount"];
    
    remark=[[[[arr objectAtIndex:indexConfirmJob]objectForKey:@"job_transaction"] objectAtIndex:0]objectForKey:@"remarks"];
    
   
    

    
    string_acceptJobDate=[[[[arr objectAtIndex:indexConfirmJob]objectForKey:@"job_awarded"]objectAtIndex:0]objectForKey:@"created_at"];
 
    // Convert string to date object
    NSDateFormatter *dateFormat = [[NSDateFormatter alloc] init];
    [dateFormat setDateFormat:@"yyyy-MM-dd HH:mm:ss"];
    NSDate *date = [dateFormat dateFromString:string_acceptJobDate];
    
    
    NSDateFormatter *dateformate=[[NSDateFormatter alloc]init];
    NSTimeInterval secondsInOneHours = 24 * 60 * 60;
    NSDate *dateOneHoursAhead = [date dateByAddingTimeInterval:secondsInOneHours];
    [dateformate setDateFormat:@"yyyy-MM-dd HH:mm:ss"];
    
    // get today date
    NSDate *todayDate = [NSDate date];
    NSDateFormatter *dateFormatter = [[NSDateFormatter alloc] init];
    [dateFormatter setDateFormat:@"yyyy-MM-dd HH:mm:ss"];
    
    
//    if ([status_WorkerCard isEqualToString:@"1"])
//    {
//           if ([dateOneHoursAhead compare:todayDate] == NSOrderedAscending) {
//                NSLog(@"date1 is earlier than date2");
                 [SVProgressHUD show];
               [self performSelector:@selector(commandToReleasePayment) withObject:nil afterDelay:0.3f];
        
//            } else
//            {
//                UIAlertView *alert=[[UIAlertView alloc]initWithTitle:@"Alert!" message:@"Please make payment after 24 hours of Job Accept time." delegate:nil cancelButtonTitle:@"OK" otherButtonTitles:nil];
//                [alert show];
//            }
    
    
    
    
    
    
      //  [self performSelector:@selector(commandToReleasePayment) withObject:nil afterDelay:0.3f];
   // }
//    else
//    {
//        UIAlertView *alert=[[UIAlertView alloc]initWithTitle:@"Alert" message:@"Sorry! We are unable to release this payment. Please inform applicant to add card details." delegate:nil cancelButtonTitle:@"OK" otherButtonTitles:nil];
//        [alert show];
//    }
    
 
    
}




#pragma mark
#pragma mark-command To release payment then mark job as completed   //mark by customer

-(void)commandToReleasePayment

{
    
    NSString *token=[NSString stringWithFormat:@"%@",[defaults objectForKey:@"user_token"]];
    
    NSString *url=[NSString stringWithFormat:@"%@relesePayment?token=%@",serviceLink,token];
    NSMutableDictionary *jsonDict=[[NSMutableDictionary alloc]init];
    
    [jsonDict setValue:transationId forKey:@"transaction_id"] ;
    [jsonDict setValue:jobId forKey:@"job_id"] ;
    [jsonDict setValue:PaidTo forKey:@"paid_to"] ;
    [jsonDict setValue:paidBy forKey:@"paid_by"] ;
    [jsonDict setValue:amount forKey:@"amount"] ;
    [jsonDict setValue:remark forKey:@"remark"] ;

    
    if ([Helper isConnectedToInternet])
    {

        id json=[WebService signup:jsonDict :url];
        
        if ([[json objectForKey:@"message"]isEqualToString:@"Success"])
        {
            
            [self markJObAsConfirm];
        }
        else
        {
            UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"Alert!" message:@"Warning! Some error is encountered to release payment." delegate:nil cancelButtonTitle:@"OK" otherButtonTitles:nil];
            [alert show];
            [SVProgressHUD dismiss];
  
        }
        
    }
    else{
        UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"No Internet" message:@"Please check your internet connection." delegate:nil cancelButtonTitle:@"OK" otherButtonTitles:nil];
        
        [alert show];
        [SVProgressHUD dismiss];

    }
   
}
-(void)markJObAsConfirm
{
    NSMutableArray *arr=[[NSMutableArray alloc]init];
   // NSString *id_job;
    if (fromSrchArr.count)
    {
        arr=[NSMutableArray arrayWithArray:fromSrchArr];
    }
    else
    {
        arr=[NSMutableArray arrayWithArray:arr_onGoingJob];
    }
    
//    for (int j=0;j<arr.count; j++)
//    {
//        NSString *st=[[[[arr objectAtIndex:j]objectForKey:@"job_applications"]objectAtIndex:j] objectForKey:@"applicant_job_status"];
//        if([st  isEqualToString:@"completed"] ) //
//        {
//        id_job=  [[[[arr  objectAtIndex:indexConfirmJob ]objectForKey:@"job_applications"]objectAtIndex:j]objectForKey:@"id"];
//        }
//        
//    }
    NSString *id_job=  [[[[arr  objectAtIndex:indexConfirmJob ]objectForKey:@"job_applications"]objectAtIndex:0]objectForKey:@"id"];
    
    NSString *proposedAmount=[[[[arr objectAtIndex:indexConfirmJob]objectForKey:@"job_awarded"] objectAtIndex:0]objectForKey:@"proposed_amount"];
    
  
    NSString *token=[NSString stringWithFormat:@"%@",[defaults objectForKey:@"user_token"]];
    
    NSString *url=[NSString stringWithFormat:@"%@job-status?token=%@",serviceLink,token];
    NSMutableDictionary *jsonDict=[[NSMutableDictionary alloc]init];
    
    [jsonDict setValue:id_job forKey:@"job_application_id"] ;
    [jsonDict setValue:@"confirmed" forKey:@"applicant_job_status"];
    [jsonDict setValue:proposedAmount forKey:@"proposed_amount"];
    [jsonDict setValue:@""forKey:@"comment"];
    
    //change to Notification
    
    [jsonDict setValue:[[[[arr objectAtIndex:indexConfirmJob] objectForKey:@"job_awarded"] objectAtIndex:0]objectForKey:@"job_id"] forKey:@"job_id"];
    
    [jsonDict setValue:[defaults objectForKey:@"user_id"] forKey:@"job_user_id"];
    
    [jsonDict setValue:[defaults objectForKey:@"user_id"] forKey:@"sender_id"];
    
    [jsonDict setValue:[[[[[arr objectAtIndex:indexConfirmJob] objectForKey:@"job_awarded"] objectAtIndex:0]objectForKey:@"user"] objectForKey:@"id"] forKey:@"receiver_id"];
    
    [jsonDict setValue:[defaults objectForKey:@"p_name"] forKey:@"sender_name"];
    
    
    id json=[WebService signup:jsonDict :url];
    if ([[json objectForKey:@"message"]isEqualToString:@"Success"])
    {
        UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"Success!" message:@"Your job has been  completed." delegate:nil cancelButtonTitle:@"OK" otherButtonTitles:nil];
        [alert show];
       // UIAlertView *alert=[[UIAlertView alloc]initWithTitle:@"" message:@"You have confirmed this job." delegate:nil cancelButtonTitle:@"OK" otherButtonTitles: nil];
        [self onGoningJob];
        [alert show];
    }
    else
    {
        UIAlertView *alert=[[UIAlertView alloc]initWithTitle:@"Alert!" message:@"Payment has been released but Some error is encountered while going for confirm job." delegate:nil cancelButtonTitle:@"OK" otherButtonTitles: nil];
        [alert show];
        [SVProgressHUD dismiss];
    }
    
    [SVProgressHUD dismiss];
    
}
#pragma mark-alert view delegate
- (void)alertView:(UIAlertView *)alertView clickedButtonAtIndex:(NSInteger)buttonIndex
{
//    MakePaymentVC *view=[self.storyboard instantiateViewControllerWithIdentifier:@"makePaymentVC"];
//    view.dict=[arr_onGoingJob objectAtIndex:indexConfirmJob];
    NSInteger click=buttonIndex;
    
    if (alertView.tag==1)
    {
    switch (click)
     {
        case 0:
//             [SVProgressHUD show];
             [self confirmJobStatus];
            // [self performSelector:@selector(confirmJobStatus) withObject:nil afterDelay:0.3f];
           //  [self performSelector:@selector(setAuthentication) withObject:nil afterDelay:0.3f];
             break;
        case 1:
            break;
        default:
            break;
     }
    }
    
    
    if (alertView.tag==2)
    {
        switch (click)
        {
            case 0:
                [SVProgressHUD show];
               // [self confirmJobStatus];
                 [self performSelector:@selector(RefundPaymentFunc) withObject:nil afterDelay:0.3f];
               
                break;
            case 1:
                break;
            default:
                break;
        }
    }

   
}

- (IBAction)appliedjobs:(id)sender
{
    searchBar_obj.text=nil;
    fromSrchArr=nil;

    [self showdata];
     Jobstatus=@"1";
    [btn_appliedObj setBackgroundImage:[UIImage imageNamed:@"mysidejob-active"] forState:UIControlStateNormal];
    [btn_ongoingObj setBackgroundImage:[UIImage imageNamed:@"ongoingcontact"] forState:UIControlStateNormal];
    [btn_jobCompleteObj setBackgroundImage:[UIImage imageNamed:@"jobcomplete@2x-1"] forState:UIControlStateNormal];
    
    

}

- (IBAction)jobcomplete:(id)sender
{
    searchBar_obj.text=nil;
    fromSrchArr=nil;

     Jobstatus=@"3";
    [btn_appliedObj setBackgroundImage:[UIImage imageNamed:@"mysidejob"] forState:UIControlStateNormal];
    [btn_ongoingObj setBackgroundImage:[UIImage imageNamed:@"ongoingcontact"] forState:UIControlStateNormal];
    [btn_jobCompleteObj setBackgroundImage:[UIImage imageNamed:@"jobcomplete-active@2x-1"] forState:UIControlStateNormal];
    
    
    
    view_ongoingContracts.frame=CGRectMake(1000,view_ongoingContracts.frame.origin.y, view_ongoingContracts.frame.size.width, view_ongoingContracts.frame.size.height);
    View_AppliedJobsWorker.frame=CGRectMake(1000,View_AppliedJobsWorker.frame.origin.y, View_AppliedJobsWorker.frame.size.width, View_AppliedJobsWorker.frame.size.height);
    View_JobCompletedOfWorker.frame = CGRectMake(0, View_JobCompletedOfWorker.frame.origin.y, View_JobCompletedOfWorker.frame.size.width, View_JobCompletedOfWorker.frame.size.height);
    
    [SVProgressHUD showWithStatus:@"Loading..."];
    [self performSelector:@selector(CompletedJobList) withObject:nil afterDelay:0.3f];
     }
-(void)CompletedJobList
{
    noRecord_lbl.hidden=YES;
    arr_CompleteJob=[[NSMutableArray alloc]init];
    NSString *token=[NSString stringWithFormat:@"%@",[defaults objectForKey:@"user_token"]];
    NSString *url=[NSString stringWithFormat:@"%@customer-ongoing?token=%@",serviceLink,token];
    NSMutableDictionary *dict=[[NSMutableDictionary alloc]init];
    [dict setValue:[defaults objectForKey:@"user_id"] forKey:@"id"];
    id json = [WebService signup:dict :url];
    NSMutableArray *arr_Response=[[NSMutableArray alloc]init];
    if ([[json objectForKey:@"message"]isEqualToString:@"Success"])
    {
        arr_CompleteJob=[[NSMutableArray alloc]init];
        arr_Response=[json objectForKey:@"response"];
        
        for (int i=0; i<arr_Response.count; i++)
        {
            NSMutableArray *arr=[[arr_Response objectAtIndex:i]objectForKey:@"job_applications"];
            if (arr.count)
            {
                NSLog(@"%@", [[arr_Response objectAtIndex:i]objectForKey:@"job_applications"]);
                
                if ([[[[[arr_Response objectAtIndex:i]objectForKey:@"job_applications"]objectAtIndex:0]objectForKey:@"applicant_job_status"]isEqualToString:@"confirmed"] ||[[[[[arr_Response objectAtIndex:i]objectForKey:@"job_applications"]objectAtIndex:0]objectForKey:@"applicant_job_status"]isEqualToString:@"refunded"] )
             {
                [arr_CompleteJob addObject:[arr_Response objectAtIndex:i]];
             }
            }
        }
        tableView_JobCompletedWorker.dataSource=self;
        tableView_JobCompletedWorker.delegate=self;
        [tableView_JobCompletedWorker reloadData];
    }
    
    else if ([[json objectForKey:@"error"]isEqualToString:@"token_expired"])
    {
        [SVProgressHUD dismiss];
        UIAlertView *alert=[[UIAlertView alloc]initWithTitle:@"" message:@"Session Expired Please login agian" delegate:nil cancelButtonTitle:@"OK" otherButtonTitles: nil];
        [alert show];

        [defaults setBool:NO forKey:@"logined"];
        [defaults synchronize];
        [[NSUserDefaults standardUserDefaults]synchronize];
        [delegate loginStatus];
    }
    
    if (arr_CompleteJob.count)
    {
        [SVProgressHUD dismiss];
        lbl_NoRecordSeg.hidden = YES;
        [tableView_JobCompletedWorker reloadData];
    }else
    {
           lbl_NoRecordSeg.hidden = NO;
    }
    NSLog(@"%@",arr_CompleteJob);
    [SVProgressHUD dismiss];
  
}

- (IBAction)ongoingJob:(id)sender
{
    searchBar_obj.text=nil;
    fromSrchArr=nil;
    
    Jobstatus=@"2";
    noRecord_lbl.hidden = true;
    
    view_ongoingContracts.frame=CGRectMake(0,view_ongoingContracts.frame.origin.y, view_ongoingContracts.frame.size.width, view_ongoingContracts.frame.size.height);
    View_AppliedJobsWorker.frame=CGRectMake(1000,View_AppliedJobsWorker.frame.origin.y, View_AppliedJobsWorker.frame.size.width, View_AppliedJobsWorker.frame.size.height);
    View_JobCompletedOfWorker.frame = CGRectMake(1000, View_JobCompletedOfWorker.frame.origin.y, View_JobCompletedOfWorker.frame.size.width, View_JobCompletedOfWorker.frame.size.height);
    [SVProgressHUD showWithStatus:@"Loading..."];
    [self performSelector:@selector(onGoningJob) withObject:nil afterDelay:0.3f];
    
    [btn_appliedObj setBackgroundImage:[UIImage imageNamed:@"mysidejob"] forState:UIControlStateNormal];
    [btn_ongoingObj setBackgroundImage:[UIImage imageNamed:@"ongoingcontact-active"] forState:UIControlStateNormal];
    [btn_jobCompleteObj setBackgroundImage:[UIImage imageNamed:@"jobcomplete@2x-1"] forState:UIControlStateNormal];
    
}
-(void)showdata
{
     if (jobsArr.count < 1)
    {
        noRecord_lbl.hidden = FALSE;
        lbl_NoRecordSeg.hidden=NO;
    }
    else
    {
        noRecord_lbl.hidden = TRUE;
          lbl_NoRecordSeg.hidden=YES;
    }
    View_AppliedJobsWorker.frame=CGRectMake(0,View_AppliedJobsWorker.frame.origin.y, View_AppliedJobsWorker.frame.size.width, View_AppliedJobsWorker.frame.size.height);
    View_JobCompletedOfWorker.frame = CGRectMake(1000, View_JobCompletedOfWorker.frame.origin.y, View_JobCompletedOfWorker.frame.size.width, View_JobCompletedOfWorker.frame.size.height);
    view_ongoingContracts.frame = CGRectMake(1000, view_ongoingContracts.frame.origin.y, view_ongoingContracts.frame.size.width, view_ongoingContracts.frame.size.height);
   
}


#pragma mark
#pragma mark-Payment
-(void)setAuthentication
{
    
    NSString *token=[NSString stringWithFormat:@"%@",[defaults objectForKey:@"user_token"]];
    
    NSString *url=[NSString stringWithFormat:@"%@createClientToken?token=%@",serviceLink,token];
    NSMutableDictionary *dict=[[NSMutableDictionary alloc]init];
    [dict setObject:[defaults valueForKey:@"customer_id"] forKey:@"customer_id"];
    
    jobsArr=[[NSMutableArray alloc]init];
    if ([Helper isConnectedToInternet])
    {
        id json1=[WebService signup:dict :url];
        if ([[json1 objectForKey:@"message"]isEqualToString:@"Success"])
        {
          paymentToken=[[json1 objectForKey:@"response"] objectForKey:@"token"];
          
            
        }
        else
        {
            
        }
    }
    else
    {
        UIAlertView *alert=[[UIAlertView alloc]initWithTitle:@"No Internet" message:@"Please check intenet connection." delegate:nil cancelButtonTitle:@"OK" otherButtonTitles:nil];
        [alert show];
    }

    
    // Initialize `Braintree` once per checkout session
    
    if (paymentToken==nil||[paymentToken isEqualToString:@""])
    {
        UIAlertView *alert=[[UIAlertView alloc]initWithTitle:@"Alert!" message:@"Server error." delegate:nil cancelButtonTitle:@"OK" otherButtonTitles:nil];
        [alert show];
    }
    else
    {
        braintree = [Braintree braintreeWithClientToken:paymentToken];
        NSLog(@"%@",braintree);
        [self getnonce];
   
    }
    
    // As an example, you may wish to present our Drop-In UI at this point.
    // Continue to the next section to learn more...
//     }] resume];
    
    
    // Do any additional setup after loading the view, typically from a nib.
}

-(void)getnonce
{
   // NSMutableArray *arr_lastcard=[cardDetails lastObject];
    //NSString *cardno=[[[cardDetails objectAtIndex:0]objectForKey:@"_attributes"] objectForKey:@"maskedNumber"];
    if (cardDetails.count)
    {
        BTClientCardRequest *request = [[BTClientCardRequest alloc] init];
        request.number = @"4111111111111111";
        request.expirationMonth = @"10";
        request.expirationYear = @"2022";
        request.cvv=@"123";
        
    
        
        [self->braintree tokenizeCard:request completion:^(NSString *nonce, NSError *error)
         {
             if (error)
             {
                 //self.progressBlock([NSString stringWithFormat:@"Error: %@", error]);
                 [[[UIAlertView alloc] initWithTitle:@"Error"
                                             message:[error localizedDescription]
                                            delegate:nil
                                   cancelButtonTitle:@"OK"
                                   otherButtonTitles:nil] show];
             }
        
             
             if (nonce)
             {
                 NSLog(@"Nonce %@",nonce);
                // [self varifyCard];
                 //            self.progressBlock([NSString stringWithFormat:@"Card tokenized -> Nonce Received: %@", nonce]);
                 //             self.completionBlock(nonce);
             }
         }];

    }
    
    else
    {
        
    }
    
   }

/*-(void)varifyCard
{
    if (self.latestPaymentMethodOrNonce)
    {
        NSString *nonce = [self.latestPaymentMethodOrNonce isKindOfClass:[BTPaymentMethod class]] ? [self.latestPaymentMethodOrNonce nonce] : self.latestPaymentMethodOrNonce;
        //[self updateStatus:@"Creating Transaction…"];
        [[UIApplication sharedApplication] setNetworkActivityIndicatorVisible:YES];
        [[BraintreeDemoMerchantAPI sharedService] makeTransactionWithPaymentMethodNonce:nonce
                                                                             completion:^(NSString *transactionId, NSError *error){
                                                                                 [[UIApplication sharedApplication] setNetworkActivityIndicatorVisible:NO];
                                                                                 self.latestPaymentMethodOrNonce = nil;
                                                                                 if (error) {
                                                                                     NSLog(@"%@",error.localizedDescription);
                                                                                 //    [self updateStatus:error.localizedDescription];
                                                                                 } else {
                                                                                     NSLog(@"transaction id=%@",transactionId);
                                                                          //           [self updateStatus:transactionId];
                                                                                 }
                                                                             }];
    }

}
*/
- (void)dropInViewController:(__unused BTDropInViewController *)viewController didSucceedWithPaymentMethod:(BTPaymentMethod *)paymentMethod
{
    //    [self postNonceToServer:paymentMethod.nonce]; // Send payment method nonce to your server
    //
    //    [self dismissViewControllerAnimated:YES completion:nil];
}

- (void)dropInViewControllerDidCancel:(__unused BTDropInViewController *)viewController
{
    //    [self dismissViewControllerAnimated:YES completion:nil];
}


- (void)postNonceToServer:(NSString *)paymentMethodNonce
{
    //    // Update URL with your server
    //    NSLog(@"Nonce %@", paymentMethodNonce);
    //
    //    NSURL *paymentURL = [NSURL URLWithString:@"https://your-server.example.com/payment-methods"];
    //
    //    NSMutableURLRequest *request = [NSMutableURLRequest requestWithURL:paymentURL];
    //
    //    request.HTTPBody = [[NSString stringWithFormat:@"payment_method_nonce=%@", paymentMethodNonce] dataUsingEncoding:NSUTF8StringEncoding];
    //
    //    request.HTTPMethod = @"POST";
    //
    //    [[[NSURLSession sharedSession] dataTaskWithRequest:request completionHandler:^(NSData * _Nullable data, NSURLResponse * _Nullable response, NSError * _Nullable error)
    //    {
    //        // TODO: Handle success and failure
    //    }] resume];
}

//- (void)userDidCancelPayment
//{
//    [self dismissViewControllerAnimated:YES completion:nil];
//}


#pragma mark
#pragma mark-get card details
-(void)getCarddetails
{
    NSMutableDictionary *jsonDict=[[NSMutableDictionary alloc]init];
    [jsonDict setValue:[defaults objectForKey:@"customer_id"] forKey:@"id"];
    NSString *url=[NSString stringWithFormat:@"%@find-customer?token=%@",serviceLink,[defaults objectForKey:@"user_token"]];
    id  json = [WebService signup:jsonDict:url];
    cardDetails=[NSMutableArray new];
    
    
    if ([[json objectForKey:@"message"]isEqualToString:@"Success"])
    {
     NSMutableDictionary*   dictResponse=[[NSMutableDictionary alloc]init];
     NSMutableArray* arr_token=[[NSMutableArray alloc]init];
     NSMutableArray*  arr_cardCount=[[NSMutableArray alloc]init];

        
        dictResponse=[json objectForKey:@"response"];
        cardDetails=[dictResponse objectForKey:@"creditCards"];
        
        for (NSMutableDictionary *dict in [cardDetails self])
        {
            
            NSString *str_Token=[[dict objectForKey:@"_attributes"]objectForKey:@"token"];
            [arr_cardCount addObject:[dict objectForKey:@"_attributes"]];
            [arr_token addObject:str_Token];
        }
    }
    
    
    else if([[json objectForKey:@"error"]isEqualToString:@"token_expired"])
    {
        
        UIAlertView *alert=[[UIAlertView alloc]initWithTitle:@"" message:@"Session Expired Please login agian." delegate:nil cancelButtonTitle:@"OK" otherButtonTitles: nil];
        [alert show];
        [defaults setBool:NO forKey:@"logined"];
        [defaults synchronize];
        [[NSUserDefaults standardUserDefaults]synchronize];
        [delegate loginStatus];
        
    }
   
}

#pragma mark
#pragma mark - check worker card

/*-(void)CheckWorkerCard
{
    NSMutableArray *array;
    if (fromSrchArr.count)
    {
        array=[NSMutableArray arrayWithArray:fromSrchArr];
    }
    else
    {
        array=[NSMutableArray arrayWithArray:arr_onGoingJob];
    }
    
    PaidTo=[[[[array objectAtIndex:indexConfirmJob]objectForKey:@"job_transaction"] objectAtIndex:0]objectForKey:@"paid_to"];
  
    if (PaidTo != nil)
    {
        NSString *url=[NSString stringWithFormat:@"%@findCustomerCard/%@",serviceLink,PaidTo];
        id  json = [WebService GetwithoutDict:url];
        if ([[json objectForKey:@"message"]isEqualToString:@"Success"])
        {
            [SVProgressHUD dismiss];
           status_WorkerCard= [json objectForKey:@"status"];
            NSLog(@"status_wokerCard  %@",status_WorkerCard);
        }
        else
        {
            [SVProgressHUD dismiss];
            status_WorkerCard=@"0";
            
        }
        
    }
    else
    {
        [SVProgressHUD dismiss];
        UIAlertView *alert=[[UIAlertView alloc]initWithTitle:@"Alert!" message:@"Some error occured to find worker have card or not?" delegate:nil cancelButtonTitle:@"OK" otherButtonTitles: nil];
        [alert show];
    }
  
   [SVProgressHUD dismiss];
}*/



@end
