//
//  OnGoingCustomerTableViewCell.m
//  sidejobs
//
//  Created by DebutMac4 on 16/09/15.
//  Copyright (c) 2015 DebutMac4. All rights reserved.
//

#import "OnGoingCustomerTableViewCell.h"

@implementation OnGoingCustomerTableViewCell

- (void)awakeFromNib {
    // Initialization code
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];
    _btn_amount.layer.masksToBounds=YES;
    _btn_amount.layer.cornerRadius=_btn_amount.bounds.size.width/2;
    _img_worker.layer.masksToBounds=YES;
    _img_worker.layer.cornerRadius=_img_worker.bounds.size.width/2;
    
    _viewBackground.layer.borderColor=[[UIColor grayColor]CGColor];
    _viewBackground.layer.borderWidth=0.3f;

    // Configure the view for the selected state
}

@end
