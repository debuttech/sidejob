//
//  TableViewCellMyInVitation.h
//  sidejobs
//
//  Created by DebutMac4 on 14/10/15.
//  Copyright © 2015 DebutMac4. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "ASStarRatingView.h"

@interface TableViewCellMyInVitation : UITableViewCell
@property (strong, nonatomic) IBOutlet ASStarRatingView *staticStarRatingView;
@property (strong, nonatomic) IBOutlet UILabel *lbl_UserName;
@property (strong, nonatomic) IBOutlet UIImageView *imgV_User;
@property (strong, nonatomic) IBOutlet UILabel *lbl_date;
@property (strong, nonatomic) IBOutlet UIImageView *img1;
@property (strong, nonatomic) IBOutlet UIImageView *img2;
@property (strong, nonatomic) IBOutlet UIImageView *img3;
@property (strong, nonatomic) IBOutlet UIView *View_background;
@property (strong, nonatomic) IBOutlet UILabel *lbl_Status;
@property (strong, nonatomic) IBOutlet UIButton *btn_Accept;

@end
