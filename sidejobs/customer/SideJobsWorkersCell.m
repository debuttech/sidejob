//
//  SideJobsWorkersCell.m
//  WorkerEarning
//
//  Created by Debut Mac 4 on 19/08/15.
//  Copyright (c) 2015 debut.infotech. All rights reserved.
//

#import "SideJobsWorkersCell.h"

@implementation SideJobsWorkersCell

- (void)awakeFromNib
{
    // Initialization code
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated
{
    [super setSelected:selected animated:animated];
    CGFloat borderWidth = 0.3f;
    
    _view_AppliedJob.layer.borderColor = [UIColor lightGrayColor].CGColor;
    _view_AppliedJob.layer.borderWidth = borderWidth;
    _img_1.layer.masksToBounds=YES;
    _img_1.layer.cornerRadius=_img_1.layer.bounds.size.width/2;
    _img_2.layer.masksToBounds=YES;
    _img_2.layer.cornerRadius=_img_1.layer.bounds.size.width/2;
    _img_3.layer.masksToBounds=YES;
    _img_3.layer.cornerRadius=_img_1.layer.bounds.size.width/2;
    _img_4.layer.masksToBounds=YES;
    _img_4.layer.cornerRadius=_img_1.layer.bounds.size.width/2;
    _view_Status.layer.masksToBounds=YES;
    _view_Status.layer.cornerRadius=_view_Status.layer.bounds.size.width/2;
}

@end
