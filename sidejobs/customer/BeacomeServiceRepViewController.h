//
//  BeacomeServiceRepViewController.h
//  sidejobs
//
//  Created by DebutMac4 on 16/03/16.
//  Copyright © 2016 DebutMac4. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface BeacomeServiceRepViewController : UIViewController
{
    NSUserDefaults *defaults;
    UIActivityIndicatorView *indicator;
}
@property (strong, nonatomic) IBOutlet UITextView *textView_ServiceRep;
- (IBAction)button_send:(id)sender;

@end
