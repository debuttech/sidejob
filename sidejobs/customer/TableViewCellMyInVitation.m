//
//  TableViewCellMyInVitation.m
//  sidejobs
//
//  Created by DebutMac4 on 14/10/15.
//  Copyright © 2015 DebutMac4. All rights reserved.
//

#import "TableViewCellMyInVitation.h"

@implementation TableViewCellMyInVitation

- (void)awakeFromNib {
    // Initialization code
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];
    _View_background.layer.masksToBounds=YES;
    _View_background.layer.borderColor=[[UIColor grayColor]CGColor];
    _View_background.layer.borderWidth=0.3f;
    _imgV_User.layer.masksToBounds=YES;
     _imgV_User.layer.cornerRadius=_imgV_User.bounds.size.width/2 ;

    // Configure the view for the selected state
}

@end
