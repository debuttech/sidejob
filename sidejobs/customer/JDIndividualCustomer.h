//
//  JDIndividualCustomer.h
//  sidejobs
//
//  Created by DebutMac4 on 29/10/15.
//  Copyright © 2015 DebutMac4. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "ASStarRatingView.h"

@interface JDIndividualCustomer : UIViewController
{
    
    IBOutlet UILabel *lbl_Title;
    
    IBOutlet UILabel *lbl_user;
    
    IBOutlet UIImageView *img_user;
    
    IBOutlet UILabel *lbl_Completed;
    IBOutlet UILabel *lbl_startTime;
    IBOutlet UILabel *lbl_memberSince;
    IBOutlet UIView *view_Background;
    IBOutlet UILabel *lbl_amount;
    IBOutlet UILabel *lbl_Category;
    IBOutlet UIImageView *img_category;
    IBOutlet ASStarRatingView *staticStarRatingView;
}

@property(strong,nonatomic)NSMutableDictionary *dict;

//@property(strong,nonatomic)NSString *strStatus;

//-(void)getvalue:(NSString*)status;
@end
