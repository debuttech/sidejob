//
//  ReportAnIssueViewController.h
//  sidejobs
//
//  Created by DebutMac4 on 16/03/16.
//  Copyright © 2016 DebutMac4. All rights reserved.
//

#import <UIKit/UIKit.h>



@interface ReportAnIssueViewController : UIViewController
@property (strong, nonatomic) IBOutlet UIImageView *imageView;
@property (strong, nonatomic) IBOutlet UITextView *textview_sendReport;
- (IBAction)button_Submit:(id)sender;

@end
