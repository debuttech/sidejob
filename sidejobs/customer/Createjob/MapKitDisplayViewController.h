//
//  MapKitDisplayViewController.h
//  SideJobsMileston2
//
//  Created by Debut Mac 4 on 03/09/15.
//  Copyright (c) 2015 debut.infotech. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <MapKit/MapKit.h>
#import <CoreLocation/CoreLocation.h>

@interface MapKitDisplayViewController : UIViewController <MKMapViewDelegate, CLLocationManagerDelegate>
{
    CLLocation *currentLocation;
    NSString *country,*pincode,*address,*lat,*lon,*city;
    IBOutlet UISearchBar *searchBar_Obj;
    NSString *strLocaton;
    CLPlacemark *placemarkLocation;
    IBOutlet UITableView *table_LocationList;
    NSTimer *timer;
    NSString *str_Location;
    NSString *str_TypeGetLocation;
    
    IBOutlet UIView *view_TableView;
}

@property (strong, nonatomic) IBOutlet UILabel *myAddress;
@property (nonatomic, strong) CLLocationManager* locationManager;
@property (strong, nonatomic) IBOutlet MKMapView *mapView;
- (IBAction)btn_done:(id)sender;
- (IBAction)btn_cancel:(id)sender;

@end
