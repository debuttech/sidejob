//
//  MapKitDisplayViewController.m
//  SideJobsMileston2
//
//  Created by Debut Mac 4 on 03/09/15.
//  Copyright (c) 2015 debut.infotech. All rights reserved.
//

#import "MapKitDisplayViewController.h"
#import "CreateJobController.h"
#import "Helper.h"
#import "Reachability.h"
#import "MapViewAnnotation.h"

@interface MapKitDisplayViewController (){
    
    CLLocation *pinLocation;
    CreateJobController *create_View;
    NSUserDefaults *defaults;
    NSArray *Arr_loc;
    NSString *location;
    NSMutableArray *arr_ResultLocation;
    NSString *str_search;
    NSString *strSearchLocation;
}
@end

@implementation MapKitDisplayViewController

@synthesize mapView;
- (void)viewDidLoad
{
    [super viewDidLoad];
    
    view_TableView.layer.masksToBounds=NO;
    view_TableView.layer.borderWidth=0.4f;
    view_TableView.layer.shadowOffset = CGSizeMake(-1, 20);
    view_TableView.layer.shadowRadius = 5;
    view_TableView.layer.shadowOpacity = 0.5;
    
    defaults=[NSUserDefaults standardUserDefaults];
    
    //Zoom on current Location
    self.mapView.showsUserLocation=NO;
    [self.mapView setUserTrackingMode:MKUserTrackingModeFollow animated:YES];
    
    UITapGestureRecognizer *tapRecognizer = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector (foundTap:)];
    tapRecognizer.numberOfTapsRequired = 1;
    tapRecognizer.numberOfTouchesRequired = 1;
    [self.mapView addGestureRecognizer:tapRecognizer];
    
    self.mapView.delegate = self;
    self.locationManager = [[CLLocationManager alloc] init];
    [self.locationManager requestAlwaysAuthorization];
    [self.locationManager setDelegate:self];
    [self.locationManager setDesiredAccuracy:kCLLocationAccuracyNearestTenMeters];
    [self.locationManager startUpdatingLocation];
}

- (BOOL)searchBarShouldBeginEditing:(UISearchBar *)searchBar
{
    UITextField *searchBarTextField = nil;
    for (UIView *subView in searchBar_Obj.subviews)
    {
        for (UIView *sndSubView in subView.subviews)
        {
            if ([sndSubView isKindOfClass:[UITextField class]])
            {
                searchBarTextField = (UITextField *)sndSubView;
                break;
            }
        }
    }
    searchBarTextField.enablesReturnKeyAutomatically = NO;
    return YES;
}
-(void)searchBarSearchButtonClicked:(UISearchBar *)searchBar
{
    [self.view endEditing:YES];
}

#pragma mark
#pragma mark-search location by enter address

#pragma mark - searchBar Delegate methods
- (void)searchBar:(UISearchBar *)searchBar textDidChange:(NSString *)searchText
{
    str_search = searchText;
    
    if (!timer)
    {
        timer = [NSTimer scheduledTimerWithTimeInterval:1.0f
                                                 target:self
                                               selector:@selector(_timerFired:)
                                               userInfo:nil
                                                repeats:NO];
    }
}

- (void)stopTimer
{
    if ([timer isValid])
    {
        [timer invalidate];
    }
    timer = nil;
}

- (void)_timerFired:(NSTimer *)timer
{
    [self stopTimer];
    
    if (str_search.length != 0)
    {
        NSString *str = [str_search stringByTrimmingCharactersInSet: [NSCharacterSet whitespaceCharacterSet]];
        NSString *addressStr = [str stringByReplacingOccurrencesOfString:@" " withString:@"+"];
        
        dispatch_async(dispatch_get_main_queue(), ^{
            // code here
            //[self searchPlace:addressStr];
            [self performSelectorInBackground:@selector(searchPlace:) withObject:addressStr];
            
        });
        
        
    }
    else
    {
        
        [view_TableView setFrame:CGRectMake(1000, view_TableView.frame.origin.y, view_TableView.frame.size.width, view_TableView.frame.size.height)];
        // [table_LocationList reloadData];
    }
}
- (void) searchPlace : (NSString *) addressStr
{
    arr_ResultLocation=nil;
    arr_ResultLocation = [[NSMutableArray alloc] init];
    @synchronized(self)
    {
        //sleep(1);
        
        //New: AIzaSyAvP9a9LnuB4HbIo9Nwir8iYV-FDETLuXk
        //Old:
        // running:
        // key AIzaSyCwj5gNaxmsgmMknlcLn7_CBberxoN7rzQ
        // key=key=AIzaSyBgTbarfxhrw2rLs0fMX8ZAjl4a5xsw_Wg
        // AIzaSyBi0WRygdQ6cOD_MHBrsZ9yv-7bKFJayo8
        // paid: AIzaSyCd2-sNHiqZCHVItvTr4sVxFpTcLg6vj9c
        if ([Helper isConnectedToInternet])
        {
            NSString *address1 = [NSString stringWithFormat:@"https://maps.googleapis.com/maps/api/place/textsearch/json?query=%@&key=AIzaSyCd2-sNHiqZCHVItvTr4sVxFpTcLg6vj9c", addressStr];
            //Create URL and Request
            NSURL * url = [NSURL URLWithString:address1];
            NSURLRequest * request = [NSURLRequest requestWithURL:url];
            NSURLResponse * response;
            NSError * error = nil;
            //Send Request
            NSData * data = [NSURLConnection sendSynchronousRequest:request returningResponse:&response error:&error];
            
            if (error == nil)
            {
                NSDictionary * json = [NSJSONSerialization JSONObjectWithData:data options:kNilOptions error:&error];
                arr_ResultLocation = [[NSMutableArray alloc] init];
                arr_ResultLocation= [json valueForKey:@"results"];
                NSLog(@"result:..%@",arr_ResultLocation);
                
                
                if (arr_ResultLocation.count > 0)
                {
                    
                    if ([arr_ResultLocation valueForKey:@"name"] !=nil)
                    {
                        [view_TableView setFrame:CGRectMake(0, view_TableView.frame.origin.y, view_TableView.frame.size.width, view_TableView.frame.size.height)];
                        
                        dispatch_async(dispatch_get_main_queue(), ^{
                            [table_LocationList reloadData];
                        });
                        
                        
                    }
                    
                }
            }
            else
            {
                UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"Alert!!" message:@"Please check search adress." delegate:nil cancelButtonTitle:@"OK" otherButtonTitles:nil];
                
                [alert show];
            }
        }
        else
        {
            UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"No Internet" message:@"Please check your internet connection." delegate:nil cancelButtonTitle:@"OK" otherButtonTitles:nil];
            
            [alert show];
        }
    }
    
}
#pragma mark - Table view data source
- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    return arr_ResultLocation.count;
}

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    if (arr_ResultLocation.count)
    {
        return 1;
    }
    return 0;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    static NSString *customCellIdentifier = @"showAddresOfMapCell";
    
    UITableViewCell *cell = (UITableViewCell *)[tableView dequeueReusableCellWithIdentifier:customCellIdentifier];
    
    if(cell == nil)
    {
        cell=[[UITableViewCell alloc]initWithStyle:UITableViewCellStyleSubtitle reuseIdentifier:customCellIdentifier];
        
    }
    
    NSString *str_name= [[arr_ResultLocation objectAtIndex:indexPath.row] valueForKey:@"name"];
    if (str_name.length>0 || str_name!=nil)
    {
        
         [view_TableView setFrame:CGRectMake(0, view_TableView.frame.origin.y, view_TableView.frame.size.width, view_TableView.frame.size.height)];
        
        cell.textLabel.text = [[arr_ResultLocation objectAtIndex:indexPath.row] valueForKey:@"name"];
        cell.detailTextLabel.text = [[arr_ResultLocation objectAtIndex:indexPath.row] valueForKey:@"formatted_address"];
    }
    else
    {
        [view_TableView setFrame:CGRectMake(1000, view_TableView.frame.origin.y, view_TableView.frame.size.width, view_TableView.frame.size.height)];
    }
    
    
    
    
    return cell;
}
- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    [searchBar_Obj resignFirstResponder];
    
    if (arr_ResultLocation.count)
    {
        str_Location=nil;
        strSearchLocation=[[NSString alloc]init];
        
        // strSearchLocation = [[arr_ResultLocation objectAtIndex:indexPath.row] valueForKey:@"name"];
        strSearchLocation= [[arr_ResultLocation objectAtIndex:indexPath.row] valueForKey:@"formatted_address"];
        lat = [[[[arr_ResultLocation objectAtIndex:indexPath.row] valueForKey:@"geometry"] valueForKey:@"location"] valueForKey:@"lat"];
        lon = [[[[arr_ResultLocation objectAtIndex:indexPath.row] valueForKey:@"geometry"] valueForKey:@"location"] valueForKey:@"lng"];
        
        
        
        NSArray *existingpoints = mapView.annotations;
        if ([existingpoints count])
            [mapView removeAnnotations:existingpoints];
        
        NSMutableArray * arr=[NSMutableArray new];
        arr=[self createAnnotations];
        [mapView addAnnotations:arr];
        
        
        
        NSLog(@"%@",[NSString stringWithFormat:@"lat %@ long %@",lat,lon]);
        NSLog(@"%@",strSearchLocation);
        
        
        [view_TableView setFrame:CGRectMake(1000, view_TableView.frame.origin.y, view_TableView.frame.size.width, view_TableView.frame.size.height)];
        
    }else
    {
        
    }
    
}

- (NSMutableArray *)createAnnotations
{
    NSMutableArray *annotations = [[NSMutableArray alloc] init];
    
    //Create coordinates from the latitude and longitude values
    CLLocationCoordinate2D coord;
    coord.latitude = lat.doubleValue;
    coord.longitude = lon.doubleValue;
    
    MKCoordinateRegion region;
    region.center.latitude = [lat integerValue];
    region.center.longitude =[lon integerValue];
    
    MKCoordinateSpan span;
    span.latitudeDelta =  .08;
    span.longitudeDelta = .08;
    region.center = coord;
    region.span = span;
    
    
    
    [self.mapView setRegion:region animated:YES];
    
    MapViewAnnotation *annotation1 = [[MapViewAnnotation alloc] initWithCoordinates:coord withtitle:nil withTagValue:nil image:nil jobId:nil];
    [annotations addObject:annotation1];
    
    
    [defaults setObject:[NSString stringWithFormat:@"%@",strSearchLocation] forKey:@"location"];
    [defaults setObject:[NSString stringWithFormat:@"%@",lat] forKey:@"latitude"];
    [defaults setObject:[NSString stringWithFormat:@"%@",lon] forKey:@"longitude"];
    [defaults synchronize];
    
    
    return annotations;
}

#pragma Pin to new Location on Map

-(IBAction)foundTap:(UITapGestureRecognizer *)recognizer
{
    CGPoint point = [recognizer locationInView:self.mapView];
    CLLocationCoordinate2D tapPoint = [self.mapView convertPoint:point toCoordinateFromView:self.view];
    pinLocation = [[CLLocation alloc] initWithLatitude:tapPoint.latitude longitude:tapPoint.longitude];
    [self locateAddress:pinLocation point:tapPoint];
}

#pragma CLGeocoder get Address Click on map
-(void)locateAddress:(CLLocation *) addressLocation point:(CLLocationCoordinate2D )cPoint
{
    CLGeocoder *geocoder = [[CLGeocoder alloc] init];
    [geocoder reverseGeocodeLocation:addressLocation completionHandler:^(NSArray *placemarks, NSError *error)
     {
         if (error == nil && [placemarks count] > 0)
         {
             NSString *strAdd;
             CLPlacemark *placemark  = [placemarks lastObject];
             // strAdd -> take bydefault value nil
             // create_View.arr_loc=placemarks;
             // Arr_loc=[NSArray arrayWithObject:placemark];
             // [self passValue:Arr_loc];
             
             if ([placemark.subThoroughfare length] != 0)
             {
                 strAdd = placemark.subThoroughfare;
             }
             if ([placemark.thoroughfare length] != 0)
             {
                 if ([strAdd length] != 0)    // strAdd -> store value of current location
                 {
                     strAdd = [NSString stringWithFormat:@"%@, %@",strAdd,[placemark thoroughfare]];
                 }
                 else
                 {
                     strAdd = placemark.thoroughfare;  // strAdd -> store only this value,which is not null
                 }
             }
             if ([placemark.postalCode length] != 0)
             {
                 if ([strAdd length] != 0)
                 {
                     strAdd = [NSString stringWithFormat:@"%@, %@",strAdd,[placemark postalCode]];
                 }
                 else
                 {
                     strAdd = placemark.postalCode;
                     pincode=placemark.postalCode;
                 }
             }
             if ([placemark.locality length] != 0)
             {
                 if ([strAdd length] != 0)
                 {
                     strAdd = [NSString stringWithFormat:@"%@, %@",strAdd,[placemark locality]];
                     city=[placemark locality];
                 }
                 else
                 {
                     strAdd = placemark.locality;
                 }
             }
             if ([placemark.administrativeArea length] != 0)
             {
                 if ([strAdd length] != 0)
                 {
                     strAdd = [NSString stringWithFormat:@"%@, %@",strAdd,[placemark administrativeArea]];
                     address=[placemark administrativeArea];
                 }
                 else
                 {
                     strAdd = placemark.administrativeArea;
                 }
             }
             
             if ([placemark.country length] != 0)
             {
                 if ([strAdd length] != 0)
                 {
                     strAdd = [NSString stringWithFormat:@"%@, %@",strAdd,[placemark country]];
                     _myAddress.text = strAdd;
                     country=[placemark country];
                 }
                 else
                 {
                     strAdd = placemark.country;
                     _myAddress.text = strAdd;
                 }
             }
             MKPointAnnotation *point1 = [[MKPointAnnotation alloc] init];
             point1.coordinate = cPoint;
             point1.title =strAdd;
             // point1.subtitle = [NSString stringWithFormat:@"Lat: %f, Long: %f ", cPoint.latitude, cPoint.longitude];
             //    point1.subtitle = [NSString stringWithFormat:@"Lat: %f, Long: %f ", cPoint.latitude, cPoint.longitude];
             //.......pas latitude and laongitude to show pin on map.................
             lat=[NSString stringWithFormat:@"%f",cPoint.latitude];
             lon=[NSString stringWithFormat:@"%f",cPoint.longitude];
             location=[NSString stringWithFormat:@"%@",strAdd];
             [self passValue:location and:lat and:lon];
             for (id annotation in self.mapView.annotations)
             {
                 [self.mapView removeAnnotation:annotation];
             }
             [self.mapView addAnnotation:point1];
         }
         NSLog(@"%@",placemarks);
     }];
}
- (IBAction)btn_done:(id)sender
{
    
    NSLog(@"%@",[defaults objectForKey:@"location"]);
    [self dismissViewControllerAnimated:YES completion:nil];
}

- (IBAction)btn_cancel:(id)sender {
    [defaults setObject:nil forKey:@"location"];
    [defaults setObject:nil forKey:@"latitude"];
    [defaults setObject:nil forKey:@"longitude"];
    [defaults synchronize];
    
    [self dismissViewControllerAnimated:YES completion:nil];
}
-(void)passValue:(NSString *)str and:(NSString*)latitude and:(NSString*)longitude
{
    [defaults setObject:str forKey:@"location"];
    [defaults setObject:lat forKey:@"latitude"];
    [defaults setObject:lon forKey:@"longitude"];
    [defaults synchronize];
    NSLog(@"%@",[defaults objectForKey:@"location"]);
}

#pragma mark - CLLocationManagerDelegate

- (void)locationManager:(CLLocationManager*)manager didFailWithError:(NSError *)error
{
    NSLog(@"didFailWithError: %@", error);
    UIAlertView *errorAlert = [[UIAlertView alloc]
                               initWithTitle:@"Error" message:@"Failed to Get Your Location" delegate:nil cancelButtonTitle:@"OK" otherButtonTitles:nil];
    [errorAlert show];
}


- (void)locationManager:(CLLocationManager *)manager didUpdateToLocation:(CLLocation *)newLocation fromLocation:(CLLocation *)oldLocation
{
    currentLocation = newLocation;
    if (currentLocation != nil)
    {
        
        [_locationManager stopUpdatingLocation];
        
        CLGeocoder *geocoder = [[CLGeocoder alloc] init];
        geocoder = [[CLGeocoder alloc] init];
        
        
        [geocoder reverseGeocodeLocation:currentLocation completionHandler:^(NSArray *placemarks, NSError *error)
         {
             [_locationManager stopUpdatingLocation];
             if (error == nil && [placemarks count] > 0)
             {                 placemarkLocation = [placemarks lastObject];
                 strLocaton = [NSString stringWithFormat:@"%@ %@\n%@\n%@",
                               placemarkLocation.postalCode, placemarkLocation.locality,
                               placemarkLocation.administrativeArea,
                               placemarkLocation.country];
                 
             }
             else
             {
                 placemarkLocation = [placemarks lastObject];
                 strLocaton = [NSString stringWithFormat:@"%@ %@\n%@ %@\n%@\n%@",
                               placemarkLocation.subThoroughfare, placemarkLocation.thoroughfare,
                               placemarkLocation.postalCode, placemarkLocation.locality,
                               placemarkLocation.administrativeArea,
                               placemarkLocation.country];
                 
             }
             
             [defaults setObject:[NSString stringWithFormat:@"%@",strLocaton] forKey:@"location"];
             [defaults setObject:[NSString stringWithFormat:@"%f", currentLocation.coordinate.latitude] forKey:@"latitude"];
             [defaults setObject:[NSString stringWithFormat:@"%f",currentLocation.coordinate.longitude] forKey:@"longitude"];
             [defaults synchronize];
             NSLog(@"..%@",[defaults objectForKey:@"latitude"]);
             NSLog(@"..%@",[defaults objectForKey:@"longitude"]);
             
             
             NSLog(@"location..%@",str_Location);
             //   NSLog(@"%@",[defaults objectForKey:@"location"]);
         } ];
        
    }
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
}

/*
 #pragma mark - Navigation
 
 // In a storyboard-based application, you will often want to do a little preparation before navigation
 - (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
 // Get the new view controller using [segue destinationViewController].
 // Pass the selected object to the new view controller.
 }
 */


@end
