//
//  ImageUtilitiesClass.h
//  SideJobs
//
//  Created by Debut05 on 8/21/15.
//  Copyright (c) 2015 Narinder. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface ImageUtilitiesClass : NSObject


+ (UIImage *)scaleAndRotateImage:(UIImage *) image ;

@end
