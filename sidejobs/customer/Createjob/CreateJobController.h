//
//  CreateJobController.h
//  SideJobs
//
//  Created by Debut05 on 8/17/15.
//  Copyright (c) 2015 Narinder. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <CoreLocation/CoreLocation.h>

@interface CreateJobController : UIViewController<CLLocationManagerDelegate,UITableViewDataSource,UITableViewDelegate,UITextViewDelegate,UIImagePickerControllerDelegate,UINavigationControllerDelegate>{
    id json;
    IBOutlet UITextField *titleTF;
    IBOutlet UITextView *descriptionTF;
    
    IBOutlet UITextField *LocationTF;
    IBOutlet UITextField *priceTF;
    IBOutlet UIScrollView *mainSV;
    
    IBOutlet  UIButton *selectTimeSlotBtn;
    
    IBOutlet UIButton *btn_publishjob;
    IBOutlet UIButton *createJobBtn;
    IBOutlet UIButton *dateOfCompletionBtn;
    IBOutlet UIButton *addImg1Btn,*delImg1Btn;
    IBOutlet UIButton *addImg2Btn,*delImg2Btn;
    IBOutlet UIButton *addImg3btn,*delImg3Btn;
    
    IBOutlet UIView *bottomView;
    
    IBOutlet UIView *selectTimeView;
    IBOutlet UIDatePicker *dp1;
    IBOutlet UIDatePicker *dp2;
    
    IBOutlet UIButton *btn_TimeSlot3;
    IBOutlet UIButton *btn_TimeSlot2;
    IBOutlet UIButton *btn_TimeSlot1;
    
    IBOutlet UIView *view_forShowtimeSlot;
    NSString *startTime1,*startTime2,*startTime3,*endTime1,*endTime2,*endTime3;
    NSString *str_title,*str_description,*str_latitude,*str_longitude,*str_amount,*str_categoryId,*str_subCategoryId;
    NSString *jobId;
    NSInteger count;
    NSUserDefaults *defaults;
    
    UIImage *img1,*img2,*img3;
    NSMutableArray *arr_uploadeImg;
    IBOutlet UIButton *btn_SelectTimeSlot_Object;
    NSInteger count_uploadimge;
    
    IBOutlet UIView *view_complitionDate;
    IBOutlet UIDatePicker *datePicker_compilationDtae;
    IBOutlet UITextField *txtField_compilationdtae;
    
    NSInteger count_1,count_2,count_3;
    IBOutlet UITableView *table_selectTimeSlot;
     NSMutableArray *arr_Time;
}
- (IBAction)btn_Cancel:(id)sender;

@property (strong, nonatomic)  NSString *modifyTypeStr;
@property (strong, nonatomic) NSMutableDictionary *jobDetailDict;

-(IBAction)btn_dropDownTimeslot:(id)sender;
-(IBAction)btnDone_fromTimeSlot:(id)sender;

-(IBAction)selectTimeSlotBtnClicked:(id)sender;
-(IBAction)dateOfCompletionBtnClicked:(id)sender;

-(IBAction)addImg1BtnClicked:(id)sender;
-(IBAction)addImg2BtnClicked:(id)sender;
-(IBAction)addImg3btnClicked:(id)sender;

-(IBAction)delImg1BtnClicked:(id)sender;
-(IBAction)delImg2BtnClicked:(id)sender;
-(IBAction)delImg3btnClicked:(id)sender;


-(IBAction)doneDpBtnClicked:(id)sender;

- (IBAction)btn_createAction:(id)sender;
- (IBAction)btn_publishAction:(id)sender;
- (IBAction)btnGetlocation:(id)sender;


-(void)categoryId:(NSString*)str;
-(void)uploadImagesToServer:(UIImage*)img;

@end
