//
//  CreateJobController.m
//  SideJobs
//
//  Created by Debut05 on 8/17/15.
//  Copyright (c) 2015 Narinder. All rights reserved.
//

#import "CreateJobController.h"
#import "MySideJobsFromWorkers.h"
#import "WebService.h"
#import "AsyncURLConnection.h"
#import "JSON.h"
#import <AVFoundation/AVFoundation.h>
#import "Reachability.h"
#import <UIImageView+WebCache.h>

@class MapKitDisplayViewController;

@interface CreateJobController ()
{
    UIVisualEffect *blurEffect;
    UIVisualEffectView *visualEffectView;
    NSString *imageIndex;
    CLLocationManager *locationManager;
    int tag_btn;
    NSInteger removeIndex;
    UIImagePickerController *imagePicker;
    NSString *curentDate;
    UIImage *i1,*i2,*i3;
    NSString *Itag;
    UIImage *ig1,*ig2,*ig3;
    NSMutableArray *arr_ig,*arr_imgId;
    NSString *url;
    NSString *str_jobstatus;
    NSString *slotId;
    NSMutableArray *arr_SlotId;
    NSMutableArray *arraylocaltime;
    AppDelegate *delegate;
    NSString *str_selectTime;
    NSDate *date_Current;
    UIDatePicker *datePicker ;
     float h;
}

@property (nonatomic, strong) QBPopupMenu *popupMenu;

@end

@implementation CreateJobController

-(void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:YES];
    LocationTF.text=[defaults objectForKey:@"location"];
    
    delegate=(AppDelegate*)[[UIApplication sharedApplication]delegate];
     if ([self.modifyTypeStr isEqualToString:@"update"])
     {
         NSLog(@"%@",delegate.applicantCount);
         if ([delegate.applicantCount intValue]>=1)
         {
             selectTimeSlotBtn.userInteractionEnabled=YES;
             titleTF.userInteractionEnabled=NO;
             txtField_compilationdtae.userInteractionEnabled=NO;
         }
     }
    else
    {
        selectTimeSlotBtn.userInteractionEnabled=YES;
        titleTF.userInteractionEnabled=YES;
        txtField_compilationdtae.userInteractionEnabled=YES;
    }
    
    NSDateFormatter *dateFormat = [[NSDateFormatter alloc] init];
    [dateFormat setDateFormat:@"hh"];
    NSString  *str=[NSString stringWithFormat:@"%@",[dateFormat stringFromDate:dp2.date]];
    NSInteger H=[str integerValue];

    NSDate * now = [[NSDate alloc] init];
    NSCalendar *cal = [NSCalendar currentCalendar];
//    NSDateComponents * comps = [cal components:NSYearCalendarUnit|NSMonthCalendarUnit|NSDayCalendarUnit fromDate:now];
    NSDateComponents * comps = [cal components:NSCalendarUnitYear|NSCalendarUnitMonth|NSCalendarUnitDay|NSCalendarUnitHour|NSCalendarUnitMinute  fromDate:now];
    
    [comps setHour:H];
    [comps setMinute:0];
    [comps setSecond:0];
    NSDate * date = [cal dateFromComponents:comps];
    [dp2 setDate:date animated:TRUE];
    [self setDateMethod];
}

-(void)setDateMethod
{
    [dp1 addTarget:self action:@selector(dp1Method:) forControlEvents:UIControlEventValueChanged];
}

-(void)dp1Method:(NSDate*)d1
{
    [self setDate:d1 animated:NO];
}
- (void)setDate:(NSDate *)date animated:(BOOL)animated
{
   
    NSDate *datefromDp1 = [dp1 date];
    NSTimeInterval secondsInEightHours = 0.5 * 60 * 60;
    NSDate *dateEightHoursAhead = [datefromDp1 dateByAddingTimeInterval:secondsInEightHours];

    [dp2 setDate:dateEightHoursAhead animated:NO];
}
#pragma mark
#pragma mark:set date 1 after Add
-(void)setdateFordp1:(NSDate*)d1
{
    NSDate *datefromDp1 = [dp1 date];
    NSTimeInterval secondsInEightHours = 0.5 * 60 * 60;
    NSDate *dateEightHoursAhead = [datefromDp1 dateByAddingTimeInterval:secondsInEightHours];
    
    [dp1 setDate:dateEightHoursAhead animated:NO];
}

-(void)viewWillDisappear:(BOOL)animated
{
    [super viewWillDisappear:YES];
    [view_forShowtimeSlot removeFromSuperview];
   // [defaults setObject:@""forKey:@"location"];

   //LocationTF.text=@"";
   // [defaults objectForKey:@"location"]=nil;
}


- (void)viewDidLoad {
    [super viewDidLoad];
    [self createUserExperience];
   

}
-(void)createUserExperience
{
    NSLog(@"%@",self.jobDetailDict);
    addImg1Btn.layer.masksToBounds=YES;
    addImg1Btn.layer.cornerRadius=5.0f;
    addImg1Btn.layer.borderColor=[[UIColor grayColor]CGColor];
    addImg1Btn.layer.borderWidth=0.2f;
    
    addImg2Btn.layer.masksToBounds=YES;
    addImg2Btn.layer.cornerRadius=5.0f;
    addImg2Btn.layer.borderColor=[[UIColor grayColor]CGColor];
    addImg2Btn.layer.borderWidth=0.2f;
    
    addImg3btn.layer.masksToBounds=YES;
    addImg3btn.layer.cornerRadius=5.0f;
    addImg3btn.layer.borderColor=[[UIColor grayColor]CGColor];
    addImg3btn.layer.borderWidth=0.2f;
    
    
    
    
    
    str_jobstatus=[[NSString alloc]init];
    date_Current=[NSDate date];
    
    if ([self.modifyTypeStr isEqualToString:@"update"]) {
        //[SVProgressHUD showWithStatus:@"Fetching Data"];
    }else{
        
    }
    arraylocaltime=[[NSMutableArray alloc]init];
    arr_SlotId=[[NSMutableArray alloc]init];
    
    defaults=[NSUserDefaults standardUserDefaults];
    
    arr_uploadeImg=[[NSMutableArray alloc]init];
    arr_ig=[NSMutableArray new];
    arr_Time=[[NSMutableArray alloc]init];
    arr_imgId=[[NSMutableArray alloc]init];
    descriptionTF.textColor=[UIColor lightGrayColor];
    
    count=0;
    self.title=@"";
    imageIndex=@"";
    
    table_selectTimeSlot.dataSource=self;
    table_selectTimeSlot.delegate=self;
    
    NSDictionary *size = [NSDictionary dictionaryWithObjectsAndKeys:[UIFont fontWithName:@"HelveticaNeue-Light" size:20.0],NSFontAttributeName, [UIColor whiteColor],NSForegroundColorAttributeName, nil];
    self.navigationController.navigationBar.titleTextAttributes = size;
    
    if ([self.modifyTypeStr isEqualToString:@"update"]) {
        // [self performSelector:@selector(SetValuestoTxtFields) withObject:nil afterDelay:0.1f];
        [self SetValuestoTxtFields];
       
    }else
    {
        self.navigationItem.title=@"Create a Job";
    }
    
    [self setttingTFandBtns];
    UITapGestureRecognizer *tap=[[UITapGestureRecognizer alloc]initWithTarget:self action:@selector(tapGesture:)];
    tap.numberOfTapsRequired=1;
    [self.view addGestureRecognizer:tap];
    self.navigationItem.rightBarButtonItem=[self backButtonToLandnig:self];
    [self settingFrame];
    
    UIButton *backButton=[UIButton buttonWithType:UIButtonTypeCustom];
    [backButton setFrame:CGRectMake(0, 0, 23, 21)];
    [backButton setImage:[UIImage imageNamed:@"arrow.png"] forState:UIControlStateNormal];
    [backButton addTarget:self action:@selector(leftBarButton:) forControlEvents:UIControlEventTouchUpInside];
    self.navigationItem.leftBarButtonItem=[[UIBarButtonItem alloc]initWithCustomView:backButton];
}

-(void)leftBarButton:(UIButton*)sender
{
    [self.navigationController popViewControllerAnimated:YES];
}
- (UIBarButtonItem *)backButtonToLandnig:(id)target
{
    UIImage *image = [UIImage imageNamed:@"icon"];
    CGRect buttonFrame = CGRectMake(0, 0, 25, 25);
    UIButton *button = [[UIButton alloc] initWithFrame:buttonFrame];
    [button setImage:image forState:UIControlStateNormal];
    UIBarButtonItem *item= [[UIBarButtonItem alloc] initWithCustomView:button];
    return item;
}


-(void)viewDidAppear:(BOOL)animated
{
    [super viewDidAppear:YES];
}

-(void)settingFrame
{
//    float h;
    (arr_Time.count==0)?h=49:(h=arr_Time.count*49);
    table_selectTimeSlot.frame=CGRectMake(table_selectTimeSlot.frame.origin.x, table_selectTimeSlot.frame.origin.y, table_selectTimeSlot.frame.size.width, h);
    
    bottomView.frame=CGRectMake(0, table_selectTimeSlot.frame.origin.y+table_selectTimeSlot.frame.size.height+10, self.view.bounds.size.width, bottomView.frame.size.height);
    [table_selectTimeSlot reloadData];
    [self setttingTFandBtns];
}

-(void)tapGesture:(UITapGestureRecognizer *)sender
{
    [self.popupMenu dismissAnimated:YES];
    [titleTF resignFirstResponder];
    [descriptionTF resignFirstResponder];
    [LocationTF resignFirstResponder];
    [priceTF resignFirstResponder];
    [txtField_compilationdtae resignFirstResponder];
    [view_forShowtimeSlot removeFromSuperview];
}

#pragma mark
#pragma mark-Edit jobs
-(void)SetValuestoTxtFields{
     NSLog(@"Start Setup textfield");
    titleTF.text=[self.jobDetailDict valueForKey:@"title"];
    descriptionTF.text=[self.jobDetailDict valueForKey:@"description"];
    priceTF.text=[self.jobDetailDict valueForKey:@"amount"];
    LocationTF.text=[self.jobDetailDict valueForKey:@"location"];
    
    txtField_compilationdtae.text=[self.jobDetailDict valueForKey:@"date_of_completion"];
    for (NSMutableDictionary *dict  in [self.jobDetailDict valueForKey:@"job_images"])
    {
        NSString *str=[NSString stringWithFormat:@"%@%@",serviceLink,[dict objectForKey:@"image_link"]];
        [arr_ig addObject:str];
       [arr_imgId addObject:[dict objectForKey:@"id"]];
        
        
        NSLog(@"%@=======",self.jobDetailDict);
    }
    
    
    
    
    for (int i=0; i<[arr_ig count]; i++)
    {
            NSURL *aURL = [NSURL URLWithString:[[arr_ig objectAtIndex:i] stringByAddingPercentEscapesUsingEncoding: NSUTF8StringEncoding]];
//            UIImage *aImage = [UIImage imageWithData:[NSData dataWithContentsOfURL:aURL]];
                if (i==0)
                {
                    [addImg1Btn sd_setBackgroundImageWithURL:aURL forState:UIControlStateNormal];
                    delImg1Btn.tag=[[arr_imgId objectAtIndex:i]integerValue];
//                    i1=aImage;
                    delImg1Btn.hidden=false;
                }
                if (i==1)
                {
                    [addImg2Btn sd_setBackgroundImageWithURL:aURL forState:UIControlStateNormal];

                    delImg2Btn.tag=[[arr_imgId objectAtIndex:i]integerValue];
//                    i2=aImage;
                    delImg2Btn.hidden=false;
                }
                if (i==2)
                {
//                    i3=aImage;
                    [addImg3btn sd_setBackgroundImageWithURL:aURL forState:UIControlStateNormal];

                    delImg3Btn.tag=[[arr_imgId objectAtIndex:i]integerValue];
                    delImg3Btn.hidden=false;
                }
    }
    
    
    
    descriptionTF.textColor=[UIColor darkGrayColor];
    self.navigationItem.title=@"Update Job";
    
    NSLog(@"%@",self.jobDetailDict);
        int countimg=0;
    for (NSMutableDictionary *dict  in [self.jobDetailDict valueForKey:@"job_time_slots"]) {
        countimg++;
       
        if (countimg==1)
        {
            NSString *str=[NSString stringWithFormat:@"%@ To %@",[dict objectForKey:@"start_time"],[dict objectForKey:@"end_time"]];
            NSLog(@"%@",str);
            [arr_Time addObject:str];
            [arr_SlotId addObject:[dict objectForKey:@"id"]];
            [btn_SelectTimeSlot_Object setTitle:[arr_Time objectAtIndex:0] forState:UIControlStateNormal];

        }
        if (countimg==2)
        {
            NSString *str=[NSString stringWithFormat:@"%@ To %@",[dict objectForKey:@"start_time"],[dict objectForKey:@"end_time"]];
            [arr_Time addObject:str];
            [arr_SlotId addObject:[dict objectForKey:@"id"]];
        }
        if (countimg==3)
        {
            NSString *str=[NSString stringWithFormat:@"%@ To %@",[dict objectForKey:@"start_time"],[dict objectForKey:@"end_time"]];
            [arr_Time addObject:str];
            [arr_SlotId addObject:[dict objectForKey:@"id"]];
        }

        }
    
    if (arr_Time.count) {
        [self settingFrame];
        [btn_SelectTimeSlot_Object setTitleColor:[UIColor darkGrayColor] forState:UIControlStateNormal];
    }else{
        [btn_SelectTimeSlot_Object setTitleColor:[UIColor lightGrayColor] forState:UIControlStateNormal];
    }
    NSLog(@"end Setup textfield");
    
    [self performSelectorInBackground:@selector(downloadimages) withObject:nil];
    
    
    [SVProgressHUD dismiss];
}

-(void)downloadimages{
    
    for (int i=0; i<[arr_ig count]; i++)
    {
        NSURL *aURL = [NSURL URLWithString:[[arr_ig objectAtIndex:i] stringByAddingPercentEscapesUsingEncoding: NSUTF8StringEncoding]];
        UIImage *aImage = [UIImage imageWithData:[NSData dataWithContentsOfURL:aURL]];
        if (i==0)
        {
            i1=aImage;
        }
        if (i==1)
        {
            i2=aImage;
        }
        if (i==2)
        {
            i3=aImage;
        }
    }

}

- (void)scrollViewWillBeginDragging:(UIScrollView *)scrollView{
    
    [view_forShowtimeSlot removeFromSuperview];
}

#pragma mark
#pragma catecategory ID..............
-(void)categoryId:(NSString *)str
{
    str_categoryId=str;
}
#pragma end......................................

/**
 *  Setting TextViews To adjust space on leftside
 */
-(void)setttingTFandBtns{
    
    if (self.view.bounds.size.width==320) {
        mainSV.contentSize=CGSizeMake(320, 510+h);
        
    }else  if (self.view.bounds.size.width==375) {
        mainSV.contentSize=CGSizeMake(375, 600+h);
    }
    else  if (self.view.bounds.size.width==414) {
          mainSV.contentSize=CGSizeMake(375, 680+h);
    }
   // CGFloat newContentOffsetX = (mainSV.contentSize.width - mainSV.frame.size.width) / 2;
   // mainSV.contentOffset = CGPointMake(newContentOffsetX, 0);

    
    if (arr_Time.count) {
         [btn_SelectTimeSlot_Object setTitleColor:[UIColor darkGrayColor] forState:UIControlStateNormal];
    }else{
         [btn_SelectTimeSlot_Object setTitleColor:[UIColor lightGrayColor] forState:UIControlStateNormal];
    }
    
    titleTF.leftViewMode = UITextFieldViewModeAlways;
    UIView* leftView1 = [[UIView alloc] initWithFrame:CGRectMake(0, 0, 3, 10)];
    titleTF.leftView = leftView1;
    
    txtField_compilationdtae.leftViewMode = UITextFieldViewModeAlways;
    UIView* leftView3 = [[UIView alloc] initWithFrame:CGRectMake(0, 0, 3, 3)];
    txtField_compilationdtae.leftView = leftView3;
    
    locationManager = [[CLLocationManager alloc] init];
    locationManager.delegate = self;
    locationManager.desiredAccuracy = kCLLocationAccuracyBest;
    
    [locationManager startUpdatingLocation];
}

#pragma mark - CLLocationManagerDelegate

/*- (void)locationManager:(CLLocationManager *)manager didFailWithError:(NSError *)error
{
    NSLog(@"didFailWithError: %@", error);
    UIAlertView *errorAlert = [[UIAlertView alloc]
                               initWithTitle:@"Error" message:@"Failed to Get Your Location" delegate:nil cancelButtonTitle:@"OK" otherButtonTitles:nil];
    [errorAlert show];
}

- (void)locationManager:(CLLocationManager *)manager didUpdateToLocation:(CLLocation *)newLocation fromLocation:(CLLocation *)oldLocation
{
    NSLog(@"didUpdateToLocation: %@", newLocation);
    CLLocation *currentLocation = newLocation;
    
    if (currentLocation != nil) {
               str_longitude = [NSString stringWithFormat:@"%.8f", currentLocation.coordinate.longitude];
               str_latitude = [NSString stringWithFormat:@"%.8f", currentLocation.coordinate.latitude];
        
        [defaults setObject:str_latitude forKey:@"latitude"];
        [defaults setObject:str_longitude forKey:@"longitude"];


        
    }
}*/

-(void)QbPopUpMenu2items:(BOOL)isImageset{
    
    QBPopupMenuItem *item = [QBPopupMenuItem itemWithTitle:@"Capture Image" target:self action:@selector(TakePictureWithCamera:)];
    QBPopupMenuItem *item2 = [QBPopupMenuItem itemWithTitle:@"Pick from library" target:self action:@selector(selectPhoto:)];
    
    NSArray *items = @[item, item2];
    
    QBPopupMenu *popupMenu = [[QBPopupMenu alloc] initWithItems:items];
    popupMenu.height=50;
    popupMenu.highlightedColor = [[UIColor colorWithRed:0 green:0.478 blue:1.0 alpha:1.0] colorWithAlphaComponent:0.8];
    
    self.popupMenu = popupMenu;
}


- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/**
 *  @author Narinder, 15-08-21 10:08:13
 *
 *  SelectTimeButton used to pick Time slot for job comlition
 *
 *  @param sender <#sender description#>
 */
- (IBAction)btn_dropDownTimeslot:(id)sender
{
    
    [view_forShowtimeSlot setFrame:CGRectMake(8, view_forShowtimeSlot.frame.origin.y, view_forShowtimeSlot.frame.size.width, 150)];
    UIBezierPath *shadowPath = [UIBezierPath bezierPathWithRect:view_forShowtimeSlot.bounds];
    view_forShowtimeSlot.layer.masksToBounds = NO;
    view_forShowtimeSlot.layer.shadowColor = [UIColor blackColor].CGColor;
    view_forShowtimeSlot.layer.shadowOffset = CGSizeMake(0.0f, 5.0f);
    view_forShowtimeSlot.layer.shadowOpacity = 0.5f;
    view_forShowtimeSlot.layer.shadowPath = shadowPath.CGPath;
    [self.view.window addSubview:view_forShowtimeSlot];
    [table_selectTimeSlot reloadData];
  }

- (IBAction)btnDone_fromTimeSlot:(id)sender
{
    [view_forShowtimeSlot removeFromSuperview];
    self.view.userInteractionEnabled=YES;
    
}
#pragma mark
#pragma mark-Add time slot
-(IBAction)selectTimeSlotBtnClicked:(id)sender
{
      [self setdateFordp1:dp1];
     [self setDate:dp1 animated:NO];
    
    [view_forShowtimeSlot removeFromSuperview];
    NSString *str=txtField_compilationdtae.text;
    if ([str isEqualToString:@""])
    {
        UIAlertView *alert=[[UIAlertView alloc]initWithTitle:@"" message:@"Please select first, completion time." delegate:nil cancelButtonTitle:@"OK" otherButtonTitles:nil];
        [alert show];
    }
    else
    {
    
    if ([arr_Time count]<3)
    {
        [UIView animateWithDuration:0.3f
                         animations:^{
                             blurEffect = [UIBlurEffect effectWithStyle:UIBlurEffectStyleExtraLight];
                             visualEffectView = [[UIVisualEffectView alloc] initWithEffect:blurEffect];
                             visualEffectView.alpha=1.0f;
                             visualEffectView.frame = self.view.bounds;
                             selectTimeView.center=self.view.center;
                             [self.view addSubview:visualEffectView];
                         }
                         completion:^(BOOL finished)
         {
             [visualEffectView addSubview:selectTimeView];
         }
         ];
    }
    
    else
    {
        UIAlertView *alert=[[UIAlertView alloc]initWithTitle:@"" message:@"You can't be add more than 3 time slot.Please delete time slot to take new time slot." delegate:nil cancelButtonTitle:@"OK" otherButtonTitles:nil];
        [alert show];
    }
        
    }
    
}


-(BOOL)checkDate :(NSDate *) startDate endDateIs:(NSDate *) endDate
{
    NSDateFormatter *dateFormatter = [[NSDateFormatter alloc] init];
    [dateFormatter setDateFormat:@"HH:mm"]; //24hr time format
    
    NSString *startDateStr = [NSString stringWithFormat:@"%@",[dateFormatter stringFromDate:startDate]];
    NSString *endDateStr = [NSString stringWithFormat:@"%@",[dateFormatter stringFromDate:endDate]];
    
    NSDate *startDateIs = [dateFormatter dateFromString:startDateStr];
    NSDate *endDateIs = [dateFormatter dateFromString:endDateStr];
    
    //        NSDate *startDate = [dateFormatter stringFromDate:dp1.date];
    //        NSLog(@"startDate: %@", startDate);
    //        NSLog(@"endDate: %@ ", endDate);
    
    
    NSDateComponents *dayComponent = [[NSDateComponents alloc] init];
    dayComponent.minute = 30;
    
    NSCalendar *theCalendar = [NSCalendar currentCalendar];
    NSDate *nextDate = [theCalendar dateByAddingComponents:dayComponent toDate:startDateIs options:0];
    
    NSLog(@"nextDate: %@ ...", nextDate);
    
    if ([nextDate compare:endDateIs] == NSOrderedDescending)
    {
        return true;
    }
    
    return false;
}

-(IBAction)doneDpBtnClicked:(id)sender
{
    [visualEffectView removeFromSuperview];
    
    NSDateFormatter *dateFormatter = [[NSDateFormatter alloc] init];
    [dateFormatter setDateFormat:@"HH:mm"]; //24hr time format
    
    NSDateFormatter *dateFormattertest = [[NSDateFormatter alloc] init];
    [dateFormattertest setDateFormat:@"hh"]; //24hr time format
    
    
    
    //TO COMPARE DATE....
    NSDate *now = [NSDate date];
    
    NSDate *todayDate = [NSDate date]; // get today date
    NSDateFormatter *dateFormatter1 = [[NSDateFormatter alloc] init]; // here we create NSDateFormatter object for change the Format of date..
    [dateFormatter1 setDateFormat:@"yyyy-MM-dd"]; //Here we can set the format which we need
    curentDate = [dateFormatter1 stringFromDate:todayDate];// here convert date in
    NSLog(@"Today formatted date is %@",curentDate);
    NSLog(@"||%@",datePicker.date);
    NSLog(@"%@",arr_Time);
    
    if ([arr_Time count]==0)
    {
        NSDateFormatter *dateFormat = [[NSDateFormatter alloc] init];
        [dateFormat setDateFormat:@"hh:mm a"]; //24hr time format
        NSString  *str=[NSString stringWithFormat:@"%@ TO %@",[dateFormat stringFromDate:dp1.date],[dateFormat stringFromDate:dp2.date]];
        
        
        if ([txtField_compilationdtae.text isEqualToString:curentDate])
        {
            if ([dp1.date compare:now] == NSOrderedDescending)
            {
                if ([self checkDate:dp1.date endDateIs:dp2.date] == true)
                {
                    UIAlertView *alertView=[[UIAlertView alloc]initWithTitle:@"" message:@"End Time should be 30 Minutes greater than Start Time"delegate:nil cancelButtonTitle:@"OK" otherButtonTitles:nil];
                    [alertView show];
                }
               else if ([dp1.date compare:dp2.date] == NSOrderedAscending)
                {
                    startTime1 = [dateFormatter stringFromDate:dp1.date];
                    endTime1 = [dateFormatter stringFromDate:dp2.date];
                    [arr_Time addObject:str];
                }
               
                else
                {
                    UIAlertView *alertView=[[UIAlertView alloc]initWithTitle:@"" message:@"End Time should be greater than Start Time"delegate:nil cancelButtonTitle:@"OK" otherButtonTitles:nil];
                    [alertView show];
                }
                
            }
            else{
                UIAlertView *alertView=[[UIAlertView alloc]initWithTitle:@"" message:@"Start Time should be greater than Current Time"delegate:nil cancelButtonTitle:@"OK" otherButtonTitles:nil];
                [alertView show];
            }
        }
        else
        {
            
            NSLog(@"%@-------%@",dp1.date,dp2.date);
        
            if ([self checkDate:dp1.date endDateIs:dp2.date] == true)
            {
                UIAlertView *alertView=[[UIAlertView alloc]initWithTitle:@"" message:@"End Time should be 30 Minutes greater than Start Time"delegate:nil cancelButtonTitle:@"OK" otherButtonTitles:nil];
                [alertView show];
            }

            else if ([dp1.date compare:dp2.date] == NSOrderedAscending)
            {
                startTime1 = [dateFormatter stringFromDate:dp1.date];
                endTime1 = [dateFormatter stringFromDate:dp2.date];
                [arr_Time addObject:str];
                
            }
            else
            {
                UIAlertView *alertView=[[UIAlertView alloc]initWithTitle:@"" message:@"End Time should be greater than Start Time"delegate:nil cancelButtonTitle:@"OK" otherButtonTitles:nil];
                [alertView show];
            }
        }
        
        
      /* if ([txtField_compilationdtae.text isEqualToString:curentDate])
        {
            if ([dp1.date compare:now] == NSOrderedDescending) {
                if ([dp1.date compare:dp2.date] == NSOrderedAscending)
                {
                    startTime1 = [dateFormatter stringFromDate:dp1.date];
                    endTime1 = [dateFormatter stringFromDate:dp2.date];
                    [arr_Time addObject:str];
                }
                else
                {
                    UIAlertView *alertView=[[UIAlertView alloc]initWithTitle:@"" message:@"End Time should be greater than Start Time"delegate:nil cancelButtonTitle:@"OK" otherButtonTitles:nil];
                    [alertView show];
                }

            }else{
                UIAlertView *alertView=[[UIAlertView alloc]initWithTitle:@"" message:@"Start Time should be greater than Current Time"delegate:nil cancelButtonTitle:@"OK" otherButtonTitles:nil];
                [alertView show];
            }
        }
        else{

            NSLog(@"%@-------%@",dp1.date,dp2.date);
            
         if ([dp1.date compare:dp2.date] == NSOrderedAscending)
        {
            startTime1 = [dateFormatter stringFromDate:dp1.date];
            endTime1 = [dateFormatter stringFromDate:dp2.date];
            [arr_Time addObject:str];
  
        }
        else
        {
            UIAlertView *alertView=[[UIAlertView alloc]initWithTitle:@"" message:@"End Time should be greater than Start Time"delegate:nil cancelButtonTitle:@"OK" otherButtonTitles:nil];
            [alertView show];
        }
    }*/
        
    }
    
    
    else if ([arr_Time count]==1)
    {
       
        NSDateFormatter *dateFormat = [[NSDateFormatter alloc] init];
        [dateFormat setDateFormat:@"hh:mm a"]; //24hr time format
        str_selectTime=[NSString stringWithFormat:@"%@ TO %@",[dateFormat stringFromDate:dp1.date],[dateFormat stringFromDate:dp2.date]];

       
        
        if ([txtField_compilationdtae.text isEqualToString:curentDate])
        {
            
            if ([dp1.date compare:now] == NSOrderedDescending)
            {
               if ([self checkDate:dp1.date endDateIs:dp2.date] == true)
              {
                  UIAlertView *alertView=[[UIAlertView alloc]initWithTitle:@"" message:@"End Time should be 30 Minutes greater than Start Time"delegate:nil cancelButtonTitle:@"OK" otherButtonTitles:nil];
                  [alertView show];
              }
            else if ([dp1.date compare:dp2.date] == NSOrderedAscending)
            {
                if ([self cheackTimeslot])
                    {
                        
                    }
                    else
                    {
                    startTime2 = [dateFormatter stringFromDate:dp1.date];
                    endTime2 = [dateFormatter stringFromDate:dp2.date];
                    [arr_Time addObject:str_selectTime];
                    }
                }
                else
                {
                    UIAlertView *alertView=[[UIAlertView alloc]initWithTitle:@"" message:@"End Time should be greater than Start Time"delegate:nil cancelButtonTitle:@"OK" otherButtonTitles:nil];
                    [alertView show];
                }
                
            }else{
                UIAlertView *alertView=[[UIAlertView alloc]initWithTitle:@"" message:@"Start Time should be greater than Current Time"delegate:nil cancelButtonTitle:@"OK" otherButtonTitles:nil];
                [alertView show];
            }

            
        }
        
        else
        {
            
            NSLog(@"%@-------%@",dp1.date,dp2.date);
            
            
            if ([dp1.date compare:dp2.date] == NSOrderedAscending)
            {
                if ([self checkDate:dp1.date endDateIs:dp2.date] == true)
                 {
                    UIAlertView *alertView=[[UIAlertView alloc]initWithTitle:@"" message:@"End Time should be 30 Minutes greater than Start Time"delegate:nil cancelButtonTitle:@"OK" otherButtonTitles:nil];
                    [alertView show];
                }
                else if ([self cheackTimeslot])
                {
                    
                }
                else
                {
                    startTime2 = [dateFormatter stringFromDate:dp1.date];
                    endTime2 = [dateFormatter stringFromDate:dp2.date];
                    [arr_Time addObject:str_selectTime];

                }
                
                               
            }
            else
            {
                UIAlertView *alertView=[[UIAlertView alloc]initWithTitle:@"" message:@"End Time should be greater than Start Time"delegate:nil cancelButtonTitle:@"OK" otherButtonTitles:nil];
                [alertView show];
            }
        }
        
    }
    
    
    
    
    
    else if ([arr_Time count]==2)
    {
        NSDateFormatter *dateFormat = [[NSDateFormatter alloc] init];
        [dateFormat setDateFormat:@"hh:mm a"]; //24hr time format
         str_selectTime=[NSString stringWithFormat:@"%@ TO %@",[dateFormat stringFromDate:dp1.date],[dateFormat stringFromDate:dp2.date]];
      NSLog(@"%@",str_selectTime);
        //  [arr_Time addObject:str_selectTime];
        
        
        if ([txtField_compilationdtae.text isEqualToString:curentDate])
        {
            
            if ([dp1.date compare:now] == NSOrderedDescending)
            {
                if ([self checkDate:dp1.date endDateIs:dp2.date] == true)
                {
                    UIAlertView *alertView=[[UIAlertView alloc]initWithTitle:@"" message:@"End Time should be 30 Minutes greater than Start Time"delegate:nil cancelButtonTitle:@"OK" otherButtonTitles:nil];
                    [alertView show];
                }
                
               else if ([dp1.date compare:dp2.date] == NSOrderedAscending)
                {
                    if ([self cheackTimeslot])
                    {
                        
                    }
                    else
                    {
                        startTime3 = [dateFormatter stringFromDate:dp1.date];
                        endTime3 = [dateFormatter stringFromDate:dp2.date];
                        [arr_Time addObject:str_selectTime];
                    }
                }
                else
                {
                    UIAlertView *alertView=[[UIAlertView alloc]initWithTitle:@"" message:@"End Time should be greater than Start Time"delegate:nil cancelButtonTitle:@"OK" otherButtonTitles:nil];
                    [alertView show];
                }
                
            }else{
                UIAlertView *alertView=[[UIAlertView alloc]initWithTitle:@"" message:@"Start Time should be greater than Current Time"delegate:nil cancelButtonTitle:@"OK" otherButtonTitles:nil];
                [alertView show];
            }
            
            
        }
        
        else{
            
            NSLog(@"%@-------%@",dp1.date,dp2.date);
            
            
            
            if ([self checkDate:dp1.date endDateIs:dp2.date] == true)
            {
                UIAlertView *alertView=[[UIAlertView alloc]initWithTitle:@"" message:@"End Time should be 30 Minutes greater than Start Time"delegate:nil cancelButtonTitle:@"OK" otherButtonTitles:nil];
                [alertView show];
            }
            
           else if ([dp1.date compare:dp2.date] == NSOrderedAscending)
            {
//                startTime3 = [dateFormatter stringFromDate:dp1.date];
//                endTime3 = [dateFormatter stringFromDate:dp2.date];
//                [arr_Time addObject:str_selectTime];
                
                if ([self cheackTimeslot])
                {
                    
                }
                else
                {
                    startTime3 = [dateFormatter stringFromDate:dp1.date];
                    endTime3 = [dateFormatter stringFromDate:dp2.date];
                    [arr_Time addObject:str_selectTime];
                    
                }
                
            }
            else
            {
                UIAlertView *alertView=[[UIAlertView alloc]initWithTitle:@"" message:@"End Time should be greater than Start Time"delegate:nil cancelButtonTitle:@"OK" otherButtonTitles:nil];
                [alertView show];
            }
        }

    }

    [table_selectTimeSlot reloadData];
    [self settingFrame];
    if (arr_Time.count)
    {
         [btn_SelectTimeSlot_Object setTitle:[arr_Time objectAtIndex:0] forState:UIControlStateNormal];
    }
    [btn_SelectTimeSlot_Object setTitleColor:[UIColor darkGrayColor] forState:UIControlStateNormal];
    
}
-(BOOL)cheackTimeslot
{
    NSLog(@"%@",arr_Time);
    if (arr_Time.count==1)
    {
   NSString *time1 = [arr_Time objectAtIndex:0];
    if ([str_selectTime.uppercaseString isEqualToString:time1.uppercaseString])
    {
        
        UIAlertView *alert=[[UIAlertView alloc]initWithTitle:@"" message:@"you can't select same time slot." delegate:nil cancelButtonTitle:@"OK" otherButtonTitles:nil];
        [alert show];
        return YES;
    }
        
    }
    else if(arr_Time.count==2)
    {
        NSLog(@"%@",str_selectTime);
        NSLog(@"%@",[arr_Time objectAtIndex:1]);
        NSLog(@"%@",[arr_Time objectAtIndex:0]);
        NSString *time1 = [arr_Time objectAtIndex:0];
        NSString *time2 = [arr_Time objectAtIndex:1];
        if ([str_selectTime.uppercaseString isEqualToString:time2.uppercaseString]||[str_selectTime isEqualToString:time1.uppercaseString])
        {
            UIAlertView *alert=[[UIAlertView alloc]initWithTitle:@"" message:@"you can't select same time slot." delegate:nil cancelButtonTitle:@"OK" otherButtonTitles:nil];
            [alert show];
            return YES;
        }
        
    }
   
    return NO;
}

#pragma mark popup datepicker in keyboard...........start.......................
-(void)textFieldDidBeginEditing:(UITextField *)textField
{
    
    if (textField==LocationTF)
    {
        UIViewController *mapView=[self.storyboard instantiateViewControllerWithIdentifier:@"MapKitDisplayViewController"];
        [self presentViewController:mapView animated:YES completion:nil];
    }
    if (textField==txtField_compilationdtae)
    {
        UIToolbar *bar=[[UIToolbar alloc]initWithFrame:CGRectMake(0, 0, self.view.bounds.size.width,40)];
        UIBarButtonItem *item=[[UIBarButtonItem alloc]initWithTitle:@"Done" style:UIBarButtonItemStyleDone target:self action:@selector(Done:)];
        NSArray *arr=@[item];
        [bar setItems:arr];
        
        datePicker = [[UIDatePicker alloc] init];
        datePicker.date =
        [[ NSDate alloc ] initWithTimeIntervalSinceNow: (NSTimeInterval) 2 ];
        datePicker.minimumDate =
        [[ NSDate alloc ] initWithTimeIntervalSinceNow: (NSTimeInterval) 0 ];
        datePicker.datePickerMode = UIDatePickerModeDate;
        
        [datePicker setFrame:CGRectMake(0, 400, self.view.frame.size.width, self.view.bounds.size.height/3.0)];
        [datePicker addTarget:self action:@selector(datePickerValueChanged:) forControlEvents:UIControlEventValueChanged];
        txtField_compilationdtae.inputView = datePicker;
        txtField_compilationdtae.inputAccessoryView=bar;
        txtField_compilationdtae.inputAccessoryView.backgroundColor=[UIColor orangeColor];
        datePicker.backgroundColor=[UIColor whiteColor];
        
        NSDate *todayDate = [NSDate date]; // get today date
        NSDateFormatter *dateFormatter = [[NSDateFormatter alloc] init]; // here we create NSDateFormatter object for change the Format of date..
        [dateFormatter setDateFormat:@"yyyy-MM-dd"]; //Here we can set the format which we need
        curentDate = [dateFormatter stringFromDate:todayDate];// here convert date in
        NSLog(@"Today formatted date is %@",curentDate);
        if ([txtField_compilationdtae.text isEqualToString:@""])
        {
            txtField_compilationdtae.text=curentDate;

        }
        else
        {
            
        }
    }
}

-(void)Done:(id)sender
{
    
    [txtField_compilationdtae resignFirstResponder];
    
}

-(void)datePickerValueChanged:(UIDatePicker*)datePicker1
{
    NSDate *todayDate = [NSDate date]; // get today date
    NSDateFormatter *dateFormatter1 = [[NSDateFormatter alloc] init]; // here we create NSDateFormatter object for change the Format of date..
    [dateFormatter1 setDateFormat:@"yyyy-MM-dd"]; //Here we can set the format which we need
    curentDate = [dateFormatter1 stringFromDate:todayDate];// here convert date in String

    
    NSDateFormatter *dateFormatter = [[NSDateFormatter alloc] init];
    [dateFormatter setDateFormat:@"yyyy-MM-dd"];
    NSString *strDate = [dateFormatter stringFromDate:datePicker1.date];
    
    if ([strDate compare:curentDate] == NSOrderedDescending)
    {
        txtField_compilationdtae.text=strDate;
        NSLog(@"date1 is later than date2");
        NSTimeInterval t = [datePicker1.date timeIntervalSince1970];
         NSLog(@"date1 checker skjadh %f",t);
        
    } else if ([strDate compare:curentDate] == NSOrderedAscending){
        NSLog(@"date1 is earlier than date2");
        UIAlertView *alert=[[UIAlertView alloc]initWithTitle:@"" message:@"Date should not be less than current date" delegate:nil cancelButtonTitle:@"OK" otherButtonTitles: nil];
        [alert show];
        txtField_compilationdtae.text=@"";
        
    } else
    {
//        NSCalendar *gregorian = [[NSCalendar alloc]
//                                 initWithCalendarIdentifier:NSGregorianCalendar];
//        
//        NSUInteger unitFlags = NSMonthCalendarUnit | NSDayCalendarUnit;
//        
//        NSDateComponents *components = [gregorian components:unitFlags fromDate:startDate toDate:endDate options:0];
//        NSInteger months = [components month];
//        NSInteger days = [components day];
        txtField_compilationdtae.text=strDate;
        NSLog(@"dates are the same");
    }
    
 //[self removeDate];
}
-(void)removeDate
{
    arr_Time=[[NSMutableArray alloc]init];
    startTime1=nil;
    startTime2=nil;
    startTime3=nil;
    endTime1=nil;
    endTime2=nil;
    endTime3=nil;
    [self settingFrame];
}

#pragma mark-UITextFild Delegate
- (BOOL)textField:(UITextField *)textField shouldChangeCharactersInRange:(NSRange)range replacementString:(NSString *)string
{
    if (textField==titleTF)
    {
        if (titleTF.text.length<1) {
            if ([string isEqualToString:@" "]) {
                return NO;
            }
           
        }
        if ([string isEqualToString:@""]) {
            return YES;
        }
        if (titleTF.text.length<50)
        {
            return YES;
        }
        else
        {
            return NO;
        }
    }
    
    if (textField==LocationTF)
    {
        
        return NO;
//        if (LocationTF.text.length<1) {
//            if ([string isEqualToString:@" "]) {
//                return NO;
//            }
//        }
    }
    if (textField==priceTF) {
        if (priceTF.text.length>5)
        {
            if ([string isEqualToString:@""]) {
                return YES;
            }
            else
            {
                return NO;
            }
        }
       
    }
    if (textField==txtField_compilationdtae)
    {
       
            return NO;
   
    }
    return YES ;
}


- (BOOL)textFieldShouldReturn:(UITextField *)textField
{
    [LocationTF resignFirstResponder];
    [titleTF resignFirstResponder];
    [priceTF resignFirstResponder];
    return YES;
}

# pragma mark UItextView Delegates 
- (BOOL)textView:(UITextView *)textView shouldChangeTextInRange:(NSRange)range replacementText:(NSString *)text
{
    if (textView==descriptionTF){
        if (descriptionTF.text.length<1) {
            if ([text isEqualToString:@" "]) {
                return NO;
            }
        }
        if ([text isEqualToString:@""]) {
            return YES;
        }
        if (descriptionTF.text.length<=300) {
            if ([text isEqualToString:@"\n"]) {
                [descriptionTF resignFirstResponder];
                return NO;
            }
            return YES;
        }
        if ([text isEqualToString:@"\n"]) {
            [descriptionTF resignFirstResponder];
            return NO;
        }
    }
    return YES;
}

-(void)textViewDidBeginEditing:(UITextView *)textView
{
    if (textView==descriptionTF)
    {
        if ([descriptionTF.text isEqualToString:@"Add Job Description"]) {
            descriptionTF.text=@"";
            descriptionTF.textColor=[UIColor darkGrayColor];
        }
    }
}

-(void)textViewDidEndEditing:(UITextView *)textView
{
    if (textView==descriptionTF)
    {
        if ([descriptionTF.text isEqualToString:@""]) {
            descriptionTF.text=@"Add Job Description";
            descriptionTF.textColor=[UIColor lightGrayColor];
        }
    }
}

#pragma mark popup datepicker in keyboard...........End.......................

#pragma mark
#pragma create the jobs....................

- (IBAction)btn_createAction:(id)sender {
    
    [UIView animateWithDuration:0.1f
                     animations:^
     {
         [sender setAlpha:0.3f];
     }
                     completion:^(BOOL finished)
     {
         [sender setAlpha:1.0f];
        
          [SVProgressHUD showWithStatus:@"Loading..." maskType:SVProgressHUDMaskTypeClear];
         [self performSelector:@selector(saveOrPublishJob:) withObject:@"0" afterDelay:0.1];
     }
     ];
   
}

- (IBAction)btn_publishAction:(id)sender
{
    
    [UIView animateWithDuration:0.1f
                     animations:^
     {
         [sender setAlpha:0.1f];
     }
                     completion:^(BOOL finished)
     {
         [sender setAlpha:1.0f];
          [SVProgressHUD showWithStatus:@"Loading..." maskType:SVProgressHUDMaskTypeClear];
         [self performSelector:@selector(saveOrPublishJob:) withObject:@"1" afterDelay:0.1];

     }
     ];
}

-(void)saveOrPublishJob:(NSString*)status
{
   
    str_jobstatus=status;
    [self createJob];
}


- (IBAction)btnGetlocation:(id)sender
{
    UIViewController *mapView=[self.storyboard instantiateViewControllerWithIdentifier:@"MapKitDisplayViewController"];
    [self presentViewController:mapView animated:YES completion:nil];
}
-(void)createJob{
    
    str_title=titleTF.text;
    str_description=descriptionTF.text;
    
    if (str_title.length>0) {
        if (str_description.length>0) {
            if (arr_Time.count) {
                { if (txtField_compilationdtae.text.length>2)
                    {
                        if (LocationTF.text.length>0) {
//                            if (priceTF.text.length>0) {
                            
                                NSString *token=[NSString stringWithFormat:@"%@",[defaults objectForKey:@"user_token"]];
                                
                                NSMutableDictionary *jsonDict=[[NSMutableDictionary alloc]init];
                                [jsonDict setValue:str_title forKey:@"title"];
                                [jsonDict setValue:str_description forKey:@"description"];
                                [jsonDict setValue:[defaults objectForKey:@"latitude"] forKey:@"latitude"];
                                [jsonDict setValue:[defaults objectForKey:@"longitude"] forKey:@"longitude"];
                                [jsonDict setValue:str_jobstatus forKey:@"published"];
                                [jsonDict setValue:@"" forKey:@"amount"];
                                
                                [jsonDict setValue:startTime1 forKey:@"start_time1"];
                                [jsonDict setValue:startTime2 forKey:@"start_time2"];
                                [jsonDict setValue:startTime3 forKey:@"start_time3"];
                                [jsonDict setValue:endTime1 forKey:@"end_time1"];
                                [jsonDict setValue:endTime2 forKey:@"end_time2"];
                                [jsonDict setValue:endTime3 forKey:@"end_time3"];
                                
                                [jsonDict setValue:str_categoryId forKey:@"category_id"];
                                [jsonDict setValue:@"1" forKey:@"sub_category_id"];
                                [jsonDict setValue:[defaults objectForKey:@"user_id"] forKey:@"user_id"];
                                [jsonDict setValue:txtField_compilationdtae.text forKey:@"date_of_completion"];
                                [jsonDict setValue:LocationTF.text forKey:@"location"];
                                
                                NSString *urltype;
                                if ([self.modifyTypeStr isEqualToString:@"update"]) {
                                    urltype=[NSString stringWithFormat:@"%@edit-job/%@?token=%@",serviceLink,[self.jobDetailDict valueForKey:@"id"],token];
                                }
                                else{
                                    urltype=[NSString stringWithFormat:@"%@create-job?token=%@",serviceLink,token];
                                }
                                json = [WebService signup:jsonDict:urltype];
                                
                            if ([[json objectForKey:@"message"]isEqualToString:@"Success"])
                                {
                                    NSDictionary *dict=[NSDictionary new];
                                    dict=  [json objectForKey:@"response"];
                                    
                                    jobId=[dict objectForKey:@"job_id"];
                                    NSLog(@"create jobs");
                                    
                                    if ([[json objectForKey:@"message"]isEqualToString:@"Success"])
                                    {
                                        if ([self.modifyTypeStr isEqualToString:@"update"]) {
                                            [SVProgressHUD dismiss];
                                            
                                            UIAlertView *alert=[[UIAlertView alloc]initWithTitle:@"" message:@"Job updated successfully" delegate:self cancelButtonTitle:@"OK" otherButtonTitles:nil];
                                            alert.tag=500;
                                            [alert show];



                                        }
                                        else{
                                            
                                            if (i1==nil&&i2==nil&&i3==nil) {
                                                [SVProgressHUD dismiss];
                                                if ([str_jobstatus isEqualToString:@"0"])
                                                {
                                                    UIAlertView *alert=[[UIAlertView alloc]initWithTitle:@"" message:@"Wow!!! You have successfully created a job." delegate:nil cancelButtonTitle:@"OK" otherButtonTitles:nil];
                                                    [alert show];
  
                                                }
                                                else
                                                {
                                                    UIAlertView *alert=[[UIAlertView alloc]initWithTitle:@"" message:@"Wow!!! You have successfully publish a job." delegate:nil cancelButtonTitle:@"OK" otherButtonTitles:nil];
                                                    [alert show];
                                                }
                                                
                                    
                                                [self.navigationController popToRootViewControllerAnimated:NO];
                                            }
                                        }
                                    }
                                
                                    if ([self.modifyTypeStr isEqualToString:@"update"])
                                    {
                                        
                                    }
                                    else
                                    {
                                        if (!(i1==nil)) {
                                            [self uploadImagesToServer:i1];
                                        }
                                        if (!(i2==nil)) {
                                            [self uploadImagesToServer:i2];
                                        }
                                        if (!(i3==nil)) {
                                            [self uploadImagesToServer:i3];
                                        }
                                    }
                                }
                                else if ([[json objectForKey:@"error"]isEqualToString:@"token_expired"])
                                {
                                    [SVProgressHUD dismiss];
                                    UIAlertView *alert=[[UIAlertView alloc]initWithTitle:@"" message:@"Session Expired Please login agian" delegate:nil cancelButtonTitle:@"OK" otherButtonTitles: nil];
                                    [alert show];
                                  //  AppDelegate *delegate;
                                  //  delegate=(AppDelegate*)[[UIApplication sharedApplication]delegate];
                                    [defaults setBool:NO forKey:@"logined"];
                                    [defaults synchronize];
                                    [[NSUserDefaults standardUserDefaults]synchronize];
                                    [delegate loginStatus];
                                }
                                else if ([[json objectForKey:@"message"]isEqualToString:@"Error :: Record Not verified."])
                                {
                                   
                                    [SVProgressHUD dismiss];
                                    NSString *msg=[NSString stringWithFormat:@"%@",[[json objectForKey:@"errors"] objectForKey:@"title"]];
                                    
//                                    UIAlertView *alert=[[UIAlertView alloc]initWithTitle:@"" message:@"Error: Record Not verified" delegate:nil cancelButtonTitle:@"OK" otherButtonTitles: nil];
//                                    [alert show];
                                    UIAlertView *alert=[[UIAlertView alloc]initWithTitle:@"Error" message:@"The Job title has been already taken." delegate:nil cancelButtonTitle:@"OK" otherButtonTitles: nil];
                                    [alert show];
                                }
                                else{
                                    [SVProgressHUD dismiss];
                                    UIAlertView *alert=[[UIAlertView alloc]initWithTitle:@"" message:@"Error: Server Error" delegate:nil cancelButtonTitle:@"OK" otherButtonTitles: nil];
                                    [alert show];
                                }
//                            }
//                            else{[SVProgressHUD dismiss];
//                                UIAlertView *alert=[[UIAlertView alloc]initWithTitle:@"" message:@"Please enter price" delegate:nil cancelButtonTitle:@"OK" otherButtonTitles: nil];
//                                [alert show];
//                            }
                        }
                        else{[SVProgressHUD dismiss];
                            UIAlertView *alert=[[UIAlertView alloc]initWithTitle:@"" message:@"Please enter location" delegate:nil cancelButtonTitle:@"OK" otherButtonTitles: nil];
                            [alert show];
                        }
                    }
                    else{[SVProgressHUD dismiss];
                        UIAlertView *alert=[[UIAlertView alloc]initWithTitle:@"" message:@"Please select date of completion" delegate:nil cancelButtonTitle:@"OK" otherButtonTitles: nil];
                        [alert show];
                    }
                }
            }else{[SVProgressHUD dismiss];
                UIAlertView *alert=[[UIAlertView alloc]initWithTitle:@"" message:@"Please select time slot" delegate:nil cancelButtonTitle:@"OK" otherButtonTitles: nil];
                [alert show];
            }
        }else{[SVProgressHUD dismiss];
            UIAlertView *alert=[[UIAlertView alloc]initWithTitle:@"" message:@"Please enter job description" delegate:nil cancelButtonTitle:@"OK" otherButtonTitles: nil];
            [alert show];
        }
    }else{[SVProgressHUD dismiss];
        UIAlertView *alert=[[UIAlertView alloc]initWithTitle:@"" message:@"Please enter a valid job title" delegate:nil cancelButtonTitle:@"OK" otherButtonTitles: nil];
        [alert show];
    }
}

- (void)alertView:(UIAlertView *)alertView clickedButtonAtIndex:(NSInteger)buttonIndex
{
    if (buttonIndex==0)
    {
        if (alertView.tag==500)
        {
          [self.navigationController popToViewController:[[self.navigationController viewControllers]objectAtIndex:1] animated:YES];
        }
    }
}

-(void)UpdateJobImg:(UIImage *)img :(NSString*)imgid
{
    
    NSMutableURLRequest *request = [[NSMutableURLRequest alloc] initWithURL:[NSURL URLWithString:[NSString stringWithFormat:@"%@uploadimage/%@",serviceLink,jobId]]];
    
    NSData *imageData = UIImageJPEGRepresentation(img, 1.0);
    
    [request setCachePolicy:NSURLRequestReloadIgnoringLocalCacheData];
    [request setHTTPShouldHandleCookies:NO];
    [request setTimeoutInterval:60];
    [request setHTTPMethod:@"POST"];
    NSString *boundary = @"unique-consistent-string";
    // set Content-Type in HTTP header
    NSString *contentType = [NSString stringWithFormat:@"multipart/form-data; boundary=%@", boundary];
    [request setValue:contentType forHTTPHeaderField: @"Content-Type"];
    // post body
    NSMutableData *body = [NSMutableData data];
    // add params (all params are strings)
    [body appendData:[[NSString stringWithFormat:@"--%@\r\n", boundary] dataUsingEncoding:NSUTF8StringEncoding]];
    [body appendData:[[NSString stringWithFormat:@"Content-Disposition: form-data; name=%@\r\n\r\n", @"imageCaption"] dataUsingEncoding:NSUTF8StringEncoding]];
    [body appendData:[[NSString stringWithFormat:@"%@\r\n", @"Some Caption"] dataUsingEncoding:NSUTF8StringEncoding]];
    // add image data
    if (imageData) {
        [body appendData:[[NSString stringWithFormat:@"--%@\r\n", boundary] dataUsingEncoding:NSUTF8StringEncoding]];
        [body appendData:[[NSString stringWithFormat:@"Content-Disposition: form-data; name=%@; filename=imageName.jpg\r\n", @"photo"] dataUsingEncoding:NSUTF8StringEncoding]];
        [body appendData:[@"Content-Type: image/jpeg\r\n\r\n" dataUsingEncoding:NSUTF8StringEncoding]];
        [body appendData:imageData];
        [body appendData:[[NSString stringWithFormat:@"\r\n"] dataUsingEncoding:NSUTF8StringEncoding]];
    }
    [body appendData:[[NSString stringWithFormat:@"--%@--\r\n", boundary] dataUsingEncoding:NSUTF8StringEncoding]];
    // setting the body of the post to the reqeust
    [request setHTTPBody:body];
    // set the content-length
    NSString *postLength = [NSString stringWithFormat:@"%lu", (unsigned long)[body length]];
    [request setValue:postLength forHTTPHeaderField:@"Content-Length"];
    
    [NSURLConnection sendAsynchronousRequest:request queue:[NSOperationQueue currentQueue] completionHandler:^(NSURLResponse *response, NSData *data, NSError *error) {
        if(data.length > 0)
        {
            count_uploadimge--;
            //success
            NSMutableString *responseStr = [[NSMutableString alloc] initWithData:data encoding:NSUTF8StringEncoding];
            NSMutableDictionary *dict = [[NSMutableDictionary alloc]init];
            dict = [responseStr JSONValue];
            if (count_uploadimge==0)
            {
                [SVProgressHUD dismiss];
                if ([[dict objectForKey:@"message"]isEqualToString:@"Success"])
                {
                    UIAlertView *alert=[[UIAlertView alloc]initWithTitle:@"" message:@"Wow!!! You have successfully created a job" delegate:nil cancelButtonTitle:@"OK" otherButtonTitles:nil];
                    [alert show];


                    [self.navigationController popToRootViewControllerAnimated:YES];
                }
            }
        }else
        {
            [SVProgressHUD dismiss];
        }
    }];
}



-(void)uploadImagesToServer:(UIImage*)img
{
    NSMutableURLRequest *request = [[NSMutableURLRequest alloc] initWithURL:[NSURL URLWithString:[NSString stringWithFormat:@"%@uploadimage/%@",serviceLink,jobId]]];
    
    NSData *imageData = UIImageJPEGRepresentation(img, 1.0);
    
    [request setCachePolicy:NSURLRequestReloadIgnoringLocalCacheData];
    [request setHTTPShouldHandleCookies:NO];
    [request setTimeoutInterval:60];
    [request setHTTPMethod:@"POST"];
    NSString *boundary = @"unique-consistent-string";
    // set Content-Type in HTTP header
    NSString *contentType = [NSString stringWithFormat:@"multipart/form-data; boundary=%@", boundary];
    [request setValue:contentType forHTTPHeaderField: @"Content-Type"];
    // post body
    NSMutableData *body = [NSMutableData data];
    // add params (all params are strings)
    [body appendData:[[NSString stringWithFormat:@"--%@\r\n", boundary] dataUsingEncoding:NSUTF8StringEncoding]];
    [body appendData:[[NSString stringWithFormat:@"Content-Disposition: form-data; name=%@\r\n\r\n", @"imageCaption"] dataUsingEncoding:NSUTF8StringEncoding]];
    [body appendData:[[NSString stringWithFormat:@"%@\r\n", @"Some Caption"] dataUsingEncoding:NSUTF8StringEncoding]];
    // add image data
    if (imageData) {
        [body appendData:[[NSString stringWithFormat:@"--%@\r\n", boundary] dataUsingEncoding:NSUTF8StringEncoding]];
        [body appendData:[[NSString stringWithFormat:@"Content-Disposition: form-data; name=%@; filename=imageName.jpg\r\n", @"photo"] dataUsingEncoding:NSUTF8StringEncoding]];
        [body appendData:[@"Content-Type: image/jpeg\r\n\r\n" dataUsingEncoding:NSUTF8StringEncoding]];
        [body appendData:imageData];
        [body appendData:[[NSString stringWithFormat:@"\r\n"] dataUsingEncoding:NSUTF8StringEncoding]];
    }
    [body appendData:[[NSString stringWithFormat:@"--%@--\r\n", boundary] dataUsingEncoding:NSUTF8StringEncoding]];
    // setting the body of the post to the reqeust
    [request setHTTPBody:body];
    // set the content-length
    NSString *postLength = [NSString stringWithFormat:@"%lu", (unsigned long)[body length]];
    [request setValue:postLength forHTTPHeaderField:@"Content-Length"];
    
    [NSURLConnection sendAsynchronousRequest:request queue:[NSOperationQueue currentQueue] completionHandler:^(NSURLResponse *response, NSData *data, NSError *error) {
        if(data.length > 0)
        {
            count_uploadimge--;
            //success
            NSMutableString *responseStr = [[NSMutableString alloc] initWithData:data encoding:NSUTF8StringEncoding];
            NSMutableDictionary *dict = [[NSMutableDictionary alloc]init];
            dict = [responseStr JSONValue];
            if (count_uploadimge==0)
            {
                [SVProgressHUD dismiss];
                if ([[dict objectForKey:@"message"]isEqualToString:@"Success"])
                {
                    if ([self.modifyTypeStr isEqualToString:@"update"])
                    {
                    }
                    else{
                        
                        if ([str_jobstatus isEqualToString:@"0"])
                        {
                        UIAlertView *alert=[[UIAlertView alloc]initWithTitle:@"" message:@"Wow!!! You have successfully created a job" delegate:nil cancelButtonTitle:@"OK" otherButtonTitles:nil];
                                                   [alert show];
                        }
                        else
                        {
                        UIAlertView *alert=[[UIAlertView alloc]initWithTitle:@"" message:@"Wow!!! You have successfully publish a job" delegate:nil cancelButtonTitle:@"OK" otherButtonTitles:nil];
                                                  [alert show];
                        }

                       [self.navigationController popToRootViewControllerAnimated:NO];
                    }
                }
            }else{
                 [SVProgressHUD dismiss];
            }
        }else
        {
            [SVProgressHUD dismiss];
        }
    }];
}
#pragma mark
#pragma add image to upload...................................
-(IBAction)addImg1BtnClicked:(id)sender
{
    if (i1==nil) {
    
    Itag=@"1";
    count_1++;
    [self QbPopUpMenu2items:YES];
    
    UIButton *button = (UIButton *)sender;
    [self.popupMenu showInView:bottomView targetRect:button.frame animated:YES];
    [addImg1Btn setSelected:YES];
    
    [addImg2Btn setSelected:NO];
    [addImg3btn setSelected:NO];
    }
    
    
}
-(IBAction)addImg2BtnClicked:(id)sender{
    
     if (i2==nil) {
    count_2++;
    Itag=@"2";
    [self QbPopUpMenu2items:YES];
    UIButton *button = (UIButton *)sender;
    [self.popupMenu showInView:bottomView targetRect:button.frame animated:YES];
    [addImg1Btn setSelected:NO];
    [addImg3btn setSelected:NO];
    [addImg2Btn setSelected:YES];
     }
    
}
-(IBAction)addImg3btnClicked:(id)sender{
    
     if (i3==nil) {
    count_3++;
    Itag=@"3";
    [self QbPopUpMenu2items:YES];
    UIButton *button = (UIButton *)sender;
    [self.popupMenu showInView:bottomView targetRect:button.frame animated:YES];
    
    [addImg1Btn setSelected:NO];
    [addImg2Btn setSelected:NO];
    [addImg3btn setSelected:YES];
     }
}

-(IBAction)delImg1BtnClicked:(id)sender{
    
    UIAlertController *alert = [UIAlertController alertControllerWithTitle:@"Do you really want to delete image?"  message:nil preferredStyle:UIAlertControllerStyleAlert];
    
    UIAlertAction *resetAction = [UIAlertAction
                                  actionWithTitle:NSLocalizedString(@"Delete", @"Reset action")
                                  style:UIAlertActionStyleDestructive
                                  handler:^(UIAlertAction *action)
                                  {
                                      Itag=@"1";
                                      count_uploadimge--;
                                      
                                      if ([self.modifyTypeStr isEqualToString:@"update"])
                                      {
                                          NSString *id_img=[NSString stringWithFormat:@"%ld",(long)delImg1Btn.tag];
                                          [self DelImage];
                                          [self deleteimagefromServer :id_img];
                                      }else{
                                          [self DelImage];
                                      }
                                  }];
    
   
    
    UIAlertAction *cancelAction = [UIAlertAction
                                   actionWithTitle:NSLocalizedString(@"Cancel", @"Cancel action")
                                   style:UIAlertActionStyleCancel
                                   handler:^(UIAlertAction *action)
                                   {
                                       
                                   }];
    
    [alert addAction:resetAction];
    [alert addAction:cancelAction];
    
    [self presentViewController:alert animated:YES completion:nil];
}


-(IBAction)delImg2BtnClicked:(id)sender{
    
    
    UIAlertController *alert = [UIAlertController alertControllerWithTitle:@"Do you really want to delete image?"  message:nil preferredStyle:UIAlertControllerStyleAlert];
    
    UIAlertAction *resetAction = [UIAlertAction
                                  actionWithTitle:NSLocalizedString(@"Delete", @"Reset action")
                                  style:UIAlertActionStyleDestructive
                                  handler:^(UIAlertAction *action)
                                  {
                                      Itag=@"2";
                                      count_uploadimge--;
                                      
                                      if ([self.modifyTypeStr isEqualToString:@"update"])
                                      {
                                          NSString *id_img=[NSString stringWithFormat:@"%ld",(long)delImg2Btn.tag];
                                          [self deleteimagefromServer :id_img];
                                          [self DelImage];
                                      }else{
                                          [self DelImage];
                                      }

                                  }];
    
    
    
    UIAlertAction *cancelAction = [UIAlertAction
                                   actionWithTitle:NSLocalizedString(@"Cancel", @"Cancel action")
                                   style:UIAlertActionStyleCancel
                                   handler:^(UIAlertAction *action)
                                   {
                                       
                                   }];
    
    [alert addAction:resetAction];
    [alert addAction:cancelAction];
    
    [self presentViewController:alert animated:YES completion:nil];
    
   }


-(IBAction)delImg3btnClicked:(id)sender{
    UIAlertController *alert = [UIAlertController alertControllerWithTitle:@"Do you really want to delete image?"  message:nil preferredStyle:UIAlertControllerStyleAlert];
    
    UIAlertAction *resetAction = [UIAlertAction
                                  actionWithTitle:NSLocalizedString(@"Delete", @"Reset action")
                                  style:UIAlertActionStyleDestructive
                                  handler:^(UIAlertAction *action)
                                  {
                                      Itag=@"3";
                                      count_uploadimge--;
                                      
                                      if ([self.modifyTypeStr isEqualToString:@"update"])
                                      {
                                          NSString *id_img=[NSString stringWithFormat:@"%ld",(long)delImg3Btn.tag];
                                          [self deleteimagefromServer :id_img];
                                          [self DelImage];
                                      }else{
                                          [self DelImage];
                                      }
                                      
                                  }];
    
    
    
    UIAlertAction *cancelAction = [UIAlertAction
                                   actionWithTitle:NSLocalizedString(@"Cancel", @"Cancel action")
                                   style:UIAlertActionStyleCancel
                                   handler:^(UIAlertAction *action)
                                   {
                                       
                                   }];
    
    [alert addAction:resetAction];
    [alert addAction:cancelAction];
    
    [self presentViewController:alert animated:YES completion:nil];

}




-(void)clickImage
{
    
    UIImagePickerController *picker = [[UIImagePickerController alloc] init];
    picker.delegate = self;
    picker.modalPresentationStyle = UIModalPresentationCurrentContext;
    picker.sourceType = UIImagePickerControllerSourceTypeCamera;
    picker.allowsEditing = YES;
    [self presentViewController:picker animated:YES completion:nil];
}
-(void)TakePictureWithCamera:(id)sender
{
    if (![UIImagePickerController isSourceTypeAvailable:UIImagePickerControllerSourceTypeCamera]) {
        
        UIAlertView *myAlertView = [[UIAlertView alloc] initWithTitle:@"Error"
                                                              message:@"Device has no camera"
                                                             delegate:nil
                                                    cancelButtonTitle:@"OK"
                                                    otherButtonTitles: nil];
        [myAlertView show];
    }else
    {
        [self performSelector:@selector(clickImage) withObject:nil afterDelay:0.3f];

    }
}

- (void)selectPhoto:(UIButton *)sender{
    
//    AVAuthorizationStatus authStatus = [AVCaptureDevice authorizationStatusForMediaType:AVMediaTypeVideo];
//    if(authStatus == AVAuthorizationStatusAuthorized) {
//        // do your logic
//    } else if(authStatus == AVAuthorizationStatusDenied){
//        // denied
//    } else if(authStatus == AVAuthorizationStatusRestricted){
//        // restricted, normally won't happen
//    } else if(authStatus == AVAuthorizationStatusNotDetermined){
//        // not determined?!
//        [AVCaptureDevice requestAccessForMediaType:AVMediaTypeVideo completionHandler:^(BOOL granted) {
//            if(granted){
//                UIImagePickerController *picker = [[UIImagePickerController alloc] init];
//                    picker.delegate = self;
//                    picker.allowsEditing = YES;
//                    picker.sourceType = UIImagePickerControllerSourceTypePhotoLibrary;
//                    [self presentViewController:picker animated:YES completion:NULL];
//                NSLog(@"Granted access to %@", AVMediaTypeVideo);
//            } else {
//                NSLog(@"Not granted access to %@", AVMediaTypeVideo);
//            }
//        }];
//    } else {
//        // impossible, unknown authorization status
//    }
    UIImagePickerController *picker = [[UIImagePickerController alloc] init];
    picker.delegate = self;
    picker.allowsEditing = YES;
    picker.sourceType = UIImagePickerControllerSourceTypePhotoLibrary;
    
    [self presentViewController:picker animated:YES completion:NULL];
}

/**
 *  @author DebutMac4, 15-08-24 13:08:43
 *
 *  ImagePicker Delegates
 *
 *  @param picker UiImagePickerController
 *  @param info   NsDictionary info
 */
- (void)imagePickerController:(UIImagePickerController *)picker didFinishPickingMediaWithInfo:(NSDictionary *)info {
    
    UIImage *chosenImage = info[UIImagePickerControllerEditedImage];
    chosenImage=[ImageUtilitiesClass scaleAndRotateImage:chosenImage];
    
    CGFloat compression = 0.9f;
    CGFloat maxCompression = 0.1f;
    int maxFileSize = 250*720;
    
    NSData *imageData = UIImageJPEGRepresentation(chosenImage, compression);
    
    while ([imageData length] > maxFileSize && compression > maxCompression)
    {
        compression -= 0.1;
        imageData = UIImageJPEGRepresentation(chosenImage, compression);
    }
    
    [self setImage:chosenImage];
    [picker dismissViewControllerAnimated:YES completion:NULL];
}

-(void)imagePickerControllerDidCancel:(UIImagePickerController *)picker
{
    [SVProgressHUD dismiss];
    [picker dismissViewControllerAnimated:YES completion:nil];
}


-(void)setImage:(UIImage *)img{
    
    if ([Itag isEqualToString:@"1"]) {
        [addImg1Btn setBackgroundImage:img forState:UIControlStateNormal];
        count_uploadimge++;
        i1=img;
         [addImg1Btn setBackgroundImage:i1 forState:UIControlStateNormal];
        if ([self.modifyTypeStr isEqualToString:@"update"]) {
            jobId=[self.jobDetailDict valueForKey:@"id"];
             [self uploadImagesToServer:i1];
        }
        
        delImg1Btn.hidden=false;
    }
    else  if ([Itag isEqualToString:@"2"]) {
        
        count_uploadimge++;
        i2=img;
        [addImg2Btn setBackgroundImage:i2 forState:UIControlStateNormal];
        if ([self.modifyTypeStr isEqualToString:@"update"]) {
              jobId=[self.jobDetailDict valueForKey:@"id"];
            [self uploadImagesToServer:i2];
        }
        delImg2Btn.hidden=false;
    }
    else  if ([Itag isEqualToString:@"3"]) {
      
        count_uploadimge++;
        i3=img;
          [addImg3btn setBackgroundImage:i3 forState:UIControlStateNormal];
        if ([self.modifyTypeStr isEqualToString:@"update"]) {
              jobId=[self.jobDetailDict valueForKey:@"id"];
            [self uploadImagesToServer:i3];
        }
        delImg3Btn.hidden=false;
    }
}


-(void)DelImage
{
    if ([Itag isEqualToString:@"1"]) {
        [addImg1Btn setBackgroundImage:nil forState:UIControlStateNormal];
        [addImg1Btn setBackgroundImage:[UIImage imageNamed:@"add_img.png"] forState:UIControlStateNormal];
        i1=nil;
        delImg1Btn.hidden=true;
    }
    else  if ([Itag isEqualToString:@"2"]) {
        [addImg2Btn setBackgroundImage:nil forState:UIControlStateNormal];
        [addImg2Btn setBackgroundImage:[UIImage imageNamed:@"add_img.png"] forState:UIControlStateNormal];
        i2=nil;
        delImg2Btn.hidden=true;
    }
    else  if ([Itag isEqualToString:@"3"]) {
        [addImg3btn setBackgroundImage:nil forState:UIControlStateNormal];
        [addImg3btn setBackgroundImage:[UIImage imageNamed:@"add_img.png"] forState:UIControlStateNormal];
        i3=nil;
        delImg3Btn.hidden=true;
    }
}

-(void)deleteimagefromServer : (NSString *)imgId
{
   
     NSString *str=[NSString stringWithFormat:@"%@destroyJobImages/%@?token=%@",serviceLink,imgId,[defaults objectForKey:@"user_token"]];
     id jsonz = [WebService deleteData:str];
    if ([[jsonz objectForKey:@"message"]isEqualToString:@"Success"])
    {
          [self performSelector:@selector(DelImage) withObject:nil afterDelay:0.1f];
//        UIAlertView *alert=[[UIAlertView alloc]initWithTitle:@"" message:@"delete image" delegate:nil cancelButtonTitle:@"OK" otherButtonTitles:nil];
//        [alert show];
    }
    NSLog(@"%@",jsonz);
}

#pragma mark
#pragma  mark-UITableView Delegates....................
- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    if ([arr_Time count]==0)
    {
        return  1;
    }
    else
    
    return  [arr_Time count];
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    static NSString *simpleTableIdentifier = @"SimpleTableItem";
    
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:simpleTableIdentifier];
    if (cell == nil) {
        cell = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:simpleTableIdentifier];
    }
    
    if ([arr_Time count]==0)
    {
        UIFont *myFont = [ UIFont fontWithName: @"Helvetica Neue 14.0" size: 10.0 ];
        cell.textLabel.font  = myFont;
        cell.textLabel.textColor=[UIColor lightGrayColor];
        cell.textLabel.text=@"Select Time Slot";
    }
    else{
        UIFont *myFont = [ UIFont fontWithName: @"Helvetica Neue" size: 14.0 ];
        cell.textLabel.font  = myFont;
        cell.textLabel.textColor=[UIColor darkGrayColor];
        cell.textLabel.text=[arr_Time objectAtIndex:indexPath.row];
        
    }
    return cell;
}

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    return 1;
}


- (BOOL)tableView:(UITableView *)tableView canEditRowAtIndexPath:(NSIndexPath *)indexPath
{
    if (!arr_Time.count)
    {
      return NO;
    }
      return YES;
}
//- (void)tableView:(UITableView *)tableView commitEditingStyle:(UITableViewCellEditingStyle)editingStyle forRowAtIndexPath:(NSIndexPath *)indexPath
//{
//    
//    if (editingStyle == UITableViewCellEditingStyleDelete) {
//        removeIndex =indexPath.row;
//        if (arr_SlotId.count) {
//            slotId=[arr_SlotId objectAtIndex:indexPath.row];
//            if (slotId.length>0)
//            {
//                [self deleteTimeSlot];
//            }
//        }
//        else
//        {
//          [arr_Time removeObjectAtIndex:indexPath.row];
//          [table_selectTimeSlot reloadData];
//          [self deleteStartTimeEndTime];
//        }
//        [self settingFrame];
//    }
//}
-(NSArray *)tableView:(UITableView *)tableView editActionsForRowAtIndexPath:(NSIndexPath *)indexPath {
    UITableViewRowAction *button = [UITableViewRowAction rowActionWithStyle:UITableViewRowActionStyleDefault title:@"Delete" handler:^(UITableViewRowAction *action, NSIndexPath *indexPath)
                                    {
                                        NSLog(@"Action to perform with Button 1");
                                         removeIndex =indexPath.row;
                                        NSLog(@"%@",arr_SlotId );
                                        if (removeIndex < arr_SlotId.count) {
                                            slotId=[NSString stringWithFormat:@"%@",[arr_SlotId objectAtIndex:indexPath.row] ];
                                            if (slotId.length>0)
                                            {
                                                [self deleteTimeSlot];
                                            }
                                        }
                                        else
                                        {
                                            [arr_Time removeObjectAtIndex:indexPath.row];
                                            [table_selectTimeSlot reloadData];
                                            [self deleteStartTimeEndTime];
                                        }
                                        [self settingFrame];
                                    }];
    button.backgroundColor = [UIColor orangeColor];
    
    return @[button];
}

//- (NSString *)tableView:(UITableView *)tableView titleForDeleteConfirmationButtonForRowAtIndexPath:(NSIndexPath *)indexPath
//{
//    NSString *str=@"dlt";
//    return str;
//}
-(UIView *)tableView:(UITableView *)tableView viewForFooterInSection:(NSInteger)section
{
    UIView *view = [[UIView alloc] initWithFrame:CGRectMake(0, 0, 414, 0)];
    return view;
}
-(void)deleteStartTimeEndTime
{
    if (removeIndex==0)
    {
        startTime1=@"";
        endTime1=@"";
    }
    if (removeIndex==1)
    {
        startTime2=@"";
        endTime2=@"";
    }
    if (removeIndex==2)
    {
        startTime3=@"";
        endTime3=@"";
    }
}
-(void)deleteTimeSlot
{
    NSString *token=[NSString stringWithFormat:@"%@",[defaults objectForKey:@"user_token"]];
    NSString *url1=[NSString stringWithFormat:@"%@destroyJobTimeSlot/%@?token=%@",serviceLink,slotId,token];
    
    NSMutableDictionary *jsonDict=[[NSMutableDictionary alloc]init];
    [jsonDict setValue:slotId forKey:@"id"];
    
    id json1 = [WebService deleteData:url1];
    if ([[json1 objectForKey:@"message"]isEqualToString:@"Success"])
    {
        [arr_Time removeObjectAtIndex:removeIndex];
        [arr_SlotId removeObjectAtIndex:removeIndex];

        if (removeIndex==0)
        {
            startTime1=@"";
            endTime1=@"";
        }
        else if (removeIndex==1)
        {
            startTime2=@"";
            endTime2=@"";
        }
        else if (removeIndex==3)
        {
            startTime3=@"";
            endTime3=@"";
        }
        [table_selectTimeSlot reloadData];
        UIAlertView *alert=[[UIAlertView alloc]initWithTitle:@"" message:@"Time slot deleted" delegate:nil cancelButtonTitle:@"OK" otherButtonTitles: nil];
        [alert show];
    }
    else
    {
        NSLog(@"not deleted");
    }
 }

- (IBAction)btn_Cancel:(id)sender {
     [visualEffectView removeFromSuperview];
}

@end
