//
//  DEMOViewController.m
//  RESideMenuStoryboards
//
//  Created by Roman Efimov on 10/9/13.
//  Copyright (c) 2013 Roman Efimov. All rights reserved.
//

#import "DEMORootViewController.h"
#import "DrawerViewController.h"
#import "ContactUsViewController.h"
@interface DEMORootViewController ()
{
    NSUserDefaults *defaults;
}

@end

@implementation DEMORootViewController

- (void)awakeFromNib
{
    defaults =[NSUserDefaults standardUserDefaults];
    NSString *status=[defaults valueForKey:@"user_status"];
    
    if ([status isEqualToString:@"customer"]) {
        self.contentViewController = [self.storyboard instantiateViewControllerWithIdentifier:@"contentViewController"];
    }else if([status isEqualToString:@"worker"]){
        self.contentViewController = [self.storyboard instantiateViewControllerWithIdentifier:@"workercontentViewController"];
    }else{
        self.contentViewController = [self.storyboard instantiateViewControllerWithIdentifier:@"contentViewController"];
    }
    
    self.leftMenuViewController = [self.storyboard instantiateViewControllerWithIdentifier:@"leftMenuViewController"];
    self.backgroundImage = [UIImage imageNamed:@"Stars"];
    self.delegate = self;
}

#pragma mark -
#pragma mark RESideMenu Delegate

- (void)sideMenu:(RESideMenu *)sideMenu willShowMenuViewController:(UIViewController *)menuViewController
{
    NSLog(@"willShowMenuViewController: %@", NSStringFromClass([menuViewController class]));
}

- (void)sideMenu:(RESideMenu *)sideMenu didShowMenuViewController:(UIViewController *)menuViewController
{
    NSLog(@"didShowMenuViewController: %@", NSStringFromClass([menuViewController class]));
}

- (void)sideMenu:(RESideMenu *)sideMenu willHideMenuViewController:(UIViewController *)menuViewController
{
    NSLog(@"willHideMenuViewController: %@", NSStringFromClass([menuViewController class]));
}

- (void)sideMenu:(RESideMenu *)sideMenu didHideMenuViewController:(UIViewController *)menuViewController
{
    NSLog(@"didHideMenuViewController: %@", NSStringFromClass([menuViewController class]));
}

@end
