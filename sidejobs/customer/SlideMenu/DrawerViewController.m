//
//  ViewController.m
//  SideJobs
//
//  Created by Debut05 on 8/13/15.
//  Copyright (c) 2015 Narinder. All rights reserved.
//

#import "DrawerViewController.h"
//#import "PaymentViewViewController.h"
#import "DEMORootViewController.h"
#import "ContactUsViewController.h"
#import <QuartzCore/QuartzCore.h>
#import <Helper.h>


@interface DrawerViewController ()
{
    NSMutableArray *randomSelection;
    UISwitch *switchview ;
    NSUserDefaults *defaults;
    CAShapeLayer *circleLayer;
    AppDelegate *delegate;
    DEMORootViewController *DRView;
    UIActivityIndicatorView *indicator;
    UITableView *tableView ;
}
@property (strong, readwrite, nonatomic) UITableView *tableView;
@end

@implementation DrawerViewController
- (void)viewDidLoad {
    [super viewDidLoad];
    
    
    
   
    
    delegate=(AppDelegate*)[[UIApplication sharedApplication]delegate];
    

    CALayer *webViewLayer = imgView_profile.layer;
    webViewLayer.cornerRadius= imgView_profile.bounds.size.width/2 ;
     imgView_profile.contentMode = UIViewContentModeScaleAspectFill;
    
      // now you can do a lot of stuff like borders:
    [webViewLayer setBorderColor: [[UIColor darkGrayColor] CGColor]];
    [webViewLayer setBorderWidth: 2.5];
     webViewLayer.masksToBounds=YES;
    
    defaults=[NSUserDefaults standardUserDefaults];
    lbl_userName.text=[defaults objectForKey:@"p_name"];
    
    switchview = [[UISwitch alloc] initWithFrame:CGRectZero];
    
    
      self.tableView = ({
        tableView = [[UITableView alloc] initWithFrame:CGRectMake(0, (self.view.frame.size.height - 40 * 5) / 2.0f, self.view.frame.size.width-100, 54 * 5) style:UITableViewStylePlain];
        tableView.autoresizingMask = UIViewAutoresizingFlexibleTopMargin | UIViewAutoresizingFlexibleBottomMargin | UIViewAutoresizingFlexibleWidth;
        tableView.delegate = self;
        tableView.dataSource = self;
        tableView.opaque = NO;
        tableView.backgroundColor = [UIColor clearColor];
        tableView.backgroundView = nil;
       // tableView.separatorStyle = UITableViewCellSeparatorStyleNone;
        tableView.separatorColor=[UIColor colorWithRed:1 green:1 blue:1 alpha:0.4];
        tableView.bounces = NO;
        tableView.scrollsToTop = NO;
        tableView;
    });
    [self.view addSubview:self.tableView];
    
    defaults =[NSUserDefaults standardUserDefaults];
    NSString *status=[defaults valueForKey:@"user_status"];
    if ([status isEqualToString:@"customer"]) {
        switchview.on=YES;
    }else if([status isEqualToString:@"worker"]){
         switchview.on=NO;
    }
    
    
}

-(void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:YES];
    
    NSString *imageUrl =[NSString stringWithFormat:@"%@", [defaults objectForKey:@"p_photo"]];
    
    if ([imageUrl isEqualToString:serviceLink])
    {
        imgView_profile.image=[UIImage imageNamed:@"defuser.png"];
    }
    else if ([imageUrl  isEqualToString:@""])
    {
     imgView_profile.image=[UIImage imageNamed:@"defuser.png"];
    }
    else
    {
        [imgView_profile sd_setImageWithURL:[NSURL URLWithString:imageUrl] placeholderImage:[UIImage imageNamed:@"defuser.png"]];
    }
    lbl_userName.text=[defaults objectForKey:@"p_name"];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

#pragma mark -
#pragma mark UITableView Delegate

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    [tableView deselectRowAtIndexPath:indexPath animated:YES];
    
    if ([[defaults objectForKey:@"user_status"]isEqualToString:@"worker"])
    {
        switch (indexPath.row)
        {
            case 0:
                [self.sideMenuViewController setContentViewController:[[UINavigationController alloc] initWithRootViewController:[self.storyboard instantiateViewControllerWithIdentifier:@"HelpViewController"]]
                                                             animated:YES];
                [self.sideMenuViewController hideMenuViewController];
                break;
            case 1:
                [self.sideMenuViewController setContentViewController:[[UINavigationController alloc] initWithRootViewController:[self.storyboard instantiateViewControllerWithIdentifier:@"ProfileViewController"]]
                                                             animated:YES];
                [self.sideMenuViewController hideMenuViewController];

                break;
                
            case 2:
//                [self.sideMenuViewController setContentViewController:[[UINavigationController alloc] initWithRootViewController:[self.storyboard instantiateViewControllerWithIdentifier:@"ContactUsViewController"]]
//                                                             animated:YES];
//                [self.sideMenuViewController hideMenuViewController];
                
                [self.sideMenuViewController setContentViewController:[[UINavigationController alloc] initWithRootViewController:[self.storyboard instantiateViewControllerWithIdentifier:@"secondViewController"]]
                                                             animated:YES];
                [self.sideMenuViewController hideMenuViewController];
                break;
                
            case 3:
//                [self.sideMenuViewController setContentViewController:[[UINavigationController alloc] initWithRootViewController:[self.storyboard instantiateViewControllerWithIdentifier:@"NotificationVCViewController"]]
//                                                             animated:YES];
//                [self.sideMenuViewController hideMenuViewController];
               
                break;
            case 4:
                [defaults setBool:NO forKey:@"logined"];
                [defaults synchronize];
                [[NSUserDefaults standardUserDefaults]synchronize];
                [delegate loginStatus];
                
                break;
                

            default:
                break;
        }
    }
    
    else
    {
    switch (indexPath.row) {
        case 0:
            [self.sideMenuViewController setContentViewController:[[UINavigationController alloc] initWithRootViewController:[self.storyboard instantiateViewControllerWithIdentifier:@"HelpViewController"]]
                                                         animated:YES];
            [self.sideMenuViewController hideMenuViewController];
            break;
        case 1:
            
            [self.sideMenuViewController setContentViewController:[[UINavigationController alloc] initWithRootViewController:[self.storyboard instantiateViewControllerWithIdentifier:@"ProfileViewController"]]
                                                         animated:YES];
            [self.sideMenuViewController hideMenuViewController];
//            [self.sideMenuViewController setContentViewController:[[UINavigationController alloc] initWithRootViewController:[self.storyboard instantiateViewControllerWithIdentifier:@"secondViewController"]]
//                                                         animated:YES];
//            [self.sideMenuViewController hideMenuViewController];
            break;
            
        case 2:
            [self.sideMenuViewController setContentViewController:[[UINavigationController alloc] initWithRootViewController:[self.storyboard instantiateViewControllerWithIdentifier:@"secondViewController"]]
                                                                     animated:YES];
            [self.sideMenuViewController hideMenuViewController];
            
//            [self.sideMenuViewController setContentViewController:[[UINavigationController alloc] initWithRootViewController:[self.storyboard instantiateViewControllerWithIdentifier:@"ContactUsViewController"]]
//                                                         animated:YES];
//            [self.sideMenuViewController hideMenuViewController];
            break;
        case 3:
//            [self.sideMenuViewController setContentViewController:[[UINavigationController alloc] initWithRootViewController:[self.storyboard instantiateViewControllerWithIdentifier:@"NotificationVCViewController"]]
//                                                         animated:YES];
//            [self.sideMenuViewController hideMenuViewController];
            break;

        case 4:
            [self logout];
//            [defaults setBool:NO forKey:@"logined"];
//            [defaults synchronize];
//            [[NSUserDefaults standardUserDefaults]synchronize];
//            [delegate loginStatus];
            break;
            
        default:
            break;
            
            
    }
        
    }
}

-(void)logout
{
    
    UIAlertView *alert=[[UIAlertView alloc]initWithTitle:@"Are you sure?" message:@"Do you want to logout?" delegate:self cancelButtonTitle:@"OK" otherButtonTitles:@"Cancel", nil];
    
    [alert show];
   }

- (void)alertView:(UIAlertView *)alertView clickedButtonAtIndex:(NSInteger)buttonIndex;
{
    switch (buttonIndex) {
        case 0:
        {
            [defaults setBool:NO forKey:@"logined"];
            [defaults synchronize];
            [[NSUserDefaults standardUserDefaults]synchronize];
            [delegate loginStatus];

        }
        break;
            case 1:
            break;
        default:
            break;
    }
}
#pragma mark -
#pragma mark UITableView Datasource

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    return 54;
}

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    return 1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)sectionIndex
{
    if ([[defaults objectForKey:@"user_status"]isEqualToString:@"worker"])
    {
        return 5;
    }
    else
        return 5;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    static NSString *cellIdentifier = @"Cell";
    
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:cellIdentifier];
    
    if (cell == nil) {
        cell = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:cellIdentifier];
        cell.backgroundColor = [UIColor clearColor];
        cell.textLabel.font = [UIFont fontWithName:@"HelveticaNeue" size:14 ];
        cell.textLabel.textColor = [UIColor whiteColor];
        cell.textLabel.highlightedTextColor = [UIColor lightGrayColor];
        cell.selectedBackgroundView = [[UIView alloc] init];
    }
    if ([[defaults objectForKey:@"user_status"]isEqualToString:@"worker"])
    {
     
        if (indexPath.row==3) {
            switchview.thumbTintColor=[UIColor orangeColor];
            //         switchview.transform = CGAffineTransformMakeScale(0.75, 0.75);
            [switchview setOnTintColor:[UIColor lightGrayColor]];
            cell.accessoryView = switchview;
            [switchview addTarget:self action:@selector(switchUser:) forControlEvents:UIControlEventValueChanged];
        }
        
        
        defaults =[NSUserDefaults standardUserDefaults];
        NSString *status=[defaults valueForKey:@"user_status"];
        if ([status isEqualToString:@"customer"]) {
            status=@"SERVICE REPS";
        }else if([status isEqualToString:@"worker"]){
            status=@"CUSTOMER";
        }
        
//        NSArray *titles = @[@"MY PROFILE", status, @"CONTACT US", @"NOTIFICATIONS",@"Log Out"];
//        NSArray *images = @[@"person", @"customer", @"contact", @"bell",@"logout"];
        
        NSArray *titles = @[@"HELP", @"SETTINGS",@"PAYMENT",status,@"LOGOUT"];
        NSArray *images = @[@"helopiconn", @"settingiconn",@"pay", @"customer",@"logout"];
        cell.textLabel.text = titles[indexPath.row];
        //    cell.textLabel.textAlignment=NSTextAlignmentCenter;
        cell.imageView.image = [UIImage imageNamed:images[indexPath.row]];

    }
    
    else
    {
    if (indexPath.row==3) {
        switchview.thumbTintColor=[UIColor orangeColor];
        [switchview setOnTintColor:[UIColor lightGrayColor]];
        cell.accessoryView = switchview;
        [switchview addTarget:self action:@selector(switchUser:) forControlEvents:UIControlEventValueChanged];
    }
    
    
    defaults =[NSUserDefaults standardUserDefaults];
    NSString *status=[defaults valueForKey:@"user_status"];
    if ([status isEqualToString:@"customer"]) {
        status=@"SERVICE REPS";
    }else if([status isEqualToString:@"worker"]){
         status=@"Customer";
    }
    

//     NSArray *titles = @[@"MY PROFILE", @"PAYMENT", status,@"CONTACT US", @"NOTIFICATIONS",@"Log Out"];
    NSArray *titles = @[@"HELP", @"SETTINGS",@"PAYMENT",status,@"Log Out"];
    NSArray *images = @[@"helopiconn", @"settingiconn",@"pay", @"customer",@"logout"];
    cell.textLabel.text = titles[indexPath.row];
//    cell.textLabel.textAlignment=NSTextAlignmentCenter;
    cell.imageView.image = [UIImage imageNamed:images[indexPath.row]];
        
    }
    
    return cell;
}
-(UIView *)tableView:(UITableView *)tableView viewForFooterInSection:(NSInteger)section
{
    UIView *view = [[UIView alloc] initWithFrame:CGRectMake(0, 0, 320, 0)];
    return view;
}

-(IBAction)switchUser:(id)sender
{
    
    indicator = [[UIActivityIndicatorView alloc] initWithFrame:CGRectMake(0.0f, 0.0f, 100.0f, 100.0f)];
//    [indicator setCenter:CGPointMake(160.0f, 208.0f)];
    [indicator setCenter:CGPointMake(160.0f, 208.0f)];

    [indicator setActivityIndicatorViewStyle:UIActivityIndicatorViewStyleGray];
    indicator.center=switchview.center;
    [switchview addSubview:indicator];
    
    NSString *userstatus=[defaults objectForKey:@"user_status"];
    if ([userstatus isEqualToString:@"customer"])
    {
       
        
//        [defaults setObject:@"worker" forKey:@"user_status"];
//        [defaults synchronize];
//        
//        [self switched];
        
        if ([[defaults objectForKey:@"worker_approval"] isEqualToString:@"approval"])
        {
            UIAlertView *alert=[[UIAlertView alloc]initWithTitle:@"" message:@"Request is pending for approval." delegate:nil cancelButtonTitle:@"OK" otherButtonTitles:nil];
            [alert show];

        }
        else if ([[defaults objectForKey:@"worker_approval"] isEqualToString:@"approved"])
        {
            [defaults setObject:@"worker" forKey:@"user_status"];
            [defaults synchronize];
            
            [self switched];
            UIAlertView *alert=[[UIAlertView alloc]initWithTitle:@"" message:@"Your role has been successfully changed to worker." delegate:nil cancelButtonTitle:@"OK" otherButtonTitles:nil];
            [alert show];
 
        }
        else
        {
            [indicator startAnimating];
            [self performSelector:@selector(changeUserType) withObject:nil afterDelay:5.0];
        }
    }
    else if ([userstatus isEqualToString:@"worker"])
    {
        
        
        [defaults setObject:@"customer" forKey:@"user_status"];
        [defaults synchronize];
        [self switched];
        UIAlertView *alert=[[UIAlertView alloc]initWithTitle:@"" message:@"Your role has been successfully changed to customer." delegate:nil cancelButtonTitle:@"OK" otherButtonTitles:nil];
        [alert show];
    }
    
    if (switchview.on)
    {
        [switchview setOn:NO animated:YES];
    }
    else
    {
    [switchview setOn:YES animated:YES];
    }
     
}
-(void)switched
{
    UIViewController *root = [[UIStoryboard storyboardWithName:@"Main" bundle:NULL] instantiateViewControllerWithIdentifier:@"rootController"];
    UIWindow *wnd = [[[UIApplication sharedApplication] delegate] window];
    wnd.rootViewController = root;
    [wnd makeKeyAndVisible];
}

-(void)changeUserType
{
   NSString *token=[NSString stringWithFormat:@"%@",[defaults objectForKey:@"user_token"]];
    NSString *url=[NSString stringWithFormat:@"%@workersApproval/%@?token=%@",serviceLink,[defaults objectForKey:@"user_id"],token];
    
    if ([Helper isConnectedToInternet])
    {
        id  json = [WebService GetwithoutDict:url];
        if ([[json objectForKey:@"message"]isEqualToString:@"Success"])
        {
            [indicator stopAnimating];
            
            UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"" message:@"You have been requested for switch as worker." delegate:nil cancelButtonTitle:@"OK" otherButtonTitles:nil];
            [alert show];
            

        }
        else
        {
            UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"" message:@"Some error occured to switch as worker." delegate:nil cancelButtonTitle:@"OK" otherButtonTitles:nil];
            [alert show];
        }
        
    }
    
    else
    {
        
        
        UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"No Internet" message:@"Please check your internet connection." delegate:nil cancelButtonTitle:@"OK" otherButtonTitles:nil];
        [alert show];
        
    }

}

@end
