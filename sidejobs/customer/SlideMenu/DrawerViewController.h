//
//  ViewController.h
//  SideJobs
//
//  Created by Debut05 on 8/13/15.
//  Copyright (c) 2015 Narinder. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "RESideMenu.h"

@interface DrawerViewController :UIViewController <UITableViewDataSource, UITableViewDelegate, RESideMenuDelegate>{

    IBOutlet UIImageView *imgView_profile;
    IBOutlet UILabel *lbl_userName;
}
@end

