//
//  ContactViewController.h
//  sidejobs
//
//  Created by DebutMac4 on 16/03/16.
//  Copyright © 2016 DebutMac4. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface ContactViewController : UIViewController<UITextViewDelegate, UITextFieldDelegate>
{
    
    IBOutlet UITextView *textview_Message;
    IBOutlet UITextField *TextField_Email;
    IBOutlet UITextField *txtField_Name;
}
@property (strong, nonatomic) IBOutlet UITextView *textView_CU;
- (IBAction)buttonSubmit_Action:(id)sender;

@end
