//
//  JobDetailTableViewCell.m
//  sidejobs
//
//  Created by DebutMac4 on 10/09/15.
//  Copyright (c) 2015 DebutMac4. All rights reserved.
//

#import "JobDetailTableViewCell.h"

@implementation JobDetailTableViewCell

- (void)awakeFromNib {
    // Initialization code
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];
    _img_applicant.layer.masksToBounds=YES;
    _img_applicant.layer.cornerRadius=_img_applicant.bounds.size.width/2;
    _view_background.layer.masksToBounds=YES;
    _view_background.layer.borderColor=[[UIColor grayColor]CGColor];
    _view_background.layer.borderWidth=0.3f;


}

@end
