//
//  HowSideJobsWorkVCViewController.m
//  sidejobs
//
//  Created by DebutMac4 on 16/03/16.
//  Copyright © 2016 DebutMac4. All rights reserved.
//

#import "HowSideJobsWorkVCViewController.h"
#import <Helper.h>

@interface HowSideJobsWorkVCViewController ()
{
    NSString *strint_TC;
}

@end

@implementation HowSideJobsWorkVCViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    
    self.navigationItem.title=@"Help";
    
    self.navigationItem.hidesBackButton = true;
    UIButton *button=[UIButton buttonWithType:UIButtonTypeCustom];
    [button setFrame:CGRectMake(0, 0, 23, 21)];
    [button addTarget:self action:@selector(backBarButtonItem:) forControlEvents:UIControlEventTouchUpInside];
    [button setImage:[UIImage imageNamed:@"arrow.png"] forState:UIControlStateNormal];
    self.navigationItem.leftBarButtonItem=[[UIBarButtonItem alloc]initWithCustomView:button];
    
    
    UIButton *Right_button=[UIButton buttonWithType:UIButtonTypeCustom];
    [Right_button setFrame:CGRectMake(0, 0, 23, 21)];
    [Right_button setImage:[UIImage imageNamed:@"icon.png"] forState:UIControlStateNormal];
    Right_button.userInteractionEnabled = false;
    self.navigationItem.rightBarButtonItem=[[UIBarButtonItem alloc]initWithCustomView:Right_button];
    
    [SVProgressHUD show];
    [self performSelector:@selector(Data) withObject:nil afterDelay:0.3f];

}

-(void)backBarButtonItem:(id)sender
{
    [self.navigationController popViewControllerAnimated:YES];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

-(void)Data
{
    id json;
    NSString *url=[NSString stringWithFormat:@"%@pagesContent",serviceLink];
    
    if ([Helper isConnectedToInternet])
    {
        json = [WebService GetwithoutDict:url];
        if ([[json objectForKey:@"message" ]isEqualToString:@"Success"])
        {
            NSArray *arr = [json objectForKey:@"response"];
            [SVProgressHUD dismiss];
            
            for (int i=0; i < arr.count; i++)
            {
                NSString *ID = [NSString stringWithFormat:@"%@",[[arr objectAtIndex:i]objectForKey:@"id"] ];
                if ([ID isEqualToString:@"5"])
                {
                    strint_TC = [[arr objectAtIndex:i] objectForKey:@"content"];
                    NSAttributedString *attributedString = [[NSAttributedString alloc] initWithData:[strint_TC dataUsingEncoding:NSUnicodeStringEncoding] options:@{ NSDocumentTypeDocumentAttribute: NSHTMLTextDocumentType } documentAttributes:nil error:nil];
                    _txtView.attributedText = attributedString;
                }
            }
            
        }
        
        
    }
    else
    {
        UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"No Internet" message:@"Please check your internet connection." delegate:nil cancelButtonTitle:@"OK" otherButtonTitles:nil];
        [alert show];
        [SVProgressHUD dismiss];
        
    }
    
    
    [SVProgressHUD dismiss];
}

@end
