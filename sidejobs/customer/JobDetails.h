//
//  JobDetails.h
//  SideJobs
//
//  Created by Debut05 on 9/1/15.
//  Copyright (c) 2015 DebutMac4. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "ASStarRatingView.h"

@interface JobDetails : UIViewController<UITableViewDataSource,UITableViewDelegate>
{
    IBOutlet UILabel *jobtypeLbl;
    IBOutlet UILabel *cattypeLbl;
    IBOutlet UILabel *dateLbl;
    IBOutlet UITextView *desTV;
    IBOutlet UIButton *editBtn;
    
    IBOutlet UIButton *deletBtn;
    IBOutlet UIImageView *image_OfCategory;
    IBOutlet UISwitch *toggle_object;
    IBOutlet UIButton *dolrBtn;
    IBOutlet UITableView *tableView_Applicant;
    IBOutlet UIButton *btn_ApplicantObj;
    IBOutlet UIButton *btn_InvitiesObj;
    IBOutlet UILabel *lbl_NoApplicant;
    
    IBOutlet UIView *view_CommentPopUp;
    IBOutlet UITextView *textView_Comment;
    IBOutlet UIView *viewPopUpInside;
    IBOutlet UITableView *tableViewMyInvitation;
}

@property (strong, nonatomic) NSMutableDictionary *jobDict;

- (IBAction)tongle_Action:(id)sender;
- (IBAction)btn_EditJob:(id)sender;
- (IBAction)btn_deleteJob:(id)sender;
- (IBAction)btn_Applicants:(id)sender;
- (IBAction)btn_Invities:(id)sender;
- (IBAction)btn_HideComment:(id)sender;

@end
