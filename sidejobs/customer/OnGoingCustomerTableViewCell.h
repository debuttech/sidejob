//
//  OnGoingCustomerTableViewCell.h
//  sidejobs
//
//  Created by DebutMac4 on 16/09/15.
//  Copyright (c) 2015 DebutMac4. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface OnGoingCustomerTableViewCell : UITableViewCell

@property (strong, nonatomic) IBOutlet UILabel *lbl_category;
@property (strong, nonatomic) IBOutlet UILabel *lbl_startdate;
@property (strong, nonatomic) IBOutlet UIImageView *img_category;
@property (strong, nonatomic) IBOutlet UILabel *lbl_nameWorker;
@property (strong, nonatomic) IBOutlet UILabel *lbl_title;
@property (strong, nonatomic) IBOutlet UIImageView *img_worker;
@property (strong, nonatomic) IBOutlet UIButton *btn_amount;
@property (strong, nonatomic) IBOutlet UIView *viewBackground;
@property (strong, nonatomic) IBOutlet UIButton *btn_GiveReview;
@property (strong, nonatomic) IBOutlet UIButton *btn_ConfirmJob;
@property (strong, nonatomic) IBOutlet UIButton *btn_Rating;
@property (strong, nonatomic) IBOutlet UIButton *button_Refund;


@end
