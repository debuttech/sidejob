//
//  JDIndividualCustomer.m
//  sidejobs
//
//  Created by DebutMac4 on 29/10/15.
//  Copyright © 2015 DebutMac4. All rights reserved.
//

#import "JDIndividualCustomer.h"

@interface JDIndividualCustomer ()
{
    NSUserDefaults *defaults;
}

@end

@implementation JDIndividualCustomer

- (void)viewDidLoad {
    [super viewDidLoad];
    [self createUserExperinece];
    
}
-(void)createUserExperinece
{
    defaults=[NSUserDefaults standardUserDefaults];
    [defaults setObject:@"yes" forKey:@"pushjd"];
    [defaults removeObjectForKey:@"pushjd1"];
    [defaults synchronize];
    
    view_Background.layer.masksToBounds=YES;
    view_Background.layer.cornerRadius=10;
    
    img_user.layer.masksToBounds=YES;
    img_user.layer.cornerRadius=img_user.frame.size.width/2;
    NSLog(@"jd..%@",self.dict);
    self.navigationItem.hidesBackButton=YES;
    
    NSDictionary *size = [NSDictionary dictionaryWithObjectsAndKeys:[UIFont fontWithName:@"HelveticaNeue-Light" size:18.0],NSFontAttributeName, [UIColor whiteColor],NSForegroundColorAttributeName, nil];
    self.navigationController.navigationBar.titleTextAttributes = size;
    self.navigationItem.title=@"Completed Job Details";
    
    UIButton *button=[UIButton buttonWithType:UIButtonTypeCustom];
    [button setFrame:CGRectMake(0, 0, 23, 21)];
    [button setImage:[UIImage imageNamed:@"arrow"] forState:UIControlStateNormal];
    [button addTarget:self action:@selector(leftbutton:) forControlEvents:UIControlEventTouchUpInside];
    self.navigationItem.leftBarButtonItem=[[UIBarButtonItem alloc]initWithCustomView:button];
    [self showJd];
}
//-(void)getvalue:(NSString *)status
//{
//    _strStauts=status;
//}
-(void)leftbutton:(id)sender
{
    [self.navigationController popViewControllerAnimated:YES];
}
-(void)showJd
{
    lbl_user.text=[[[[self.dict objectForKey:@"job_awarded"]objectAtIndex:0]objectForKey:@"user"] objectForKey:@"name"];
    
    NSString *img=[[[[self.dict objectForKey:@"job_awarded"]objectAtIndex:0]objectForKey:@"user"]objectForKey:@"profile_image"];
    if (img==nil||[img isEqualToString:@""])
    {
        img_user.image=[UIImage imageNamed:@"defuser.png"];
    }
    else
    {
    NSString *pimg=[NSString stringWithFormat:@"%@%@",serviceLink,img];
    img_user.image=[UIImage imageWithData:[NSData dataWithContentsOfURL:[NSURL URLWithString:pimg]]];
    }
    
    
    NSString *rating=[[[self.dict objectForKey:@"job_awarded"]objectAtIndex:0]objectForKey:@"rating"];
    staticStarRatingView.canEdit = YES;
    staticStarRatingView.maxRating = 5;
    staticStarRatingView.minAllowedRating = 1;
    staticStarRatingView.maxAllowedRating = 5;
    staticStarRatingView.rating =[rating floatValue];
    
    NSString *d=[[[[self.dict objectForKey:@"job_awarded"]objectAtIndex:0]objectForKey:@"user"] objectForKey:@"created_at"];
    NSArray *ad=[d componentsSeparatedByString:@" "];
    NSString *memberSincedte=[ad objectAtIndex:0];
    lbl_memberSince.text=memberSincedte;
    
    NSString *amount=[[[self.dict objectForKey:@"job_awarded"]objectAtIndex:0]objectForKey:@"proposed_amount"];
    lbl_amount.text=[NSString stringWithFormat:@"$%@",amount];
    
    //job Details
    lbl_Title.text=[self.dict objectForKey:@"title"];
    
    NSString *catId=[NSString stringWithFormat:@"%@",[self.dict objectForKey:@"category_id"] ];
    if ([catId isEqualToString:@"1"]) {
        lbl_Category.text = @"Home" ;
        img_category.image=[UIImage imageNamed:@"cat_home"];
        
    }else if([catId isEqualToString:@"2"])
    {
       lbl_Category.text = @"Auto" ;
       img_category .image=[UIImage imageNamed:@"car_icon"];
        
    }else if([catId isEqualToString:@"3"])
    {
       lbl_Category.text =@"Electronic" ;
        img_category.image=[UIImage imageNamed:@"cat_elc"];
    }
    
    
    //complete job time
    NSString *completeTime=[[[self.dict objectForKey:@"job_applications"]objectAtIndex:0] objectForKey:@"updated_at"];
    
    NSArray *Arrtime=[completeTime componentsSeparatedByString:@" "];
    NSString* seprateCompleteTime=[Arrtime objectAtIndex:0];
   lbl_Completed.text=seprateCompleteTime;
    
    //start job time
    NSString *startTime=[[[self.dict objectForKey:@"job_awarded"]objectAtIndex:0] objectForKey:@"created_at"];
    NSArray *ArrStart=[startTime componentsSeparatedByString:@" "];
    NSString* seprateStarttime=[ArrStart objectAtIndex:0];
    lbl_startTime.text=seprateStarttime;
    

}
- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
