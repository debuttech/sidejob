//
//  CompleteJobCustomerTableViewCell.h
//  sidejobs
//
//  Created by DebutMac4 on 16/09/15.
//  Copyright (c) 2015 DebutMac4. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface CompleteJobCustomerTableViewCell : UITableViewCell
@property (strong, nonatomic) IBOutlet UILabel *lbl_Title;
@property (strong, nonatomic) IBOutlet UILabel *lbl_UserNmae;
@property (strong, nonatomic) IBOutlet UILabel *lbl_categoryName;
@property (strong, nonatomic) IBOutlet UILabel *lbl_StartTime;
@property (strong, nonatomic) IBOutlet UILabel *lbl_CompleteTime;
@property (strong, nonatomic) IBOutlet UIImageView *img_User;
@property (strong, nonatomic) IBOutlet UIImageView *img_Category;
@property (strong, nonatomic) IBOutlet UILabel *lbl_Amount;

@property (strong, nonatomic) IBOutlet UIButton *btn_GiveReview;
@property (strong, nonatomic) IBOutlet UIButton *btn_ConfirmJob;
@property (strong, nonatomic) IBOutlet UIView *view_Background;
@property (strong, nonatomic) IBOutlet UILabel *label_status;


@end
