//
//  ReportAnIssueViewController.m
//  sidejobs
//
//  Created by DebutMac4 on 16/03/16.
//  Copyright © 2016 DebutMac4. All rights reserved.
//

#import "ReportAnIssueViewController.h"

@interface ReportAnIssueViewController ()
{
    NSUserDefaults *defaults;
}

@end

@implementation ReportAnIssueViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    defaults = [NSUserDefaults standardUserDefaults];
    
    self.navigationItem.title=@"Help";    
    self.navigationItem.hidesBackButton = true;
    UIButton *button=[UIButton buttonWithType:UIButtonTypeCustom];
    [button setFrame:CGRectMake(0, 0, 23, 21)];
    [button addTarget:self action:@selector(backBarButtonItem:) forControlEvents:UIControlEventTouchUpInside];
    [button setImage:[UIImage imageNamed:@"arrow.png"] forState:UIControlStateNormal];
    self.navigationItem.leftBarButtonItem=[[UIBarButtonItem alloc]initWithCustomView:button];
    
    UIButton *Right_button=[UIButton buttonWithType:UIButtonTypeCustom];
    [Right_button setFrame:CGRectMake(0, 0, 23, 21)];
    [Right_button setImage:[UIImage imageNamed:@"icon.png"] forState:UIControlStateNormal];
      Right_button.userInteractionEnabled = false;
    self.navigationItem.rightBarButtonItem=[[UIBarButtonItem alloc]initWithCustomView:Right_button];
    [self shadow];
    
    
    UITapGestureRecognizer *tap=[[UITapGestureRecognizer alloc]initWithTarget:self action:@selector(tapGesture)];
    tap.numberOfTapsRequired=1;
    [self.view addGestureRecognizer:tap];
}



-(void)tapGesture
{
    [self.textview_sendReport resignFirstResponder];
}


-(void)shadow
{
    self.imageView.layer.borderWidth = 1;
   // self.imageView.layer.borderColor = [UIColor colorWithRed:222/256 green:222/256 blue:222/256 alpha:1.0].CGColor;
    self.imageView.layer.borderColor = [self colorWithHexString:@"dcdcdc"].CGColor;
    self.imageView.layer.cornerRadius = 2;
    self.imageView.layer.shadowOffset = CGSizeMake(0, 1);
    self.imageView.layer.shadowOpacity = 0.1;
}


-(UIColor*)colorWithHexString:(NSString*)hex
{
    NSString *cString = [[hex stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceAndNewlineCharacterSet]] uppercaseString];
    
    // String should be 6 or 8 characters
    if ([cString length] < 6) return [UIColor grayColor];
    // strip 0X if it appears
    if ([cString hasPrefix:@"0X"]) cString = [cString substringFromIndex:2];
    if ([cString length] != 6) return  [UIColor grayColor];
    
    // Separate into r, g, b substrings
    NSRange range;
    range.location = 0;
    range.length = 2;
    NSString *rString = [cString substringWithRange:range];
    range.location = 2;
    NSString *gString = [cString substringWithRange:range];
    range.location = 4;
    NSString *bString = [cString substringWithRange:range];
    
    // Scan values
    unsigned int r, g, b;
    [[NSScanner scannerWithString:rString] scanHexInt:&r];
    [[NSScanner scannerWithString:gString] scanHexInt:&g];
    [[NSScanner scannerWithString:bString] scanHexInt:&b];
    
    return [UIColor colorWithRed:((float) r / 255.0f)
                           green:((float) g / 255.0f)
                            blue:((float) b / 255.0f)
                           alpha:1.0f];
}


-(void)mail
{
    
    NSMutableDictionary *jsonDict=[[NSMutableDictionary alloc]init];
    NSString *token=[NSString stringWithFormat:@"%@",[defaults objectForKey:@"user_token"]];
    
   // [jsonDict setValue:[defaults objectForKey:@"p_email"] forKey:@"from"];
    [jsonDict setValue:@"luckylaksh7@gmail.com" forKey:@"from"];

    [jsonDict setObject:_textview_sendReport.text forKey:@"content"];
    
    NSString *url=[NSString stringWithFormat:@"%@emailAdmin?token=%@",serviceLink,token];
    id  json = [WebService signup:jsonDict:url];
    if ([[json objectForKey:@"message"]isEqualToString:@"Success"])
    {
        [SVProgressHUD dismiss];
        [self.navigationController popViewControllerAnimated:YES];

        UIAlertView *alert=[[UIAlertView alloc]initWithTitle:@"" message:@"Email sent!" delegate:nil cancelButtonTitle:@"OK" otherButtonTitles: nil];
        [alert show];
        
    }
    else if ([[json objectForKey:@"error"]isEqualToString:@"token_expired"])
    {
          [SVProgressHUD dismiss];
        
        UIAlertView *alert=[[UIAlertView alloc]initWithTitle:@"" message:@"Session has been expired. Please login again..." delegate:nil cancelButtonTitle:@"OK" otherButtonTitles: nil];
        [alert show];
        AppDelegate *delegate;
        delegate=(AppDelegate*)[[UIApplication sharedApplication]delegate];
        [defaults setBool:NO forKey:@"logined"];
        [defaults synchronize];
        [[NSUserDefaults standardUserDefaults]synchronize];
        [delegate loginStatus];
    }
    
    
  else
    {
        UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"No Internet" message:@"Please check your internet connection." delegate:nil cancelButtonTitle:@"OK" otherButtonTitles:nil];
        [alert show];
        [SVProgressHUD dismiss];
        
    }
    [SVProgressHUD dismiss];


    
}


-(void)backBarButtonItem:(id)sender
{
    [self.navigationController popViewControllerAnimated:YES];
}
- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}



- (IBAction)button_Submit:(id)sender
{
    [SVProgressHUD show];
    [self performSelector:@selector(mail) withObject:nil afterDelay:0.3f];
}
@end
