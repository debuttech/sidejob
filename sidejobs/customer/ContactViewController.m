//
//  ContactViewController.m
//  sidejobs
//
//  Created by DebutMac4 on 16/03/16.
//  Copyright © 2016 DebutMac4. All rights reserved.
//
#define ACCEPTABLE_CHARACTERS @"ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz "

#import "ContactViewController.h"
#import <Helper.h>

@interface ContactViewController ()
{
    NSString *strint_TC;
}

@end

@implementation ContactViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    
    self.navigationItem.title=@"Help";
    
    self.navigationItem.hidesBackButton = true;
    UIButton *button=[UIButton buttonWithType:UIButtonTypeCustom];
    [button setFrame:CGRectMake(0, 0, 23, 21)];
    [button addTarget:self action:@selector(backBarButtonItem:) forControlEvents:UIControlEventTouchUpInside];
    [button setImage:[UIImage imageNamed:@"arrow.png"] forState:UIControlStateNormal];
    self.navigationItem.leftBarButtonItem=[[UIBarButtonItem alloc]initWithCustomView:button];
    
    UIButton *Right_button=[UIButton buttonWithType:UIButtonTypeCustom];
    [Right_button setFrame:CGRectMake(0, 0, 23, 21)];
    [Right_button setImage:[UIImage imageNamed:@"icon.png"] forState:UIControlStateNormal];
    Right_button.userInteractionEnabled = false;
    self.navigationItem.rightBarButtonItem=[[UIBarButtonItem alloc]initWithCustomView:Right_button];
    
//    [SVProgressHUD show];
//    [self performSelector:@selector(contactUs) withObject:nil afterDelay:0.3f];
   
    
    textview_Message.text = @"Type Message Here";
    textview_Message.textColor = [UIColor lightGrayColor];
    textview_Message.delegate = self;
    
    UITapGestureRecognizer * tapGesture = [[UITapGestureRecognizer alloc]
                                           initWithTarget:self
                                           action:@selector(hideKeyBoard)];
    tapGesture.numberOfTapsRequired = 1;
    [self.view addGestureRecognizer:tapGesture];
    
}

-(void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:YES];
    textview_Message.layer.borderColor = [[UIColor lightGrayColor] CGColor];
}

-(void)hideKeyBoard
{
    [self.view endEditing:YES];
}


- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}


#pragma mark Submit Button Tapped Methods 

- (IBAction)buttonSubmit_Action:(id)sender
{
   NSString * str_Name=txtField_Name.text;
   NSString *  str_Email=TextField_Email.text;
   NSString *  str_Message=textview_Message.text;
//    str_password=_txtField_Password.text;
    
    if ([[str_Name stringByReplacingOccurrencesOfString:@" " withString:@""] length]<3 ||str_Name.length>50)
    {
        UIAlertView *alert=[[UIAlertView alloc]initWithTitle:@"" message:@"Please enter your name in between 3 to 50 characters." delegate:self cancelButtonTitle:@"OK" otherButtonTitles: nil];
        [alert show];
    }
    else if ([[str_Email stringByReplacingOccurrencesOfString:@" " withString:@""] length]<1 || ![self isValidEmail:str_Email])
    {
        UIAlertView *alert=[[UIAlertView alloc]initWithTitle:@"" message:@"Please enter a valid email." delegate:self cancelButtonTitle:@"OK" otherButtonTitles: nil];
        [alert show];
        
        
        
    }
    else if ([[str_Message stringByReplacingOccurrencesOfString:@" " withString:@""] length]<1 ||str_Name.length>200 )
    {
        UIAlertView *alert=[[UIAlertView alloc]initWithTitle:@"" message:@"Please enter a valid email." delegate:self cancelButtonTitle:@"OK" otherButtonTitles: nil];
        [alert show];
    }
    else
    {
        [SVProgressHUD showWithStatus:@"Loading..."];
        [self performSelector:@selector(addContactInfo) withObject:nil afterDelay:0.3f];

    }

}

-(void)addContactInfo
{
    NSUserDefaults *defaults;
    defaults=[NSUserDefaults standardUserDefaults];

    if ([Helper isConnectedToInternet])
    {
        NSMutableDictionary *jsonDict=[[NSMutableDictionary alloc]init];
        [jsonDict setValue:txtField_Name.text forKey:@"name"];
        [jsonDict setObject:TextField_Email.text forKey:@"email"];
        [jsonDict setObject:textview_Message.text forKey:@"message"];
        NSString *token =  [defaults objectForKey:@"user_token"] ;
        NSString *url=[NSString stringWithFormat:@"%@addContactUs?token=%@", serviceLink,token];
        
        id  json = [WebService signup:jsonDict:url];
        
        if ([[json objectForKey:@"message"] isEqualToString:@"Success"])
        {
            [SVProgressHUD dismiss];
            NSLog(@"Account Detail:%@",json);
            UIAlertView *alert=[[UIAlertView alloc]initWithTitle:@"" message:@"Email send successfully." delegate:self cancelButtonTitle:@"OK" otherButtonTitles: nil];
            alert.tag = 100;
            [alert show];

//            [self.navigationController popViewControllerAnimated:YES];
        }
        else
        {
            [SVProgressHUD dismiss];
            UIAlertView *alert=[[UIAlertView alloc]initWithTitle:@"" message:@"Some error is occured to sending email" delegate:nil cancelButtonTitle:@"OK" otherButtonTitles: nil];
            [alert show];
        }
        
    }
    
    else
    {
        UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"No Internet" message:@"Please check your internet connection." delegate:nil cancelButtonTitle:@"OK" otherButtonTitles:nil];
        [alert show];
    }
    

}

- (void)alertView:(UIAlertView *)alertView clickedButtonAtIndex:(NSInteger)buttonIndex
{
    if (alertView.tag == 100)
    {
        txtField_Name.text = @"";
        TextField_Email.text = @"";
        textview_Message.text = @"";
        [self.navigationController popViewControllerAnimated:YES];
    }
}

#pragma mark UITextField Delegate Methods 

- (BOOL)textFieldShouldReturn:(UITextField *)textField
{
    if (textField == txtField_Name)
    {
        [TextField_Email becomeFirstResponder];
    }
    else if (textField == TextField_Email)
    {
        [TextField_Email resignFirstResponder];
    }
    return YES;
}

-(BOOL)textField:(UITextField *)textField shouldChangeCharactersInRange:(NSRange)range replacementString:(NSString *)string {
    
    if (textField==txtField_Name )
    {
        if (txtField_Name.text.length<1)
        {
            if ([string isEqualToString:@" "]) {
                return NO;
            }
        }
        if (txtField_Name.text.length>50)
        {
            if ([string isEqualToString:@""]) {
                return YES;
            }
            else
            {
                return NO;
            }
        }
        NSCharacterSet *cs = [[NSCharacterSet characterSetWithCharactersInString:ACCEPTABLE_CHARACTERS] invertedSet];
        NSString *filtered = [[string componentsSeparatedByCharactersInSet:cs] componentsJoinedByString:@""];
        return [string isEqualToString:filtered];
    }
    if (textField==TextField_Email)
    {
        if (TextField_Email.text.length < 1)
        {
            if ([string isEqualToString:@" "])
            {
                return NO;
            }
        }
        
        NSString *textFieldText = [TextField_Email.text stringByReplacingCharactersInRange:range withString:string];
        if (textFieldText.length==0||[textFieldText length]<=50)
        {
            return YES;
        }
        else
    return NO;
    }
    return YES;
}

#pragma mark UITextView Delegate Methods 
/*- (void)textViewDidBeginEditing:(UITextView *)textView
{
    if (textView == textview_Message)
    {
        if ([textview_Message.text isEqualToString:@"Type Message Here"])
        {
            textview_Message.text = @"";
        }
    }
}*/


- (BOOL) textViewShouldBeginEditing:(UITextView *)textView
{
    if (textView == textview_Message)
    {
        if ([textview_Message.text isEqualToString:@"Type Message Here"])
        {
            textview_Message.text = @"";
            textview_Message.textColor = [UIColor blackColor];
            return YES;
        }
    }
    return YES;
}
- (BOOL)textViewShouldEndEditing:(UITextView *)textView
{
    if ([textview_Message.text isEqualToString:@""])
    {
        textview_Message.text = @"Type Message Here";
        textview_Message.textColor = [UIColor lightGrayColor];
        return YES;
    }
    return YES;
}
-(void) textViewDidChange:(UITextView *)textView
{
    if(textview_Message.text.length == 0)
    {
        textview_Message.textColor = [UIColor lightGrayColor];
        textview_Message.text = @"Type Message Here";
        [textview_Message resignFirstResponder];
    }
}
#pragma mark Other Friendly Methods 

-(void)backBarButtonItem:(id)sender
{
    [self.navigationController popViewControllerAnimated:YES];
}


-(BOOL)isValidEmail:(NSString *)checkString
{
    BOOL stricterFilter = NO; // Discussion http://blog.logichigh.com/2010/09/02/validating-an-e-mail-address/
    NSString *stricterFilterString = @"[A-Z0-9a-z\\._%+-]+@([A-Za-z0-9-]+\\.)+[A-Za-z]{2,4}";
    NSString *laxString = @".+@([A-Za-z0-9-]+\\.)+[A-Za-z]{2}[A-Za-z]*";
    NSString *emailRegex = stricterFilter ? stricterFilterString : laxString;
    NSPredicate *emailTest = [NSPredicate predicateWithFormat:@"SELF MATCHES %@", emailRegex];
    return [emailTest evaluateWithObject:checkString];
}

-(void)contactUs
{
    id json;
    NSString *url=[NSString stringWithFormat:@"%@pagesContent",serviceLink];
    
    if ([Helper isConnectedToInternet])
    {
        json = [WebService GetwithoutDict:url];
        if ([[json objectForKey:@"message" ]isEqualToString:@"Success"])
        {
            NSArray *arr = [json objectForKey:@"response"];
            [SVProgressHUD dismiss];
            
            for (int i=0; i < arr.count; i++)
            {
                NSString *ID = [NSString stringWithFormat:@"%@",[[arr objectAtIndex:i]objectForKey:@"id"] ];
                if ([ID isEqualToString:@"3"])
                {
                    strint_TC = [[arr objectAtIndex:i] objectForKey:@"content"];
                    NSAttributedString *attributedString = [[NSAttributedString alloc] initWithData:[strint_TC dataUsingEncoding:NSUnicodeStringEncoding] options:@{ NSDocumentTypeDocumentAttribute: NSHTMLTextDocumentType } documentAttributes:nil error:nil];
                    _textView_CU.attributedText = attributedString;
                }
            }
            
        }
        
        
    }
    else
    {
        UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"No Internet" message:@"Please check your internet connection." delegate:nil cancelButtonTitle:@"OK" otherButtonTitles:nil];
        [alert show];
        [SVProgressHUD dismiss];
        
    }
    
    
    [SVProgressHUD dismiss];
}

@end
