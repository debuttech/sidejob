
//
//  JobDetails.m
//  SideJobs
//
//  Created by Debut05 on 9/1/15.
//  Copyright (c) 2015 DebutMac4. All rights reserved.
//

#import "JobDetails.h"
#import "JobDetailTableViewCell.h"
#import "TableViewCellMyInVitation.h"
#import "ASStarRatingView.h"
#import "Helper.h"

@implementation JobDetails{
    NSUserDefaults *defaults;
    NSString *status,*sendStatus;
    NSMutableArray *arrApplicant;
    NSMutableArray *arr_invitation,*arr_AppliedWorker;
   // UIAlertView *alert1;
    
    NSMutableArray *cardDetails;
    NSInteger jobIndex;
    NSInteger jobIndex_Reject;
    NSString *status_WorkerCard;
    NSString *merchant_ID;
 }

-(void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:YES];
    [self performSelectorInBackground:@selector(fetchJobDetail) withObject:nil];

}

- (void)viewDidLoad
{
    [super viewDidLoad];
    [self createUserExperience];
}
-(void)createUserExperience
{
    defaults=[NSUserDefaults standardUserDefaults];
    [defaults removeObjectForKey:@"pushjd"];
    [defaults removeObjectForKey:@"pushjd1"];
    [defaults synchronize];
    
    //get card details to check customer have card or not
    [self performSelectorInBackground:@selector(getCarddetails) withObject:nil];
    
    btn_ApplicantObj.selected=YES;
    
    self.navigationItem.hidesBackButton=YES;
    self.navigationItem.title=@"My SideJobs";
    
    UIBezierPath *shadowPath = [UIBezierPath bezierPathWithRect:view_CommentPopUp.bounds];
    view_CommentPopUp.layer.masksToBounds = NO;
    view_CommentPopUp.layer.shadowColor = [UIColor grayColor].CGColor;
    view_CommentPopUp.layer.shadowOffset = CGSizeMake(0.0f, 0.0f);
    view_CommentPopUp.layer.shadowOpacity = 0.9f;
    view_CommentPopUp.layer.shadowPath = shadowPath.CGPath;
    view_CommentPopUp.backgroundColor=[UIColor clearColor];
    view_CommentPopUp.alpha=1.0;
    
    viewPopUpInside.layer.masksToBounds=YES;
    viewPopUpInside.layer.cornerRadius=7.0f;
    viewPopUpInside.layer.borderWidth=3.0f;
    viewPopUpInside.layer.borderColor=[[UIColor grayColor]CGColor];
    
    UIButton *button=[UIButton buttonWithType:UIButtonTypeCustom];
    [button setFrame:CGRectMake(0, 0, 23, 21)];
    [button setImage:[UIImage imageNamed:@"arrow.png"] forState:UIControlStateNormal];
    [button addTarget:self action:@selector(leftButton:) forControlEvents:UIControlEventTouchUpInside];
    self.navigationItem.leftBarButtonItem=[[UIBarButtonItem alloc]initWithCustomView:button];
    
    NSDictionary *size = [NSDictionary dictionaryWithObjectsAndKeys:[UIFont fontWithName:@"HelveticaNeue-Light" size:20.0],NSFontAttributeName, [UIColor whiteColor],NSForegroundColorAttributeName, nil];
    self.navigationController.navigationBar.titleTextAttributes = size;
    self.navigationItem.title=@"My SideJobs";
    
    dolrBtn.layer.cornerRadius= dolrBtn.bounds.size.width/2 ;
    dolrBtn.layer.masksToBounds=true;
    
    
    [SVProgressHUD showWithStatus:@"Loading..."];
    [self performSelector:@selector(setJobDetail) withObject:nil afterDelay:0.1f];
//    NSLog(@"setjob..%@",self.jobDict);
}
-(void)leftButton:(UIButton*)sender
{
 [self.navigationController popViewControllerAnimated:YES];
}
-(void)viewDidAppear:(BOOL)animated
{
    [super viewDidAppear:YES];
}

#pragma  mark
#pragma mark-fetch jobs details
-(void)fetchJobDetail
{
    NSString *token=[NSString stringWithFormat:@"%@",[defaults objectForKey:@"user_token"]];
    NSString *url=[NSString stringWithFormat:@"%@job-detail/?token=%@",serviceLink,token];
    NSMutableDictionary *jsonDict=[[NSMutableDictionary alloc]init];
    [jsonDict setValue:[self.jobDict valueForKey:@"id"] forKey:@"id"];
    
    id json = [WebService signup:jsonDict:url];
    if ([[json objectForKey:@"message"]isEqualToString:@"Success"])
    {
        self.jobDict = [json valueForKey:@"response"];
        arrApplicant=[[NSMutableArray alloc]init];
        arrApplicant=[self.jobDict valueForKey:@"job_applications"];
        arr_AppliedWorker =[[NSMutableArray alloc]init];
        for (int i=0; i<[arrApplicant count]; i++)
        {
            NSString *str=[NSString stringWithFormat:@"%@",[[arrApplicant objectAtIndex:i]objectForKey:@"invited"]];
            if ([str isEqualToString:@"0"])
            {
              if ([[[arrApplicant objectAtIndex:i]objectForKey:@"applicant_job_status"]isEqualToString:@"applied"])
              {
                [arr_AppliedWorker addObject:[arrApplicant objectAtIndex:i]];
               }
                
            }
            else
            {
                
            }
        }
        
        //Invite_Applied add in applicant arrayi
        NSMutableArray *arrInivited_Applied=[[NSMutableArray alloc]init];
        for (int i=0; i<[arrApplicant count]; i++)
        {
            NSString *str=[NSString stringWithFormat:@"%@",[[arrApplicant objectAtIndex:i]objectForKey:@"invited"]];
            if ([str isEqualToString:@"1"])
            {
                NSString *strInviteAppliedStatus=[NSString stringWithFormat:@"%@",[[arrApplicant objectAtIndex:i]objectForKey:@"invited_applied"]];
                if ([strInviteAppliedStatus isEqualToString:@"1"])
                {
                    [arrInivited_Applied addObject:[arrApplicant objectAtIndex:i]];
                }
                
            }
            else
            {
                
            }
        }

        [arr_AppliedWorker addObjectsFromArray:arrInivited_Applied];
        
        if (arr_AppliedWorker.count)
        {
            tableView_Applicant.dataSource=self;
            tableView_Applicant.delegate=self;
            [tableView_Applicant reloadData];
        }
        else
        {

        }
        status= [self.jobDict objectForKey:@"published"];
        if ([status isEqualToString:@"0"])
        {
            [toggle_object setOn:NO];
        }
        else
            [toggle_object setOn:YES];
    }
    else if ([[json objectForKey:@"message"]isEqualToString:@"token_expired"])
    {
        [self tokenExpired];
    }
    if (arr_AppliedWorker.count==0)
    {
      lbl_NoApplicant.hidden=NO;
    }else
    {
          lbl_NoApplicant.hidden=YES;
     
    }
    
    [SVProgressHUD dismiss];
}
#pragma mark
#pragma mark-show jobDetails
-(void)setJobDetail{
    jobtypeLbl.text=[self.jobDict valueForKey:@"title"];
    NSString *catId=[NSString stringWithFormat:@"%@",[self.jobDict valueForKey:@"category_id"] ];
    if ([catId isEqualToString:@"1"])
    {
        cattypeLbl.text = @"Home" ;
        image_OfCategory.image=[UIImage imageNamed:@"cat_home"];
    }else if([catId isEqualToString:@"2"])
    {
        cattypeLbl.text = @"Auto" ;
        image_OfCategory.image=[UIImage imageNamed:@"car_icon"];
    }else if([catId isEqualToString:@"3"])
    {
        cattypeLbl.text =@"Electronic" ;
        image_OfCategory.image=[UIImage imageNamed:@"cat_elc"];
    }
    desTV.text=[self.jobDict valueForKey:@"description"];
    dateLbl.text=[self.jobDict valueForKey:@"created_at"];
    
    NSString *amt=[NSString stringWithFormat:@"$ %@",[self.jobDict valueForKey:@"amount"]];
    
    [dolrBtn setTitle:amt forState:UIControlStateNormal];
  //  [SVProgressHUD dismiss];
}

#pragma mark
#pragma mark-tableView Delegate

-(NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    return 1;
}
- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section;
{
    if (btn_ApplicantObj.selected)
  //  if (tableView_Applicant)
    {
      return [arr_AppliedWorker count];
    }
    else
      return [arr_invitation count];
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    
    if (btn_ApplicantObj.selected)
      //  if (tableView_Applicant)
    {
        
        static NSString *customIdentifierForSideJobsFromWorkers = @"cell_applicant";
        JobDetailTableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:customIdentifierForSideJobsFromWorkers];
        if (cell == nil)
        {
            cell = [[JobDetailTableViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:customIdentifierForSideJobsFromWorkers];
        }


        cell.btn_AcceptObj.hidden=NO;
        cell.btn_RejectObj.hidden=NO;
         cell.btn_CancelToInvities.hidden=YES;

        NSMutableDictionary *dict=[[NSMutableDictionary alloc]init];
        dict=[[arr_AppliedWorker objectAtIndex:indexPath.row]objectForKey:@"user"];
        cell.lbl_name.text=[dict objectForKey:@"name"];
        
        NSString *date=[NSString stringWithFormat:@"%@",[[arr_AppliedWorker objectAtIndex:indexPath.row]objectForKey:@"created_at"]];
        
        NSArray* foo = [date componentsSeparatedByString: @" "];
        NSString* dateWithOutTime = [foo objectAtIndex: 0];
        cell.lbl_time.text=dateWithOutTime;
       // cell.lbl_time.text=[[arr_AppliedWorker objectAtIndex:indexPath.row]objectForKey:@"created_at"];
        cell.lbl_Amount.text=[NSString stringWithFormat:@"$ %@",[[arr_AppliedWorker objectAtIndex:indexPath.row]objectForKey:@"proposed_amount"] ];
        NSString *imageurl=[dict objectForKey:@"profile_image"];
        if (imageurl.length<1)
        {
            //cell.lbl_NoPhoto.hidden=NO;
             cell.img_applicant.image=[UIImage imageNamed:@"user.png"];
        }
        else
        {
            // cell.lbl_NoPhoto.hidden=YES;
            NSString *url=[NSString stringWithFormat:@"%@%@",serviceLink,imageurl];
//            cell.img_applicant.image=[UIImage imageWithData:[NSData dataWithContentsOfURL:[NSURL URLWithString:url]]];
            
            
            dispatch_async(dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_BACKGROUND, 0), ^{
                NSData * imageData = [NSData dataWithContentsOfURL:[NSURL URLWithString:url]];
                UIImage *image = [UIImage imageWithData:imageData];
                
                dispatch_sync(dispatch_get_main_queue(), ^{ //in main thread update the image
                    cell.img_applicant.image = image;
                   
                });
            });

        }
        //set star rating
        cell.staticStarRatingView.canEdit = YES;
       cell. staticStarRatingView.maxRating = 5;
       cell. staticStarRatingView.minAllowedRating = 1;
       cell. staticStarRatingView.maxAllowedRating = 5;
       cell. staticStarRatingView.rating =[[[[arr_AppliedWorker objectAtIndex:indexPath.row]objectForKey:@"user"]objectForKey:@"rating"]integerValue];
        
        
        
        
        cell.lbl_Amount.hidden=NO;
         cell.btnComment.hidden=NO;
        cell.img_applicant.layer.cornerRadius= cell.imageView.bounds.size.width/2 ;
        dolrBtn.layer.masksToBounds=true;
        cell.btn_AcceptObj.tag=indexPath.row;
        [cell.btn_AcceptObj addTarget:self action:@selector(acceptJob:) forControlEvents:UIControlEventTouchUpInside];
        cell.btn_RejectObj.tag=indexPath.row;
        
        
      
        [cell.btn_RejectObj addTarget:self action:@selector(rejectjob:) forControlEvents:UIControlEventTouchUpInside];
        cell.btnComment.tag=indexPath.row;
        [cell.btnComment addTarget:self action:@selector(CommentShow:) forControlEvents:UIControlEventTouchUpInside];
        return cell;
    }
    else
    {
        static NSString *customIdentifierForSideJobsFromWorkers = @"cell_Invities1";
        TableViewCellMyInVitation *cell = [tableView dequeueReusableCellWithIdentifier:customIdentifierForSideJobsFromWorkers];
        if (cell == nil)
        {
            cell = [[TableViewCellMyInVitation alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:customIdentifierForSideJobsFromWorkers];
        }
        
        
        //set star rating
        cell.staticStarRatingView.canEdit = YES;
        cell. staticStarRatingView.maxRating = 5;
        cell. staticStarRatingView.minAllowedRating = 1;
        cell. staticStarRatingView.maxAllowedRating = 5;
        cell. staticStarRatingView.rating =[[[[arr_invitation objectAtIndex:indexPath.row]objectForKey:@"user"]objectForKey:@"rating"]floatValue];
        
        NSMutableDictionary *dict=[[NSMutableDictionary alloc]init];
        dict=[[arr_invitation objectAtIndex:indexPath.row]objectForKey:@"user"];
        cell.lbl_UserName.text=[dict objectForKey:@"name"];
        NSString *date=[NSString stringWithFormat:@"%@",[[arr_invitation objectAtIndex:indexPath.row]objectForKey:@"created_at"]];
        
        NSArray* foo = [date componentsSeparatedByString: @" "];
        NSString* dateWithOutTime = [foo objectAtIndex: 0];
        cell.lbl_date.text=dateWithOutTime;
        NSString *str=[[arr_invitation objectAtIndex:indexPath.row]objectForKey:@"applicant_job_status"];
        NSString *cap=[str capitalizedString];
        cell.lbl_Status.text=cap;
        cell.btn_Accept.tag=indexPath.row;
 
        NSString *imageurl=[dict objectForKey:@"profile_image"];
        if (imageurl.length==0)
        {
          
             cell.imgV_User.image=[UIImage imageNamed:@"user.png"];
        }
        else
        {
    
            NSString *url=[NSString stringWithFormat:@"%@%@",serviceLink,imageurl];
           // cell.imgV_User.image=[UIImage imageWithData:[NSData dataWithContentsOfURL:[NSURL URLWithString:url]]];
            
            dispatch_async(dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_BACKGROUND, 0), ^{
                NSData * imageData = [NSData dataWithContentsOfURL:[NSURL URLWithString:url]];
                UIImage *image = [UIImage imageWithData:imageData];
                
                dispatch_sync(dispatch_get_main_queue(), ^{ //in main thread update the image
                    cell.imgV_User.image = image;
                    cell.textLabel.text = @""; //add this update will reflect the changes
                });
            });

            
        }
        cell.imgV_User.layer.cornerRadius= cell.imageView.bounds.size.width/2 ;
        dolrBtn.layer.masksToBounds=true;
      
        return cell;
    }
  
  
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    
}

//-(CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
//{
////    Feedback *item = [self.items objectAtIndex:indexPath.row];
////    
////    CGFloat textHeight = [self getLabelHeightForText:item.comment andWidth:162];//give your label width here
//   // return 106;
//}

//-(void)AcceptJObFromInvitiesList:(id)sender
//{
//    UIButton *button=(UIButton*)sender;
//    NSInteger Tag=button.tag;
//    
//    
//    NSString *id_job=  [[arr_invitation  objectAtIndex:Tag]objectForKey:@"id"];
//    NSString *proposedAmount=[[arr_invitation objectAtIndex:Tag]objectForKey:@"proposed_amount"];
//    
//    NSString *token=[NSString stringWithFormat:@"%@",[defaults objectForKey:@"user_token"]];
//    NSString *url=[NSString stringWithFormat:@"%@job-status?token=%@",serviceLink,token];
//    NSMutableDictionary *jsonDict=[[NSMutableDictionary alloc]init];
//    
//    [jsonDict setValue:id_job forKey:@"job_application_id"] ;
//    [jsonDict setValue:@"accepted" forKey:@"applicant_job_status"];
//    [jsonDict setValue:proposedAmount forKey:@"proposed_amount"];
//    [jsonDict setValue:@""forKey:@"comment"];
//    
//    id json=[WebService signup:jsonDict :url];
//    if ([[json objectForKey:@"message"]isEqualToString:@"Success"])
//    {
//        [self fetchJobDetail];
//        UIAlertView *alert=[[UIAlertView alloc]initWithTitle:@"" message:@"You have successfully accepted the job." delegate:nil cancelButtonTitle:@"OK" otherButtonTitles: nil];
//        [alert show];
//        [self.navigationController popViewControllerAnimated:YES];
//    }
//    else
//    {
//        UIAlertView *alert=[[UIAlertView alloc]initWithTitle:@"" message:@"Some error is encountered while accepting the job." delegate:nil cancelButtonTitle:@"OK" otherButtonTitles: nil];
//        [alert show];
//    }
//
//    
//}

#pragma mark
#pragma mark-before accept job payment release to adimin
-(void)acceptJob:(id)sender
{
     [SVProgressHUD showWithStatus:@"Loading..."];
    //[SVProgressHUD showWithMaskType:SVProgressHUDMaskTypeNone];
    UIButton *button=(UIButton*)sender;
    jobIndex=button.tag;
    
   
   // [self performSelector:@selector(payment) withObject:nil afterDelay:0.3f];
    
    [self performSelector:@selector(checkCardforPaidTo) withObject:nil afterDelay:0.3];

}
-(void)checkCardforPaidTo
{
//    NSMutableArray *array;
    
     NSString *paidto = [[[arr_AppliedWorker  objectAtIndex:jobIndex]objectForKey:@"user"] objectForKey:@"id"];
    
//          NSString *url=[NSString stringWithFormat:@"%@findCustomerCard/%@",serviceLink,paidto];
     NSString *token=[NSString stringWithFormat:@"%@",[defaults objectForKey:@"user_token"]];
      NSString *url=[NSString stringWithFormat:@"%@read-user/%@?token=%@",serviceLink,paidto,token];
    
    if (paidto != nil)
    {
        id  json = [WebService GetwithoutDict:url];
       
        if ([[json objectForKey:@"message"]isEqualToString:@"Success"])
        {
            
            status_WorkerCard= [json objectForKey:@"status"];
            NSLog(@"status_wokerCard  %@",status_WorkerCard);
            
            merchant_ID = [json valueForKeyPath:@"response.merchant_id"];
        }
        else
        {
            status_WorkerCard=@"0";
            merchant_ID=@"";
        }
        [self performSelector:@selector(payment) withObject:nil afterDelay:0.3f];
    }
    
    else
    {
        [SVProgressHUD dismiss];
        UIAlertView *alert=[[UIAlertView alloc]initWithTitle:@"Alert!" message:@"Some error occured to find worker have card or not?" delegate:nil cancelButtonTitle:@"OK" otherButtonTitles: nil];
        [alert show];
        [SVProgressHUD dismiss];
    }
    
   
}
-(void)payment
{
    
    if (merchant_ID != nil)
    {
    
        if ([Helper isConnectedToInternet])
        {
            if (cardDetails.count==0)
            {
                UIAlertView *alert=[[UIAlertView alloc]initWithTitle:@"Alert!" message:@"Please add card before payment." delegate:nil cancelButtonTitle:@"OK" otherButtonTitles:nil];
                [alert show];
                [SVProgressHUD dismiss];
                
            }
            else
            {
                NSMutableArray *arr=[[NSMutableArray alloc]init];
                arr=[NSMutableArray arrayWithArray:arr_AppliedWorker];
                
                NSString *proposedAmount=[[arr objectAtIndex:jobIndex]objectForKey:@"proposed_amount"];
                NSString *token=[NSString stringWithFormat:@"%@",[defaults objectForKey:@"user_token"]];
                
                NSMutableDictionary *jsonDict=[[NSMutableDictionary alloc]init];
                
                [jsonDict setValue:[[arr  objectAtIndex:jobIndex]objectForKey:@"job_id"]forKey:@"job_id"];
                [jsonDict setValue:[defaults objectForKey:@"customer_id"] forKey:@"paid_customer_id"];
                
                [jsonDict setObject:[[[arr  objectAtIndex:jobIndex]objectForKey:@"user"] objectForKey:@"customer_id"] forKey:@"customer_id"];
                [jsonDict setValue:proposedAmount forKey:@"amount"];
                [jsonDict setValue:@"payment"forKey:@"remark"];
//              [jsonDict setValue:[defaults objectForKey:@"merchant_id"] forKey:@"merchant_id"];
                [jsonDict setValue:merchant_ID forKey:@"merchant_id"];
                [jsonDict setValue:[[[arr  objectAtIndex:jobIndex]objectForKey:@"user"] objectForKey:@"id"]forKey:@"paid_to"];
                [jsonDict setValue:[defaults objectForKey:@"user_id"]forKey:@"paid_by"];
                
                
                
                NSString *url=[NSString stringWithFormat:@"%@createPayment?token=%@",serviceLink,token];
                
                if ([[[arr  objectAtIndex:jobIndex]objectForKey:@"user"] objectForKey:@"customer_id"]!=nil&&[[[[arr  objectAtIndex:jobIndex]objectForKey:@"user"] objectForKey:@"customer_id"]isEqualToString:@""])
                {
                    UIAlertView *alert=[[UIAlertView alloc]initWithTitle:@"Alert!" message:@"User details not found." delegate:nil cancelButtonTitle:@"OK" otherButtonTitles:nil];
                    [alert show];
                }
                else
                {
                    id json=[WebService signup:jsonDict :url];
                    NSLog(@"%@", json);
                    if ([[json objectForKey:@"message"]isEqualToString:@"Success"])
                    {
                        [self paymentReleaseToAdmin];
                    }
                    else if (json == nil)
                    {
                        UIAlertView *alert=[[UIAlertView alloc]initWithTitle:@"Alert!" message:@"User details not found." delegate:nil cancelButtonTitle:@"OK" otherButtonTitles:nil];
                        [alert show];
                        [SVProgressHUD dismiss];
                        
                    }
                    else
                    {
                        UIAlertView *alert=[[UIAlertView alloc]initWithTitle:@"Alert!" message:@"Sorry! We are unable to accept this proposal. Please inform applicant to add card details." delegate:nil cancelButtonTitle:@"OK" otherButtonTitles:nil];
                        [alert show];
                        
                        [SVProgressHUD dismiss];
                    }
                }
            }
            
        }else
        {
            UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"No Internet" message:@"Please check your internet connection." delegate:nil cancelButtonTitle:@"OK" otherButtonTitles:nil];
            
            [alert show];
        }
    }
        else
        {
            UIAlertView *alert=[[UIAlertView alloc]initWithTitle:@"Alert" message:@"Sorry! We are unable to release this payment. Please inform applicant to Account details." delegate:nil cancelButtonTitle:@"OK" otherButtonTitles:nil];
            [alert show];
            [SVProgressHUD dismiss];
        }

}

#pragma mark
#pragma mark-payment release to admin  *********** mark job as accept after payment release ***********

-(void)paymentReleaseToAdmin
{
    if ([Helper isConnectedToInternet])
    {
        NSMutableArray *arr=[[NSMutableArray alloc]init];
        
        arr=[NSMutableArray arrayWithArray:arr_AppliedWorker];
        NSString *id_job=  [[arr  objectAtIndex:jobIndex]objectForKey:@"id"];
        NSString *proposedAmount=[[arr objectAtIndex:jobIndex]objectForKey:@"proposed_amount"];
        
        NSString *token=[NSString stringWithFormat:@"%@",[defaults objectForKey:@"user_token"]];
        NSString *url=[NSString stringWithFormat:@"%@job-status?token=%@",serviceLink,token];
        
        NSMutableDictionary *jsonDict=[[NSMutableDictionary alloc]init];
        
        [jsonDict setValue:id_job forKey:@"job_application_id"];
        [jsonDict setValue:@"accepted" forKey:@"applicant_job_status"];
        [jsonDict setValue:proposedAmount forKey:@"proposed_amount"];
        
        [jsonDict setValue:@""forKey:@"comment"];
        
        //change to notification
        [jsonDict setValue:[[arr objectAtIndex:jobIndex] objectForKey:@"job_id"] forKey:@"job_id"];
        [jsonDict setValue:[defaults objectForKey:@"user_id"] forKey:@"job_user_id"];
        
        [jsonDict setValue:[defaults objectForKey:@"user_id"] forKey:@"sender_id"];
        [jsonDict setValue:[[[arr objectAtIndex:jobIndex] objectForKey:@"user"]objectForKey:@"id"] forKey:@"receiver_id"];
        [jsonDict setValue:[defaults objectForKey:@"p_name"] forKey:@"sender_name"];
        
        
        
        id json=[WebService signup:jsonDict :url];
        
        if ([[json objectForKey:@"message"]isEqualToString:@"Success"])
        {
            UIAlertView *alert1=[[UIAlertView alloc]initWithTitle:@"Success" message:@"You have successfully accepted the job and amount has been deduct from your account." delegate:self cancelButtonTitle:@"OK" otherButtonTitles:nil];
            alert1.tag=1;
            [alert1 show];
        }
        else
        {
            UIAlertView *alert=[[UIAlertView alloc]initWithTitle:@"Alert!" message:@"Some error is encountered while accepting the job But amount has been detect from your account." delegate:nil cancelButtonTitle:@"OK" otherButtonTitles: nil];
            [alert show];
        }
    }else
    {
        UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"No Internet" message:@"Please check your internet connection." delegate:nil cancelButtonTitle:@"OK" otherButtonTitles:nil];
        
        [alert show];
    }
    [SVProgressHUD dismiss];
    
}
- (void)alertView:(UIAlertView *)alertView clickedButtonAtIndex:(NSInteger)buttonIndex
{
    if (alertView.tag==1)
    {
        NSInteger T=buttonIndex;
        switch (T)
        {
            case 0:
                [self.navigationController popViewControllerAnimated:YES];
                break;
                
            default:
                break;
        }
        
    }
    
    else if (alertView.tag==2)
    {
        if (buttonIndex==0)
        {
           [self.navigationController popViewControllerAnimated:YES];
        }
    }
}
-(void)rejectjob:(id)sender
{
    
    [SVProgressHUD showWithStatus:@"Loading..."];
   
    UIButton *button=(UIButton*)sender;
    jobIndex_Reject=button.tag;
    
    [self performSelector:@selector(rejectJobMethod) withObject:nil afterDelay:0.3f];
    
 
}

-(void)rejectJobMethod
{
    NSMutableArray *arr=[[NSMutableArray alloc]init];
    if (btn_ApplicantObj.selected)
    {
        arr=[NSMutableArray arrayWithArray:arr_AppliedWorker];
    }
    else
    {
        arr=[NSMutableArray arrayWithArray:arr_invitation];
    }
    
    
    
    NSString *id_job=  [[arr  objectAtIndex:jobIndex_Reject]objectForKey:@"id"];
    NSString *proposedAmount=[[arr objectAtIndex:jobIndex_Reject]objectForKey:@"proposed_amount"];
    
    NSString *token=[NSString stringWithFormat:@"%@",[defaults objectForKey:@"user_token"]];
    NSString *url=[NSString stringWithFormat:@"%@job-status?token=%@",serviceLink,token];
    NSMutableDictionary *jsonDict=[[NSMutableDictionary alloc]init];
    [jsonDict setValue:id_job forKey:@"job_application_id"] ;
    [jsonDict setValue:@"rejected" forKey:@"applicant_job_status"];
    [jsonDict setValue:proposedAmount forKey:@"proposed_amount"];
    [jsonDict setValue:@""forKey:@"comment"];
    
    //change to notification
    
    [jsonDict setValue:[[arr objectAtIndex:jobIndex] objectForKey:@"job_id"] forKey:@"job_id"];
    [jsonDict setValue:[defaults objectForKey:@"user_id"] forKey:@"job_user_id"];
    
    [jsonDict setValue:[defaults objectForKey:@"user_id"] forKey:@"sender_id"];
    
    [jsonDict setValue:[[[arr objectAtIndex:jobIndex] objectForKey:@"user"]objectForKey:@"id"] forKey:@"receiver_id"];
    [jsonDict setValue:[defaults objectForKey:@"p_name"] forKey:@"sender_name"];
    
    id json=[WebService signup:jsonDict :url];
    
    if ([[json objectForKey:@"message"]isEqualToString:@"Success"])
    {
        [SVProgressHUD dismiss];
       // [self performSelectorInBackground:@selector(fetchJobDetail) withObject:nil];
        //[self fetchJobDetail];
        UIAlertView *alert=[[UIAlertView alloc]initWithTitle:@"" message:@"You have rejected this user for the job." delegate:self cancelButtonTitle:@"OK" otherButtonTitles: nil];
        alert.tag=2;
        [alert show];
        
    }
    else
    {
         [SVProgressHUD dismiss];
        UIAlertView *alert=[[UIAlertView alloc]initWithTitle:@"" message:@"Some error is encountered while rejecting the job." delegate:nil cancelButtonTitle:@"OK" otherButtonTitles: nil];
        [alert show];
        
    }
  [SVProgressHUD dismiss];
}


#pragma mark
#pragma mark-show comment
-(IBAction)CommentShow:(id)sender
{
    UIButton *button=(UIButton*)sender;
    NSInteger Tag=button.tag;
    if (btn_ApplicantObj.selected)
    {
        textView_Comment.text=[[arr_AppliedWorker objectAtIndex:Tag]objectForKey:@"comment"];
    }
    else
    {
       textView_Comment.text=[[arr_invitation objectAtIndex:Tag]objectForKey:@"comment"];
    }
    
    [UIView transitionWithView:view_CommentPopUp
                      duration:0.75
                       options:UIViewAnimationOptionTransitionNone
                    animations:^{
                        
                        [view_CommentPopUp setFrame:CGRectMake(0, view_CommentPopUp.frame.origin.y, view_CommentPopUp.frame.size.width, view_CommentPopUp.frame.size.height)];
                        [viewPopUpInside setCenter:view_CommentPopUp.center];
                        [self.view.window addSubview:view_CommentPopUp];
                    }
                    completion:nil];
}

- (IBAction)btn_HideComment:(id)sender
{
   [UIView transitionWithView:view_CommentPopUp
                      duration:0.75
                       options:UIViewAnimationOptionTransitionNone
                    animations:^{
                        [view_CommentPopUp setFrame:CGRectMake(1000, view_CommentPopUp.frame.origin.y, view_CommentPopUp.frame.size.width, view_CommentPopUp.frame.size.height)];
                        [self.view.window addSubview:view_CommentPopUp];
                    }
                    completion:nil];

}

- (IBAction)tongle_Action:(id)sender{
    if (toggle_object.on)
    {
        sendStatus=@"1";
       // [self publishjob];
        [self performSelectorInBackground:@selector(publishjob) withObject:nil];
    }
    else
    {
         sendStatus=@"0";
        // [self publishjob];
        [self performSelectorInBackground:@selector(publishjob) withObject:nil];
        NSLog(@"not published jobs");
    }
}

-(void)publishjob
{
    
    NSString *token=[NSString stringWithFormat:@"%@",[defaults objectForKey:@"user_token"]];
    NSString *url=[NSString stringWithFormat:@"%@publish?token=%@",serviceLink,token];
    
    NSMutableDictionary *jsonDict=[[NSMutableDictionary alloc]init];
    [jsonDict setValue:[self.jobDict valueForKey:@"id"] forKey:@"id"];
    [jsonDict setValue:sendStatus forKey:@"published"];
    
    id json = [WebService signup:jsonDict:url];
    if ([[json objectForKey:@"message"]isEqualToString:@"Success"])
    {
//        UIAlertView *alert=[[UIAlertView alloc]initWithTitle:@"" message:@"Job published" delegate:self cancelButtonTitle:@"OK" otherButtonTitles:nil];
//        [alert show];
    }
  
}

- (IBAction)btn_EditJob:(id)sender{
    CreateJobController *root =[self.storyboard instantiateViewControllerWithIdentifier:@"createjob"];
     root.modifyTypeStr=@"update";
    root.jobDetailDict=self.jobDict;
    [self.navigationController pushViewController:root animated:YES];
    NSLog(@"%@",self.jobDict);
}

#pragma mark 
#pragma delete job start................................
- (IBAction)btn_deleteJob:(id)sender{
     [self showAlertWidOptions];
}
-(void)deleteJob{
    NSString *token=[NSString stringWithFormat:@"%@",[defaults objectForKey:@"user_token"]];
    NSString *url=[NSString stringWithFormat:@"%@delete-job/%@?token=%@",serviceLink,[self.jobDict valueForKey:@"id"],token];
    
    NSMutableArray *jobsArr=[NSMutableArray new];
    id json=[WebService deleteData:url];
    jobsArr = [json valueForKey:@"response"];
    if ([[json objectForKey:@"message"]isEqualToString:@"Success"]) {
        
        [SVProgressHUD showImage:[UIImage imageNamed:@"btn.png"] status:@"Job Deleted"];
        [self.navigationController performSelector:@selector(popViewControllerAnimated:) withObject:nil afterDelay:0.3f];
    }
    else if ([[json objectForKey:@"message"]isEqualToString:@"token_expired"])
    {
        
        [self tokenExpired];
    }else{
        [SVProgressHUD dismiss];
    }
       [SVProgressHUD dismiss];
}
#pragma mark delete job.................. End................................

#pragma mark
#pragma mark-show applicants details
- (IBAction)btn_Applicants:(id)sender{
   [btn_InvitiesObj setBackgroundImage:[UIImage imageNamed:@"invitebtn.png"] forState:UIControlStateNormal];
    [btn_ApplicantObj setBackgroundImage:[UIImage imageNamed:@"appliedbtn-active.png"] forState:UIControlStateNormal];
//     [self fetchJobDetail];
    btn_ApplicantObj.selected=YES;
    btn_InvitiesObj.selected=NO;
   
    
    [tableViewMyInvitation setFrame:CGRectMake(1000, tableViewMyInvitation.frame.origin.y, tableViewMyInvitation.frame.size.width, tableViewMyInvitation.frame.size.height)];
    [tableView_Applicant setFrame:CGRectMake(0, tableView_Applicant.frame.origin.y, tableView_Applicant.frame.size.width, tableView_Applicant.frame.size.height)];
    [self performSelectorInBackground:@selector(fetchJobDetail) withObject:nil];
   //  [self fetchJobDetail];
    
   // [tableView_Applicant reloadData];
}
- (IBAction)btn_Invities:(id)sender
{
    btn_ApplicantObj.selected=NO;
    btn_InvitiesObj.selected=YES;
    arr_invitation=[[NSMutableArray alloc]init];
   
    for (int i=0; i<[arrApplicant count]; i++)
    {
        NSString *str=[NSString stringWithFormat:@"%@",[[arrApplicant objectAtIndex:i]objectForKey:@"invited"]];
        if ([str isEqualToString:@"1"])
        {
            if ([[[arrApplicant objectAtIndex:i]objectForKey:@"applicant_job_status"]isEqualToString:@"invited"] ||[[[arrApplicant objectAtIndex:i]objectForKey:@"applicant_job_status"]isEqualToString:@"Declined"]||[[[arrApplicant objectAtIndex:i]objectForKey:@"applicant_job_status"]isEqualToString:@"applied"])
            {
                [arr_invitation addObject:[arrApplicant objectAtIndex:i]];
            }
        }
        else
        {
            
        }
       
    }
    if (arr_invitation.count)
    {
        [tableViewMyInvitation setFrame:CGRectMake(0, tableViewMyInvitation.frame.origin.y, tableViewMyInvitation.frame.size.width, tableViewMyInvitation.frame.size.height)];
        
        lbl_NoApplicant.hidden=YES;
        tableViewMyInvitation.dataSource=self;
        tableViewMyInvitation.delegate=self;
        [tableViewMyInvitation reloadData];
    }
    else
    {
        lbl_NoApplicant.hidden=NO;
    }
    [btn_InvitiesObj setBackgroundImage:[UIImage imageNamed:@"invitebtn-active.png"] forState:UIControlStateNormal];
    [btn_ApplicantObj setBackgroundImage:[UIImage imageNamed:@"appliedbtn.png"] forState:UIControlStateNormal];
  
    
   
    
    [tableView_Applicant setFrame:CGRectMake(1000, tableView_Applicant.frame.origin.y, tableView_Applicant.frame.size.width, tableView_Applicant.frame.size.height)];
    
}


-(void)showAlertWidOptions{
    UIAlertController *alert = [UIAlertController alertControllerWithTitle:@"Do you want to delete this job?"  message:nil preferredStyle:UIAlertControllerStyleAlert];
    
    
    UIAlertAction *afterWeek = [UIAlertAction
                                actionWithTitle:NSLocalizedString(@"OK", @"Cancel action")
                                style:UIAlertActionStyleDefault
                                handler:^(UIAlertAction *action)
                                {
                                    [SVProgressHUD showWithStatus:@"Deleting Job"];
                                    [self performSelector:@selector(deleteJob) withObject:nil afterDelay:0.3f];
                                }];
    
    UIAlertAction *cancelAction = [UIAlertAction
                                   actionWithTitle:NSLocalizedString(@"Cancel", @"Cancel action")
                                   style:UIAlertActionStyleCancel
                                   handler:^(UIAlertAction *action)
                                   {
                                       
                                   }];
    
    [alert addAction:afterWeek];
    [alert addAction:cancelAction];
    
    [self presentViewController:alert animated:YES completion:nil];
}

#pragma mark
#pragma mark-Token expired
-(void)tokenExpired
{
    UIAlertView *alert=[[UIAlertView alloc]initWithTitle:@"" message:@"Session Expired Please login agian" delegate:nil cancelButtonTitle:@"OK" otherButtonTitles:nil];
    [alert show];
    AppDelegate *delegate;
    delegate=(AppDelegate*)[[UIApplication sharedApplication]delegate];
    [defaults setBool:NO forKey:@"logined"];
    [defaults synchronize];
    [[NSUserDefaults standardUserDefaults]synchronize];
    [delegate loginStatus];
}



#pragma mark
#pragma mark- Get card details..........
-(void)getCarddetails
{
    NSMutableDictionary *jsonDict=[[NSMutableDictionary alloc]init];
    
    [jsonDict setValue:[defaults objectForKey:@"customer_id"] forKey:@"id"];
    
    NSString *url=[NSString stringWithFormat:@"%@find-customer?token=%@",serviceLink,[defaults objectForKey:@"user_token"]];
    id  json = [WebService signup:jsonDict:url];
    
    cardDetails=[NSMutableArray new];
    
    
    if ([[json objectForKey:@"message"]isEqualToString:@"Success"])
    {
     NSMutableDictionary*   dictResponse=[[NSMutableDictionary alloc]init];
    //  NSMutableArray*  arr_token=[[NSMutableArray alloc]init];
        
        dictResponse=[json objectForKey:@"response"];
        cardDetails=[dictResponse objectForKey:@"creditCards"];
       // arr_cardCount=[[NSMutableArray alloc]init];
        
        for (NSMutableDictionary *dict in [cardDetails self])
        {
           
         //   NSString *str_Token=[[dict objectForKey:@"_attributes"]objectForKey:@"token"];
            
          //  [arr_cardCount addObject:[dict objectForKey:@"_attributes"]];
          //  [arr_token addObject:str_Token];
        }
    }
    
    
    else if([[json objectForKey:@"error"]isEqualToString:@"token_expired"])
    {
        
        [self tokenExpired];
        
    }
    
}



@end