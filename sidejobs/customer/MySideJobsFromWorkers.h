//
//  MySideJobs.h
//  WorkerEarning
//
//  Created by Debut Mac 4 on 19/08/15.
//  Copyright (c) 2015 debut.infotech. All rights reserved.
//

// this is customer side class 

#import <UIKit/UIKit.h>


@interface MySideJobsFromWorkers : UIViewController <UITableViewDataSource,UITableViewDelegate>
{
    IBOutlet UIView *view_ongoingContracts;
    IBOutlet UIView *View_AppliedJobsWorker;
    IBOutlet UIView *View_JobCompletedOfWorker;
    IBOutlet UITableView *tableView_OngoingWorkers;
    IBOutlet UITableView *tableView_JobCompletedWorker;
    IBOutlet UITableView *tableView_AppliedJobsWorker;
    
    IBOutlet UISearchBar *searchBar_obj;
   
    IBOutlet UILabel *noRecord_lbl;
    IBOutlet UIButton *btn_appliedObj;
    IBOutlet UIButton *btn_ongoingObj;
    IBOutlet UIButton *btn_jobCompleteObj;
   
    IBOutlet UILabel *lbl_NoRecordSeg;
}
@property (nonatomic, strong) id latestPaymentMethodOrNonce;
- (IBAction)appliedjobs:(id)sender;
- (IBAction)jobcomplete:(id)sender;

- (IBAction)ongoingJob:(id)sender;
@end
