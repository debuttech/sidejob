//
//  CustomerDashboardController.m
//  SideJobs
//
//  Created by Debut05 on 8/13/15.
//  Copyright (c) 2015 Narinder. All rights reserved.
//

#import "CustomerDashboardController.h"
#import "CreateJobController.h"
#import "ListingOfAllWorkersVC.h"
#import <MessageUI/MessageUI.h>
#import "MessageViewController.h"
#import "NotificationVCViewController.h"
#import "AppDelegate.h"

@interface CustomerDashboardController ()
{
    AppDelegate *delegate;
    NSUserDefaults *defaults;
}

@end

@implementation CustomerDashboardController

- (void)viewDidLoad {
    [super viewDidLoad];
    delegate = (AppDelegate*)[[UIApplication sharedApplication]delegate];
    
    defaults=[NSUserDefaults standardUserDefaults];
    self.navigationController.navigationBar.topItem.title = @"";
    NSDictionary *size = [NSDictionary dictionaryWithObjectsAndKeys:[UIFont fontWithName:@"HelveticaNeue-Light" size:20.0],NSFontAttributeName,[UIColor whiteColor],NSForegroundColorAttributeName, nil];
    self.navigationController.navigationBar.titleTextAttributes = size;
    
    UIButton *noti_Bell=[UIButton buttonWithType:UIButtonTypeCustom];
    [noti_Bell setFrame:CGRectMake(0, 0, 26, 26)];
    [noti_Bell setImage:[UIImage imageNamed:@"notificationbelln"] forState:UIControlStateNormal];
    [noti_Bell addTarget:self action:@selector(notification:) forControlEvents:UIControlEventTouchUpInside];

   /* UILabel *lbl_Noti=[[UILabel alloc]initWithFrame:CGRectMake(12, 0, 16, 16)];
    lbl_Noti.layer.cornerRadius = lbl_Noti.frame.size.width/2;
    lbl_Noti.layer.masksToBounds = YES;
    lbl_Noti.font = [UIFont systemFontOfSize:10.0];
    lbl_Noti.textAlignment = NSTextAlignmentCenter;
    lbl_Noti.backgroundColor = [UIColor redColor];
    lbl_Noti.textColor = [UIColor whiteColor];
    lbl_Noti.text = delegate.Noti_badgeNumber;
    NSLog(@"%@",delegate.Noti_badgeNumber);*/
    
    
    UILabel *lbl_Noti=[[UILabel alloc]initWithFrame:CGRectMake(12, 0, 16, 16)];
    lbl_Noti.layer.cornerRadius = lbl_Noti.frame.size.width/2;
    lbl_Noti.layer.masksToBounds = YES;
    lbl_Noti.font = [UIFont systemFontOfSize:10.0];
    lbl_Noti.textAlignment = NSTextAlignmentCenter;
    lbl_Noti.backgroundColor = [UIColor redColor];
    lbl_Noti.textColor = [UIColor whiteColor];
    
   // lbl_Noti.text = @"10";
    lbl_Noti.text = [NSString stringWithFormat:@"%ld", (long)[[UIApplication sharedApplication] applicationIconBadgeNumber]];
//    NSLog(@"Notification Count%@",delegate.Noti_badgeNumber);
//    NSLog(@"Notification UserDefault%@",[defaults objectForKey:@"noti"]);


    UIView *view_Noti = [[UIView alloc]initWithFrame:CGRectMake(0, 0, 26, 26)];
    [view_Noti addSubview:noti_Bell];
    if ([[UIApplication sharedApplication] applicationIconBadgeNumber] > 0)
    {
        
     [view_Noti addSubview:lbl_Noti];

    }
    else
    {
        
    }
   
    self.navigationItem.rightBarButtonItem=[[UIBarButtonItem alloc]initWithCustomView:view_Noti];
    
}

-(void)notification:(id)sender
{
    NotificationVCViewController *obj = [self.storyboard instantiateViewControllerWithIdentifier:@"NotificationVCViewController"];
//    [UIApplication sharedApplication].applicationIconBadgeNumber = 0;

    UINavigationController *nav = [[UINavigationController alloc]initWithRootViewController:obj];
   [self.navigationController presentViewController:nav animated:YES completion:nil];
    
}


- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
   }

- (UIBarButtonItem *)DrawerButtonClicked:(id)target action:(SEL)action
{
    UIImage *image = [UIImage imageNamed:@"icon.png"];
    CGRect buttonFrame = CGRectMake(0, 0, 25, 25);
    UIButton *button = [[UIButton alloc] initWithFrame:buttonFrame];
    
    [button setImage:image forState:UIControlStateNormal];
    [button addTarget:target action:action forControlEvents:UIControlEventTouchUpInside];
    UIBarButtonItem *item= [[UIBarButtonItem alloc] initWithCustomView:button];
    return item;
}
#pragma mark-Button Pressed Events
-(void)backButtonClicked
{
    
}

-(IBAction)createjobButtonClicked:(id)sender{
    CreateJobController *createJObController = [self.storyboard instantiateViewControllerWithIdentifier:@"createjob"];
    [self.navigationController pushViewController:createJObController animated:YES];

}
-(IBAction)workerButtonClicked:(id)sender{
    ListingOfAllWorkersVC *view_listing=[self.storyboard instantiateViewControllerWithIdentifier:@"listingOfAllWorkersVC"];
    [self.navigationController pushViewController:view_listing animated:YES];
}
-(IBAction)MysidejobButtonClicked:(id)sender{
    
}
-(IBAction)messageButtonClicked:(id)sender
{
    MessageViewController *MVC=[self.storyboard instantiateViewControllerWithIdentifier:@"MessageViewController"];
    [self.navigationController pushViewController:MVC animated:YES];
    
}


@end
