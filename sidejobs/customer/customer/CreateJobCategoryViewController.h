//
//  CreateJobCategoryViewController.h
//  sideJobs
//
//  Created by DebutMac4 on 25/08/15.
//  Copyright (c) 2015 DebutMac4. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface CreateJobCategoryViewController : UIViewController
- (IBAction)btn_autoAction:(id)sender;
- (IBAction)btn_homeAction:(id)sender;
- (IBAction)btn_electronicsAction:(id)sender;

@end
