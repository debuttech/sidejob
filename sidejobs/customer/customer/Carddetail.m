//
//  Carddetail.m
//  SideJobsMileston2
//
//  Created by Debut Mac 4 on 24/09/15.
//  Copyright (c) 2015 debut.infotech. All rights reserved.
//

#import "Carddetail.h"
#import "Carddetailcell.h"
#import "Submittcarddetail.h"
#import "WebService.h"
#import "Helper.h"

@interface Carddetail ()
{
    NSUserDefaults *defaults;
    NSMutableArray *arr_cardCount;
    NSMutableDictionary *dictResponse;
    NSMutableArray *arr_token;
    NSString *cardToken;
    AppDelegate *delegate;
    NSInteger activeCardIndex;
    NSMutableArray *cardDetails;
    BOOL changeColorFlag;
    
    
}

@end

@implementation Carddetail

- (void)viewDidLoad
{
    [super viewDidLoad];
    delegate=(AppDelegate*)[[UIApplication sharedApplication]delegate];
    defaults=[NSUserDefaults standardUserDefaults];
    self.navigationItem.hidesBackButton=YES;
    self.navigationItem.title=@"Card Details";
    UIButton *backButton=[UIButton buttonWithType:UIButtonTypeCustom];
    [backButton setFrame:CGRectMake(0, 0, 23, 21)];
    [backButton setImage:[UIImage imageNamed:@"arrow.png"] forState:UIControlStateNormal];
    [backButton addTarget:self action:@selector(leftBarButton:) forControlEvents:UIControlEventTouchUpInside];
    self.navigationItem.leftBarButtonItem=[[UIBarButtonItem alloc]initWithCustomView:backButton];
    
    
}
-(void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:YES];
    [SVProgressHUD showWithStatus:@"Loading..."];
    [self performSelector:@selector(getCarddetails) withObject:nil afterDelay:0.3f];
}
-(void)leftBarButton:(UIButton*)sender
{
    [self.navigationController popViewControllerAnimated:YES];
}
#pragma mark
#pragma mark- Get card details..........
-(void)getCarddetails
{
    
    
    if ([Helper isConnectedToInternet])
    {
        changeColorFlag = NO;
        NSMutableDictionary *jsonDict=[[NSMutableDictionary alloc]init];
        [jsonDict setValue:[defaults objectForKey:@"customer_id"] forKey:@"id"];
        NSString *url=[NSString stringWithFormat:@"%@find-customer?token=%@",serviceLink,[defaults objectForKey:@"user_token"]];
        id  json = [WebService signup:jsonDict:url];
        cardDetails=[NSMutableArray new];
        
        NSLog(@"Card Details:%@",json);
        if ([[json objectForKey:@"message"]isEqualToString:@"Success"])
        {
            dictResponse=[[NSMutableDictionary alloc]init];
            arr_token=[[NSMutableArray alloc]init];
            
            dictResponse=[json objectForKey:@"response"];
            cardDetails=[dictResponse objectForKey:@"creditCards"];
            NSLog(@"card count:%lu",(unsigned long)cardDetails.count
                  );
            arr_cardCount=[[NSMutableArray alloc]init];
            
            for (NSMutableDictionary *dict in [cardDetails self])
            {
                
                NSString *str_Token=[dict objectForKey:@"token"];
                //[arr_cardCount addObject:[dict objectForKey:@"_attributes"]];
                [arr_token addObject:str_Token];
            }
        }
        
        
        else if([[json objectForKey:@"error"]isEqualToString:@"token_expired"])
        {
            
            UIAlertView *alert=[[UIAlertView alloc]initWithTitle:@"" message:@"Session Expired Please login agian." delegate:nil cancelButtonTitle:@"OK" otherButtonTitles: nil];
            [alert show];
            [defaults setBool:NO forKey:@"logined"];
            [defaults synchronize];
            [[NSUserDefaults standardUserDefaults]synchronize];
            [delegate loginStatus];
            
        }
        
        if ([cardDetails count]==0)
        {
            _lbl_noCard.hidden=NO;
        }
        else
        {
            _lbl_noCard.hidden=YES;
            
        }
        [_tableView_cardDetails reloadData];
        [SVProgressHUD dismiss];
        
    }
    
    else
    {
        UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"No Internet" message:@"Please check your internet connection." delegate:nil cancelButtonTitle:@"OK" otherButtonTitles:nil];
        [alert show];
    }
    
}
- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

#pragma mark - Table view data source and delegate

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
    
    //return cardDetails.count;
    return 1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    return [cardDetails count];
}
- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    
    static NSString *customIdentifierListingWorkers = @"carddetailcell";
    Carddetailcell *cell = [tableView dequeueReusableCellWithIdentifier:customIdentifierListingWorkers];
    if (cell == nil)
    {
        cell = [[Carddetailcell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:customIdentifierListingWorkers];
    }
    NSMutableArray *arr=[[NSMutableArray alloc]init];
    arr=[dictResponse objectForKey:@"creditCards"];
    NSString *str_cardtype;
    NSString *str_fourDigit;
    
    
    NSMutableDictionary *dict=[NSMutableDictionary new];
    dict=[arr objectAtIndex:indexPath.row];
    //       str_fourDigit=[[dict objectForKey:@"_attributes"]objectForKey:@"last4"];
    //       str_cardtype=[[dict objectForKey:@"_attributes"]objectForKey:@"imageUrl"];
    str_fourDigit=[dict objectForKey:@"last4"];
    str_cardtype=[dict objectForKey:@"cardType"];
    str_cardtype=[dict objectForKey:@"imageUrl"];
    
    
    
    cell.cardimage.image=[UIImage imageWithData:[NSData dataWithContentsOfURL:[NSURL URLWithString:str_cardtype]]];
    NSString *str_lastDigit=[NSString stringWithFormat:@"xxxx-xxxx-xxxx-%@",str_fourDigit];
    cell.carddetaill_lbl.text=str_lastDigit;
    cell.carddetail_view.layer.cornerRadius = 5;
    cell.cancelbtn.tag=indexPath.row;
    [cell.cancelbtn addTarget:self action:@selector(DeleteCard:) forControlEvents:UIControlEventTouchUpInside];
    
    // changeColorFlag for change color of selected account
    if (!changeColorFlag)
    {
        if ([[[cardDetails objectAtIndex:indexPath.row] valueForKey:@"default"] isEqualToNumber:[NSNumber numberWithInt:1]])
        {
            cell.carddetail_view.backgroundColor=[UIColor orangeColor];
        }
        else
        {
            
            //    [cell.cancelbtn addTarget:self action:@selector(DeleteCard:) forControlEvents:UIControlEventTouchUpInside];
            cell.carddetail_view.backgroundColor=[UIColor grayColor];
        }
    }
    else
    {
        if (activeCardIndex==indexPath.row)
        {
            cell.carddetail_view.backgroundColor=[UIColor orangeColor];
        }
        
        
        else
        {
            cell.carddetail_view.backgroundColor=[UIColor grayColor];
        }
        
    }
    
    
    
    
    //    cell.carddetail_view.backgroundColor=[UIColor grayColor];
    //    if (activeCardIndex==indexPath.row)
    //    {
    //         cell.carddetail_view.backgroundColor=[UIColor orangeColor];
    //    }
    return cell;
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    changeColorFlag = YES;
    Carddetailcell *cell = [tableView cellForRowAtIndexPath:indexPath];
    cell.carddetail_view.backgroundColor=[UIColor orangeColor];
    activeCardIndex=indexPath.row;
    // defaults setObject:<#(nullable id)#> forKey:<#(nonnull NSString *)#>
    NSLog(@"%ld",(long)activeCardIndex);
    
    [tableView reloadData];
    
    
    //    NSString *merchantID = [[dictResponse objectAtIndex:indexPath.row]valueForKey:@"id"];
    NSString *cardToken = [[cardDetails objectAtIndex:indexPath.row]valueForKey:@"token"];
    [tableView reloadData];
    UIView *bgColorView = [[UIView alloc] init];
    cell.carddetail_view.backgroundColor=[UIColor orangeColor];
    [cell setSelectedBackgroundView:bgColorView];
    [tableView deselectRowAtIndexPath:indexPath animated:YES];
    //    NSString *cellText = cell.textLabel.text;
    dispatch_async(dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_DEFAULT, 0), ^{
        //Do background work
        [self defaultCardList:cardToken];
    });
    
    
    //    UIView *bgColorView = [[UIView alloc] init];
    //    cell.carddetail_view.backgroundColor=[UIColor orangeColor];
    //    [cell setSelectedBackgroundView:bgColorView];
    //
    //    [tableView deselectRowAtIndexPath:indexPath animated:YES];
    
    //    NSString *cellText = cell.textLabel.text;
}


-(void)defaultCardList:(NSString *)cardToken
{
    
    NSMutableDictionary *jsonDict=[[NSMutableDictionary alloc]init];
    [jsonDict setValue:cardToken forKey:@"token"];
    //    [jsonDict setObject:user forKey:@"user_id"];
    
    NSString *url=[NSString stringWithFormat:@"%@default_credit_card", serviceLink];
    
    id  json = [WebService signup:jsonDict:url];
    NSLog(@"Account Detail %@",json);
    //    accountDetialDict=[NSMutableDictionary new];
    
    
    
    if ([[json objectForKey:@"message"]isEqualToString:@"Success"])
    {
        // [self merchantAccountList];
    }
    else
    {
        UIAlertView *alert=[[UIAlertView alloc]initWithTitle:@"" message:@"Some error is occured to activate your account." delegate:nil cancelButtonTitle:@"OK" otherButtonTitles: nil];
        [alert show];
    }
    
}



#pragma mark
#pragma mark-Addcard details
- (IBAction)btnAddCardDetail:(id)sender
{
    if (cardDetails.count>=5)
    {
        UIAlertView *alert=[[UIAlertView alloc]initWithTitle:@"Warning!" message:@"You can't be add more than 5 cards." delegate:nil cancelButtonTitle:@"OK" otherButtonTitles:nil];
        [alert show];
    }
    else
    {
        Submittcarddetail *submitView=[self.storyboard instantiateViewControllerWithIdentifier:@"submittcarddetail"];
        [self.navigationController pushViewController:submitView animated:YES];
    }
    
}

#pragma mark
#pragma mark-delete Card Details
-(IBAction)DeleteCard:(UIButton*)sender
{
    cardToken=[arr_token objectAtIndex:sender.tag];
    UIAlertView *alert=[[UIAlertView alloc]initWithTitle:@"" message:@"Are you sure you want to delete this card?" delegate:self cancelButtonTitle:@"OK" otherButtonTitles:@"Cancel",nil];
    [alert show];
    
}
- (void)alertView:(UIAlertView *)alertView clickedButtonAtIndex:(NSInteger)buttonIndex
{
    
    switch (buttonIndex) {
        case 0:
            [SVProgressHUD showWithMaskType:SVProgressHUDMaskTypeClear];
            [self performSelector:@selector(deletecard) withObject:nil afterDelay:0.3f];
            break;
        case 1:
            break;
            
        default:
            break;
    }
}

-(void)deletecard
{
    NSMutableDictionary *jsonDict=[[NSMutableDictionary alloc]init];
    [jsonDict setValue:cardToken forKey:@"id"];
    NSString *url=[NSString stringWithFormat:@"%@delete-card?token=%@",serviceLink,[defaults objectForKey:@"user_token"]];
    id  jsonDelete = [WebService signup:jsonDict:url];
    if ([[jsonDelete objectForKey:@"message"]isEqualToString:@"Success"])
    {
        
        [self performSelector:@selector(getCarddetails) withObject:nil afterDelay:0.3f];
        
    }
    else
    {
        UIAlertView *alert=[[UIAlertView alloc]initWithTitle:@"" message:@"Error to delete this card!" delegate:nil cancelButtonTitle:@"OK" otherButtonTitles:nil];
        [alert show];
    }
    
}
@end
