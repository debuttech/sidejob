//
//  ProfileController.h
//  SideJobs
//
//  Created by Debut05 on 8/14/15.
//  Copyright (c) 2015 Narinder. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface ProfileController : UIViewController

{
    IBOutlet UITextField *nameTF;
    IBOutlet UITextField *emailTF;
    IBOutlet UITextField *aboutTF;
    IBOutlet UITextField *phoneTF;
    
    IBOutlet UIScrollView *buttonsScrollView;
}

@end
