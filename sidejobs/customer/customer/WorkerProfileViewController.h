//
//  WorkerProfileViewController.h
//  sidejobs
//
//  Created by DebutMac4 on 21/09/15.
//  Copyright (c) 2015 DebutMac4. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "ASStarRatingView.h"

@interface WorkerProfileViewController : UIViewController
{
    
    IBOutlet UIButton *imageViewProfile;
    IBOutlet UITextField *txtfldNAme;
    IBOutlet UITextField *txtFldEmail;
    IBOutlet UITextField *txtFldAboutMe;
    IBOutlet UITextField *TxtFldPhone;
    IBOutlet UIImageView *imgViewWorkerProfile;
}
@property(strong,nonatomic)NSMutableDictionary *dict;
@property (strong, nonatomic) IBOutlet ASStarRatingView *staticStarRatingView;
@end
