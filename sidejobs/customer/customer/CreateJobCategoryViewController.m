 //
//  CreateJobCategoryViewController.m
//  sideJobs
//
//  Created by DebutMac4 on 25/08/15.
//  Copyright (c) 2015 DebutMac4. All rights reserved.
//

#import "CreateJobCategoryViewController.h"
#import "CreateJobController.h"

@interface CreateJobCategoryViewController ()
{
    NSUserDefaults *defaults;
}

@end

@implementation CreateJobCategoryViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    defaults=[NSUserDefaults standardUserDefaults];
    
    self.navigationItem.hidesBackButton=YES;
    self.navigationItem.title=@"My SideJobs";
    UIButton *button=[UIButton buttonWithType:UIButtonTypeCustom];
    [button setFrame:CGRectMake(0, 0, 23, 21)];
    [button setImage:[UIImage imageNamed:@"arrow.png"] forState:UIControlStateNormal];
    [button addTarget:self action:@selector(leftButton:) forControlEvents:UIControlEventTouchUpInside];
    self.navigationItem.leftBarButtonItem=[[UIBarButtonItem alloc]initWithCustomView:button];

}

-(void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:YES];
    [defaults removeObjectForKey:@"location"];
    [defaults synchronize];
    
}
-(void)leftButton:(UIButton*)sender
{
    [self.navigationController popViewControllerAnimated:YES];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

- (IBAction)btn_autoAction:(id)sender {
    CreateJobController *createJobs=[self.storyboard instantiateViewControllerWithIdentifier:@"createjob"];
    [createJobs categoryId:@"2"];
    createJobs.modifyTypeStr=@"create";
    [self.navigationController pushViewController:createJobs animated:YES];
    
}

- (IBAction)btn_homeAction:(id)sender {
    CreateJobController *createJobs=[self.storyboard instantiateViewControllerWithIdentifier:@"createjob"];
    [createJobs categoryId:@"1"];
     createJobs.modifyTypeStr=@"create";
    [self.navigationController pushViewController:createJobs animated:YES];
    

}

- (IBAction)btn_electronicsAction:(id)sender {
    CreateJobController *createJobs=[self.storyboard instantiateViewControllerWithIdentifier:@"createjob"];
    [createJobs categoryId:@"3"];
     createJobs.modifyTypeStr=@"create";
    [self.navigationController pushViewController:createJobs animated:YES];
    

}
@end
