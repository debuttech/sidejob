//
//  ListingOfAllWorkersVC.h
//  SideJobsMileston2
//
//  Created by Debut Mac 4 on 02/09/15.
//  Copyright (c) 2015 debut.infotech. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface ListingOfAllWorkersVC : UIViewController <UITableViewDataSource, UITableViewDelegate>
{
    IBOutlet UITextField *searchBarTF;
    IBOutlet UIButton *btnCancelInvite_obj;
    IBOutlet UIButton *btnInvite_obj;
    IBOutlet UIButton *dropdown_btn;
    IBOutlet UIView *dropdownV;
    IBOutlet UIView *popUp_View;
    IBOutlet UIView *view_popUP;
    IBOutlet UITableView *tableView_ListingOfWorkers;
    IBOutlet UITableView *dropdownTV;
    NSMutableArray *jobs;
    BOOL isShow;
    
    IBOutlet UISearchBar *searchBar_object;
    IBOutlet UILabel *lblNoWorkers;
    IBOutlet UILabel *lbl_nojobs;
}
- (IBAction)btnCancelInvite_Action:(id)sender;
- (IBAction)dropdown_btn:(id)sender;
- (IBAction)btn_search:(id)sender;
- (IBAction)btnInvite_Action:(id)sender;
- (IBAction)cancel_dropdown:(id)sender;

@end



