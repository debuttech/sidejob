//
//  Carddetailcell.h
//  SideJobsMileston2
//
//  Created by Debut Mac 4 on 24/09/15.
//  Copyright (c) 2015 debut.infotech. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface Carddetailcell : UITableViewCell


@property (strong, nonatomic) IBOutlet UIButton *cancelbtn;


@property (strong, nonatomic) IBOutlet UILabel *carddetaill_lbl;

@property (weak, nonatomic) IBOutlet UILabel *label_accountNameWorker;

@property (strong, nonatomic) IBOutlet UIImageView *cardimage;

@property (strong, nonatomic) IBOutlet UIView *carddetail_view;

@end
