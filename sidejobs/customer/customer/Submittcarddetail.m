//
//  Submittcarddetail.m
//  SideJobsMileston2
//
//  Created by Debut Mac 4 on 24/09/15.
//  Copyright (c) 2015 debut.infotech. All rights reserved.
//

#import "Submittcarddetail.h"
#define ACCEPTABLE_CHARACTERS @"ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz "
#import <Braintree.h>
#import "Helper.h"

@interface Submittcarddetail ()
{
    NSUserDefaults *defaults;
    UIPickerView *myPickerView;
    NSMutableArray *monthsArray,*yearsArray;
    NSString *yearString1;
    NSString  *selectedStringFromPickerView;
    UIAlertView *alertView1;
    NSString *nonceRecieved;
    
    NSString *cuurentMonth;
    NSString *cuurntYear;
    
     Braintree *braintree;
}
@property(nonatomic,strong)NSNumber *selectedMonth,*selectedYear;
@end

@implementation Submittcarddetail


- (void)viewDidLoad
{
    [super viewDidLoad];
    defaults=[NSUserDefaults standardUserDefaults];
    self.navigationItem.hidesBackButton=YES;
    self.navigationItem.title=@"Add Card Details";
    UIButton *backButton=[UIButton buttonWithType:UIButtonTypeCustom];
    [backButton setFrame:CGRectMake(0, 0, 23, 21)];
    [backButton setImage:[UIImage imageNamed:@"arrow.png"] forState:UIControlStateNormal];
    [backButton addTarget:self action:@selector(leftBarButton:) forControlEvents:UIControlEventTouchUpInside];
    self.navigationItem.leftBarButtonItem=[[UIBarButtonItem alloc]initWithCustomView:backButton];
    
    
    
    monthsArray=[[NSMutableArray alloc]initWithObjects:@"January",@"February",@"March",@"April",@"May",@"June",@"July",@"August",@"September",@"October",@"November",@"December",nil];
    
    NSDateFormatter *formatter = [[NSDateFormatter alloc] init];
    [formatter setDateFormat:@"mm-yyyy"];
    yearString1 = [formatter stringFromDate:[NSDate date]];
    yearsArray=[[NSMutableArray alloc]init];
    for (int i=0; i<100; i++)
    {
        [yearsArray addObject:[NSString stringWithFormat:@"%d",[yearString1 intValue]+i]];
    }
    
    myPickerView = [[UIPickerView alloc] init];
    myPickerView.dataSource=self;
    myPickerView.delegate = self;
    myPickerView.showsSelectionIndicator = YES;
    
    UITapGestureRecognizer *tap=[[UITapGestureRecognizer alloc]initWithTarget:self action:@selector(tapGesture:)];
    tap.numberOfTapsRequired=1;
    [self.view addGestureRecognizer:tap];
    
    
    
    NSDateFormatter *formatter1 = [[NSDateFormatter alloc] init];
    formatter1.dateFormat = @"d.M.yyyy";
    NSString *string = [formatter1 stringFromDate:[NSDate date]];
    NSArray *datearr=[string componentsSeparatedByString:@"."];
    cuurentMonth=[datearr objectAtIndex:1];
    cuurntYear=[datearr objectAtIndex:2];
    
}

-(void)tapGesture:(UITapGestureRecognizer *)sender
{
    [_txtFldName resignFirstResponder];
    [_txtFldCardNumber resignFirstResponder];
    [_txtFldExpiryDtae resignFirstResponder];
    [_txtFldCVc resignFirstResponder];
    
}
-(void)leftBarButton:(UIButton*)sender
{
    [self.navigationController popViewControllerAnimated:YES];
}

-(BOOL)textField:(UITextField *)textField shouldChangeCharactersInRange:(NSRange)range replacementString:(NSString *)string
{
    
    if (textField==_txtFldName )
    {
        if (_txtFldName.text.length<1)
        {
            if ([string isEqualToString:@" "]) {
                return NO;
            }
        }
        if (_txtFldName.text.length>50)
        {
            if ([string isEqualToString:@""]) {
                return YES;
            }
            else
            {
                return NO;
            }
        }
        NSCharacterSet *cs = [[NSCharacterSet characterSetWithCharactersInString:ACCEPTABLE_CHARACTERS] invertedSet];
        NSString *filtered = [[string componentsSeparatedByCharactersInSet:cs] componentsJoinedByString:@""];
        return [string isEqualToString:filtered];
    }
    if (textField==_txtFldExpiryDtae)
    {
        return NO;
    }
    
    
    if (textField==_txtFldCVc)
    {
        NSString *textFieldText = [_txtFldCVc.text stringByReplacingCharactersInRange:range withString:string];
        if (textFieldText.length==0||[textFieldText length]<=4)
        {
            return YES;
        }
        else
            return NO;
    }
    return YES;
}


- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}


- (IBAction)submitCardDetails:(id)sender
{
  
    if (_txtFldName.text.length<1)
        {
            
            UIAlertView *alertView=[[UIAlertView alloc]initWithTitle:@"Alert!" message:@"Please enter card holder name." delegate:nil cancelButtonTitle:@"OK" otherButtonTitles:nil];
            [alertView show];
        }
        else if(_txtFldCardNumber.text.length<1)
        {
            UIAlertView *alertView=[[UIAlertView alloc]initWithTitle:@"Alert!" message:@"Please enter card number." delegate:nil cancelButtonTitle:@"OK" otherButtonTitles:nil];
            [alertView show];
        }
        
        else if(_txtFldExpiryDtae.text.length<1)
        {
            UIAlertView *alertView=[[UIAlertView alloc]initWithTitle:@"Alert!" message:@"Please enter expiry date." delegate:nil cancelButtonTitle:@"OK" otherButtonTitles:nil];
            [alertView show];
        }
        else if( _txtFldCVc.text.length<3)
        {
            UIAlertView *alertView=[[UIAlertView alloc]initWithTitle:@"Alert!" message:@"Please enter valid cvc number." delegate:nil cancelButtonTitle:@"OK" otherButtonTitles:nil];
            [alertView show];
        }
       else
        {
        [SVProgressHUD showWithStatus:@"Saving..."];
        [self performSelector:@selector(setAuthentication) withObject:nil afterDelay:0.3];
        }
}

#pragma mark
#pragma mark-varify card details
-(void)varifyCardDtails
{
    if (_txtFldName.text.length>0 && _txtFldCardNumber.text.length>0&&_txtFldExpiryDtae.text.length>0&&_txtFldCVc.text.length>0)
    {
        NSString *str=_txtFldExpiryDtae.text;
        NSString* month1;
        NSString *year1;
        if (str.length>0)
        {
            NSArray* date = [str componentsSeparatedByString: @"-"];
            month1 = [date objectAtIndex: 0];
            year1=[date objectAtIndex:1];
        }
        NSMutableDictionary *jsonDict=[[NSMutableDictionary alloc]init];
        
        [jsonDict setObject:_txtFldCardNumber.text forKey:@"number"];
        [jsonDict setObject:month1 forKey:@"expirationMonth"];
        [jsonDict setObject:year1 forKey:@"expirationYear"];
        [jsonDict setObject:_txtFldCVc.text forKey:@"cvv"];
        [jsonDict setObject:[defaults objectForKey:@"customer_id"]forKey:@"id"];
        [jsonDict setObject:_txtFldName.text forKey:@"CardholderName"];
        
        
        
        NSString *urlstr=[NSString stringWithFormat:@"%@add-card",serviceLink];
        id json1 = [WebService signup:jsonDict:urlstr];
        
        if ([[json1 objectForKey:@"message"]isEqualToString:@"Success"])
        {
            [self setAuthentication];
        }
        else
        {
            NSString *str=[NSString stringWithFormat:@"%@",[json1 valueForKeyPath:@"errors"]];
            
            NSString *msg=[[str stringByReplacingOccurrencesOfString:@"(" withString:@""] stringByReplacingOccurrencesOfString:@")" withString:@""];
            
      
            UIAlertView *alertView=[[UIAlertView alloc]initWithTitle:@"Alert!" message:msg delegate:nil cancelButtonTitle:@"OK" otherButtonTitles:nil];
            [alertView show];
            [SVProgressHUD dismiss];
        }
    }
    else
    {
        UIAlertView *alert=[[UIAlertView alloc]initWithTitle:@"" message:@"Please enter card details" delegate:nil cancelButtonTitle:@"OK" otherButtonTitles:nil];
        [alert show];
        
        [SVProgressHUD dismiss];
    }
}

-(void)setAuthentication
{
    NSString *url=[NSString stringWithFormat:@"%@createClientToken",serviceLink];
    NSMutableDictionary *dict=[[NSMutableDictionary alloc]init];
    [dict setObject:[defaults valueForKey:@"customer_id"] forKey:@"customer_id"];
    // [dict setObject:@"61904407" forKey:@"customer_id"];
    
    //  jobsArr=[[NSMutableArray alloc]init];
    if ([Helper isConnectedToInternet])
    {
        id json1=[WebService signup:dict :url];
        if ([[json1 objectForKey:@"message"]isEqualToString:@"Success"])
        {
            paymentToken=[[json1 objectForKey:@"response"] objectForKey:@"token"];
            
            
        }
        else
        {
            
        }
    }
    else
    {
        UIAlertView *alert=[[UIAlertView alloc]initWithTitle:@"No Internet" message:@"Please check intenet connection." delegate:nil cancelButtonTitle:@"OK" otherButtonTitles:nil];
        [alert show];
    }
    
    
    // Initialize `Braintree` once per checkout session
    
    if (paymentToken==nil||[paymentToken isEqualToString:@""])
    {
        UIAlertView *alert=[[UIAlertView alloc]initWithTitle:@"Alert!" message:@"Server error." delegate:nil cancelButtonTitle:@"OK" otherButtonTitles:nil];
        [alert show];
        [SVProgressHUD dismiss];
    }
    else
    {
        braintree = [Braintree braintreeWithClientToken:paymentToken];
        NSLog(@"%@",braintree);
        [self getnonce];
        
    }
    
    
}


-(void)getnonce
{
    // NSMutableArray *arr_lastcard=[cardDetails lastObject];
    //NSString *cardno=[[[cardDetails objectAtIndex:0]objectForKey:@"_attributes"] objectForKey:@"maskedNumber"];
    //    if (cardDetails.count)
    //    {
    NSString *str=_txtFldExpiryDtae.text;
    NSString* month1;
    NSString *year1;
    
    
    if (str.length>0)
    {
        NSArray* date = [str componentsSeparatedByString: @"-"];
        month1 = [date objectAtIndex: 0];
        year1=[date objectAtIndex:1];
    }
    

    
    
    BTClientCardRequest *request = [[BTClientCardRequest alloc] init];
    request.number =_txtFldCardNumber.text ;
    request.expirationMonth = month1;
    request.expirationYear =year1;
    //request.cvv=@"123";
    
    
    
    [self->braintree tokenizeCard:request completion:^(NSString *nonce, NSError *error)
     {
         if (error)
         {
             //self.progressBlock([NSString stringWithFormat:@"Error: %@", error]);
             [[[UIAlertView alloc] initWithTitle:@"Error"
                                         message:[error localizedDescription]
                                        delegate:nil
                               cancelButtonTitle:@"OK"
                               otherButtonTitles:nil] show];
         }
         
         
         if (nonce)
         {
             nonceRecieved=nonce;
             NSLog(@"Nonce %@",nonce);
             [self passNounceToServer];
             // [self varifyCard];
             //            self.progressBlock([NSString stringWithFormat:@"Card tokenized -> Nonce Received: %@", nonce]);
             //             self.completionBlock(nonce);
         }
     }];
    
}

#pragma mark
#pragma mark- Create payement Method......********....................
-(void)passNounceToServer
{
    //  NSString *token=[NSString stringWithFormat:@"%@",[defaults objectForKey:@"user_token"]];
    
    NSString *url=[NSString stringWithFormat:@"%@createPaymentMethod",serviceLink];
    NSMutableDictionary *dict=[[NSMutableDictionary alloc]init];
    
    [dict setObject:[defaults valueForKey:@"customer_id"] forKey:@"customerId"];
    [dict setObject:nonceRecieved forKey:@"nonce"];
    
    id json1=[WebService signup:dict :url];
    
    if ([[json1 objectForKey:@"message"]isEqualToString:@"Success"])
    {
        NSLog(@"%@",json1);
        UIAlertView *alert=[[UIAlertView alloc]initWithTitle:@"" message:@"Your card details are saved successfully!" delegate:self cancelButtonTitle:@"OK" otherButtonTitles:nil];
        alert.tag=1;
        
        [alert show];

    }
    else
    {
        NSLog(@"%@",json1);
        UIAlertView *alert=[[UIAlertView alloc]initWithTitle:@"" message:@"Please enter the correct card details." delegate:nil cancelButtonTitle:@"OK" otherButtonTitles:nil];
        [alert show];
    }
    
       [SVProgressHUD dismiss];
    
}


//-(void)savecard
//{
//
//    if (_txtFldName.text.length>0 && _txtFldCardNumber.text.length>0&&_txtFldExpiryDtae.text.length>0&&_txtFldCVc.text.length>0)
//    {
//        
//        NSString *str=_txtFldExpiryDtae.text;
//        NSString* month1;
//        NSString *year1;
//        if (str.length>0)
//        {
//            NSArray* date = [str componentsSeparatedByString: @"-"];
//            month1 = [date objectAtIndex: 0];
//            year1=[date objectAtIndex:1];
//        }
//        
//        
//        NSMutableDictionary *jsonDict=[[NSMutableDictionary alloc]init];
//        
//        [jsonDict setObject:_txtFldCardNumber.text forKey:@"number"];
//        [jsonDict setObject:month1 forKey:@"expirationMonth"];
//        [jsonDict setObject:year1 forKey:@"expirationYear"];
//        [jsonDict setObject:_txtFldCVc.text forKey:@"cvv"];
//        [jsonDict setObject:[defaults objectForKey:@"customer_id"]forKey:@"id"];
//        [jsonDict setObject:_txtFldName.text forKey:@"CardholderName"];
//        
//        
//        NSString *urlstr=[NSString stringWithFormat:@"%@add-card",serviceLink];
//        id json = [WebService signup:jsonDict:urlstr];
//        
//        if ([[json objectForKey:@"message"]isEqualToString:@"Success"])
//        {
//            
//            alertView1=[[UIAlertView alloc]initWithTitle:@"" message:@"You have successfully added new card" delegate:self cancelButtonTitle:@"OK" otherButtonTitles:nil];
//            [alertView1 show];
//            
//            
//        }
//        else
//        {
//            UIAlertView *alertView=[[UIAlertView alloc]initWithTitle:@"" message:@"Please enter correct card details" delegate:nil cancelButtonTitle:@"OK" otherButtonTitles:nil];
//            [alertView show];
//        }
//        
//    }
//    
//    else
//    {
//        UIAlertView *alert=[[UIAlertView alloc]initWithTitle:@"" message:@"Please enter correct card details" delegate:nil cancelButtonTitle:@"OK" otherButtonTitles:nil];
//        [alert show];
//    }
//    
//    [SVProgressHUD dismiss];
//}

#pragma mark
#pragma mark-alertView Delegate method
- (void)alertView:(UIAlertView *)alertView clickedButtonAtIndex:(NSInteger)buttonIndex
{
   
    if (alertView.tag==1)
    {
        switch (buttonIndex)
        {
            case 0:
                [self.navigationController popViewControllerAnimated:YES];
                break;
                
            default:
                break;
        }
    }else{
        
    }
    
}

#pragma mark popup datepicker in keyboard...........start.......................
-(void)textFieldDidBeginEditing:(UITextField *)textField
{
    if (textField==_txtFldExpiryDtae)
    {
        UIToolbar *bar=[[UIToolbar alloc]initWithFrame:CGRectMake(0, 0, self.view.bounds.size.width,40)];
        UIBarButtonItem *item=[[UIBarButtonItem alloc]initWithTitle:@"Done" style:UIBarButtonItemStyleDone target:self action:@selector(Done:)];
        NSArray *arr=@[item];
        [bar setItems:arr];
        _txtFldExpiryDtae.inputView=myPickerView;
        _txtFldExpiryDtae.inputAccessoryView=bar;
    }
}
#pragma txtfield delegate method
#pragma mark
- (BOOL)textFieldShouldReturn:(UITextField *)textField
{
    if (textField == _txtFldName) {
        [_txtFldName resignFirstResponder];
        [_txtFldCardNumber becomeFirstResponder];
    }
    else if (textField == _txtFldCardNumber) {
        [_txtFldCardNumber resignFirstResponder];
        [_txtFldExpiryDtae becomeFirstResponder];
    }
    else if (textField == _txtFldExpiryDtae) {
        [_txtFldExpiryDtae resignFirstResponder];
        [_txtFldCVc becomeFirstResponder];
    }
    else if (textField == _txtFldCVc) {
        [_txtFldCVc resignFirstResponder];
    }
    
    
    return YES;
}


-(void)Done:(id)sender
{
    [_txtFldExpiryDtae resignFirstResponder];
}


#pragma mark
#pragma UIPicker for show date........................................
- (NSInteger)numberOfComponentsInPickerView:(UIPickerView *)pickerView
{
    return 2;
}
- (NSInteger)pickerView:(UIPickerView *)pickerView numberOfRowsInComponent:(NSInteger)component
{
    if (component==0)
    {
        return monthsArray.count;
    }else{
        return yearsArray.count;
    }
    return 0;
}

- (NSString *)pickerView:(UIPickerView *)pickerView titleForRow:(NSInteger)row forComponent:(NSInteger)component
{
    NSString *returningString;
    if (component == 0) {
        //Expiration month
        //  return monthArray[row];
        returningString=monthsArray[row];
    }
    else {
        //Expiration year
        NSDateFormatter *dateFormatter = [[NSDateFormatter alloc] init];
        [dateFormatter setDateFormat:@"yyyy"];
        NSInteger currentYear = [[dateFormatter stringFromDate:[NSDate date]] integerValue];
        returningString=[NSString stringWithFormat:@"%ld", currentYear + row];
    }
    
    return returningString;
    
}
- (void)pickerView:(UIPickerView *)pickerView didSelectRow:(NSInteger)row inComponent:(NSInteger)component
{
//    if (component == 0)
//    {
//        
//     if ([cuurentMonth integerValue]<=row+1)
//     {
//     self.selectedMonth = @(row + 1);
//        
//     }
//     else
//     {
//            self.selectedMonth = nil;
//     }
//    }
//    
//    else
//    {
//        
//        NSString *yearString = [self pickerView:pickerView titleForRow:row forComponent:1];
//        self.selectedYear = @([yearString integerValue]);
//    }
//    
//    if (!self.selectedMonth) {
//        [pickerView selectRow:[cuurentMonth integerValue]-1 inComponent:0 animated:YES];
//        NSNumberFormatter *f = [[NSNumberFormatter alloc] init];
//        f.numberStyle = NSNumberFormatterDecimalStyle;
//        self.selectedMonth = [f numberFromString:cuurentMonth];
//    }
//    
//    if (!self.selectedYear) {
//        [pickerView selectRow:0 inComponent:1 animated:YES];
//        NSString *yearString = [self pickerView:pickerView titleForRow:0 forComponent:1];
//        self.selectedYear = @([yearString integerValue]); //Default to current year if no selection
//    }
//    selectedStringFromPickerView=[NSString stringWithFormat:@"%@/%@", self.selectedMonth, self.selectedYear];
//    [self set];
    
    
    if (component == 0) {
        self.selectedMonth = @(row + 1);
        //   month=[monthsArray objectAtIndex:self.selectedMonth];
    }
    else {
        NSString *yearString = [self pickerView:pickerView titleForRow:row forComponent:1];
        self.selectedYear = @([yearString integerValue]);
    }
    if (!self.selectedMonth) {
        [pickerView selectRow:0 inComponent:0 animated:YES];
        self.selectedMonth = @(1); //Default to January if no selection
    }
    
    if (!self.selectedYear) {
        [pickerView selectRow:0 inComponent:1 animated:YES];
        NSString *yearString = [self pickerView:pickerView titleForRow:0 forComponent:1];
        self.selectedYear = @([yearString integerValue]); //Default to current year if no selection
    }
    selectedStringFromPickerView=[NSString stringWithFormat:@"%@/%@", self.selectedMonth, self.selectedYear];
    [self set];

}

-(void)set
{
    if ([self.selectedYear compare:[NSNumber numberWithInt:[yearString1 intValue]]] == NSOrderedDescending)
    {
        NSLog(@"date1 is later than date2");
    }
    _txtFldExpiryDtae.text=[NSString stringWithFormat:@"%@-%@",self.selectedMonth,self.selectedYear];
}

- (CGFloat)pickerView:(UIPickerView *)pickerView widthForComponent:(NSInteger)component
{
    if (component == 0)
    {
        return (self.view.frame.size.width * 55 ) / 100  ;
    }
    else
    {
        return (self.view.frame.size.width * 30 ) / 100  ;
    }
}

@end
