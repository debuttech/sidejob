//
//  ProfileViewController.m
//  sideJobs
//
//  Created by DebutMac4 on 24/08/15.
//  Copyright (c) 2015 DebutMac4. All rights reserved.
//

#import "ProfileViewController.h"
#import "WebService.h"
#import "Carddetail.h"
#import "UIImageView+WebCache.h"
#import "AppDelegate.h"
#import "AccountDetailsViewController.h"

@interface ProfileViewController ()
{
    NSUserDefaults *defaults;
    NSString *userId;
    UIVisualEffect *blurEffect;
    UIVisualEffectView *visualEffectView;

}
@property (nonatomic, strong) QBPopupMenu *popupMenu;

@end

@implementation ProfileViewController
@synthesize staticStarRatingView;

- (void)viewDidLoad {
    [super viewDidLoad];
    
     defaults=[NSUserDefaults standardUserDefaults];
    
    staticStarRatingView.canEdit = YES;
    staticStarRatingView.maxRating = 5;
    staticStarRatingView.minAllowedRating = 0;
    staticStarRatingView.maxAllowedRating = 5;
   
    NSString *rating=[defaults valueForKey:@"rating_user"];
    NSLog(@"raadhafdhafda%@",rating);
    staticStarRatingView.rating =[rating floatValue];

    
    btn_cross.hidden=YES;
    viewPopUpInside.layer.masksToBounds=NO;
    viewPopUpInside.layer.cornerRadius=7.0;
    viewPopUpInside.layer.borderColor=[[UIColor grayColor]CGColor];
    viewPopUpInside.layer.borderWidth=0.4f;
    
    NSLog(@"my earning %@",[defaults objectForKey:@"myearning"]);
    lbl_Price.text=[defaults objectForKey:@"myearning"];
    
    self.navigationController.navigationBar.tintColor=[UIColor colorWithRed:70 green:70 blue:70 alpha:1.0];
    UIButton *rightButton=[UIButton buttonWithType:UIButtonTypeCustom];
    [rightButton setFrame:CGRectMake(0, 0, 23, 23)];
    [rightButton setImage:[UIImage imageNamed:@"arrow1.png"] forState:UIControlStateNormal];
    [rightButton addTarget:self action:@selector(DashBarButton:) forControlEvents:UIControlEventTouchUpInside];
    self.navigationItem.rightBarButtonItem=[[UIBarButtonItem alloc]initWithCustomView:rightButton];
   
    [self.navigationController.navigationBar setBackgroundImage:[UIImage imageNamed:@"top_bar2.png"] forBarMetrics:UIBarMetricsDefault];
    
    self.navigationItem.backBarButtonItem = [[UIBarButtonItem alloc] initWithTitle:@"" style:self.navigationItem.backBarButtonItem.style target:nil action:nil];
   
    NSDictionary *size = [NSDictionary dictionaryWithObjectsAndKeys:[UIFont fontWithName:@"HelveticaNeue-Light" size:20.0],NSFontAttributeName, [UIColor whiteColor],NSForegroundColorAttributeName, nil];
    self.navigationController.navigationBar.titleTextAttributes = size;
    self.navigationItem.title=@"Account Settings";
    
    [[UINavigationBar appearance]setBackgroundColor:[UIColor colorWithRed:71.0f green:71.0f blue:71.0f alpha:1.0]];

   // [scrollView_Obj setContentSize:CGSizeMake(scrollView_Obj.frame.size.width, 500)];
    [self showUserDetails];
    UITapGestureRecognizer *tap=[[UITapGestureRecognizer alloc]initWithTarget:self action:@selector(tapGesture:)];
    tap.numberOfTapsRequired=1;
    [self.view addGestureRecognizer:tap];
    
        imageView_object.layer.masksToBounds=YES;
        imageView_object.layer.cornerRadius=2;
        imageView_object.layer.borderWidth=0.1;
        imageView_object.layer.borderColor=[[UIColor clearColor]CGColor];
       // imageView_object.contentMode =  UIViewContentModeScaleAspectFit;
    
    
    btn_edit_object.frame=CGRectMake(0, self.view.bounds.size.height-58, self.view.bounds.size.width, 58);
    
    if ([[defaults objectForKey:@"user_status"]isEqualToString:@"customer"])
    {
        [button_DetailCard setTitle:@"Enter Card Details" forState:UIControlStateNormal];
        if (self.view.bounds.size.width==320)
        {
            scrollView_Obj.contentSize=CGSizeMake(320, 680);
            
        }else  if (self.view.bounds.size.width==375)
        {
            scrollView_Obj.contentSize=CGSizeMake(375, 800);
        }
        else  if (self.view.bounds.size.width==414) {
            //  mainSV.contentSize=CGSizeMake(375, 520);
        }
    }
    else
    {
        [button_DetailCard setTitle:@"Enter Account Details" forState:UIControlStateNormal];
        
        if (self.view.bounds.size.width==320)
        {
            scrollView_Obj.contentSize=CGSizeMake(320, 750);
             
        }else  if (self.view.bounds.size.width==375) {
            scrollView_Obj.contentSize=CGSizeMake(375, 800);
        }
        else  if (self.view.bounds.size.width==414)
        {
        }
    }
}



-(void)tapGesture:(UITapGestureRecognizer *)sender
{
    [txtF_name resignFirstResponder];
    [txtF_email resignFirstResponder];
    [txtF_about resignFirstResponder];
    [textView_About resignFirstResponder];
    [txtF_phone resignFirstResponder];
    [txtF_about resignFirstResponder];
}

-(void)DashBarButton:(id)sender
{
    UIWindow *wnd = [[[UIApplication sharedApplication] delegate] window];
    [UIView transitionWithView:wnd
                      duration:0.5
                       options:UIViewAnimationOptionTransitionFlipFromLeft
                    animations:^{
                        
                        UIViewController *root = [[UIStoryboard storyboardWithName:@"Main" bundle:NULL] instantiateViewControllerWithIdentifier:@"rootController"];
                        [wnd makeKeyAndVisible];
                        wnd.rootViewController = root;
                    }
                    completion:nil];

}
-(void)leftBarButton:(id)sender
{
    [self.navigationController popToRootViewControllerAnimated:YES];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

#pragma mark
#pragma mark- Take picture from camera
-(void)TakePictureWithCamera
{
    if (![UIImagePickerController isSourceTypeAvailable:UIImagePickerControllerSourceTypeCamera]) {
        
        UIAlertView *myAlertView = [[UIAlertView alloc] initWithTitle:@"Error"
                                                              message:@"Device has no camera"
                                                             delegate:nil
                                                    cancelButtonTitle:@"OK"
                                                    otherButtonTitles: nil];
        [myAlertView show];
    }else
    {
        UIImagePickerController *picker = [[UIImagePickerController alloc] init];
        picker.allowsEditing = YES;
         picker.delegate = self;
        picker.sourceType = UIImagePickerControllerSourceTypeCamera;
        [self presentViewController:picker animated:YES completion:nil];
    }
}

- (void)selectPhoto{
    
    UIImagePickerController *picker = [[UIImagePickerController alloc] init];
    picker.delegate = self;
    picker.allowsEditing = YES;
    picker.sourceType = UIImagePickerControllerSourceTypePhotoLibrary;
    
    [self presentViewController:picker animated:YES completion:NULL];
}
- (void)imagePickerController:(UIImagePickerController *)picker didFinishPickingMediaWithInfo:(NSDictionary *)info {
    
    UIImage *chosenImage = info[UIImagePickerControllerEditedImage];
    chosenImage=[ImageUtilitiesClass scaleAndRotateImage:chosenImage];
    
    [SVProgressHUD showWithStatus:@"Uploading Image..."];
    [self performSelector:@selector(uploadProfileImage:) withObject:chosenImage afterDelay:0.3f];
   // [self setSelectedImage:chosenImage];
    [picker dismissViewControllerAnimated:YES completion:NULL];
}

#pragma mark
#pragma mark-update profile image
- (IBAction)editImageBtn_Action:(id)sender{
    
    UIActionSheet *popup = [[UIActionSheet alloc] initWithTitle:@"Profile Photo Action:" delegate:self cancelButtonTitle:@"Cancel" destructiveButtonTitle:nil otherButtonTitles:
                            @"Capture Image",
                            @"Pick from library",@"Delete Profile Picture",
                            nil];
    popup.tag = 1;
    [popup showInView:self.view];
    

}

#pragma mark
#pragma mark-actionsheet delegate method
- (void)actionSheet:(UIActionSheet *)popup clickedButtonAtIndex:(NSInteger)buttonIndex
{
    switch (buttonIndex) {
        case 0:
            [self TakePictureWithCamera];
            break;
            case 1:
            [self selectPhoto];
              break;
            case 2:
            [self deleteProfilePic];
              break;
        default:
            break;
    }}

#pragma mark
#pragma mark-delete profile image
-(void)deleteProfilePic
{
    NSString *token=[NSString stringWithFormat:@"%@",[defaults objectForKey:@"user_token"]];
    NSString *url=[NSString stringWithFormat:@"%@delete-image/%@?token=%@",serviceLink,[defaults valueForKey:@"user_id"],token];
    
    id jsons=[WebService deleteData:url];
    
    if ([[jsons objectForKey:@"message"]isEqualToString:@"Success"]) {
        [SVProgressHUD dismiss];
        [defaults setObject:@"" forKey:@"p_photo"];
        [defaults synchronize];
        [imageView_object setImage:nil];
        [self.navigationController popViewControllerAnimated:YES];
    }
    
    
    else if ([[jsons objectForKey:@"message"]isEqualToString:@"token_expired"])
    {
        [SVProgressHUD dismiss];
        UIAlertView *alert=[[UIAlertView alloc]initWithTitle:@"" message:@"Session Expired please login again" delegate:nil cancelButtonTitle:@"OK" otherButtonTitles: nil];
        [alert show];
        AppDelegate *delegate;
        delegate=(AppDelegate*)[[UIApplication sharedApplication]delegate];
        [defaults setBool:NO forKey:@"logined"];
        [defaults synchronize];
        [[NSUserDefaults standardUserDefaults]synchronize];
        [delegate loginStatus];
    }
    
    
    else
    {
        [SVProgressHUD dismiss];
    }
    if (imageView_object.image==nil)
    {
        imageView_object.image=[UIImage imageNamed:@"defuser.png"];
    }
    

}
- (IBAction)btn_crossAction:(id)sender
{
    
   // http://112.196.27.243:8000/delete-image
    
//    NSString *token=[NSString stringWithFormat:@"%@",[defaults objectForKey:@"user_token"]];
//    NSString *url=[NSString stringWithFormat:@"%@delete-image/%@?token=%@",serviceLink,[defaults valueForKey:@"user_id"],token];
//    
//    id jsons=[WebService deleteData:url];
//    
//    if ([[jsons objectForKey:@"message"]isEqualToString:@"Success"]) {
//        [SVProgressHUD dismiss];
//        btn_cross.hidden=YES;
//        [defaults setObject:@"" forKey:@"p_photo"];
//        [defaults synchronize];
//        [imageView_object setImage:nil];
//        [self.navigationController popViewControllerAnimated:YES];
//    }
//    else if ([[jsons objectForKey:@"message"]isEqualToString:@"token_expired"])
//    {
//        [SVProgressHUD dismiss];
//        UIAlertView *alert=[[UIAlertView alloc]initWithTitle:@"" message:@"Session Expired please login again" delegate:nil cancelButtonTitle:@"OK" otherButtonTitles: nil];
//        [alert show];
//        AppDelegate *delegate;
//        delegate=(AppDelegate*)[[UIApplication sharedApplication]delegate];
//        [defaults setBool:NO forKey:@"logined"];
//        [defaults synchronize];
//        [[NSUserDefaults standardUserDefaults]synchronize];
//        [delegate loginStatus];
//    }else{
//        [SVProgressHUD dismiss];
//    }
//    

    
}
#pragma mark
#pragma mark -Add Card detail
- (IBAction)btnAddCardDetails:(id)sender
{
 
    UIBezierPath *shadowPath = [UIBezierPath bezierPathWithRect:viewPopUp.bounds];
    viewPopUp.layer.masksToBounds = NO;
    viewPopUp.layer.shadowColor = [UIColor grayColor].CGColor;
    viewPopUp.layer.shadowOffset = CGSizeMake(0.0f, 0.0f);
    viewPopUp.layer.shadowOpacity = 0.9f;
    viewPopUp.layer.shadowPath = shadowPath.CGPath;
    viewPopUp.backgroundColor=[UIColor clearColor];
    viewPopUp.alpha=1.0;
   
    [UIView transitionWithView:viewPopUp
                      duration:0.75
                       options:UIViewAnimationOptionTransitionNone
                    animations:^{
                        [viewPopUp setFrame:CGRectMake(0, viewPopUp.frame.origin.y, viewPopUp.frame.size.width, viewPopUp.frame.size.height)];
                        [self.view.window addSubview:viewPopUp];
                                         }
                    completion:nil];
    //[self.view.window addSubview:viewPopUp];
}

- (IBAction)btnConfirm:(id)sender
{
    [txtFldConfirmPassword resignFirstResponder];
    NSString *str=txtFldConfirmPassword.text;
    NSString *userPasword=[defaults objectForKey:@"userPassword"];
    if ([userPasword isEqualToString:str])
    {
        txtFldConfirmPassword.text=@"";

        [viewPopUp setFrame:CGRectMake(1000, viewPopUp.frame.origin.y, viewPopUp.frame.size.width, viewPopUp.frame.size.height)];
        
        if ([[defaults objectForKey:@"user_status"]isEqualToString:@"customer"])
        {
            Carddetail *CardView=[self.storyboard instantiateViewControllerWithIdentifier:@"Carddetail"];
            [self.navigationController pushViewController:CardView animated:YES];

        }
        else
        {
            AccountDetailsViewController *accountDetails = [self.storyboard instantiateViewControllerWithIdentifier:@"accountDetailsViewController"];
            [self.navigationController pushViewController:accountDetails animated:YES];
        }
    }
    else
    {
       txtFldConfirmPassword.text=@"";
        UIAlertView *alert=[[UIAlertView alloc]initWithTitle:@"Alert!" message:@"Please enter a correct password." delegate:nil cancelButtonTitle:@"OK" otherButtonTitles:nil];
        [alert  show];
    }
}

- (IBAction)btnCancel:(id)sender
{
    [txtFldConfirmPassword resignFirstResponder];
    txtFldConfirmPassword.text=@"";
    [UIView transitionWithView:viewPopUp
                      duration:0.75
                       options:UIViewAnimationOptionTransitionNone
                    animations:^{
                        [viewPopUp setFrame:CGRectMake(1000, viewPopUp.frame.origin.y, viewPopUp.frame.size.width, viewPopUp.frame.size.height)];
                        //  [self.view.window addSubview:viewPopUp];
                        
                    }
                    completion:nil];

}

-(void)uploadProfileImage:(UIImage*)img
{
    NSMutableURLRequest *request = [[NSMutableURLRequest alloc] initWithURL:[NSURL URLWithString:[NSString stringWithFormat:@"%@uploadprofileimage/%@",serviceLink,[defaults objectForKey:@"user_id"]]]];
    
    NSData *imageData = UIImageJPEGRepresentation(img, 1.0);
    [request setCachePolicy:NSURLRequestReloadIgnoringLocalCacheData];
    [request setHTTPShouldHandleCookies:NO];
    [request setTimeoutInterval:60];
    [request setHTTPMethod:@"POST"];
    NSString *boundary = @"unique-consistent-string";
    // set Content-Type in HTTP header
    NSString *contentType = [NSString stringWithFormat:@"multipart/form-data; boundary=%@", boundary];
    [request setValue:contentType forHTTPHeaderField: @"Content-Type"];
    // post body
    NSMutableData *body = [NSMutableData data];
    // add params (all params are strings)
    [body appendData:[[NSString stringWithFormat:@"--%@\r\n", boundary] dataUsingEncoding:NSUTF8StringEncoding]];
    [body appendData:[[NSString stringWithFormat:@"Content-Disposition: form-data; name=%@\r\n\r\n", @"imageCaption"] dataUsingEncoding:NSUTF8StringEncoding]];
    [body appendData:[[NSString stringWithFormat:@"%@\r\n", @"Some Caption"] dataUsingEncoding:NSUTF8StringEncoding]];
    // add image data
    if (imageData) {
        [body appendData:[[NSString stringWithFormat:@"--%@\r\n", boundary] dataUsingEncoding:NSUTF8StringEncoding]];
        [body appendData:[[NSString stringWithFormat:@"Content-Disposition: form-data; name=%@; filename=imageName.jpg\r\n", @"photo"] dataUsingEncoding:NSUTF8StringEncoding]];
        [body appendData:[@"Content-Type: image/jpeg\r\n\r\n" dataUsingEncoding:NSUTF8StringEncoding]];
        [body appendData:imageData];
        [body appendData:[[NSString stringWithFormat:@"\r\n"] dataUsingEncoding:NSUTF8StringEncoding]];
    }
    [body appendData:[[NSString stringWithFormat:@"--%@--\r\n", boundary] dataUsingEncoding:NSUTF8StringEncoding]];
    // setting the body of the post to the reqeust
    [request setHTTPBody:body];
    // set the content-length
    NSString *postLength = [NSString stringWithFormat:@"%lu", (unsigned long)[body length]];
    [request setValue:postLength forHTTPHeaderField:@"Content-Length"];
    [NSURLConnection sendAsynchronousRequest:request queue:[NSOperationQueue currentQueue] completionHandler:^(NSURLResponse *response, NSData *data, NSError *error) {
        if(data.length > 0)
        {
            //success
            NSMutableString *responseStr = [[NSMutableString alloc] initWithData:data encoding:NSUTF8StringEncoding];
            NSMutableDictionary *dict = [[NSMutableDictionary alloc]init];
            dict = [responseStr JSONValue];
            NSLog(@"%@",dict);
            NSString *proPic=[NSString stringWithFormat:@"%@",[dict objectForKey:@"link"]];
            if ([[dict objectForKey:@"message"]isEqualToString:@"Success"])
            {
                [SVProgressHUD dismiss];
//               [imageView_object setImage:img];
                imageView_object.image=img;
              //  btn_cross.hidden=NO;
                [defaults setObject:proPic forKey:@"p_photo"];
                [defaults synchronize];
                UIAlertView *alert=[[UIAlertView alloc]initWithTitle:@"" message:@"Image uploaded successfully" delegate:self cancelButtonTitle:@"OK" otherButtonTitles: nil];
                [alert show];
        
            }
            else if ([[dict objectForKey:@"message"]isEqualToString:@"Error :: User Image Not Found"])
            {
                [SVProgressHUD dismiss];
                UIAlertView *alert=[[UIAlertView alloc]initWithTitle:@"" message:@"Please select the profile image" delegate:self cancelButtonTitle:@"OK" otherButtonTitles: nil];
                [alert show];
            }
            
        }
    }];
}
- (BOOL)textFieldShouldReturn:(UITextField *)textField
{
    [txtF_name resignFirstResponder];
    [txtF_about resignFirstResponder];
    [txtF_email resignFirstResponder];
    [txtF_phone resignFirstResponder];
    [txtFldConfirmPassword resignFirstResponder];
    return YES;
}
#pragma mark
#pragma show user details..........................
-(void)showUserDetails
{
    [defaults  objectForKey:@"user_id"];
    [defaults objectForKey:@"p_name"];
    [defaults  objectForKey:@"p_email"];
    [defaults objectForKey:@"p_phone"];
    [defaults objectForKey:@"p_about"];
    [defaults objectForKey:@"p_photo"];
    [defaults synchronize];
    NSString *imageUrl =[NSString stringWithFormat:@"%@", [defaults objectForKey:@"p_photo"]];
        
                            if ([imageUrl isEqualToString:serviceLink])
                            {
                                imageView_object.image=[UIImage imageNamed:@"defuser.png"];
                            }
                            else if ([imageUrl isEqualToString:@""])
                            {
                                imageView_object.image=[UIImage imageNamed:@"defuser.png"];
                            }
                            else if(imageUrl !=nil)
                            {
                                [imageView_object sd_setImageWithURL:[NSURL URLWithString:imageUrl] placeholderImage:[UIImage imageNamed:@"defuser.png"]];
                                
                            }
                            NSString *str_About=[NSString stringWithFormat:@"%@",[defaults objectForKey:@"p_about"]];
                            NSLog(@"About is..%@",[defaults objectForKey:@"p_about"]);
                            
                            if ([str_About isEqual:nil] ||[str_About isEqualToString:@""])
                            {
                               textView_About.text=@"About Me";
                            }
                            
                            else
                            {
                             textView_About.text=[defaults objectForKey:@"p_about"];
                            }
                            
                            txtF_name.text= [defaults objectForKey:@"p_name"];
                            txtF_email.text= [defaults  objectForKey:@"p_email"];
                            //txtF_about.text=[defaults objectForKey:@"p_about"];
                            
                            
                            txtF_phone.text= [defaults objectForKey:@"p_phone"];
                            NSString *rating=[defaults objectForKey:@"rating_user"];
                            NSString *userStatus=[defaults valueForKey:@"user_status"];
                            if ([userStatus isEqualToString:@"customer"]) {
                                view_ratingBackground.hidden=YES;
                                [view_fieldBackground setFrame:CGRectMake(view_fieldBackground.frame.origin.x, view_fieldBackground.frame.origin.y-80, view_fieldBackground.frame.size.width, view_fieldBackground.frame.size.height)];
                            }else{
                                
                            }
    
    
    
    
//    else
//    {
   /* [NSURLConnection sendAsynchronousRequest:[NSURLRequest requestWithURL:[NSURL URLWithString:imageUrl]] queue:[NSOperationQueue mainQueue] completionHandler:^(NSURLResponse *response, NSData *data, NSError *error) {
        
        if ([imageUrl isEqualToString:serviceLink])
        {
            imageView_object.image=[UIImage imageNamed:@"defuser.png"];
        }
        else if ([imageUrl isEqualToString:@""])
        {
           imageView_object.image=[UIImage imageNamed:@"defuser.png"];
        }
        else if(imageUrl !=nil)
        {
            imageView_object.image = [UIImage imageWithData:data];

        }
        txtF_name.text= [defaults objectForKey:@"p_name"];
        txtF_email.text= [defaults  objectForKey:@"p_email"];
        txtF_about.text=[defaults objectForKey:@"p_about"];
        txtF_phone.text= [defaults objectForKey:@"p_phone"];
        NSString *rating=[defaults objectForKey:@"rating_user"];
        NSString *userStatus=[defaults valueForKey:@"user_status"];
        if ([userStatus isEqualToString:@"customer"]) {
            view_ratingBackground.hidden=YES;
            [view_fieldBackground setFrame:CGRectMake(view_fieldBackground.frame.origin.x, view_fieldBackground.frame.origin.y-80, view_fieldBackground.frame.size.width, view_fieldBackground.frame.size.height)];
        }else{
            
        }
        
//        if (rating.length>0)
//        {
//            NSLog(@"rating");
//           
//        }
//        else
//        {
//            view_ratingBackground.hidden=YES;
//            [view_fieldBackground setFrame:CGRectMake(view_fieldBackground.frame.origin.x, view_fieldBackground.frame.origin.y-80, view_fieldBackground.frame.size.width, view_fieldBackground.frame.size.height)];
//        }
        
    }];*/
        
   // }
}
#pragma end.....................................................
-(BOOL)textField:(UITextField *)textField shouldChangeCharactersInRange:(NSRange)range replacementString:(NSString *)string {
    
    if (textField==txtF_phone )
    {
        if (txtF_phone.text.length<1)
        {
            if ([string isEqualToString:@" "]) {
                return NO;
            }
        }
        if (txtF_phone.text.length>16)
        {
            if ([string isEqualToString:@""]) {
                return YES;
            }
            else
            {
                return NO;
            }
        }
        
//        if (textField==txtF_about)
//        {
//            if (txtF_about.text.length<1)
//            {
//                if ([string isEqualToString:@" "]) {
//                    return NO;
//                }
//            }
//            if (txtF_about.text.length>100)
//            {
//                if ([string isEqualToString:@""]) {
//                    return YES;
//                }
//                else
//                {
//                    return NO;
//                }
//            }
//        }
    }
    return YES;
  
}
#pragma mark
#pragma mark - textView delegate method

- (void)textViewDidBeginEditing:(UITextView *)textView
{
    
    if ([textView_About.text isEqualToString:@"About Me"])
    {
        textView_About.text=@"";
    }
}


- (BOOL)textView:(UITextView *)textView shouldChangeTextInRange:(NSRange)range replacementText:(NSString *)text
{
    
    if (textView==textView_About)
    {
        
        
        if (textView_About.text.length<1)
        {
            if ([text isEqualToString:@" "]) {
                return NO;
            }
        }
        if (textView_About.text.length>100)
        {
            if ([text isEqualToString:@""]) {
                return YES;
            }
            else
            {
                return NO;
            }
        }
    }

    if([text isEqualToString:@"\n"]) {
        [textView resignFirstResponder];
        return NO;
    }

    return YES;
}
#pragma mark
#pragma update user profile...............................
- (IBAction)btnEdit_Action:(id)sender
{
    if (!btn_edit_object.selected)
    {
       btn_edit_object.selected=YES;
       txtF_name.userInteractionEnabled=NO;
      // txtF_email.userInteractionEnabled=YES;
      // txtF_about.userInteractionEnabled=YES;
        textView_About.userInteractionEnabled=YES;
       txtF_phone.userInteractionEnabled=YES;
        EditImageBtn.userInteractionEnabled=YES;
       [btn_edit_object setTitle:@"Save Profile" forState:UIControlStateNormal];
        if (imageView_object.image!=nil)
        {
           // btn_cross.hidden=NO;
        }
    }
    else if(btn_edit_object.selected)
    {
        [SVProgressHUD show];
    NSMutableDictionary *jsonDict=[[NSMutableDictionary alloc]init];
    [jsonDict setValue:txtF_name.text forKey:@"name"];
    [jsonDict setValue:txtF_email.text forKey:@"email"];
    [jsonDict setValue:@"" forKey:@"password"];
    [jsonDict setValue:txtF_phone.text forKey:@"phone"];
    [jsonDict setValue:@"" forKey:@"address"];
    [jsonDict setValue:@"" forKey:@"alternate_email"];
    [jsonDict setValue:@"" forKey:@"pincode"];
    [jsonDict setValue:@"" forKey:@"code"];
   // [jsonDict setValue:txtF_about.text forKey:@"about"];
    [jsonDict setValue:textView_About.text forKey:@"about"];
        
   NSString *str=[NSString stringWithFormat:@"%@edit-user/%@?token=%@",serviceLink,[defaults objectForKey:@"user_id"],[defaults objectForKey:@"user_token"]];
        
    json = [WebService signup:jsonDict:str];
    
    if ([[json objectForKey:@"message"]isEqualToString:@"Success"])
    {
        txtF_name.userInteractionEnabled=NO;
        txtF_email.userInteractionEnabled=NO;
        
        //txtF_about.userInteractionEnabled=NO;
        textView_About.userInteractionEnabled=NO;
        txtF_phone.userInteractionEnabled=NO;
        
      //  EditImageBtn.userInteractionEnabled=NO;
        [btn_edit_object setTitle:@"Edit Profile" forState:UIControlStateNormal];
        btn_edit_object.selected=NO;
        
       // [defaults  setObject:txtF_name.text forKey:@"user_id"];
        [defaults setObject:txtF_name.text forKey:@"p_name"];
        [defaults  setObject:txtF_email.text forKey:@"p_email"];
        [defaults setObject:txtF_phone.text forKey:@"p_phone"];
        //[defaults setObject:txtF_about.text forKey:@"p_about"];
        [defaults setObject:textView_About.text forKey:@"p_about"];
        [defaults synchronize];
        
        [SVProgressHUD dismiss];
        UIViewController *root = [[UIStoryboard storyboardWithName:@"Main" bundle:NULL] instantiateViewControllerWithIdentifier:@"rootController"];
        UIWindow *wnd = [[[UIApplication sharedApplication] delegate] window];
        wnd.rootViewController = root;
        [wnd makeKeyAndVisible];

        UIAlertView *alert=[[UIAlertView alloc]initWithTitle:@"" message:@"Profile updated successfully" delegate:nil cancelButtonTitle:@"OK" otherButtonTitles: nil];
        [alert show];
    }
        
    else if ([[json objectForKey:@"error"]isEqualToString:@"token_expired"])
    {
        [SVProgressHUD dismiss];
        AppDelegate *delegate;
        delegate=(AppDelegate*)[[UIApplication sharedApplication]delegate];
        UIAlertView *alert=[[UIAlertView alloc]initWithTitle:@"" message:@"Session Expired Please login agian" delegate:nil cancelButtonTitle:@"OK" otherButtonTitles: nil];
        [alert show];
        [defaults setBool:NO forKey:@"logined"];
        [defaults synchronize];
        [[NSUserDefaults standardUserDefaults]synchronize];
        [delegate loginStatus];
    }
        else
        {
            [SVProgressHUD dismiss];
            UIAlertView *alert=[[UIAlertView alloc]initWithTitle:@"" message:@"Some error occurred to update user profle." delegate:nil cancelButtonTitle:@"OK" otherButtonTitles: nil];
            [alert show];
        }
        
    }
}
- (UIBarButtonItem *)DrawerButtonClicked:(id)target action:(SEL)action
{
    UIImage *image = [UIImage imageNamed:@"icon.png"];
    CGRect buttonFrame = CGRectMake(0, 0, 25, 25);
    UIButton *button = [[UIButton alloc] initWithFrame:buttonFrame];
    
    //[button addTarget:self action:@selector(backButtonPressed) forControlEvents:UIControlEventTouchUpInside];
    [button setImage:image forState:UIControlStateNormal];
    [button addTarget:target action:action forControlEvents:UIControlEventTouchUpInside];
    UIBarButtonItem *item= [[UIBarButtonItem alloc] initWithCustomView:button];
    return item;
}
#pragma end........................................................
@end
