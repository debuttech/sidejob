//
//  ListingOfAllWorkersVC.m
//  SideJobsMileston2
//
//  Created by Debut Mac 4 on 02/09/15.
//  Copyright (c) 2015 debut.infotech. All rights reserved.

#import "ListingOfAllWorkersVC.h"
#import <QuartzCore/QuartzCore.h>
#import "WorkerListCell.h"
#import "WorkerProfileViewController.h"
#import <UIImageView+WebCache.h>
@interface ListingOfAllWorkersVC ()
{
    NSString *str_jobInvite;
    UITapGestureRecognizer *tap;
    NSUserDefaults *defaults;
    NSMutableArray *arr_Worker,*jobsArr;
    NSString *str_userId;
    NSString *str_jobId;
    NSDictionary *dict_Jobdetails;
    NSMutableArray *fromSrchArr;
    NSMutableArray *arr_PublishJobs;
}
@end
@implementation ListingOfAllWorkersVC

- (void)viewDidLoad
{
    [super viewDidLoad];
    
    searchBar_object.layer.borderWidth = 0.0f;
    searchBar_object.layer.borderColor = [[UIColor colorWithRed:242.0f green:148.0f blue:29 alpha:1.0f] CGColor];
    
    UITapGestureRecognizer *tap1 = [[UITapGestureRecognizer alloc]
                                   initWithTarget:self
                                   action:@selector(dismissKeyboard)];
    
    [tableView_ListingOfWorkers addGestureRecognizer:tap1];
    
    defaults=[NSUserDefaults standardUserDefaults];
    searchBarTF.layer.borderColor=[[UIColor blackColor]CGColor];
    searchBarTF.layer.borderWidth=0.5;
    searchBarTF.layer.cornerRadius = 7;
    searchBarTF.layer.borderColor=[[UIColor colorWithRed:243 green:156 blue:48 alpha:0.5] CGColor];
    btnInvite_obj.layer.cornerRadius = 18;
    btnCancelInvite_obj.layer.cornerRadius = 18;

    self.navigationItem.hidesBackButton=YES;
    self.navigationItem.title=@"Search Service Reps";
    UIButton *button=[UIButton buttonWithType:UIButtonTypeCustom];
    [button setFrame:CGRectMake(0, 0, 23, 21)];
    [button setImage:[UIImage imageNamed:@"arrow.png"] forState:UIControlStateNormal];
    [button addTarget:self action:@selector(leftButton:) forControlEvents:UIControlEventTouchUpInside];
    self.navigationItem.leftBarButtonItem=[[UIBarButtonItem alloc]initWithCustomView:button];
    
    [SVProgressHUD showWithStatus:@"Loading..."];
//    [self performSelector:@selector(getWorkerList) withObject:nil afterDelay:0.3f];
//    [self performSelector:@selector(getMySidejobs) withObject:nil afterDelay:0.6f];
  
    }
- (void) dismissKeyboard
{
    [searchBar_object resignFirstResponder];
}
-(void)viewWillDisappear:(BOOL)animated
{
    [super viewWillDisappear:YES];
    [searchBar_object resignFirstResponder];

}

#pragma mark
#pragma mark- Get Worker List.......
-(void)getWorkerList
{
    
    NSMutableDictionary *jsonDict=[[NSMutableDictionary alloc]init];
    NSString *token=[NSString stringWithFormat:@"%@",[defaults objectForKey:@"user_token"]];
    [jsonDict setValue:[defaults objectForKey:@"user_id"] forKey:@"id"];
    
    NSString *url=[NSString stringWithFormat:@"%@workers-list?token=%@",serviceLink,token];
    id  json = [WebService signup:jsonDict:url];
    if ([[json objectForKey:@"message"]isEqualToString:@"Success"])
    {
        arr_Worker=[[NSMutableArray alloc]init];
        arr_Worker=[json objectForKey:@"response"];
        [tableView_ListingOfWorkers reloadData];
       
    }
    else if ([[json objectForKey:@"error"]isEqualToString:@"token_expired"])
    {
      
        UIAlertView *alert=[[UIAlertView alloc]initWithTitle:@"" message:@"Session Expired Please login agian" delegate:nil cancelButtonTitle:@"OK" otherButtonTitles: nil];
        [alert show];
        AppDelegate *delegate;
        delegate=(AppDelegate*)[[UIApplication sharedApplication]delegate];
        [defaults setBool:NO forKey:@"logined"];
        [defaults synchronize];
        [[NSUserDefaults standardUserDefaults]synchronize];
        [delegate loginStatus];
    }
    if(arr_Worker.count<1)
    {
        lblNoWorkers.hidden=NO;
    }
    else
    {
        lblNoWorkers.hidden=YES;
    }

     [SVProgressHUD dismiss];
}
-(void)leftButton:(UIButton*)sender
{
    [self.navigationController popViewControllerAnimated:YES];
}

-(void) viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:YES];
    
    [self performSelectorInBackground:@selector(getWorkerList) withObject:nil];
    
    [self performSelector:@selector(getJobSlist) withObject:nil afterDelay:0.3f];
    
//    [self performSelector:@selector(getWorkerList) withObject:nil afterDelay:0.3f];
//    [self performSelector:@selector(getMySidejobs) withObject:nil afterDelay:0.6f];
    
    CGFloat borderWidth = 0.3f;
    popUp_View.frame = CGRectInset(popUp_View.frame,-borderWidth, -borderWidth);
    popUp_View.layer.borderColor = [UIColor lightGrayColor].CGColor;
    popUp_View.layer.borderWidth = borderWidth;
    popUp_View.layer.cornerRadius = 7;

    dropdownV.layer.cornerRadius = 5;
    dropdownV.layer.masksToBounds = YES;
    dropdownV.hidden = YES;
//    [tableView_ListingOfWorkers reloadData];
}

-(void)getJobSlist{
    [self performSelectorInBackground:@selector(getMySidejobs) withObject:nil];
}

-(void)TapGesture:(UITapGestureRecognizer *)sender
{
    dropdownV.hidden = YES;
    view_popUP.hidden = YES;
   [dropdownV removeFromSuperview];
    [view_popUP removeFromSuperview];
}
#pragma mark
#pragma searchbar delegate method
- (void)searchBar:(UISearchBar *)searchBar textDidChange:(NSString *)searchText;
{
    fromSrchArr=[[NSMutableArray alloc] init];
    
    if(searchText.length > 0)
    {
        for (int i=0; i<arr_Worker.count; i++) {
            
            if ([[[arr_Worker objectAtIndex:i] valueForKey:@"name"] rangeOfString:searchText options:NSCaseInsensitiveSearch].location != NSNotFound)
            {
                [fromSrchArr addObject:[arr_Worker objectAtIndex:i]];
                
            }
        }
    }
    if (!fromSrchArr.count && searchBar.text.length)
    {
        lblNoWorkers.hidden=NO;
    }
    else if (fromSrchArr.count == 0 && searchBar.text.length == 0)
    {
        lblNoWorkers.hidden=YES;
    }
    else
    {
        lblNoWorkers.hidden=YES;
    }
    
    [tableView_ListingOfWorkers reloadData];
}
- (void)searchBarTextDidEndEditing:(UISearchBar *)searchBar
{
    if ([searchBar.text isEqualToString:@""] || !searchBar.text.length)
    {
        fromSrchArr=[[NSMutableArray alloc] init];
        
    }
    [tableView_ListingOfWorkers reloadData];
    [searchBar_object resignFirstResponder];

}
-(void)searchBarSearchButtonClicked:(UISearchBar *)searchBar
{
    [self.view endEditing:YES];
}

#pragma mark 
#pragma Fetch jobs to Invite the worker
-(void)getMySidejobs{
    NSString *token=[NSString stringWithFormat:@"%@",[defaults objectForKey:@"user_token"]];
    
    NSString *url=[NSString stringWithFormat:@"%@read-job/%@?token=%@",serviceLink,[defaults valueForKey:@"user_id"],token];
    
    jobsArr=[NSMutableArray new];
    arr_PublishJobs=[[NSMutableArray alloc]init];
    id json=[WebService GetwithoutDict:url];
    
  
    if ([[json objectForKey:@"message"]isEqualToString:@"Success"])
    {
        jobsArr = [json valueForKey:@"response"];
        
        for (int i=0; i<[jobsArr count]; i++)
        {
           
            if ([[[jobsArr objectAtIndex:i]objectForKey:@"published"]isEqualToString:@"1"])
            {
                [arr_PublishJobs addObject:[jobsArr objectAtIndex:i]];
            }
             NSLog(@"job account %lu",(unsigned long)[arr_PublishJobs count]);
        }
        

        
    }
    else if ([[json objectForKey:@"message"]isEqualToString:@"token_expired"])
    {
        [SVProgressHUD dismiss];
        UIAlertView *alert=[[UIAlertView alloc]initWithTitle:@"" message:@"Session Expired Please login agian" delegate:nil cancelButtonTitle:@"OK" otherButtonTitles: nil];
        [alert show];
        AppDelegate *delegate;
        delegate=(AppDelegate*)[[UIApplication sharedApplication]delegate];
        [defaults setBool:NO forKey:@"logined"];
        [defaults synchronize];
        [[NSUserDefaults standardUserDefaults]synchronize];
        [delegate loginStatus];
    }
    if (arr_PublishJobs.count)
    {
        [dropdownTV reloadData];
        lbl_nojobs.hidden=YES;
    }
    else
    {
        lbl_nojobs.hidden=NO;
    }

    NSLog(@"%@",jobsArr);
     [SVProgressHUD dismiss];
}
- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
}
#pragma mark
#pragma mark- tableView delegate method
-(NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    return 1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section;
{
    if (tableView == dropdownTV)
    {
         return arr_PublishJobs.count;
    }
    if (tableView ==tableView_ListingOfWorkers)
    {
        if (fromSrchArr.count)
        {
            return fromSrchArr.count;
        }
        else
        return [arr_Worker count];
    }
    return 0;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    if(tableView==tableView_ListingOfWorkers)
    {
        static NSString *customIdentifierListingWorkers = @"workerListCell";
        WorkerListCell *cell = [tableView dequeueReusableCellWithIdentifier:customIdentifierListingWorkers];

        NSLog(@"%f",cell.frame.size.height);
        
        
        if (cell == nil)
        {
            cell = [[WorkerListCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:customIdentifierListingWorkers];
        }
        
        if (fromSrchArr.count)
        {
            NSString *str=[NSString stringWithFormat:@"%@",[[fromSrchArr objectAtIndex:indexPath.row] objectForKey:@"profile_image"]];
            NSString *imageUrl=[NSString stringWithFormat:@"%@%@",serviceLink,str];
            cell.image_WorkerIcon.layer.cornerRadius = 29;
            cell.image_WorkerIcon.clipsToBounds = YES;
            [cell.btnName setTitle:[[fromSrchArr objectAtIndex:indexPath.row]objectForKey:@"name"]forState:UIControlStateNormal];

            
            cell.image_WorkerIcon.image=[UIImage imageNamed:@"defuser.png"];
            [cell.image_WorkerIcon sd_setImageWithURL:[NSURL URLWithString:imageUrl] placeholderImage:[UIImage imageNamed:@"defuser.png"]];
            cell.textLabel.text = @""; //add this update will reflect the changes
            
            
            NSArray *date=[[[fromSrchArr objectAtIndex:indexPath.row]objectForKey:@"created_at"]componentsSeparatedByString:@" "];
            NSString *dateF=[date objectAtIndex:0];
            cell.lbl_Date.text=dateF;
            cell.btn_sendInvitation.tag=indexPath.row;
            cell.btnName.tag=indexPath.row;
            [cell.btnName addTarget:self action:@selector(showProfile:) forControlEvents:UIControlEventTouchUpInside];
            [cell.btn_sendInvitation addTarget:self action:@selector(sendInvitationBtnTapped:) forControlEvents:UIControlEventTouchUpInside];
            
            cell.staticStarRatingView.canEdit = YES;
            cell.staticStarRatingView.maxRating = 5;
            cell.staticStarRatingView.minAllowedRating = 1;
            cell.staticStarRatingView.maxAllowedRating = 5;
            cell.staticStarRatingView.rating =[[[fromSrchArr objectAtIndex:indexPath.row ] objectForKey:@"rating"]floatValue];
            
            
           
        }
        
        else
        {
            
        NSString *str=[NSString stringWithFormat:@"%@",[[arr_Worker objectAtIndex:indexPath.row] objectForKey:@"profile_image"]];
        NSString *imageUrl=[NSString stringWithFormat:@"%@%@",serviceLink,str];
        cell.image_WorkerIcon.layer.cornerRadius = 29;
        cell.image_WorkerIcon.clipsToBounds = YES;
        [cell.btnName setTitle:[[arr_Worker objectAtIndex:indexPath.row]objectForKey:@"name"]forState:UIControlStateNormal];
            
            cell.image_WorkerIcon.image=[UIImage imageNamed:@"defuser.png"];
            [cell.image_WorkerIcon sd_setImageWithURL:[NSURL URLWithString:imageUrl] placeholderImage:[UIImage imageNamed:@"defuser.png"]];
            cell.textLabel.text = @""; //add this update will reflect the changes

        NSArray *date=[[[arr_Worker objectAtIndex:indexPath.row]objectForKey:@"created_at"]componentsSeparatedByString:@" "];
        NSString *dateF=[date objectAtIndex:0];
        cell.lbl_Date.text=dateF;
     
        cell.btn_sendInvitation.tag=indexPath.row;
        cell.btnName.tag=indexPath.row;
        [cell.btnName addTarget:self action:@selector(showProfile:) forControlEvents:UIControlEventTouchUpInside];
        [cell.btn_sendInvitation addTarget:self action:@selector(sendInvitationBtnTapped:) forControlEvents:UIControlEventTouchUpInside];
        
            cell.staticStarRatingView.canEdit = YES;
            cell.staticStarRatingView.maxRating = 5;
            cell.staticStarRatingView.minAllowedRating = 1;
            cell.staticStarRatingView.maxAllowedRating = 5;
            cell.staticStarRatingView.rating =[[[arr_Worker objectAtIndex:indexPath.row ] objectForKey:@"rating"]floatValue];
            
           
        }
         return cell;
    }
    else
    {
        static NSString *CellIdentifier = @"Cell";
        UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:CellIdentifier];
        if (cell == nil)
        {
            cell = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:CellIdentifier];
        }
         NSString *cellValue = [[arr_PublishJobs objectAtIndex:indexPath.row] objectForKey:@"title"];

        cell.textLabel.text = cellValue;
        return cell;
    }
    
    
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
      str_jobId=[[NSString alloc]init];

     [dropdown_btn setTitle:[[arr_PublishJobs objectAtIndex:indexPath.row] objectForKey:@"title"] forState:UIControlStateNormal];
    if (tableView==tableView_ListingOfWorkers)
    {
        if (fromSrchArr.count)
        {
            str_userId=[[fromSrchArr objectAtIndex:indexPath.row]objectForKey:@"id"];
        }
        else
        {
            str_userId=[[arr_Worker objectAtIndex:indexPath.row]objectForKey:@"id"];
  
        }

        }
    if (tableView==dropdownTV)
    {
         str_jobId=[[arr_PublishJobs objectAtIndex:indexPath.row]objectForKey:@"id"];
 
        [SVProgressHUD show];
        [self performSelectorInBackground:@selector(fetchJobDetails) withObject:nil];
    }
    [dropdownV setFrame:CGRectMake(500, dropdownV.frame.origin.y, dropdownV.frame.size.width, dropdownV.frame.size.height)];
    [popUp_View setFrame:CGRectMake(8, popUp_View.frame.origin.y, popUp_View.frame.size.width, popUp_View.frame.size.height)];
}
#pragma  mark
#pragma -Fetch Job list
-(void)fetchJobDetails
{
    dict_Jobdetails=[[NSDictionary alloc]init];
    NSString *token=[NSString stringWithFormat:@"%@",[defaults objectForKey:@"user_token"]];
    NSString *url=[NSString stringWithFormat:@"%@job-detail/?token=%@",serviceLink,token];
    NSMutableDictionary *jsonDict=[[NSMutableDictionary alloc]init];
    [jsonDict setValue:str_jobId forKey:@"id"];
    id json = [WebService signup:jsonDict:url];
   // dict_Jobdetails=json;
    if ([[json objectForKey:@"message"]isEqualToString:@"Success"])
    {
         dict_Jobdetails=json;
        [SVProgressHUD dismiss];
        
    }
    else if ([[json objectForKey:@"message"]isEqualToString:@"token_expired"])
    {
        [SVProgressHUD dismiss];
        UIAlertView *alert=[[UIAlertView alloc]initWithTitle:@"" message:@"Session Expired Please login agian" delegate:nil cancelButtonTitle:@"OK" otherButtonTitles: nil];
        [alert show];
        AppDelegate *delegate;
        delegate=(AppDelegate*)[[UIApplication sharedApplication]delegate];
        [defaults setBool:NO forKey:@"logined"];
        [defaults synchronize];
        [[NSUserDefaults standardUserDefaults]synchronize];
        [delegate loginStatus];
    }
    else
    {
        [SVProgressHUD dismiss];
    }
}

- (void)textFieldDidEndEditing:(UITextField *)textField
{
    [searchBarTF resignFirstResponder];
}

- (IBAction)dropdown_btn:(id)sender
{
    dropdownV.hidden = NO;
    [popUp_View setFrame:CGRectMake(500, popUp_View.frame.origin.y, popUp_View.frame.size.width, popUp_View.frame.size.height)];
    [dropdownV setFrame:CGRectMake(8, dropdownV.frame.origin.y, dropdownV.frame.size.width, dropdownV.frame.size.height)];
    
    [UIView animateWithDuration:0.4f animations:^{
    [dropdownV setCenter:self.view.center];
    }];
    [self.view.window addSubview:dropdownV];
    
    [UIView commitAnimations];
   
}

- (IBAction)cancel_dropdown:(id)sender
{
    [dropdownV setFrame:CGRectMake(500, dropdownV.frame.origin.y, dropdownV.frame.size.width, dropdownV.frame.size.height)];
    [popUp_View setFrame:CGRectMake(8, popUp_View.frame.origin.y, popUp_View.frame.size.width, popUp_View.frame.size.height)];
    [self setinvite];
}
#pragma mark
#pragma -show user profile
-(void)showProfile:(UIButton*)sender
{
    NSString *str=[NSString stringWithFormat:@"%ld",(long)sender.tag];
    
    WorkerProfileViewController *profileView=[self.storyboard  instantiateViewControllerWithIdentifier:@"WorkerProfileViewController"];
    
    if (fromSrchArr.count)
    {
         profileView.dict=[fromSrchArr objectAtIndex:[str integerValue]];
    }
    else
    {
       profileView.dict=[arr_Worker objectAtIndex:[str integerValue]];
    }
   
    [self.navigationController pushViewController:profileView animated:YES];
    
}

-(void)setinvite
{
    [dropdown_btn setTitle:@"Select Job" forState:UIControlStateNormal];
}
- (IBAction)btn_search:(id)sender
{
}
#pragma mark
#pragma mark-PopUp to send invitation from table
- (IBAction)sendInvitationBtnTapped:(UIButton *)sender
{
    [UIView animateWithDuration:0.3f
                     animations:^{
                         [view_popUP setFrame:CGRectMake(0, view_popUP.frame.origin.y, view_popUP.frame.size.width, view_popUP.frame.size.height)];
                         UIBezierPath *shadowPath = [UIBezierPath bezierPathWithRect:view_popUP.bounds];
                         view_popUP.layer.masksToBounds = NO;
                         view_popUP.layer.shadowColor = [UIColor blackColor].CGColor;
                         view_popUP.layer.shadowOffset = CGSizeMake(0.0f, 5.0f);
                         view_popUP.layer.shadowOpacity = 0.6f;
                         view_popUP.layer.shadowPath = shadowPath.CGPath;
                         view_popUP.backgroundColor=[UIColor clearColor];
                         view_popUP.alpha=1.0;
                         
                         [self.view.window addSubview:view_popUP];

                         
                     }];
   
    if (fromSrchArr.count)
    {
        str_userId=[[fromSrchArr objectAtIndex:sender.tag]objectForKey:@"id"];
    }
    else
    {
        str_userId=[[arr_Worker objectAtIndex:sender.tag]objectForKey:@"id"];
        
    }
}
#pragma mark
#pragma mark-Send invitation
- (IBAction)btnInvite_Action:(id)sender
{
    
    [SVProgressHUD show];
    [self performSelector:@selector(inviteJobMethod) withObject:nil afterDelay:1.0];
    

}
-(void)inviteJobMethod
{
    NSString *token=[NSString stringWithFormat:@"%@job-application?token=%@",serviceLink,[defaults objectForKey:@"user_token"]];
    NSMutableDictionary *jsonDict=[[NSMutableDictionary alloc]init];
    NSArray *arr=[[dict_Jobdetails objectForKey:@"response"] objectForKey:@"job_time_slots"];
    
    if (arr.count>0)
    {
        [jsonDict setValue:@"invited"forKey:@"applicant_job_status"];
        [jsonDict setValue:[[arr objectAtIndex:0] objectForKey:@"start_time"]forKey:@"start_time"];
        [jsonDict setValue:[[arr objectAtIndex:0] objectForKey:@"end_time"] forKey:@"end_time"];
        [jsonDict setValue:@"" forKey:@"proposed_amount"];
        [jsonDict setValue:str_jobId forKey:@"job_id"];
        [jsonDict setValue:str_userId forKey:@"applicant_id"];
        [jsonDict setValue:@"" forKey:@"comment"];
        
        
        //changes to notifaication
        
        [jsonDict setValue:[defaults objectForKey:@"user_id"] forKey:@"job_user_id"];
        
        [jsonDict setValue:[defaults objectForKey:@"user_id"] forKey:@"sender_id"];
        
        [jsonDict setValue:str_userId forKey:@"receiver_id"];
        
        [jsonDict setValue:[defaults objectForKey:@"p_name"] forKey:@"sender_name"];
        
        id  json;
        if (str_jobId>0)
        {
            json = [WebService signup:jsonDict:token];
            if ([[json objectForKey:@"message"]isEqualToString:@"Success"])
            {
                [SVProgressHUD dismiss];

                UIAlertView *alert=[[UIAlertView alloc]initWithTitle:@"" message:@"you have invited worker for this job." delegate:nil cancelButtonTitle:@"OK" otherButtonTitles:nil];
                [alert show];
            }
            else if ([[json objectForKey:@"message"]isEqualToString:@"Error :: Already applied for this job."])
            {
                [SVProgressHUD dismiss];
                UIAlertView *alert=[[UIAlertView alloc]initWithTitle:@"" message:@"You have already sent invitation to worker for this job." delegate:nil cancelButtonTitle:@"OK" otherButtonTitles:nil];
                [alert show];
            }
            else{
                [SVProgressHUD dismiss];
            }
        }
        else
        {
            [SVProgressHUD dismiss];
            UIAlertView *alert=[[UIAlertView alloc]initWithTitle:@"" message:@"Please select job before invite." delegate:nil cancelButtonTitle:@"OK" otherButtonTitles: nil];
            [alert show];
        }
        
    }
    else
    {
        [SVProgressHUD dismiss];
        UIAlertView *alert=[[UIAlertView alloc]initWithTitle:@"" message:@"Some error occured to fetch job details,please again invite user for this job." delegate:nil cancelButtonTitle:@"OK" otherButtonTitles: nil];
        [alert show];
    }
    //[SVProgressHUD dismiss];
    
    [view_popUP setFrame:CGRectMake(500, view_popUP.frame.origin.y, view_popUP.frame.size.width, view_popUP.frame.size.height)];
}

- (IBAction)btnCancelInvite_Action:(id)sender
{
    
    [UIView animateWithDuration:0.4f animations:^{
         [view_popUP setFrame:CGRectMake(500, view_popUP.frame.origin.y, view_popUP.frame.size.width, view_popUP.frame.size.height)];
      
    }];
  
    
    str_jobId=nil;
    [dropdown_btn setTitle:@"Select Job" forState:UIControlStateNormal];
    
}
@end
