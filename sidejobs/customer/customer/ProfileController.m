//
//  ProfileController.m
//  SideJobs
//
//  Created by Debut05 on 8/14/15.
//  Copyright (c) 2015 Narinder. All rights reserved.
//

#import "ProfileController.h"

@interface ProfileController ()

@end

@implementation ProfileController

- (void)viewDidLoad {
    [super viewDidLoad];
     buttonsScrollView.translatesAutoresizingMaskIntoConstraints  = NO;
    // Do any additional setup after loading the view.
    self.navigationController.navigationBar.backgroundColor=[UIColor grayColor];
    buttonsScrollView.contentSize=CGSizeMake(nameTF.frame.size.width, 300);
}

-(void)viewDidAppear:(BOOL)animated{
   [super viewDidAppear:YES];
   [self settingTextFeilds];
}

-(void)settingTextFeilds{
    nameTF.leftViewMode = UITextFieldViewModeAlways;
    emailTF.leftViewMode = UITextFieldViewModeAlways;
    aboutTF.leftViewMode = UITextFieldViewModeAlways;
    phoneTF.leftViewMode = UITextFieldViewModeAlways;
    
    UIView* leftView1 = [[UIView alloc] initWithFrame:CGRectMake(0, 0, 44, 10)];
    UIView* leftView2 = [[UIView alloc] initWithFrame:CGRectMake(0, 0, 44, 10)];
    UIView* leftView3 = [[UIView alloc] initWithFrame:CGRectMake(0, 0, 44, 10)];
    UIView* leftView4 = [[UIView alloc] initWithFrame:CGRectMake(0, 0, 44, 10)];
   
    
    nameTF.leftView = leftView1;
    emailTF.leftView = leftView2;
    aboutTF.leftView=leftView3;
    phoneTF.leftView = leftView4;
   

}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
