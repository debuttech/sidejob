//
//  WorkerProfileViewController.m
//  sidejobs
//
//  Created by DebutMac4 on 21/09/15.
//  Copyright (c) 2015 DebutMac4. All rights reserved.
//

#import "WorkerProfileViewController.h"

@interface WorkerProfileViewController ()

@end

@implementation WorkerProfileViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    imgViewWorkerProfile.layer.masksToBounds=YES;
    imgViewWorkerProfile.layer.cornerRadius=4.0f;
    NSLog(@"%@",self.dict);
    self.navigationItem.hidesBackButton=YES;
    self.navigationItem.title=@"Profile";
    UIButton *button=[UIButton buttonWithType:UIButtonTypeCustom];
    [button setFrame:CGRectMake(0, 0, 23, 21)];
    [button setImage:[UIImage imageNamed:@"arrow.png"] forState:UIControlStateNormal];
    [button addTarget:self action:@selector(leftButton:) forControlEvents:UIControlEventTouchUpInside];
    self.navigationItem.leftBarButtonItem=[[UIBarButtonItem alloc]initWithCustomView:button];

   [self.navigationController.navigationBar setBackgroundImage:[UIImage imageNamed:@"top_bar2.png"] forBarMetrics:UIBarMetricsDefault];
    [SVProgressHUD showWithStatus:@"Loading..."];
    [self performSelector:@selector(showUserProfile) withObject:nil afterDelay:0.1f];
  //  [self showUserProfile];
   
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}
-(void)leftButton:(UIButton*)Sender
{
    [self.navigationController popViewControllerAnimated:YES];
}
-(void)showUserProfile
{
    NSString *img=[self.dict objectForKey:@"profile_image"];
    NSString *url=[NSString stringWithFormat:@"%@%@",serviceLink,img];

    if ([url isEqualToString:serviceLink])
    {
        imgViewWorkerProfile.image=[UIImage imageNamed:@"defuser.png"];
    }
    else
    {
     // imgViewWorkerProfile.image=[UIImage imageWithData:[NSData dataWithContentsOfURL:[NSURL URLWithString:url]]];
        [imgViewWorkerProfile sd_setImageWithURL:[NSURL URLWithString:url] placeholderImage:[UIImage imageNamed:@"defuser.png"]];
    }
    txtfldNAme.text=[self.dict objectForKey:@"name"];
    txtFldEmail.text=[self.dict objectForKey:@"email"];
    TxtFldPhone.text=[self.dict objectForKey:@"phone"];
    txtFldAboutMe.text=[self.dict objectForKey:@"about"];
    
    _staticStarRatingView.canEdit = YES;
    _staticStarRatingView.maxRating = 5;
    _staticStarRatingView.minAllowedRating = 1;
    _staticStarRatingView.maxAllowedRating = 5;
    _staticStarRatingView.rating =[[self.dict objectForKey:@"rating"]integerValue];
    [SVProgressHUD dismiss];
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
