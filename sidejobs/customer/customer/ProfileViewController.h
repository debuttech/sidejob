//
//  ProfileViewController.h
//  sideJobs
//
//  Created by DebutMac4 on 24/08/15.
//  Copyright (c) 2015 DebutMac4. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "ASStarRatingView.h"
#import "RESideMenu.h"

@interface ProfileViewController :UIViewController<QBPopupMenuDelegate,UIImagePickerControllerDelegate,UINavigationControllerDelegate>
{
    id json;
    
    IBOutlet UITextView *textView_About;
    IBOutlet UILabel *lbl_Price;
    IBOutlet UIScrollView *scrollView_Obj;
    IBOutlet UIImageView *imageView_object;
    IBOutlet UITextField *txtF_name;
    IBOutlet UITextField *txtF_email;
    IBOutlet UITextField *txtF_about;
    IBOutlet UITextField *txtF_phone;
   
   IBOutlet UIImageView *imageView_border;
    IBOutlet UIView *view_ratingBackground;
    IBOutlet UIView *view_fieldBackground;
    IBOutlet UIButton *button_DetailCard;
    IBOutlet UIButton *btn_edit_object;
    IBOutlet UIButton *EditImageBtn;
    
    IBOutlet UIButton *btn_cross;
    IBOutlet UIView *viewPopUp;
    IBOutlet UITextField *txtFldConfirmPassword;
    IBOutlet UIView *viewPopUpInside;
     }

@property (strong, nonatomic) IBOutlet ASStarRatingView *staticStarRatingView;

- (IBAction)btnEdit_Action:(id)sender;
- (IBAction)editImageBtn_Action:(id)sender;
- (IBAction)btn_crossAction:(id)sender;
- (IBAction)btnAddCardDetails:(id)sender;
- (IBAction)btnConfirm:(id)sender;
- (IBAction)btnCancel:(id)sender;

-(void)showUserDetails;

@end
