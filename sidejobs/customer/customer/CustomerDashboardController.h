//
//  CustomerDashboardController.h
//  SideJobs
//
//  Created by Debut05 on 8/13/15.
//  Copyright (c) 2015 Narinder. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <MessageUI/MessageUI.h>

@interface CustomerDashboardController : UIViewController

{
     IBOutlet UIButton *createjobButton;
     IBOutlet UIButton *workerButton;
     IBOutlet UIButton *MysidejobButton;
     IBOutlet UIButton *messageButton;
}


-(IBAction)createjobButtonClicked:(id)sender;
-(IBAction)workerButtonClicked:(id)sender;
-(IBAction)MysidejobButtonClicked:(id)sender;
-(IBAction)messageButtonClicked:(id)sender;

@end
