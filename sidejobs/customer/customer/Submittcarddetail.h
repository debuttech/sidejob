//
//  Submittcarddetail.h
//  SideJobsMileston2
//
//  Created by Debut Mac 4 on 24/09/15.
//  Copyright (c) 2015 debut.infotech. All rights reserved.
//

#import "ViewController.h"

@interface Submittcarddetail : UIViewController<UITextFieldDelegate,UIPickerViewDataSource,UIPickerViewDelegate>
{
    NSString *paymentToken;
}


@property (strong, nonatomic) IBOutlet UITextField *txtFldName;
@property (strong, nonatomic) IBOutlet UITextField *txtFldCardNumber;
@property (strong, nonatomic) IBOutlet UITextField *txtFldExpiryDtae;
@property (strong, nonatomic) IBOutlet UITextField *txtFldCVc;

- (IBAction)submitCardDetails:(id)sender;
@end
