//
//  WorkerListCellTableViewCell.m
//  SideJobsMileston2
//
//  Created by Debut Mac 4 on 02/09/15.
//  Copyright (c) 2015 debut.infotech. All rights reserved.
//

#import "WorkerListCell.h"

@implementation WorkerListCell

- (void)awakeFromNib
{
    // Initialization code
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated{
    [super setSelected:selected animated:animated];
    
    _btn_sendInvitation.clipsToBounds=YES;
    _btn_sendInvitation.layer.cornerRadius=_btn_sendInvitation.bounds.size.width/2;
    _View_WorkerCell.layer.borderColor = [UIColor lightGrayColor].CGColor;
    _View_WorkerCell.layer.borderWidth = 0.3f;
}
@end
