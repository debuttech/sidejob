//
//  WorkerListCellTableViewCell.h
//  SideJobsMileston2
//
//  Created by Debut Mac 4 on 02/09/15.
//  Copyright (c) 2015 debut.infotech. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "ASStarRatingView.h"

@interface WorkerListCell : UITableViewCell
{
   
}
@property(strong,nonatomic) IBOutlet UIImageView *img_Star3;
@property(strong,nonatomic)  IBOutlet UIImageView *img_Star4;
@property(strong,nonatomic)  IBOutlet UIImageView *img_Star5;
@property(strong,nonatomic) IBOutlet UILabel *lbl_WorkerName;
@property(strong,nonatomic)  IBOutlet UIImageView *img_Star1;
@property(strong,nonatomic)  IBOutlet UIImageView *img_Star2;
@property (strong, nonatomic) IBOutlet UIView *View_WorkerCell;
@property (strong, nonatomic) IBOutlet UIImageView *image_WorkerIcon;
@property (strong, nonatomic) IBOutlet UILabel *lbl_Date;
@property (strong, nonatomic) IBOutlet UIButton *btn_sendInvitation;
@property (strong, nonatomic) IBOutlet UIButton *btnName;
@property (strong, nonatomic) IBOutlet ASStarRatingView *staticStarRatingView;

//@property (strong, nonatomic) IBOutlet UIView *stars_view;

@end
