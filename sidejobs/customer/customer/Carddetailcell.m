//
//  Carddetailcell.m
//  SideJobsMileston2
//
//  Created by Debut Mac 4 on 24/09/15.
//  Copyright (c) 2015 debut.infotech. All rights reserved.
//

#import "Carddetailcell.h"

@implementation Carddetailcell

- (void)awakeFromNib {
    // Initialization code
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    _carddetail_view.layer.masksToBounds=YES;
    _carddetail_view.layer.cornerRadius=5.0f;
}

@end
