//
//  CompleteJobCustomerTableViewCell.m
//  sidejobs
//
//  Created by DebutMac4 on 16/09/15.
//  Copyright (c) 2015 DebutMac4. All rights reserved.
//

#import "CompleteJobCustomerTableViewCell.h"

@implementation CompleteJobCustomerTableViewCell

- (void)awakeFromNib {
    // Initialization code
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];
    _img_User.layer.masksToBounds=YES;
    _img_User.layer.cornerRadius=_img_User.bounds.size.width/2;
    _lbl_Amount.layer.masksToBounds=YES;
    _lbl_Amount.layer.cornerRadius=_lbl_Amount.bounds.size.width/2;
    _view_Background.layer.borderColor=[[UIColor grayColor]CGColor];
    _view_Background.layer.borderWidth=0.3f;
}

@end
