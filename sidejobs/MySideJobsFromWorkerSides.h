//
//  MySideJobs.h
//  WorkerEarning
//
//  Created by Debut Mac 4 on 19/08/15.
//  Copyright (c) 2015 debut.infotech. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface MySideJobsFromWorkerSides : UIViewController <UITableViewDataSource,UITableViewDelegate>
{
    IBOutlet UISegmentedControl *segmentControler_obj;
    IBOutlet UIView *view_ongoingContracts;
    IBOutlet UIView *View_AppliedJobsWorker;
    IBOutlet UIView *View_JobCompletedOfWorker;
    IBOutlet UITableView *tableView_OngoingWorkers;
    IBOutlet UITableView *tableView_JobCompletedWorker;
    IBOutlet UITableView *tableView_AppliedJobsWorker;
    
    IBOutlet UITableView *tableViewMyInvitation;
    IBOutlet UILabel *lbl_NoRecord;
    IBOutlet UIView *viewMyInvitations;
    IBOutlet UIButton *btn_appliedObj;
    IBOutlet UIButton *btn_OngoingObj;
    IBOutlet UIButton *btn_CompeleteObj;
}
- (IBAction)btn_Cross:(id)sender;
- (IBAction)btnInvities:(id)sender;
- (IBAction)SegmentsActions:(id)sender;
- (IBAction)appliedJob:(id)sender;
- (IBAction)onGoingJob:(id)sender;
- (IBAction)completeJob:(id)sender;
@end
