//
//  NotificationVCViewController.m
//  sidejobs
//
//  Created by DebutMac4 on 30/11/15.
//  Copyright © 2015 DebutMac4. All rights reserved.
//

#import "NotificationVCViewController.h"
#import "NotificationTVC.h"
#import <Helper.h>
#import "UIImageView+WebCache.h"

@interface NotificationVCViewController ()
{
    NSUserDefaults *defaults;
    NSArray *arr_Notification;
    AppDelegate *delegate;
}

@end

@implementation NotificationVCViewController

- (void)viewDidLoad
{
    [super viewDidLoad];
    delegate = (AppDelegate*)[[UIApplication sharedApplication]delegate];
    defaults=[NSUserDefaults standardUserDefaults];
    self.navigationItem.title=@"Activities";
    
    NSDictionary *size = [NSDictionary dictionaryWithObjectsAndKeys:[UIFont fontWithName:@"HelveticaNeue-Light" size:20.0],NSFontAttributeName, [UIColor whiteColor],NSForegroundColorAttributeName, nil];
    self.navigationController.navigationBar.titleTextAttributes = size;
    [self.navigationController.navigationBar setBackgroundImage:[UIImage imageNamed:@"top_bar2.png"] forBarMetrics:UIBarMetricsDefault];
   
    UIButton *rightButton=[UIButton buttonWithType:UIButtonTypeCustom];
    [rightButton setFrame:CGRectMake(0, 0, 26, 26)];
    [rightButton setImage:[UIImage imageNamed:@"notificationbelln"] forState:UIControlStateNormal];
    [rightButton addTarget:self action:@selector(DashBarButton:) forControlEvents:UIControlEventTouchUpInside];
    self.navigationItem.rightBarButtonItem=[[UIBarButtonItem alloc]initWithCustomView:rightButton];
    
    [delegate readNotification];
    
}

-(void)DashBarButton:(id)sender
{
    
    [self dismissViewControllerAnimated:true completion:nil];
  /*  UIWindow *wnd = [[[UIApplication sharedApplication] delegate] window];
    [UIView transitionWithView:wnd
                      duration:0.5
                       options:UIViewAnimationOptionTransitionFlipFromLeft
                    animations:^{
                        
                        UIViewController *root = [[UIStoryboard storyboardWithName:@"Main" bundle:NULL] instantiateViewControllerWithIdentifier:@"rootController"];
                        [wnd makeKeyAndVisible];
                        wnd.rootViewController = root;
                    }
                    completion:nil];*/
    
}
-(void)viewDidAppear:(BOOL)animated
{
    [super viewDidAppear:true];
    [SVProgressHUD show];
    [self performSelector:@selector(getNotification) withObject:nil afterDelay:0.5f];
    [UIApplication sharedApplication].applicationIconBadgeNumber = 0;
}

-(void)getNotification
{
    NSMutableDictionary *jsonDict=[[NSMutableDictionary alloc]init];
    [jsonDict setValue:[defaults objectForKey:@"user_id"] forKey:@"receiver_id"];
    [jsonDict setValue:[defaults objectForKey:@"user_status"] forKey:@"user_type"];
    
    NSString *url=[NSString stringWithFormat:@"%@notification",serviceLink];
    
    if ([Helper isConnectedToInternet])
    {
        NSLog(@"JSON %@",[jsonDict JSONRepresentation]);
     id  json = [WebService signup:jsonDict:url];
        
     if ([[json objectForKey:@"message"]isEqualToString:@"Success"])
     {
        arr_Notification=[[NSArray alloc]init];
        arr_Notification=[json objectForKey:@"response"];
     }
     else
     {
        UIAlertView *alert=[[UIAlertView alloc]initWithTitle:@"Error!" message:@"Some error occured to showing Notification." delegate:nil cancelButtonTitle:@"OK" otherButtonTitles:nil];
        [alert show];
     }
    }
    else
    {
        UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"No Internet" message:@"Please check your internet connection." delegate:nil cancelButtonTitle:@"OK" otherButtonTitles:nil];
        [alert show];
    }
    
    
    if (arr_Notification==nil  || arr_Notification.count<1)
    {
        label_NotificqationStatus.hidden=NO;
    }
    else
    {
        label_NotificqationStatus.hidden=YES;
  
    }
    [tblView reloadData];
   
    [SVProgressHUD dismiss];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
   
}


#pragma mark
#pragma mark-TableView Delegate method....................******.......................................

-(NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    
    return 1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section;
{
    

        return arr_Notification.count;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    static NSString *customCellIdentifier = @"cell_Notification";
    NotificationTVC *cell = [tableView dequeueReusableCellWithIdentifier:customCellIdentifier];
    
    if (cell == nil)
    {
        cell = [[NotificationTVC alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:customCellIdentifier];
    }
    
    cell.lbl_notification.text=[[arr_Notification objectAtIndex:indexPath.row]objectForKey:@"title"];
    NSArray *arrdate=[[[arr_Notification objectAtIndex:indexPath.row]objectForKey:@"updated_at"] componentsSeparatedByString:@" "];
    
  
   
    
    NSString *SD=[arrdate objectAtIndex:0];
   
   // convert string in NSDate ...
    
    NSDateFormatter *dateFormat = [[NSDateFormatter alloc] init];
    [dateFormat setDateFormat:@"yyyy-MM-dd"];
    NSDate *date = [dateFormat dateFromString:SD];
  
    NSDateFormatter *formatter = [[NSDateFormatter alloc] init];
    [formatter setDateFormat:@"EEEE, MMMM dd, yyyy"];
    
    NSLocale *locale = [[NSLocale alloc] initWithLocaleIdentifier:@"el_GR"];
    [formatter setLocale:locale];
 
    
    NSString *dateAsString = [formatter stringFromDate:date];
    locale = [[NSLocale alloc] initWithLocaleIdentifier:@"en_US"];
    [formatter setLocale:locale];
 
    
    dateAsString = [formatter stringFromDate:date];
    cell.lbl_notidate.text=dateAsString;
   
    
    
    NSString *strimg=[NSString stringWithFormat:@"%@%@",serviceLink,[[[[arr_Notification objectAtIndex:indexPath.row] objectForKey:@"last_user_detail"] objectAtIndex:0]objectForKey:@"profile_image"]];
    
    
    SDWebImageManager *manager = [SDWebImageManager sharedManager];
    [manager downloadImageWithURL:[NSURL URLWithString:strimg]
                          options:0
                         progress:^(NSInteger receivedSize, NSInteger expectedSize) {
                             // progression tracking code
                         }
                        completed:^(UIImage *image, NSError *error, SDImageCacheType cacheType, BOOL finished, NSURL *imageURL) {
                            
                            if ([strimg isEqualToString:@"http://sidejobapp.co:8000/"])
                                            {
                                                cell.img_user.image = [UIImage imageNamed:@"user.png"];
                                            }
                                            else
                                            {
                                                cell.img_user.image = image;
                                
                                            }
//                            if (image) {
//                                // do something with image
//                                [img1Btn_Obj setBackgroundImage:image forState:UIControlStateNormal];
//                            }
                        }];
    

//    dispatch_async(dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_BACKGROUND, 0), ^{
//        NSData * imageData = [NSData dataWithContentsOfURL:[NSURL URLWithString:strimg]];
//        UIImage *image = [UIImage imageWithData:imageData];
//        
//        dispatch_sync(dispatch_get_main_queue(), ^{
//            if ([strimg isEqualToString:@"http://sidejobapp.co:8000/"])
//            {
//                cell.img_user.image = [UIImage imageNamed:@"user.png"];
//            }
//            else
//            {
//                cell.img_user.image = image;
//
//            }
//            
//        });
//    });

   
    
    return cell;
}
- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath;
{
  //  [self readNotification:indexPath.row];
}



/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
