//
//  MyInvitationCell.h
//  SideJobsMileston2
//
//  Created by Debut Mac 4 on 17/09/15.
//  Copyright (c) 2015 debut.infotech. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface MyInvitationCell : UITableViewCell
{
    
}
@property (strong, nonatomic) IBOutlet UILabel *lbl_Heading;
@property (strong, nonatomic) IBOutlet UILabel *lbl_UserName;
@property (strong, nonatomic) IBOutlet UILabel *lbl_CatName;
@property (strong, nonatomic) IBOutlet UILabel *lbl_InviteOn;
@property (strong, nonatomic) IBOutlet UILabel *lbl_Amount;

@property (strong, nonatomic) IBOutlet UIImageView *userIcon_img;

@property (strong, nonatomic) IBOutlet UIImageView *imgCategory;
@property (strong, nonatomic) IBOutlet UIView *view_invitationCell;
@property (strong, nonatomic) IBOutlet UIButton *btnAccept;
@property (strong, nonatomic) IBOutlet UIButton *btnDecline;

@end
