//
//  AccountDetailsViewController.h
//  sidejobs
//
//  Created by DebutMac4 on 29/03/16.
//  Copyright © 2016 DebutMac4. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface AccountDetailsViewController : UIViewController <UITableViewDataSource, UITableViewDelegate>
{
    
    IBOutlet UITableView *tableView_AccountDetailList;
    IBOutlet UILabel *label_NoAccountDetail;
    IBOutlet UIButton *button_AddAccountDetail;
}
- (IBAction)buttonTapped_AddAccountDetail:(UIButton *)sender;



@end
