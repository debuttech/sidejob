//
//  JobCompletedWorkerCell.h
//  WorkerEarning
//
//  Created by Debut Mac 4 on 20/08/15.
//  Copyright (c) 2015 debut.infotech. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface JobCompletedWorkerCell : UITableViewCell

@property (strong, nonatomic) IBOutlet UIImageView *image_OfCustomer;
@property (strong, nonatomic) IBOutlet UILabel *lblCustomerOf_JobCompleted;
@property (strong, nonatomic) IBOutlet UILabel *lblCategoty_JobCompleted;
@property (strong, nonatomic) IBOutlet UILabel *lblStartedOn_JobCompleted;
@property (strong, nonatomic) IBOutlet UILabel *lblCompletedDate_JobCompleted;
@property (strong, nonatomic) IBOutlet UIView *view_complete;
@property (strong, nonatomic) IBOutlet UILabel *lbl_Title;
@property (strong, nonatomic) IBOutlet UIImageView *img_Category;
@property (strong, nonatomic) IBOutlet UILabel *lbl_Amount;

@end
