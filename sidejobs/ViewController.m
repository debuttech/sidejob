
#import "ViewController.h"
#import "ForgatePasswordViewController.h"
#import "SignUpAsViewController.h"
#import "WorkerDashboardViewController.h"
#import "DEMORootViewController.h"
#import "ProfileViewController.h"

#import <Helper.h>
#import "TermConditionsViewController.h"
#import <Crashlytics/Crashlytics.h>

#import <FBSDKCoreKit/FBSDKCoreKit.h>
#import <FBSDKLoginKit/FBSDKLoginKit.h>
#import <FBSDKShareKit/FBSDKShareKit.h>


@interface ViewController ()

@end

@implementation ViewController
{
    ProfileViewController *profileView;
    NSString *str_lastName;
    
    BOOL loginByFacebook;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    
    
    defaults=[NSUserDefaults standardUserDefaults];
    [_btnSignup_Object setTitleColor:[UIColor colorWithPatternImage:[UIImage imageNamed:@"login_screen_login_btn.png"]] forState:UIControlStateNormal];
    UITapGestureRecognizer *tap=[[UITapGestureRecognizer alloc]initWithTarget:self action:@selector(tapGesture:)];
    tap.numberOfTapsRequired=1;
    [self.view addGestureRecognizer:tap];
    self.navigationController.navigationBarHidden=YES;
}


-(void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:YES];
  
    self.navigationController.navigationBarHidden=YES;
    
//Worker:
//        _txtField_email.text = @"as@gmail.com";
//        _txtField_passord.text = @"123456789";
    
    //................
//    _txtField_email.text = @"luckylaksh@gmail.com";
//    _txtField_passord.text = @"123456789";

//    _txtField_email.text = @"ankush.dhawan@debutinfotech.com";
//    _txtField_passord.text = @"123456";

   // _txtField_email.text = @"rajat.debut5@gmail.com";
   // _txtField_passord.text = @"rajat12345";
    
    
    //Coustmer Account
//    _txtField_email.text = @"rajat.debut1@gmail.com";
//    _txtField_passord.text = @"rajat12345";

    
}
-(void)viewWillDisappear:(BOOL)animated
{
    [super viewWillDisappear:YES];
    
}
-(void)tapGesture:(UITapGestureRecognizer *)sender
{
    [_txtField_email resignFirstResponder];
    [_txtField_passord resignFirstResponder];
}

#pragma txtfield delegate method
#pragma mark
- (BOOL)textFieldShouldReturn:(UITextField *)textField
{
    if (textField == _txtField_email) {
        [_txtField_email resignFirstResponder];
        [_txtField_passord becomeFirstResponder];
    }
    else if (textField == _txtField_passord) {
        [_txtField_passord resignFirstResponder];
    }
    
    return YES;
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
}

- (IBAction)btnCheckAction:(id)sender
{
    btn_checkObj.selected=!btn_checkObj.selected;
    
}

- (IBAction)btn_TermConditionSee:(id)sender
{
    TermConditionsViewController *TC=[self.storyboard instantiateViewControllerWithIdentifier:@"TermConditionsViewController"];
    [self.navigationController pushViewController:TC animated:YES];
}

- (IBAction)btnForgot_Action:(id)sender
{
    ForgatePasswordViewController *view_forgatePassword=[self.storyboard instantiateViewControllerWithIdentifier:@"ForgatePasswordViewController"];
    [self.navigationController pushViewController:view_forgatePassword animated:YES];
}

- (IBAction)btnSign_Action:(id)sender {
    
    if (btn_checkObj.selected)
    {
        SignUpAsViewController *signUpView=[self.storyboard instantiateViewControllerWithIdentifier:@"SignUpAsViewController"];
        [self.navigationController pushViewController:signUpView animated:YES];
    }
    else{
        UIAlertView *alert=[[UIAlertView alloc]initWithTitle:@"" message:@"Please accept the terms and conditions before proceeding." delegate:nil cancelButtonTitle:@"OK" otherButtonTitles:nil];
        [alert show];
    }
    
    
    
    
}
- (IBAction)btnLogin_Action:(id)sender
{
    loginByFacebook=false;
    
    str_email=_txtField_email.text;
    str_password=_txtField_passord.text;
    
    
    str_email = [_txtField_email.text stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceCharacterSet ]];
    str_password = [_txtField_passord.text stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceCharacterSet ]];
    
    if ([str_email length]<1 ){
        UIAlertView *alert=[[UIAlertView alloc]initWithTitle:@"" message:@"Please enter email." delegate:self cancelButtonTitle:@"OK" otherButtonTitles: nil];
        [alert show];
        return;
    }
    else if (![self isValidEmail :str_email])
    {
        UIAlertView *alert=[[UIAlertView alloc]initWithTitle:@"" message:@"Please enter valid email." delegate:self cancelButtonTitle:@"OK" otherButtonTitles: nil];
        [alert show];
        return;
    }
    else if ([str_password length]<1)
    {
        UIAlertView *alert=[[UIAlertView alloc]initWithTitle:@"" message:@"Please enter password." delegate:self cancelButtonTitle:@"OK" otherButtonTitles: nil];
        [alert show];
        return;
    }
    else
    {
        [SVProgressHUD showWithStatus:@"Loading..."];
        [self performSelector:@selector(login_user) withObject:nil afterDelay:0.3f];
    }
}


-(void)login_user;
{
    NSMutableDictionary *jsonDict=[[NSMutableDictionary alloc]init];
    [jsonDict setValue:str_email forKey:@"email"];
    [jsonDict setValue:str_password forKey:@"password"];
    
    
    if (loginByFacebook==true)
    {
        [jsonDict setValue:@"facebook" forKey:@"registration_type"];
    }
    else
    {
        [jsonDict setValue:@"" forKey:@"registration_type"];
    }
    
    NSString *url=[NSString stringWithFormat:@"%@api/authenticate",serviceLink];
    
    if ([Helper isConnectedToInternet])
    {
        json = [WebService signup:jsonDict:url];
        NSLog(@"Response %@",json);
        if ([[json objectForKey:@"message" ]isEqualToString:@"Success"])
        {
            [SVProgressHUD dismiss];
            
            NSMutableArray *Arr=[[NSMutableArray alloc]init];
            Arr=[json objectForKey:@"user"];
            
            NSMutableDictionary *dict=[NSMutableDictionary new];
            dict=[Arr objectAtIndex:0];
            
            str_nameProfile=[dict objectForKey:@"name"];
            str_phoneprofile=[dict objectForKey:@"phone"];
            str_emailProfile=[dict objectForKey:@"email"];
            str_aboutProfile=[dict objectForKey:@"about"];
            str_rating=[dict objectForKey:@"rating"];
            str_approval=[dict objectForKey:@"worker_status"];
            str_merchant_id = [dict objectForKey:@"merchant_id"];
           
            str_profilephoto=[NSString stringWithFormat:@"%@%@",serviceLink,[dict objectForKey:@"profile_image"]];
         
            str_Id=[dict objectForKey:@"id"];
            [defaults setValue:[json objectForKey:@"token"] forKey:@"user_token"];
            [defaults setValue:[dict objectForKey:@"id"] forKey:@"user_id"];
            [defaults setObject:str_nameProfile forKey:@"p_name"];
            [defaults setObject:str_emailProfile forKey:@"p_email"];
            [defaults setObject:str_aboutProfile forKey:@"p_about"];
            [defaults setObject:str_phoneprofile forKey:@"p_phone"];
            [defaults setObject:str_profilephoto forKey:@"p_photo"];
            [defaults setObject:str_merchant_id forKey:@"merchant_id"];
            
            [defaults setObject:str_rating forKey:@"rating_user"];
            [defaults setObject: str_approval forKey:@"worker_approval"];
            
            [defaults setObject:[dict valueForKey:@"user_type"] forKey:@"user_status"];
            [defaults setObject:str_password forKey:@"userPassword"];
            [defaults setObject:[dict objectForKey:@"customer_id"] forKey:@"customer_id"];
            [defaults setObject:[dict objectForKey:@"total_job_ammount"] forKey:@"myearning"];
            NSLog(@"%@",[dict objectForKey:@"total_job_amount"]);
            [defaults synchronize];
            
            [[NSUserDefaults standardUserDefaults]synchronize];
            NSLog(@"%@",[defaults objectForKey:@"user_token"]);
            NSLog(@"%@",[defaults objectForKey:@"customer_id"]);
            [defaults setBool:YES forKey:@"logined"];
            
            [self performSelectorInBackground:@selector(getDeviceToken) withObject:nil];
            
            UIViewController *root = [[UIStoryboard storyboardWithName:@"Main" bundle:NULL] instantiateViewControllerWithIdentifier:@"rootController"];
            UIWindow *wnd = [[[UIApplication sharedApplication] delegate] window];
            wnd.rootViewController = root;
            [wnd makeKeyAndVisible];
            
        }
        else if([[json objectForKey:@"error" ]isEqualToString:@"invalid_credentials"])
        {
            UIAlertView *alert=[[UIAlertView alloc]initWithTitle:@"" message:@"Invalid Credentials." delegate:self cancelButtonTitle:@"OK" otherButtonTitles: nil];
            [alert show];
        }
        else if([[json objectForKey:@"error" ]isEqualToString:@"Email is not verified."])
        {
            UIAlertView *alert=[[UIAlertView alloc]initWithTitle:@"Alert" message:@"Your account is not activated. Please check your email and activate your account." delegate:self cancelButtonTitle:@"OK" otherButtonTitles: nil];
            [alert show];
        }
        else if ([[json objectForKey:@"error"]isEqualToString:@"Worker approval is Pending."])
        {
            UIAlertView *alert=[[UIAlertView alloc]initWithTitle:@"Alert!" message:@"Your request is received. You will be notified once, your request will be approved." delegate:self cancelButtonTitle:@"OK" otherButtonTitles: nil];
            [alert show];
        }
        
        else if ([[json objectForKey:@"error"]isEqualToString:@"Worker approval is Declined."])
        {
            UIAlertView *alert=[[UIAlertView alloc]initWithTitle:@"Alert!" message:@"Your registration request as worker has been declined." delegate:self cancelButtonTitle:@"OK" otherButtonTitles: nil];
            [alert show];
 
        }
        else if ([[json objectForKey:@"error"]isEqualToString:@"Your account is deactivated by SideJobs."])
        {
            UIAlertView *alert=[[UIAlertView alloc]initWithTitle:@"Alert!" message:@"Your account is deactivated by SideJobs." delegate:self cancelButtonTitle:@"OK" otherButtonTitles: nil];
            [alert show];
            
        }
        
        else if([[json objectForKey:@"error" ]isEqualToString:@"Email not registered."])
        {
            UIAlertView *alert=[[UIAlertView alloc]initWithTitle:@"Alert!" message:@"This email is not registered." delegate:self cancelButtonTitle:@"OK" otherButtonTitles: nil];
            [alert show];
        }
        else if (json==nil )
        {
            UIAlertView *alert=[[UIAlertView alloc]initWithTitle:@"Alert!" message:@"Server error." delegate:self cancelButtonTitle:@"OK" otherButtonTitles: nil];
            [alert show];
        }
    }
    else
    {
        UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"No Internet" message:@"Please check your internet connection." delegate:nil cancelButtonTitle:@"OK" otherButtonTitles:nil];
        [alert show];
        
    }
    [SVProgressHUD dismiss];
}


-(BOOL)isValidEmail:(NSString *)checkString
{
    BOOL stricterFilter = NO; // Discussion http://blog.logichigh.com/2010/09/02/validating-an-e-mail-address/
    NSString *stricterFilterString = @"[A-Z0-9a-z\\._%+-]+@([A-Za-z0-9-]+\\.)+[A-Za-z]{2,4}";
    NSString *laxString = @".+@([A-Za-z0-9-]+\\.)+[A-Za-z]{2}[A-Za-z]*";
    NSString *emailRegex = stricterFilter ? stricterFilterString : laxString;
    NSPredicate *emailTest = [NSPredicate predicateWithFormat:@"SELF MATCHES %@", emailRegex];
    return [emailTest evaluateWithObject:checkString];
}



-(void)showAlertWidOptions{
    UIAlertController *alert = [UIAlertController alertControllerWithTitle:@"Select Sign In Option."  message:nil preferredStyle:UIAlertControllerStyleAlert];
    
    UIAlertAction *resetAction = [UIAlertAction
                                  actionWithTitle:NSLocalizedString(@"Sign In as Customer", @"Reset action")
                                  style:UIAlertActionStyleDestructive
                                  handler:^(UIAlertAction *action)
                                  {
                                      [defaults setObject:@"customer" forKey:@"user_status"];
                                      [defaults synchronize];
                                      [self loginWithFb];
                                  }];
    
    UIAlertAction *afterWeek = [UIAlertAction
                                actionWithTitle:NSLocalizedString(@"Sign In as Worker", @"Cancel action")
                                style:UIAlertActionStyleDefault
                                handler:^(UIAlertAction *action)
                                {
                                    [defaults setObject:@"worker" forKey:@"user_status"];
                                    [defaults synchronize];
                                    [self loginWithFb];
                                }];
    
    UIAlertAction *cancelAction = [UIAlertAction
                                   actionWithTitle:NSLocalizedString(@"Cancel", @"Cancel action")
                                   style:UIAlertActionStyleCancel
                                   handler:^(UIAlertAction *action)
                                   {
                                       
                                   }];
    
    [alert addAction:resetAction];
    [alert addAction:afterWeek];
    [alert addAction:cancelAction];
    
    [self presentViewController:alert animated:YES completion:nil];
    
}



- (IBAction)Btnfb_Action:(id)sender
{
    if (btn_checkObj.selected)
    {
        [self showAlertWidOptions];
    }
    else{
        UIAlertView *alert=[[UIAlertView alloc]initWithTitle:@"" message:@"Please accept the terms and conditions before proceeding." delegate:nil cancelButtonTitle:@"OK" otherButtonTitles:nil];
        [alert show];
    }
}



-(void)loginWithFb
{
    
    
    FBSDKLoginManager *login = [[FBSDKLoginManager alloc] init];
    [login logOut];
    [login
     logInWithReadPermissions: @[@"public_profile",@"email"]
     fromViewController:self
     handler:^(FBSDKLoginManagerLoginResult *result, NSError *error) {
         if (error) {
             NSLog(@"Process error");
             NSLog(@"%@",error);
         } else if (result.isCancelled) {
             NSLog(@"Cancelled");
         } else {
             NSLog(@"Logged in");
             
             
             
             FBSDKGraphRequest *request = [[FBSDKGraphRequest alloc] initWithGraphPath:@"me" parameters:@{@"fields" : @"id,email,first_name,last_name,link,locale,picture"}];
             [request startWithCompletionHandler:^(FBSDKGraphRequestConnection *connection, id result, NSError *error) {
                 if (!error) {
                     

                     
                     
                     NSDictionary *userData = (NSDictionary *)result;
                     
                     str_Femail = userData[@"email"];
                     str_FNmae = [NSString stringWithFormat:@"%@ %@",userData[@"first_name"],userData[@"last_name"]];
                     str_FPassword=@"123456789";
                     str_FprofilePics=[[[result objectForKey:@"picture"] objectForKey:@"data"] objectForKey:@"url"];
                     str_facebookID = [result objectForKey:@"id"];
                     
                     NSURL *URL = [NSURL URLWithString:[str_FprofilePics stringByAddingPercentEscapesUsingEncoding: NSUTF8StringEncoding]];

                     Image_userProfile=[[UIImage alloc]initWithData:[NSData dataWithContentsOfURL:URL]];
                     
                     [SVProgressHUD showWithStatus:@"Loading"];
                    [self performSelector:@selector(facebookRegistration) withObject:nil afterDelay:0.3];

                     
                     
                  
                 }
             }];
         }
     }];
}





- (void) fbAuthorizeResponseDelegate:(BOOL)check
{
}



#pragma mark
#pragma to login with facebook firstily registration from facebook details.............
-(void)facebookRegistration
    {
        loginByFacebook=true;
        
        NSUserDefaults *defa=[NSUserDefaults standardUserDefaults];
        NSString *type=[defa valueForKey:@"user_status"];
        
        
        NSMutableDictionary *jsonDict=[[NSMutableDictionary alloc]init];
        [jsonDict setValue:str_FNmae forKey:@"name"];
       // [jsonDict setValue:str_Femail forKey:@"email"];
        [jsonDict setValue:type forKey:@"user_type"];
        [jsonDict setValue:@"facebook_user" forKey:@"registration_type"];
        [jsonDict setValue:@"not availble" forKey:@"address"];
        [jsonDict setValue:@"" forKey:@"phone"];
        [jsonDict setValue:str_FPassword forKey:@"password"];
        [jsonDict setValue:str_facebookID forKey:@"facebook_id"];
        
        
        NSString *url=[NSString stringWithFormat:@"%@register-user",serviceLink];
        json = [WebService signup:jsonDict:url];
        
        
        NSMutableDictionary *dict=[json objectForKey:@"errors"];
        NSArray *arr=[dict objectForKey:@"email"];
        NSString *str=[NSString stringWithFormat:@"%@",[arr objectAtIndex:0]];
        
        
        
        if ([[json objectForKey:@"message" ]isEqualToString:@"Success"])
        {
       
            user_ID=[json objectForKey:@"user_id"];
            [self uploadProfileImages:Image_userProfile];
        }
        else if ([[json objectForKey:@"message"] isEqualToString:@"Error :: User Already exists"])
        {
            str_email=str_facebookID;
            str_password=str_FPassword;
            [self login_user];
        }
        else
        {
            [SVProgressHUD dismiss];
        }
}
     
     
     
     -(void)uploadProfileImages:(UIImage*)img
    {
        NSMutableURLRequest *request = [[NSMutableURLRequest alloc] initWithURL:[NSURL URLWithString:[NSString stringWithFormat:@"%@uploadprofileimage/%@",serviceLink,user_ID]]];
        
        NSData *imageData = UIImageJPEGRepresentation(img, 1.0);
        [request setCachePolicy:NSURLRequestReloadIgnoringLocalCacheData];
        [request setHTTPShouldHandleCookies:NO];
        [request setTimeoutInterval:60];
        [request setHTTPMethod:@"POST"];
        NSString *boundary = @"unique-consistent-string";
        // set Content-Type in HTTP header
        NSString *contentType = [NSString stringWithFormat:@"multipart/form-data; boundary=%@", boundary];
        [request setValue:contentType forHTTPHeaderField: @"Content-Type"];
        // post body
        NSMutableData *body = [NSMutableData data];
        // add params (all params are strings)
        [body appendData:[[NSString stringWithFormat:@"--%@\r\n", boundary] dataUsingEncoding:NSUTF8StringEncoding]];
        [body appendData:[[NSString stringWithFormat:@"Content-Disposition: form-data; name=%@\r\n\r\n", @"imageCaption"] dataUsingEncoding:NSUTF8StringEncoding]];
        [body appendData:[[NSString stringWithFormat:@"%@\r\n", @"Some Caption"] dataUsingEncoding:NSUTF8StringEncoding]];
        // add image data
        if (imageData) {
            [body appendData:[[NSString stringWithFormat:@"--%@\r\n", boundary] dataUsingEncoding:NSUTF8StringEncoding]];
            [body appendData:[[NSString stringWithFormat:@"Content-Disposition: form-data; name=%@; filename=imageName.jpg\r\n", @"photo"] dataUsingEncoding:NSUTF8StringEncoding]];
            [body appendData:[@"Content-Type: image/jpeg\r\n\r\n" dataUsingEncoding:NSUTF8StringEncoding]];
            [body appendData:imageData];
            [body appendData:[[NSString stringWithFormat:@"\r\n"] dataUsingEncoding:NSUTF8StringEncoding]];
        }
        
        [body appendData:[[NSString stringWithFormat:@"--%@--\r\n", boundary] dataUsingEncoding:NSUTF8StringEncoding]];
        // setting the body of the post to the reqeust
        [request setHTTPBody:body];
        // set the content-length
        NSString *postLength = [NSString stringWithFormat:@"%lu", (unsigned long)[body length]];
        [request setValue:postLength forHTTPHeaderField:@"Content-Length"];
        [NSURLConnection sendAsynchronousRequest:request queue:[NSOperationQueue currentQueue] completionHandler:^(NSURLResponse *response, NSData *data, NSError *error) {
            if(data.length > 0)
            {
                //success
                NSDictionary* jsonDict = [NSJSONSerialization
                                          JSONObjectWithData:data //1
                                          options:kNilOptions
                                          error:&error];
                
                if ([[jsonDict objectForKey:@"message"]isEqualToString:@"Success"])
                {
                    _txtField_email.text=@"";
                    _txtField_passord.text=@"";
                    
                    str_email=str_facebookID;
                    str_password=str_FPassword;

                    [self login_user];
                }
                else if ([[jsonDict objectForKey:@"message"]isEqualToString:@"Error :: User Image Not Found"])
                {
                    [SVProgressHUD dismiss];
                    UIAlertView *alert=[[UIAlertView alloc]initWithTitle:@"" message:@"Error with facebook login." delegate:self cancelButtonTitle:@"OK" otherButtonTitles: nil];
                    [alert show];
                }
                [SVProgressHUD dismiss];
            }
        }];
    }
     
     
     
    -(void)getDeviceToken
    {
        
        NSMutableDictionary *jsonDict=[[NSMutableDictionary alloc]init];
        NSString *token=[NSString stringWithFormat:@"%@",[defaults objectForKey:@"user_token"]];
        [jsonDict setValue:[defaults objectForKey:@"user_id"] forKey:@"id"];
        NSLog(@"Device Token %@",[defaults objectForKey:@"device_token"]);
        [jsonDict setValue:[defaults objectForKey:@"device_token"] forKey:@"device_token"];
        
        NSString *url=[NSString stringWithFormat:@"%@addDeviceToken?token=%@",serviceLink,token];
//        NSLog(@"%@",jsonDict);
        NSLog(@"getDeviceToken %@", [jsonDict JSONRepresentation]);
        id  json1 = [WebService signup:jsonDict:url];
        NSLog(@"Device token Response %@",json1);
        
        if ([[json1 objectForKey:@"message"]isEqualToString:@"Success"])
        {
            
        }
        else if ([[json objectForKey:@"error"]isEqualToString:@"token_expired"])
        {
            [SVProgressHUD dismiss];
            UIAlertView *alert=[[UIAlertView alloc]initWithTitle:@"" message:@"Session Expired Please login agian" delegate:nil cancelButtonTitle:@"OK" otherButtonTitles: nil];
            [alert show];
            AppDelegate *delegate;
            delegate=(AppDelegate*)[[UIApplication sharedApplication]delegate];
            [defaults setBool:NO forKey:@"logined"];
            [defaults synchronize];
            [[NSUserDefaults standardUserDefaults]synchronize];
            [delegate loginStatus];
        }
        else
        {
            UIAlertView *alert=[[UIAlertView alloc]initWithTitle:@"Alert!" message:@"Error to update Device token." delegate:nil cancelButtonTitle:@"OK" otherButtonTitles: nil];
            [alert show];
        }
    }
     
     @end
