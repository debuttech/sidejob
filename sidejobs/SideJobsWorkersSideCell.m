//
//  SideJobsWorkersCell.m
//  WorkerEarning
//
//  Created by Debut Mac 4 on 19/08/15.
//  Copyright (c) 2015 debut.infotech. All rights reserved.
//

#import "SideJobsWorkersSideCell.h"

@implementation SideJobsWorkersSideCell

- (void)awakeFromNib
{
    // Initialization code
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated
{
    [super setSelected:selected animated:animated];
    CGFloat borderWidth = 0.5f;
    
    _view_AppliedJob.layer.borderColor = [UIColor lightGrayColor].CGColor;
    _view_AppliedJob.layer.borderWidth = borderWidth;
    
    _btn_amount.layer.masksToBounds=YES;
    _btn_amount.layer.cornerRadius= _btn_amount.bounds.size.width/2 ;
    
    _image_OfCustomer.layer.cornerRadius = 12;
    _image_OfCustomer.clipsToBounds = YES;
    
     // Configure the view for the selected state
}

@end
