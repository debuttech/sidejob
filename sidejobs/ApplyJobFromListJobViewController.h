//
//  ApplyJobFromListJobViewController.h
//  sidejobs
//
//  Created by DebutMac4 on 09/09/15.
//  Copyright (c) 2015 DebutMac4. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <Foundation/Foundation.h>
#import "MWPhotoBrowser.h"

@interface ApplyJobFromListJobViewController : UIViewController<MWPhotoBrowserDelegate>
{
    
    IBOutlet UILabel *lbl_title;
    IBOutlet UILabel *lbl_category;
    IBOutlet UILabel *lbl_postetOn;
    IBOutlet UITextView *textView_about;
    
    IBOutlet UIImageView *imgView_category;
    IBOutlet UIButton *btn_amount;
    
    IBOutlet UIButton *btn_slot1;
    IBOutlet UIButton *btn_slot2;
    IBOutlet UIButton *btn_slot3;
    
    IBOutlet UITextField *txtfld_budget;
    IBOutlet UILabel *lbi_comment;
    IBOutlet UITextView *txtView_addcomment;
    IBOutlet UIScrollView *scrollViewSlotTime;
    IBOutlet UIButton *btn_ApplyJob;
    IBOutlet UIScrollView *scrollView_MAinView;
    IBOutlet UIButton *img1Btn_Obj;
    IBOutlet UIButton *img2Btn_Obj;
    IBOutlet UIButton *img3Btn_Obj;
    
}

@property (strong, nonatomic) NSMutableDictionary *jobDict;
@property(strong,nonatomic)NSMutableDictionary *dict_Detailinvitation;
@property(strong,nonatomic) NSString *status;

- (IBAction)btn_applyJoob:(id)sender;
- (IBAction)btnSlotAction1:(id)sender;
- (IBAction)btnSlotAction2:(id)sender;
- (IBAction)btnSlotAction3:(id)sender;
- (IBAction)btn1Action:(id)sender;

@end
