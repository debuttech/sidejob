//
//  JobCompletedWorkerCell.m
//  WorkerEarning
//
//  Created by Debut Mac 4 on 20/08/15.
//  Copyright (c) 2015 debut.infotech. All rights reserved.
//

#import "JobCompletedWorkerCell.h"

@implementation JobCompletedWorkerCell

- (void)awakeFromNib
{
    // Initialization code
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated
{
    [super setSelected:selected animated:animated];
    CGFloat borderWidth = 0.3f;

    _view_complete.layer.borderColor = [UIColor lightGrayColor].CGColor;
    _view_complete.layer.borderWidth = borderWidth;
    _image_OfCustomer.layer.masksToBounds=YES;
    _image_OfCustomer.layer.cornerRadius=_image_OfCustomer.bounds.size.width/2;
    _lbl_Amount.layer.masksToBounds=YES;
    _lbl_Amount.layer.cornerRadius=_lbl_Amount.bounds.size.width/2;
    
    // Configure the view for the selected state
}

@end
