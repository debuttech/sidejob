//
//  PaymentRecievedCell.h
//  WorkerEarning
//
//  Created by Debut Mac 4 on 18/08/15.
//  Copyright (c) 2015 debut.infotech. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface PaymentRecievedCell : UITableViewCell
{
    
}
@property (strong, nonatomic) IBOutlet UILabel *lbl_CategoryName;
@property (strong, nonatomic) IBOutlet UILabel *lbl_StartedOn;
@property (strong, nonatomic) IBOutlet UILabel *lbl_CompletedOn;
@property (strong, nonatomic) IBOutlet UILabel *lbl_SectionName;
@property (strong, nonatomic) IBOutlet UILabel *lbl_Title;
@property (strong, nonatomic) IBOutlet UIImageView *img_Category;
@property (strong, nonatomic) IBOutlet UILabel *lbl_amout;

@property (strong, nonatomic) IBOutlet UIView *view_Background;
@property (strong, nonatomic) IBOutlet UILabel *lbl_pendingPayStatus;

@end


