//
//  AppDelegate.h
//  sideJobs
//
//  Created by DebutMac4 on 13/08/15.
//  Copyright (c) 2015 DebutMac4. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface AppDelegate : UIResponder <UIApplicationDelegate,UINavigationControllerDelegate>
{
    NSUserDefaults *defaults;
}
@property(strong,nonatomic) NSString *Noti_badgeNumber;

@property (strong, nonatomic) UIWindow *window;
@property(strong,nonatomic)UINavigationController *nav;
@property(strong,nonatomic)NSString *applicantCount;
-(void)loginStatus;
-(void)readNotification;


@end

