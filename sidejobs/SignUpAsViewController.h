//
//  SignUpAsViewController.h
//  sideJobs
//
//  Created by DebutMac4 on 13/08/15.
//  Copyright (c) 2015 DebutMac4. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "SignupViewController.h"

@interface SignUpAsViewController : UIViewController
{
    SignupViewController *signupView;
}
- (IBAction)btnBack_Action:(id)sender;

- (IBAction)btnSignUp_Action:(id)sender;
@end
