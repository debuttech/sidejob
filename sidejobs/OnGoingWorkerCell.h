//
//  OnGoingWorkerCell.h
//  WorkerEarning
//
//  Created by Debut Mac 4 on 20/08/15.
//  Copyright (c) 2015 debut.infotech. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface OnGoingWorkerCell : UITableViewCell
{
    
}
@property (strong, nonatomic) IBOutlet UILabel *lbl_HeadingOnGoing;
@property (strong, nonatomic) IBOutlet UIImageView *image_OfCustomer;
@property (strong, nonatomic) IBOutlet UILabel *lbl_CustomerOfOngoing;
@property (strong, nonatomic) IBOutlet UILabel *lbl_CategoryOfOngoing;
@property (strong, nonatomic) IBOutlet UILabel *lbl_StartedDateOfOutgoing;
@property (strong, nonatomic) IBOutlet UIView *view_ongoing;
@property (strong, nonatomic) IBOutlet UIImageView *img_category;

@property (strong, nonatomic) IBOutlet UIButton *btn_amount;
@property (strong, nonatomic) IBOutlet UIButton *btn_confirmJob;
@property (strong, nonatomic) IBOutlet UILabel *lbl_Pending;


@end
