 //
//  MySideJobs.m
//  WorkerEarning
//
//  Created by Debut Mac 4 on 19/08/15.
//  Copyright (c) 2015 debut.infotech. All rights reserved.


#import "MySideJobsFromWorkerSides.h"
#import "SideJobsWorkersSideCell.h"
#import "OnGoingWorkerCell.h"
#import "JobCompletedWorkerCell.h"
#import "MyInvitationCell.h"
#import "TPKeyboardAvoidingScrollView.h"
#import "ApplyJobFromListJobViewController.h"
@interface MySideJobsFromWorkerSides ()
{
    NSUserDefaults*defaults;
    NSMutableArray *arr_AppliedJob;
    NSMutableArray *arr_onGoingJob;
    NSMutableArray *arr_invitationJob;
     NSMutableArray *arr_completedJob;
    NSString *markJObId;
}

@end        

@implementation MySideJobsFromWorkerSides

- (void)viewDidLoad
{
    [super viewDidLoad];
    [self createUserExperience];

}
-(void)createUserExperience
{
    defaults=[NSUserDefaults standardUserDefaults];
    self.navigationItem.hidesBackButton=YES;
    self.navigationItem.title=@"My SideJobs";
    segmentControler_obj.selected=0;
    
    [SVProgressHUD showWithStatus:@"Loading"];
    [self performSelector:@selector(FetchappliedJobs) withObject:nil afterDelay:0.3f];
    
    // [self FetchappliedJobs];
    UIGraphicsBeginImageContextWithOptions(CGSizeMake(1, segmentControler_obj.frame.size.height), NO, 0.0);
    UIImage *blank = UIGraphicsGetImageFromCurrentImageContext();
    UIGraphicsEndImageContext();
    [segmentControler_obj setDividerImage:blank
                      forLeftSegmentState:UIControlStateNormal
                        rightSegmentState:UIControlStateNormal
                               barMetrics:UIBarMetricsDefault];
    
    UIButton *backButton=[UIButton buttonWithType:UIButtonTypeCustom];
    [backButton setFrame:CGRectMake(0, 0, 23, 21)];
    [backButton addTarget:self action:@selector(backButton:) forControlEvents:UIControlEventTouchUpInside];
    [backButton setImage:[UIImage imageNamed:@"arrow.png"] forState:UIControlStateNormal];
    self.navigationItem.leftBarButtonItem=[[UIBarButtonItem alloc]initWithCustomView:backButton];

}
-(void)backButton:(id)sender
{
    [self.navigationController popViewControllerAnimated:YES];
}
- (UIImage *)imageWithColor:(UIColor *)color {
    CGRect rect = CGRectMake(0.0f, 0.0f, 1.0f, 1.0f);
    UIGraphicsBeginImageContext(rect.size);
    CGContextRef context = UIGraphicsGetCurrentContext();
    CGContextSetFillColorWithColor(context, [color CGColor]);
    CGContextFillRect(context, rect);
    UIImage *image = UIGraphicsGetImageFromCurrentImageContext();
    UIGraphicsEndImageContext();
    return image;
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
}
-(void)FetchappliedJobs
{
    arr_AppliedJob=[[NSMutableArray alloc]init];
   // http://192.168.0.29:8000/appliedJobList/{id}
    NSString *token=[NSString stringWithFormat:@"%@",[defaults objectForKey:@"user_token"]];
    NSString *url=[NSString stringWithFormat:@"%@appliedJobList/%@?token=%@",serviceLink,[defaults objectForKey:@"user_id"],token];
    id json = [WebService GetwithoutDict:url];
    if ([[json objectForKey:@"message"]isEqualToString:@"Success"])
    {
        NSMutableDictionary *dict=[[NSMutableDictionary alloc]init];
        dict=[json objectForKey:@"response"];
        arr_AppliedJob=[dict objectForKey:@"job_applications"];
        tableView_AppliedJobsWorker.dataSource=self;
        tableView_AppliedJobsWorker.delegate=self;
        [tableView_AppliedJobsWorker reloadData];
        
    }
    else if ([[json objectForKey:@"error"]isEqualToString:@"token_expired"])
    {
        [SVProgressHUD dismiss];
        UIAlertView *alert=[[UIAlertView alloc]initWithTitle:@"" message:@"Session Expired Please login agian" delegate:nil cancelButtonTitle:@"OK" otherButtonTitles: nil];
        [alert show];
        AppDelegate *delegate;
        delegate=(AppDelegate*)[[UIApplication sharedApplication]delegate];
        [defaults setBool:NO forKey:@"logined"];
        [defaults synchronize];
        [[NSUserDefaults standardUserDefaults]synchronize];
        [delegate loginStatus];
    }
    
    if (arr_AppliedJob.count < 1)
    {
        
         lbl_NoRecord.hidden = FALSE;
    }else{
         lbl_NoRecord.hidden = YES;
        [tableView_AppliedJobsWorker reloadData];
    }
    NSLog(@"%@",arr_AppliedJob);
    [SVProgressHUD dismiss];
    
  
}
-(void)onGoningJob
{
 //  http://112.196.27.244:8000/applied-workers
    NSString *token=[NSString stringWithFormat:@"%@",[defaults objectForKey:@"user_token"]];
    NSString *url=[NSString stringWithFormat:@"%@applied-workers?token=%@",serviceLink,token];
    NSMutableDictionary *dict=[[NSMutableDictionary alloc]init];
    [dict setValue:[defaults objectForKey:@"user_id"] forKey:@"id"];
    id json = [WebService signup:dict :url];
    if ([[json objectForKey:@"message"]isEqualToString:@"Success"])
    {
        arr_onGoingJob=[[NSMutableArray alloc]init];
        NSMutableArray *arrResponse=[[NSMutableArray alloc]init];
        
        arrResponse=[json objectForKey:@"response"];
        for (int i=0; i<arrResponse.count; i++)
        {
            if ([[[[[arrResponse objectAtIndex:i]objectForKey:@"job_applications"]objectAtIndex:0]objectForKey:@"applicant_job_status"]isEqualToString:@"accepted"]||[[[[[arrResponse objectAtIndex:i]objectForKey:@"job_applications"]objectAtIndex:0]objectForKey:@"applicant_job_status"]isEqualToString:@"completed"])
            {
                [arr_onGoingJob addObject:[arrResponse objectAtIndex:i]];
            }
        }
        tableView_OngoingWorkers.dataSource=self;
        tableView_OngoingWorkers.delegate=self;
        [tableView_OngoingWorkers reloadData];
        
    }
    else if ([[json objectForKey:@"error"]isEqualToString:@"token_expired"])
    {
        [SVProgressHUD dismiss];
        UIAlertView *alert=[[UIAlertView alloc]initWithTitle:@"" message:@"Session Expired Please login agian" delegate:nil cancelButtonTitle:@"OK" otherButtonTitles: nil];
        [alert show];
        AppDelegate *delegate;
        delegate=(AppDelegate*)[[UIApplication sharedApplication]delegate];
        [defaults setBool:NO forKey:@"logined"];
        [defaults synchronize];
        [[NSUserDefaults standardUserDefaults]synchronize];
        [delegate loginStatus];
    }

    if (arr_onGoingJob.count < 1)
    {
        lbl_NoRecord.hidden = FALSE;
    }else{
        [tableView_OngoingWorkers reloadData];
        lbl_NoRecord.hidden = YES;
    }
    NSLog(@"%@",arr_AppliedJob);
       [SVProgressHUD dismiss];
}

#pragma mark
#pragma mark -TableView Deletgate method
-(NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    return 1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section;
{
   // if (segmentControler_obj.selected==0)
    if (tableView==tableView_AppliedJobsWorker)
    {
        return [arr_AppliedJob count];
    }
  //  else if (segmentControler_obj.selected == 1)
    else if (tableView==tableView_OngoingWorkers)
    {
        return [arr_onGoingJob count];
    }
    else if (tableView==tableViewMyInvitation)
    {
        return [arr_invitationJob count];
    }
    else
    {
         return arr_completedJob.count;
    }
   
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{

    if (tableView==tableView_AppliedJobsWorker)
    {
        static NSString *customIdentifierForSideJobsFromWorkers = @"sideJobsWorkersCell1";
        SideJobsWorkersSideCell *cell = [tableView dequeueReusableCellWithIdentifier:customIdentifierForSideJobsFromWorkers];
        if (cell == nil)
        {
            cell = [[SideJobsWorkersSideCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:customIdentifierForSideJobsFromWorkers];
        }
        if ([arr_AppliedJob count]==0)
        {
        }
        else
        {
        NSMutableDictionary *job=[[NSMutableDictionary alloc]init];
        job=[[arr_AppliedJob objectAtIndex:indexPath.row]objectForKey:@"job"];
        cell.lbl_HeadingName.text=[job objectForKey:@"title"];
        cell.lbl_CustomerName.text=[[job objectForKey:@"user"]objectForKey:@"name"];
            
        NSString *profileImg=[[job objectForKey:@"user"]objectForKey:@"profile_image"];
        NSURL *url=[NSURL URLWithString:[NSString stringWithFormat:@"%@%@",serviceLink,profileImg]];
            if (profileImg.length<1)
            {
              cell.image_OfCustomer.image=[UIImage imageNamed:@"user.png"];
            }
            else
            {
                dispatch_async(dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_BACKGROUND, 0), ^{
                                      NSData * imageData = [NSData dataWithContentsOfURL:url];
                    UIImage *image = [UIImage imageWithData:imageData];
                    
                    dispatch_sync(dispatch_get_main_queue(), ^{ //in main thread update the image
                        cell.image_OfCustomer.image = image;
                        cell.textLabel.text = @""; //add this update will reflect the changes 
                    });
                });
            }
      
            //Seprated the time
            //start job time
            NSString *startTime=[[arr_AppliedJob objectAtIndex:indexPath.row]objectForKey:@"created_at"];
            
            NSArray *ArrStart=[startTime componentsSeparatedByString:@" "];
            NSString* seprateStarttime=[ArrStart objectAtIndex:0];
            cell.lbl_AppliedDateOfWorker.text=seprateStarttime;
            
        cell.lbl_PostedDateOfWorker.text=[[[arr_AppliedJob objectAtIndex:indexPath.row]objectForKey:@"job"] objectForKey:@"created_at"];
//        cell.lbl_AppliedDateOfWorker.text=[[arr_AppliedJob objectAtIndex:indexPath.row]objectForKey:@"created_at"];
        cell.lbl_Location.text=[[[arr_AppliedJob objectAtIndex:indexPath.row]objectForKey:@"job"]objectForKey:@"location"];
           NSString *amount= [NSString stringWithFormat:@"%@",[[arr_AppliedJob objectAtIndex:indexPath.row]objectForKey:@"proposed_amount"]];
            if (amount.length<=0)
            {
                cell.btn_amount.hidden=YES;
            }
            else
            {
                cell.btn_amount.hidden=NO;
               [cell.btn_amount setTitle:[NSString stringWithFormat:@"$%@",[[arr_AppliedJob objectAtIndex:indexPath.row]objectForKey:@"proposed_amount"]] forState:UIControlStateNormal];
            }
            NSString *applied=[[arr_AppliedJob objectAtIndex:indexPath.row]objectForKey:@"applicant_job_status"];
            if ([applied isEqualToString:@"applied"] ||[applied isEqualToString:@"accepted"] ||[applied isEqualToString:@"rejected"] ||[applied isEqualToString:@"reject"]||[applied isEqualToString:@"invited"]||[applied isEqualToString:@"confirmed"]||[applied isEqualToString:@"declined"])
            {
                NSString *firstCapChar = [[applied substringToIndex:1] capitalizedString];
                NSString *cappedString = [applied stringByReplacingCharactersInRange:NSMakeRange(0,1) withString:firstCapChar];
                cell.lbl_status.text=cappedString;
            }
        }
        return cell;
    }

    else if (tableView==tableView_OngoingWorkers)

    {
            static NSString *customIdentifierForOngoingWorkers = @"onGoingWorkerCell";
            OnGoingWorkerCell *cell = [tableView dequeueReusableCellWithIdentifier:customIdentifierForOngoingWorkers];
            if (cell == nil)
            {
                cell = [[OnGoingWorkerCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:customIdentifierForOngoingWorkers];
            }
            cell.image_OfCustomer.layer.cornerRadius = 12;
            cell.image_OfCustomer.clipsToBounds = YES;
        if ([arr_onGoingJob count]==0)
        {
            
        }
        else
        {
            NSMutableArray *job=[[NSMutableArray alloc]init];
            NSMutableDictionary *user=[[NSMutableDictionary alloc]init];
            NSMutableDictionary *jobsdetail=[[NSMutableDictionary alloc]init];
            job=[[arr_onGoingJob objectAtIndex:indexPath.row]objectForKey:@"job_awarded"];
            jobsdetail=[job objectAtIndex:0];
            user=[[arr_onGoingJob objectAtIndex:indexPath.row]objectForKey:@"user"];
          NSString *amount=[NSString stringWithFormat:@"%@",[jobsdetail objectForKey:@"proposed_amount"]];
          [cell.btn_amount setTitle:[NSString stringWithFormat:@"$%@",amount] forState:UIControlStateNormal];
            
            cell.btn_confirmJob.tag=indexPath.row;
            [cell.btn_confirmJob addTarget:self action:@selector(confirmJobFromOnGoing:) forControlEvents:UIControlEventTouchUpInside];
            
            NSString *profileImg=[user objectForKey:@"profile_image"];
            NSURL *url=[NSURL URLWithString:[NSString stringWithFormat:@"%@%@",serviceLink,profileImg]];
          NSString *catId=[NSString stringWithFormat:@"%@",[[arr_onGoingJob objectAtIndex:indexPath.row]objectForKey:@"category_id"] ];
          cell.lbl_CustomerOfOngoing.text=[user objectForKey:@"name"];
          cell.lbl_HeadingOnGoing.text=[[arr_onGoingJob objectAtIndex:indexPath.row]objectForKey:@"title"];
            
            //Seprated the time
            //start job time
            NSString *startTime=[[arr_onGoingJob objectAtIndex:indexPath.row]valueForKeyPath:@"updated_at"];
            NSArray *ArrStart=[startTime componentsSeparatedByString:@" "];
            NSString* seprateStarttime=[ArrStart objectAtIndex:0];
            cell.lbl_StartedDateOfOutgoing.text=seprateStarttime;
            
            

            
            //job confirmed from worker than message..
            if ([[[[[arr_onGoingJob objectAtIndex:indexPath.row]objectForKey:@"job_applications"]objectAtIndex:0]objectForKey:@"applicant_job_status"]isEqualToString:@"confirmed"])
            {
                cell.lbl_Pending.text=@"Payment Pending";
            }
            if ([[[[[arr_onGoingJob objectAtIndex:indexPath.row]objectForKey:@"job_applications"]objectAtIndex:0]objectForKey:@"applicant_job_status"]isEqualToString:@"completed"])
            {
                cell.lbl_Pending.text=@"Completed";
                cell.lbl_Pending.textColor=[self colorFromHexString:@"#008000"];
                cell.btn_confirmJob.backgroundColor=[UIColor grayColor];
            }
            if ([[[[[arr_onGoingJob objectAtIndex:indexPath.row]objectForKey:@"job_applications"]objectAtIndex:0]objectForKey:@"applicant_job_status"]isEqualToString:@"accepted"])
            {
                cell.lbl_Pending.text=@"In Progress";
            }
            
            if (profileImg.length<1)
            {
                cell.image_OfCustomer.image=[UIImage imageNamed:@"user.png"];
            }
            else
            {
                dispatch_async(dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_BACKGROUND, 0), ^{
                    NSData * imageData = [NSData dataWithContentsOfURL:url];
                    UIImage *image = [UIImage imageWithData:imageData];
                    
                    dispatch_sync(dispatch_get_main_queue(), ^{ //in main thread update the image
                        cell.image_OfCustomer.image = image;
                        cell.textLabel.text = @""; //add this update will reflect the changes
                    });
                });
                
            }
            
          if ([catId isEqualToString:@"1"]) {
              cell.lbl_CategoryOfOngoing.text = @"Home" ;
              cell.img_category.image=[UIImage imageNamed:@"cat_home"];
              
          }else if([catId isEqualToString:@"2"])
          {
              cell.lbl_CategoryOfOngoing.text = @"Auto" ;
              cell.img_category.image=[UIImage imageNamed:@"car_icon"];
              
          }else if([catId isEqualToString:@"3"])
          {
              cell.lbl_CategoryOfOngoing.text =@"Electronic" ;
              cell.img_category.image=[UIImage imageNamed:@"cat_elc"];
          }

        }
        return cell;
        }
    
    else if (tableView==tableViewMyInvitation)
    {
        static NSString *cellIdentifier = @"myInvitationCell";
        MyInvitationCell *cell = [tableView dequeueReusableCellWithIdentifier:cellIdentifier];
        if (cell == nil)
        {
            cell = [[MyInvitationCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:cellIdentifier];
        }
        
        
        cell.lbl_Amount.layer.cornerRadius = 25;
        cell.lbl_Amount.clipsToBounds = YES;
        cell.userIcon_img.layer.cornerRadius = 12;
        cell.userIcon_img.clipsToBounds = YES;
        cell.lbl_Heading.text=[[arr_invitationJob objectAtIndex:indexPath.row]objectForKey:@"title"];
        cell.lbl_UserName.text=[[[arr_invitationJob objectAtIndex:indexPath.row]objectForKey:@"user"]objectForKey:@"name"];
        cell.lbl_CatName.text=[NSString stringWithFormat:@"%@",[[arr_invitationJob objectAtIndex:indexPath.row]objectForKey:@"category_id"] ];
        cell.lbl_InviteOn.text=[[arr_invitationJob objectAtIndex:indexPath.row]valueForKeyPath:@"created_at"];
       
        cell.btnAccept.tag=indexPath.row;
        cell.btnDecline.tag=indexPath.row;
        [cell.btnAccept addTarget:self action:@selector(AcceptJob:) forControlEvents:UIControlEventTouchUpInside];
        [cell.btnDecline addTarget:self action:@selector(DeclineJob:) forControlEvents:UIControlEventTouchUpInside];
        
        NSString *image=[[[arr_invitationJob objectAtIndex:indexPath.row]objectForKey:@"user"]objectForKey:@"profile_image"];
        NSString *imgUrl=[NSString stringWithFormat:@"%@%@",serviceLink,image];
        
        
        if (image.length<1)
        {
            cell.userIcon_img.image=[UIImage imageNamed:@"user.png"];
        }
        else
        {
            dispatch_async(dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_BACKGROUND, 0), ^{
                NSData * imageData = [NSData dataWithContentsOfURL:[NSURL URLWithString:imgUrl]];
                UIImage *image = [UIImage imageWithData:imageData];
                
                dispatch_sync(dispatch_get_main_queue(), ^{ //in main thread update the image
                    cell.userIcon_img.image = image;
                    cell.textLabel.text = @""; //add this update will reflect the changes
                });
            });
           
        }

        NSString *catId=[NSString stringWithFormat:@"%@",[[arr_invitationJob objectAtIndex:indexPath.row]objectForKey:@"category_id"]];
       
        if ([catId isEqualToString:@"1"]) {
            cell.lbl_CatName.text = @"Home" ;
            cell.imgCategory.image=[UIImage imageNamed:@"cat_home"];
            
        }else if([catId isEqualToString:@"2"])
        {
            cell.lbl_CatName.text = @"Auto" ;
            cell.imgCategory.image=[UIImage imageNamed:@"car_icon"];
            
        }else if([catId isEqualToString:@"3"])
        {
           cell.lbl_CatName.text =@"Electronic" ;
            cell.imgCategory.image=[UIImage imageNamed:@"cat_elc"];
        }

        
        return cell;

    }
    else
    {
        static NSString *customIdentifierForJobCompletedWorkers = @"jobCompletedWorkerCell";
        JobCompletedWorkerCell *cell = [tableView dequeueReusableCellWithIdentifier:customIdentifierForJobCompletedWorkers];
        if (cell == nil)
        {
            cell = [[JobCompletedWorkerCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:customIdentifierForJobCompletedWorkers];
        }
        
        NSString *image=[[[arr_completedJob objectAtIndex:indexPath.row]objectForKey:@"user"]objectForKey:@"profile_image"];
        NSString *imgUrl=[NSString stringWithFormat:@"%@%@",serviceLink,image];
        
        
        if (image.length<1)
        {
            cell.image_OfCustomer.image=[UIImage imageNamed:@"user.png"];
        }
        else
        {
            dispatch_async(dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_BACKGROUND, 0), ^{
                NSData * imageData = [NSData dataWithContentsOfURL:[NSURL URLWithString:imgUrl]];
                UIImage *image = [UIImage imageWithData:imageData];
                
                dispatch_sync(dispatch_get_main_queue(), ^{ //in main thread update the image
                    cell.image_OfCustomer.image = image;
                });
            });
            
        }
        
        cell.lbl_Title.text=[[arr_completedJob objectAtIndex:indexPath.row]objectForKey:@"title"];
        cell.lbl_Amount.text=[NSString stringWithFormat:@"$%@",[[[[arr_completedJob objectAtIndex:indexPath.row] objectForKey:@"job_awarded"]objectAtIndex:0]objectForKey:@"proposed_amount"] ];
        cell.lblCustomerOf_JobCompleted.text=[[[arr_completedJob objectAtIndex:indexPath.row]objectForKey:@"user"]objectForKey:@"name"];
       
        
        //complete job time
        NSString *completeTime=[[[[arr_completedJob objectAtIndex:indexPath.row]objectForKey:@"job_applications"] objectAtIndex:0]objectForKey:@"updated_at"];
        NSArray *Arrtime=[completeTime componentsSeparatedByString:@" "];
        NSString* seprateCompleteTime=[Arrtime objectAtIndex:0];
        cell.lblCompletedDate_JobCompleted.text=seprateCompleteTime;
        
        //start job time
        NSString *startTime=[[[[arr_completedJob objectAtIndex:indexPath.row]objectForKey:@"job_awarded"] objectAtIndex:0]objectForKey:@"created_at"];
        NSArray *ArrStart=[startTime componentsSeparatedByString:@" "];
        NSString* seprateStarttime=[ArrStart objectAtIndex:0];
        cell.lblStartedOn_JobCompleted.text=seprateStarttime;
        
        
       
        NSString *catId=[NSString stringWithFormat:@"%@",[[arr_completedJob objectAtIndex:indexPath.row]objectForKey:@"category_id"] ];
        if ([catId isEqualToString:@"1"]) {
            cell.lblCategoty_JobCompleted.text = @"Home" ;
            cell.img_Category.image=[UIImage imageNamed:@"cat_home"];
            
        }else if([catId isEqualToString:@"2"])
        {
            cell.lblCategoty_JobCompleted.text = @"Auto" ;
            cell.img_Category.image=[UIImage imageNamed:@"car_icon"];
            
        }else if([catId isEqualToString:@"3"])
        {
            cell.lblCategoty_JobCompleted.text =@"Electronic" ;
            cell.img_Category.image=[UIImage imageNamed:@"cat_elc"];
        }
        

       
        return cell;
    }
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
   
}


- (UIColor *)colorFromHexString:(NSString *)hexString {
    unsigned rgbValue = 0;
    NSScanner *scanner = [NSScanner scannerWithString:hexString];
    [scanner setScanLocation:1]; // bypass '#' character
    [scanner scanHexInt:&rgbValue];
    return [UIColor colorWithRed:((rgbValue & 0xFF0000) >> 16)/255.0 green:((rgbValue & 0xFF00) >> 8)/255.0 blue:(rgbValue & 0xFF)/255.0 alpha:1.0];
}
#pragma mark
#pragma mark-confirmJob from Ongoing Side
-(void)confirmJobFromOnGoing:(UIButton*)sender
{
     markJObId=[NSString stringWithFormat:@"%ld",(long)sender.tag];
    NSString *str=[[[[arr_onGoingJob objectAtIndex:[markJObId integerValue]]objectForKey:@"job_applications"]objectAtIndex:0] objectForKey:@"applicant_job_status"];
    if ([str isEqualToString:@"completed"])
    {
        UIAlertView *alert=[[UIAlertView alloc]initWithTitle:@"" message:@"You have already mark this job as completed." delegate:nil cancelButtonTitle:@"OK" otherButtonTitles: nil];
        [alert show];

    }
   else if ([str isEqualToString:@"accepted"])
   {
       UIAlertView *alert=[[UIAlertView alloc]initWithTitle:@"" message:@"Are you sure, this job has been completed?" delegate:self cancelButtonTitle:@"Yes" otherButtonTitles:@"NO", nil];
       [alert show];
       alert.tag=1;

   }
    
    
}
-(void)markJOb
{
    
    NSString *id_job=  [[[[arr_onGoingJob  objectAtIndex:[markJObId integerValue]]objectForKey:@"job_applications"]objectAtIndex:0]objectForKey:@"id"];
    NSString *proposedAmount=[[[[arr_onGoingJob objectAtIndex:[markJObId integerValue]]objectForKey:@"job_awarded"]objectAtIndex:0] objectForKey:@"proposed_amount"];
    
    NSString *token=[NSString stringWithFormat:@"%@",[defaults objectForKey:@"user_token"]];
    NSString *url=[NSString stringWithFormat:@"%@job-status?token=%@",serviceLink,token];
    NSMutableDictionary *jsonDict=[[NSMutableDictionary alloc]init];
    
    [jsonDict setValue:id_job forKey:@"job_application_id"] ;
    [jsonDict setValue:@"completed" forKey:@"applicant_job_status"];
    [jsonDict setValue:proposedAmount forKey:@"proposed_amount"];
    [jsonDict setValue:@""forKey:@"comment"];
    
        //change to notification
    [jsonDict setValue:[[[[arr_onGoingJob objectAtIndex:[markJObId integerValue]] objectForKey:@"job_awarded"] objectAtIndex:0]objectForKey:@"job_id"] forKey:@"job_id"];
    
    [jsonDict setValue:[[[arr_onGoingJob objectAtIndex:[markJObId integerValue]]objectForKey:@"user"] objectForKey:@"id"] forKey:@"job_user_id"];
    
    [jsonDict setValue:[defaults objectForKey:@"user_id"] forKey:@"sender_id"];
    
    [jsonDict setValue:[[[arr_onGoingJob objectAtIndex:[markJObId integerValue]]objectForKey:@"user"] objectForKey:@"id"] forKey:@"receiver_id"];
    
    [jsonDict setValue:[defaults objectForKey:@"p_name"] forKey:@"sender_name"];
    
    id json=[WebService signup:jsonDict :url];
    if ([[json objectForKey:@"message"]isEqualToString:@"Success"])
    {
        [SVProgressHUD dismiss];
       
//        [SVProgressHUD showWithMaskType:SVProgressHUDMaskTypeClear];
//        [self performSelector:@selector(onGoningJob) withObject:nil afterDelay:0.1f];
        
        UIAlertView *alert=[[UIAlertView alloc]initWithTitle:@"" message:@"You have marked this job as completed." delegate:self cancelButtonTitle:@"OK" otherButtonTitles: nil];
        alert.tag=2;
        [alert show];
       
    }
    else
    {
         [SVProgressHUD dismiss];
    UIAlertView *alert=[[UIAlertView alloc]initWithTitle:@"" message:@"Some error is encountered while mark this job." delegate:nil cancelButtonTitle:@"OK" otherButtonTitles: nil];
    [alert show];
    }
     [SVProgressHUD dismiss];

}
#pragma mark
#pragma mark- alerView delegate
- (void)alertView:(UIAlertView *)alertView clickedButtonAtIndex:(NSInteger)buttonIndex
{
    if (alertView.tag==1)
    {
        switch (buttonIndex)
        {
            case 0:
                [SVProgressHUD show];
                [self performSelector:@selector(markJOb) withObject:nil afterDelay:0.2f];
               // [self markJOb];
                break;
                
            default:
                break;
        }
    }
    
    if (alertView.tag==2)
    {
        switch (buttonIndex)
        {
            case 0:
                [SVProgressHUD showWithMaskType:SVProgressHUDMaskTypeClear];
                [self performSelector:@selector(onGoningJob) withObject:nil afterDelay:0.1f];
                break;
                
            default:
                break;
        }
    }

    
}

#pragma mark
#pragma mark-Accept job and Decline Job
-(void)AcceptJob:(UIButton*)sender
{
    NSString *tag=[NSString stringWithFormat:@"%ld",(long)sender.tag];
    ApplyJobFromListJobViewController *applyView=[self.storyboard instantiateViewControllerWithIdentifier:@"ApplyJobFromListJobViewController"];
    applyView.jobDict=[arr_invitationJob objectAtIndex:[tag integerValue]];
    applyView.status=@"Myinvitation";
    applyView.dict_Detailinvitation=[arr_invitationJob objectAtIndex:[tag integerValue]];
    [self.navigationController pushViewController:applyView animated:YES];
}
-(void)DeclineJob:(UIButton*)sender
{
    NSString *tag=[NSString stringWithFormat:@"%ld",(long)sender.tag];
    
    NSString *token=[NSString stringWithFormat:@"%@",[defaults objectForKey:@"user_token"]];
    NSString *url=[NSString stringWithFormat:@"%@job-status?token=%@",serviceLink,token];
    
    NSMutableDictionary *jsonDict=[[NSMutableDictionary alloc]init];
    [jsonDict setValue:[[[[arr_invitationJob objectAtIndex:[tag integerValue]]objectForKey:@"job_application"]objectAtIndex:0] objectForKey:@"id"] forKey:@"job_application_id"];
    [jsonDict setValue:@"declined" forKey:@"applicant_job_status"];
    [jsonDict setValue:@"Not available"forKey:@"proposed_amount"];
    
    
    //change to notification
    [jsonDict setValue:[[[[arr_invitationJob objectAtIndex:[tag integerValue]] objectForKey:@"job_application"] objectAtIndex:0] objectForKey:@"job_id"] forKey:@"job_id"];
    
    [jsonDict setValue:[[[arr_invitationJob objectAtIndex:[tag integerValue]] objectForKey:@"user"] objectForKey:@"id"] forKey:@"job_user_id"];
    
    [jsonDict setValue:[defaults objectForKey:@"user_id"] forKey:@"sender_id"];
    
    [jsonDict setValue:[[[arr_invitationJob objectAtIndex:[tag integerValue]] objectForKey:@"user"] objectForKey:@"id"] forKey:@"receiver_id"];
    
    [jsonDict setValue:[defaults objectForKey:@"p_name"] forKey:@"sender_name"];

    
    
    
    id json=[WebService signup:jsonDict :url];
    if ([[json objectForKey:@"message"]isEqualToString:@"Success"])
    {
    UIAlertView *alert=[[UIAlertView alloc]initWithTitle:@"" message:@"You have Decline this job." delegate:nil cancelButtonTitle:@"OK" otherButtonTitles: nil];
            [alert show];
        [self.navigationController popViewControllerAnimated:YES];
        
    }
    else
    {
        UIAlertView *alert=[[UIAlertView alloc]initWithTitle:@"" message:@"Some error to decline this job." delegate:nil cancelButtonTitle:@"OK" otherButtonTitles: nil];
        [alert show];
    }
}
#pragma mark
#pragma mark-My invities list
- (IBAction)btn_Cross:(id)sender
{
    self.navigationController.navigationBarHidden = false;

    View_AppliedJobsWorker.frame=CGRectMake(0,View_AppliedJobsWorker.frame.origin.y, View_AppliedJobsWorker.frame.size.width, View_AppliedJobsWorker.frame.size.height);
    View_JobCompletedOfWorker.frame = CGRectMake(1000, View_JobCompletedOfWorker.frame.origin.y, View_JobCompletedOfWorker.frame.size.width, View_JobCompletedOfWorker.frame.size.height);
    view_ongoingContracts.frame = CGRectMake(1000, view_ongoingContracts.frame.origin.y, view_ongoingContracts.frame.size.width, view_ongoingContracts.frame.size.height);
    
    viewMyInvitations.frame=CGRectMake(1000,viewMyInvitations.frame.origin.y, viewMyInvitations.frame.size.width, viewMyInvitations.frame.size.height);
    [SVProgressHUD showWithStatus:@"Loading"];
    [self performSelector:@selector(FetchappliedJobs) withObject:nil afterDelay:0.3f];
    [tableView_AppliedJobsWorker reloadData];
    
    [btn_OngoingObj setBackgroundImage:[UIImage imageNamed:@"ongoingcontact"] forState:UIControlStateNormal];
    [btn_appliedObj setBackgroundImage:[UIImage imageNamed:@"appiledjob-Active"] forState:UIControlStateNormal];
    [btn_CompeleteObj setBackgroundImage:[UIImage imageNamed:@"jobcomplete@2x-1"] forState:UIControlStateNormal];

}

- (IBAction)btnInvities:(id)sender
{
    self.navigationController.navigationBarHidden = true;
    
    View_AppliedJobsWorker.frame=CGRectMake(1000,View_AppliedJobsWorker.frame.origin.y, View_AppliedJobsWorker.frame.size.width, View_AppliedJobsWorker.frame.size.height);
    View_JobCompletedOfWorker.frame = CGRectMake(1000, View_JobCompletedOfWorker.frame.origin.y, View_JobCompletedOfWorker.frame.size.width, View_JobCompletedOfWorker.frame.size.height);
    view_ongoingContracts.frame = CGRectMake(1000, view_ongoingContracts.frame.origin.y, view_ongoingContracts.frame.size.width, view_ongoingContracts.frame.size.height);
    [UIView transitionWithView:viewMyInvitations
                      duration:0.5
                       options:UIViewAnimationOptionTransitionNone
                    animations:^{
                        viewMyInvitations.frame=CGRectMake(0,viewMyInvitations.frame.origin.y, viewMyInvitations.frame.size.width, viewMyInvitations.frame.size.height);
                        // [self.view.window addSubview:viewPopUp];
                    }
                    completion:nil];
    
    [btn_OngoingObj setBackgroundImage:[UIImage imageNamed:@"ongoingcontact"] forState:UIControlStateNormal];
    [btn_appliedObj setBackgroundImage:[UIImage imageNamed:@"appiledjob"] forState:UIControlStateNormal];
    [btn_CompeleteObj setBackgroundImage:[UIImage imageNamed:@"jobcomplete@2x-1"] forState:UIControlStateNormal];
    
    
    [SVProgressHUD show];
    [self performSelector:@selector(getInvitationData) withObject:nil afterDelay:0.2];
//    [self performSelectorInBackground:@selector(getInvitationData) withObject:nil];
    
    }


-(void)getInvitationData
{
    arr_invitationJob=[[NSMutableArray alloc]init];
    NSMutableDictionary *jsonDict=[[NSMutableDictionary alloc]init];
    NSString *token=[NSString stringWithFormat:@"%@",[defaults objectForKey:@"user_token"]];
    [jsonDict setValue:[defaults objectForKey:@"user_id"] forKey:@"id"];
    
    NSString *url=[NSString stringWithFormat:@"%@invites-list?token=%@",serviceLink,token];
    id  json = [WebService signup:jsonDict:url];
    if ([[json objectForKey:@"message"]isEqualToString:@"Success"])
    {
        [SVProgressHUD dismiss];
        arr_invitationJob=[json objectForKey:@"response"];
        NSLog(@"%@",arr_invitationJob);
    }else{
        [SVProgressHUD dismiss];
    }
    if (arr_invitationJob.count)
    {
        lbl_NoRecord.hidden=YES;
//        [tableViewMyInvitation reloadData];
    }
    else
    {
        lbl_NoRecord.hidden=NO;
    }
}



- (IBAction)appliedJob:(id)sender {
    View_AppliedJobsWorker.frame=CGRectMake(0,View_AppliedJobsWorker.frame.origin.y, View_AppliedJobsWorker.frame.size.width, View_AppliedJobsWorker.frame.size.height);
    View_JobCompletedOfWorker.frame = CGRectMake(1000, View_JobCompletedOfWorker.frame.origin.y, View_JobCompletedOfWorker.frame.size.width, View_JobCompletedOfWorker.frame.size.height);
    view_ongoingContracts.frame = CGRectMake(1000, view_ongoingContracts.frame.origin.y, view_ongoingContracts.frame.size.width, view_ongoingContracts.frame.size.height);
    
    viewMyInvitations.frame=CGRectMake(1000,viewMyInvitations.frame.origin.y, viewMyInvitations.frame.size.width, viewMyInvitations.frame.size.height);
    [SVProgressHUD showWithStatus:@"Loading"];
    [self performSelector:@selector(FetchappliedJobs) withObject:nil afterDelay:0.3f];
    [tableView_AppliedJobsWorker reloadData];
    
    [btn_OngoingObj setBackgroundImage:[UIImage imageNamed:@"ongoingcontact"] forState:UIControlStateNormal];
    [btn_appliedObj setBackgroundImage:[UIImage imageNamed:@"appiledjob-Active"] forState:UIControlStateNormal];
    [btn_CompeleteObj setBackgroundImage:[UIImage imageNamed:@"jobcomplete@2x-1"] forState:UIControlStateNormal];

}
- (IBAction)onGoingJob:(id)sender {
    view_ongoingContracts.frame=CGRectMake(0,view_ongoingContracts.frame.origin.y, view_ongoingContracts.frame.size.width, view_ongoingContracts.frame.size.height);
    View_AppliedJobsWorker.frame=CGRectMake(1000,View_AppliedJobsWorker.frame.origin.y, View_AppliedJobsWorker.frame.size.width, View_AppliedJobsWorker.frame.size.height);
    View_JobCompletedOfWorker.frame = CGRectMake(1000, View_JobCompletedOfWorker.frame.origin.y, View_JobCompletedOfWorker.frame.size.width, View_JobCompletedOfWorker.frame.size.height);
    viewMyInvitations.frame=CGRectMake(1000,viewMyInvitations.frame.origin.y, viewMyInvitations.frame.size.width, viewMyInvitations.frame.size.height);
    
    [SVProgressHUD showWithStatus:@"Loading..."];
    [self performSelector:@selector(onGoningJob) withObject:nil afterDelay:0.3f];
    [tableView_OngoingWorkers reloadData];
    
    [btn_OngoingObj setBackgroundImage:[UIImage imageNamed:@"ongoingcontact-active"] forState:UIControlStateNormal];
    [btn_appliedObj setBackgroundImage:[UIImage imageNamed:@"appiledjob"] forState:UIControlStateNormal];
    [btn_CompeleteObj setBackgroundImage:[UIImage imageNamed:@"jobcomplete@2x-1"] forState:UIControlStateNormal];
}
#pragma mark
#pragma mark-complete job Action

- (IBAction)completeJob:(id)sender {
    

    [SVProgressHUD showWithStatus:@"Loading..."];
    [self performSelector:@selector(completedJoblist) withObject:nil afterDelay:0.3f];
    
  }
-(void)completedJoblist
{
    View_JobCompletedOfWorker.frame=CGRectMake(0,View_JobCompletedOfWorker.frame.origin.y, View_JobCompletedOfWorker.frame.size.width, View_JobCompletedOfWorker.frame.size.height);
    View_AppliedJobsWorker.frame=CGRectMake(1000,View_AppliedJobsWorker.frame.origin.y, View_AppliedJobsWorker.frame.size.width, View_AppliedJobsWorker.frame.size.height);
    view_ongoingContracts.frame = CGRectMake(1000, view_ongoingContracts.frame.origin.y, view_ongoingContracts.frame.size.width, view_ongoingContracts.frame.size.height);
    viewMyInvitations.frame=CGRectMake(1000,viewMyInvitations.frame.origin.y, viewMyInvitations.frame.size.width, viewMyInvitations.frame.size.height);
    
    [btn_OngoingObj setBackgroundImage:[UIImage imageNamed:@"ongoingcontact"] forState:UIControlStateNormal];
    [btn_appliedObj setBackgroundImage:[UIImage imageNamed:@"appiledjob"] forState:UIControlStateNormal];
    [btn_CompeleteObj setBackgroundImage:[UIImage imageNamed:@"jobcomplete-active@2x-1"] forState:UIControlStateNormal];
    
    
    
    NSString *token=[NSString stringWithFormat:@"%@",[defaults objectForKey:@"user_token"]];
    NSString *url=[NSString stringWithFormat:@"%@applied-workers?token=%@",serviceLink,token];
    NSMutableDictionary *dict=[[NSMutableDictionary alloc]init];
    [dict setValue:[defaults objectForKey:@"user_id"] forKey:@"id"];
    id json = [WebService signup:dict :url];
    
    if ([[json objectForKey:@"message"]isEqualToString:@"Success"])
    {
        arr_completedJob=[[NSMutableArray alloc]init];
        NSMutableArray *arrResponse=[[NSMutableArray alloc]init];
        
        arrResponse=[json objectForKey:@"response"];
        for (int i=0; i<[arrResponse count]; i++)
        {
            if ([[[[[arrResponse objectAtIndex:i]objectForKey:@"job_applications"]objectAtIndex:0]objectForKey:@"applicant_job_status"]isEqualToString:@"confirmed"])
            {
                [arr_completedJob addObject:[arrResponse objectAtIndex:i]];
            }
        }
        tableView_JobCompletedWorker.dataSource=self;
        tableView_JobCompletedWorker.delegate=self;
        [tableView_JobCompletedWorker reloadData];
    }
    else if ([[json objectForKey:@"error"]isEqualToString:@"token_expired"])
    {
        [SVProgressHUD dismiss];
        UIAlertView *alert=[[UIAlertView alloc]initWithTitle:@"" message:@"Session Expired Please login agian" delegate:nil cancelButtonTitle:@"OK" otherButtonTitles: nil];
        [alert show];
        AppDelegate *delegate;
        delegate=(AppDelegate*)[[UIApplication sharedApplication]delegate];
        [defaults setBool:NO forKey:@"logined"];
        [defaults synchronize];
        [[NSUserDefaults standardUserDefaults]synchronize];
        [delegate loginStatus];
    }
    if (arr_completedJob.count)
    {
        lbl_NoRecord.hidden = YES;
    }else{
        lbl_NoRecord.hidden = NO;
    }
    NSLog(@"%@",arr_completedJob);
    [SVProgressHUD dismiss];
    

}

@end
