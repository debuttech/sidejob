//
//  NotificationTVC.m
//  sidejobs
//
//  Created by DebutMac4 on 30/11/15.
//  Copyright © 2015 DebutMac4. All rights reserved.
//

#import "NotificationTVC.h"

@implementation NotificationTVC

- (void)awakeFromNib {
    // Initialization code
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];
    
    _img_user.layer.masksToBounds=YES;
    _img_user.layer.cornerRadius=_img_user.bounds.size.width/2;

    // Configure the view for the selected state
}

@end
