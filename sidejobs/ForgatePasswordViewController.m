//
//  ForgatePasswordViewController.m
//  sideJobs
//
//  Created by DebutMac4 on 13/08/15.
//  Copyright (c) 2015 DebutMac4. All rights reserved.
//

#import "ForgatePasswordViewController.h"
#import "WebService.h"

@interface ForgatePasswordViewController ()
{
    NSString *code;
}

@end

@implementation ForgatePasswordViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    [[UINavigationBar appearance] setBackgroundImage:[UIImage imageNamed:@"top_bar.png"] forBarMetrics:UIBarMetricsDefault];
    
    UIButton *backButton=[UIButton buttonWithType:UIButtonTypeCustom];
    [backButton setFrame:CGRectMake(0, 0, 23, 21)];
    [backButton setImage:[UIImage imageNamed:@"arrow.png"] forState:UIControlStateNormal];
    [backButton addTarget:self action:@selector(leftBarButton:) forControlEvents:UIControlEventTouchUpInside];
    self.navigationItem.leftBarButtonItem=[[UIBarButtonItem alloc]initWithCustomView:backButton];
    
    UIButton *rightButton=[UIButton buttonWithType:UIButtonTypeCustom];
    [rightButton setFrame:CGRectMake(0, 0, 23, 21)];
    [rightButton setImage:[UIImage imageNamed:@"icon.png"] forState:UIControlStateNormal];
    [rightButton addTarget:self action:@selector(rightBarButton:) forControlEvents:UIControlEventTouchUpInside];
    self.navigationItem.rightBarButtonItem=[[UIBarButtonItem alloc]initWithCustomView:rightButton];
    self.navigationController.navigationBarHidden=NO;
    
    tap=[[UITapGestureRecognizer alloc]initWithTarget:self action:@selector(tapGesture:)];
    tap.numberOfTapsRequired=1;
    [self.view addGestureRecognizer:tap];
    [View_popUp addGestureRecognizer:tap];
    [view_inPopView addGestureRecognizer:tap];
    [view_sendCode addGestureRecognizer:tap];
    
    [self.navigationController.navigationBar
     setTitleTextAttributes:@{NSForegroundColorAttributeName : [UIColor whiteColor]}];
    
    
}
- (BOOL)textFieldShouldReturn:(UITextField *)textField
{
    if (textField==txtFieldEmail_object)
    {
        [txtFieldEmail_object resignFirstResponder];
    }
    else if (textField==txtField_email)
    {
        [txtField_email resignFirstResponder];
    }
    else if (textField==txtField_password)
    {
        [txtField_password resignFirstResponder];
    }
    else if (textField==txtField_confirmPassword)
    {
        [txtField_confirmPassword resignFirstResponder];
    }
    else if (textField==txtfld_enterCode)
    {
        [txtfld_enterCode resignFirstResponder];
    }
    return YES;
}

-(void)tapGesture:(UITapGestureRecognizer *)sender
{
    [txtFieldEmail_object resignFirstResponder];
    [txtField_email resignFirstResponder];
    [txtField_password resignFirstResponder];
    [txtField_confirmPassword resignFirstResponder];
}
-(void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:YES];
}

-(void)viewWillDisappear:(BOOL)animated
{
    [super viewWillDisappear:YES];
    
    self.navigationController.navigationBarHidden=YES;
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
}
-(void)leftBarButton:(id)sender
{
    [self.navigationController popViewControllerAnimated:YES];
}
-(void)rightBarButton:(id)sender
{
    
}

- (IBAction)btnSendMail_Action:(id)sender
{
    [txtFieldEmail_object resignFirstResponder];
    
    str_email=txtFieldEmail_object.text;
    
    if ([[str_email stringByReplacingOccurrencesOfString:@" " withString:@""] length]<1)
    {
        UIAlertView *alert=[[UIAlertView alloc]initWithTitle:@"" message:@"Please enter your email." delegate:nil cancelButtonTitle:@"OK" otherButtonTitles: nil];
        [alert show];
    }
    else if (![self isValidEmail:str_email])
    {
        UIAlertView *alert=[[UIAlertView alloc]initWithTitle:@"" message:@"Please enter a valid email." delegate:nil cancelButtonTitle:@"OK" otherButtonTitles: nil];
        [alert show];
        
    }
    
    else
    {
        
        [SVProgressHUD showWithStatus:@"Loading..." maskType:SVProgressHUDMaskTypeGradient];
        [self performSelector:@selector(sendMail) withObject:nil afterDelay:0.3f];
    }
}

-(void)sendMail
{
    NSMutableDictionary *jsonDict=[[NSMutableDictionary alloc]init];
    [jsonDict setValue:str_email forKey:@"email"];
    NSString *url=[NSString stringWithFormat:@"%@forgot",serviceLink];
    json = [WebService signup:jsonDict:url];
    if ([[json objectForKey:@"message"] isEqualToString:@"Success"])
    {
        NSDictionary *dict_response=[json objectForKey:@"response"];
        id_forgotPassword=[dict_response objectForKey:@"id"];
        code=[dict_response objectForKey:@"code"];
        
        
     
        [SVProgressHUD dismiss];
        [self performSelector:@selector(sendView) withObject:nil afterDelay:0.3f];
     
    }
    else if([[json valueForKey:@"message"] isEqualToString:@"Error :: User Does Not Exists"])
    {
        UIAlertView *alert=[[UIAlertView alloc]initWithTitle:@"" message:@"Email does not exist" delegate:nil
                                           cancelButtonTitle:@"OK" otherButtonTitles: nil];
        [alert show];
        [SVProgressHUD dismiss];
    }else if ([[json valueForKey:@"message"] isEqualToString:@"Error :: Your email is not activated. Please activate your email."]){
        UIAlertView *alert=[[UIAlertView alloc]initWithTitle:@"" message:[json valueForKey:@"message"] delegate:nil
                                           cancelButtonTitle:@"OK" otherButtonTitles: nil];
        [alert show];
        [SVProgressHUD dismiss];
    }
    else if ([[json valueForKey:@"message"] isEqualToString:@"Error :: Please Change Password on http://facebook.com."]){
        UIAlertView *alert=[[UIAlertView alloc]initWithTitle:@"" message:@"Email does not exist" delegate:nil
                                           cancelButtonTitle:@"OK" otherButtonTitles: nil];
        [alert show];
        [SVProgressHUD dismiss];
    }
    else{
        [SVProgressHUD dismiss];
    }
}
-(void)sendView;
{
    [view_sendCode setFrame:CGRectMake(0, view_sendCode.frame.origin.y, view_sendCode.frame.size.width, view_sendCode.frame.size.height)];
    [self.view addSubview:view_sendCode];
    
}

- (IBAction)btnSubmitCode:(id)sender
{
    
    if ([code isEqualToString:txtfld_enterCode.text]) {
        [self varificationCode];
    }
    else
    {
        
        UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@""
                                                        message:@"Code does not match."
                                                       delegate:nil
                                              cancelButtonTitle:@"Done"
                                              otherButtonTitles:nil];
        [alert show];
    }
    
}

- (IBAction)btncancelFrom_entercode:(id)sender {
    [view_sendCode removeFromSuperview];
}

-(BOOL)isValidEmail:(NSString *)checkString
{
    BOOL stricterFilter = NO; // Discussion http://blog.logichigh.com/2010/09/02/validating-an-e-mail-address/
    NSString *stricterFilterString = @"[A-Z0-9a-z\\._%+-]+@([A-Za-z0-9-]+\\.)+[A-Za-z]{2,4}";
    NSString *laxString = @".+@([A-Za-z0-9-]+\\.)+[A-Za-z]{2}[A-Za-z]*";
    NSString *emailRegex = stricterFilter ? stricterFilterString : laxString;
    NSPredicate *emailTest = [NSPredicate predicateWithFormat:@"SELF MATCHES %@", emailRegex];
    return [emailTest evaluateWithObject:checkString];
    
}

-(BOOL)textField:(UITextField *)textField shouldChangeCharactersInRange:(NSRange)range replacementString:(NSString *)string {
    
    if (textField==txtField_confirmPassword )
    {
        NSString *textFieldText = [txtField_confirmPassword.text stringByReplacingCharactersInRange:range withString:string];
        if (textFieldText.length==0||[textFieldText length]<=20 )
        {
            return YES;
        }
        else
            return NO;
    }
    
    
    if (textField==txtField_password)
    {
        NSString *textFieldText = [txtField_password.text stringByReplacingCharactersInRange:range withString:string];
        if (textFieldText.length==0||[textFieldText length]<=20 )
        {
            return YES;
        }
        else
            return NO;
    }
    
    
    return YES;
}
-(void)varificationCode
{
    NSMutableDictionary *jsonDict=[[NSMutableDictionary alloc]init];
    [jsonDict setValue:code forKey:@"code"];
    NSString *url=[NSString stringWithFormat:@"%@code",serviceLink];
    id  jsonr = [WebService signup:jsonDict:url];
    if ([[jsonr objectForKey:@"message"] isEqualToString:@"Success"])
    {
        [self popUpview];
    }
    else
    {
        
        UIAlertView *alert=[[UIAlertView alloc]initWithTitle:@"" message:@"Code is not matched." delegate:nil cancelButtonTitle:@"OK" otherButtonTitles: nil];
        [alert show];
    }
}

#pragma mark
#pragma popView to change the password
-(void)popUpview
{
    [UIView transitionWithView:View_popUp duration:1.0f options:UIViewAnimationOptionTransitionCurlDown
                    animations:^{
                        
                    } completion:^(BOOL finished) {
                         [View_popUp setFrame:CGRectMake(0,View_popUp.frame.origin.y,View_popUp.frame.size.width,View_popUp.frame.size.height)];
                    }];
   
    [self.view addSubview:View_popUp];
    [View_popUp addGestureRecognizer:tap];
}


#pragma mark-*******enter new password
- (IBAction)btnSubmitFromPopup_Action:(id)sender
{
    str_passwordPopUp=txtField_password.text;
    str_confirmPasswordPopUp=txtField_confirmPassword.text;
    
    str_passwordPopUp = [txtField_password.text stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceCharacterSet ]];
    str_confirmPasswordPopUp = [txtField_confirmPassword.text stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceCharacterSet ]];
    
    if (str_passwordPopUp.length<6)
    {
        UIAlertView *alert=[[UIAlertView alloc]initWithTitle:@"" message:@"Please enter password with 6 character." delegate:nil cancelButtonTitle:@"OK" otherButtonTitles: nil];
        [alert show];
    }
    
    else if (str_confirmPasswordPopUp.length<6)
    {
        UIAlertView *alert=[[UIAlertView alloc]initWithTitle:@"" message:@"Please enter confirm password with 6 character." delegate:nil cancelButtonTitle:@"OK" otherButtonTitles: nil];
        [alert show];
    }
    else
    {
        if ([str_passwordPopUp isEqualToString:str_confirmPasswordPopUp] )
        {
            NSMutableDictionary *jsonDict=[[NSMutableDictionary alloc]init];
            [jsonDict setValue:str_passwordPopUp forKey:@"password"];
            [jsonDict setValue:str_confirmPasswordPopUp forKey:@"password_confirm"];
            [jsonDict setValue:id_forgotPassword forKey:@"id"];
            NSString *url=[NSString stringWithFormat:@"%@change",serviceLink];
            json_changePassword = [WebService signup:jsonDict:url];
            [View_popUp removeFromSuperview];
            if ([[json_changePassword objectForKey:@"message"] isEqualToString:@"Success"])
            {
                
                UIAlertView *alert=[[UIAlertView alloc]initWithTitle:@"" message:@"Password reset successsfully." delegate:nil cancelButtonTitle:@"OK" otherButtonTitles:nil];
                [alert show];
                [self.navigationController popViewControllerAnimated:YES];
            }
        }
        
        else
        {
            UIAlertView *alert=[[UIAlertView alloc]initWithTitle:@"" message:@"Password do not match" delegate:nil cancelButtonTitle:@"OK" otherButtonTitles: nil];
            [alert show];
        }
    }
}

- (IBAction)btn_CancelFromOPopUp:(id)sender {
    
    [UIView transitionWithView:View_popUp duration:1.0f options:UIViewAnimationOptionTransitionCurlUp
                    animations:^{
                        
                    } completion:^(BOOL finished) {
                         [View_popUp removeFromSuperview];
                    }];
  
}
//.........................................................................

- (BOOL)validateEmailWithString:(NSString*)email
{
    NSString *emailRegex = @"[A-Z0-9a-z._%+-]+@[A-Za-z0-9.-]+\\.[A-Za-z]{2,4}";
    NSPredicate *emailTest = [NSPredicate predicateWithFormat:@"SELF MATCHES %@", emailRegex];
    return [emailTest evaluateWithObject:email];
}
@end
