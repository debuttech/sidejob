//
//  MapViewAnnotation.h
//  Spottales
//
//  Created by Debut Mac 4 on 06/02/15.
//  Copyright (c) 2015 Debut Mac 4. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <MapKit/MapKit.h>

@interface MapViewAnnotation : NSObject <MKAnnotation>


@property (nonatomic, readonly) CLLocationCoordinate2D coordinate;
//@property (nonatomic, copy, readonly) NSString *image;
@property (nonatomic, copy, readonly) NSString *title;
@property (nonatomic, copy, readonly) NSString *subtitle;

@property (nonatomic, readonly) BOOL showCustomCallout;
@property (nonatomic, readonly) UIView *calloutView;
@property (nonatomic, copy, readonly) NSString *tag;
@property (nonatomic, copy, readonly) NSMutableDictionary *image;
@property(nonatomic,copy,readonly)NSMutableDictionary *job_Id;

-(id)initWithCoordinates:(CLLocationCoordinate2D)paramCoordinates withtitle:(NSString*)title  withTagValue:(NSString*)tag image:(NSMutableDictionary *)paramImage jobId:(NSMutableDictionary *)id_Job;


@end
