//
//  ApplyJobFromListJobViewController.m
//  sidejobs
//
//  Created by DebutMac4 on 09/09/15.
//  Copyright (c) 2015 DebutMac4. All rights reserved.
//

#import "ApplyJobFromListJobViewController.h"
#import <QuartzCore/QuartzCore.h>
#import "MWPhotoBrowser.h"
#import "UIImageView+WebCache.h"



@interface ApplyJobFromListJobViewController ()
{
    NSUserDefaults *defaults;
    NSMutableArray *Array_slot;
    NSString *slotTime;
    NSString* starttime;
    NSString* endtime;
    NSMutableArray *arrImg,*arrImageToP;
    NSMutableArray *photos;
    
    UIToolbar *bar;
    AppDelegate *delegate;
    
    NSMutableDictionary *dictResponse;
    NSMutableArray*  cardDetails;
     NSMutableDictionary *accountDetialDict;
    
}

@end
@implementation ApplyJobFromListJobViewController

- (void)viewDidLoad
{
    [super viewDidLoad];
    
     delegate=(AppDelegate*)[[UIApplication sharedApplication]delegate];
    //[SVProgressHUD showInfoWithStatus:@"loading..." maskType:SVProgressHUDMaskTypeClear];
    arrImageToP=[[NSMutableArray alloc]init];
    defaults=[NSUserDefaults standardUserDefaults];
    img1Btn_Obj.layer.masksToBounds=YES;
    img1Btn_Obj.layer.cornerRadius=5.0f;
    img1Btn_Obj.layer.borderColor=[[UIColor grayColor]CGColor];
    img1Btn_Obj.layer.borderWidth=0.2f;
    
    img2Btn_Obj.layer.masksToBounds=YES;
    img2Btn_Obj.layer.cornerRadius=5.0f;
    img2Btn_Obj.layer.borderColor=[[UIColor grayColor]CGColor];
    img2Btn_Obj.layer.borderWidth=0.2f;
    
    img3Btn_Obj.layer.masksToBounds=YES;
    img3Btn_Obj.layer.cornerRadius=5.0f;
    img3Btn_Obj.layer.borderColor=[[UIColor grayColor]CGColor];
    img3Btn_Obj.layer.borderWidth=0.2f;
    
    
    Array_slot=[[NSMutableArray alloc]init];
    
    NSArray *arr=[self.jobDict objectForKey:@"job_applications"];
    NSLog(@"%@",[self.jobDict objectForKey:@"job_applications"]);
    
    [self getMerchantId];
    [self setscroll];
    if (arr.count)
    {
        if ([[[[self.jobDict objectForKey:@"job_applications"]objectAtIndex:0]objectForKey:@"applicant_job_status"]isEqualToString:@"applied"])
        {
            [btn_ApplyJob setTitle:@"Already applied this job" forState:UIControlStateNormal];
            btn_ApplyJob.enabled=NO;
            
            btn_ApplyJob.layer.shadowColor = [UIColor blackColor].CGColor;
            // btn_ApplyJob.layer.shadowOffset = CGSizeMake(0.0f, 1.0f);
            btn_ApplyJob.titleLabel.layer.shadowRadius = 5.0f;
            btn_ApplyJob.layer.shadowOpacity = 0.8f;
            btn_ApplyJob.alpha=1.0f;
            btn_ApplyJob.layer.masksToBounds = NO;
            
            
        }
        else
        {
            btn_ApplyJob.enabled=YES;
        }
        
    }
    
    else
    {
        
    }
    
    
    [self showJobdetails];
    
    [self performSelector:@selector(fetchcompleteJobDetail) withObject:nil afterDelay:0.1];
    
    
    
    self.navigationItem.title=@"My SideJobs";
    btn_amount.layer.cornerRadius= btn_amount.bounds.size.width/2 ;
    btn_amount.layer.masksToBounds=true;
    UIButton *backButton=[UIButton buttonWithType:UIButtonTypeCustom];
    [backButton setFrame:CGRectMake(0, 0, 23, 21)];
    [backButton setImage:[UIImage imageNamed:@"arrow.png"] forState:UIControlStateNormal];
    [backButton addTarget:self action:@selector(leftBarButton:) forControlEvents:UIControlEventTouchUpInside];
    
    self.navigationItem.leftBarButtonItem=[[UIBarButtonItem alloc]initWithCustomView:backButton];
    UITapGestureRecognizer *tap=[[UITapGestureRecognizer alloc]initWithTarget:self action:@selector(tapGesture:)];
    tap.numberOfTapsRequired=1;
    [self.view addGestureRecognizer:tap];
    
//    [self performSelector:@selector(getMerchantId) withObject:nil];

    
    //     [self performSelectorInBackground:@selector(getMerchintId) withObject:nil];
//    [self performSelectorInBackground:@selector(getcarddetails) withObject:nil];
}


-(void)setscroll
{
  
    if (arrImg.count)
    {
        if (self.view.bounds.size.width==320) {
            scrollView_MAinView.contentSize=CGSizeMake(320, 620);
            
        }else  if (self.view.bounds.size.width==375) {
            scrollView_MAinView.contentSize=CGSizeMake(375, 700);
        }
        else  if (self.view.bounds.size.width==414) {
            //  scrollView_MAinView.contentSize=CGSizeMake(375, 520);
        }
    }
    
    else
    {
        if (self.view.bounds.size.width==320) {
            scrollView_MAinView.contentSize=CGSizeMake(320, 520);
            
        }else  if (self.view.bounds.size.width==375) {
            scrollView_MAinView.contentSize=CGSizeMake(375, 650);
        }
        else  if (self.view.bounds.size.width==414) {
            //  scrollView_MAinView.contentSize=CGSizeMake(375, 520);
        }
    }
}

-(void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:YES];
}
- (void)textFieldDidBeginEditing:(UITextField *)textField
{
    if (textField==txtfld_budget)
    {
        bar=[[UIToolbar alloc]initWithFrame:CGRectMake(0, 0, self.view.bounds.size.width, 40)];
        UIBarButtonItem *button=[[UIBarButtonItem alloc]initWithTitle:@"Done" style:UIBarButtonItemStyleDone target:self action:@selector(hideKeyBoard)];
        
         NSArray *item=@[button];
        [bar setItems:item];
        txtfld_budget.inputAccessoryView=bar;
    }
    
}
-(void)hideKeyBoard
{
    [txtfld_budget resignFirstResponder];
}
-(void)tapGesture:(UIGestureRecognizer*)sender
{
    [txtfld_budget resignFirstResponder];
    [txtView_addcomment resignFirstResponder];
    // [scrollView_MAinView setContentSize:CGSizeMake(scrollView_MAinView.frame.size.width, 700)];
}
-(void)showJobdetails
{
    dispatch_async(dispatch_get_main_queue(), ^{
        
        [SVProgressHUD showInfoWithStatus:@"loading..." maskType:SVProgressHUDMaskTypeClear];
        
    });
    
    lbl_title.text=[self.jobDict objectForKey:@"title"];
    lbl_postetOn.text=[self.jobDict objectForKey:@"created_at"];
    textView_about.text=[self.jobDict objectForKey:@"description"];
    [btn_amount setTitle:[NSString stringWithFormat:@"$ %@",[self.jobDict objectForKey:@"amount"] ] forState:UIControlStateNormal];
    
    
    NSLog(@"%@",self.jobDict);
    NSString *CatID=[NSString stringWithFormat:@"%@",[self.jobDict valueForKey:@"category_id"]];
    if ([CatID isEqualToString:@"1"])
    {
        lbl_category.text=@"Home";
        imgView_category.image=[UIImage imageNamed:@"cat_home"];
        
    } else if ([CatID isEqualToString:@"2"] )
    {
        lbl_category.text=@"Auto";
        imgView_category.image=[UIImage imageNamed:@"car_icon"];
    } else if ([CatID isEqualToString:@"3"] )
    {
        lbl_category.text=@"Electronics";
        imgView_category.image=[UIImage imageNamed:@"cat_elc"];
    }
}
-(void)leftBarButton:(UIButton*)sender
{
    [self.navigationController popViewControllerAnimated:YES];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}
-(BOOL)textField:(UITextField *)textField shouldChangeCharactersInRange:(NSRange)range replacementString:(NSString *)string {
    if (textField==txtfld_budget)
    {
    //    NSString *textFieldText = [txtfld_budget.text stringByReplacingCharactersInRange:range withString:string];
    //   NSCharacterSet *nonNumberSet = [[NSCharacterSet characterSetWithCharactersInString:@"0123456789."] invertedSet];
        
//        double currentValue = [textField.text doubleValue];
//        double cents = round(currentValue * 100.0f);
//        
//        
//        
//        if ([string length]&&[textField.text length]<6) {
//            for (size_t i = 0; i < [string length]; i++) {
//                unichar c = [string characterAtIndex:i];
//                if (isnumber(c)) {
//                    cents *= 10;
//                    cents += c - '0';
//                }
//            }
//        }
//        else {
//            // back Space
//            //cents = floor(cents / 10);
//           if ([string isEqualToString:@""])
//            {
//                return YES;
//            }
//        }
//    
//        textField.text = [NSString stringWithFormat:@"%.2f", cents / 100.0f];
//        return NO;
     
        
        NSString *textFieldText = [txtfld_budget.text stringByReplacingCharactersInRange:range withString:string];
        if ([string isEqualToString:@""]) {
            return true;
        }
        if(textField.text.length==3&& ![textField.text containsString:@"."]){
            txtfld_budget.text=[NSString stringWithFormat:@"%@.",textField.text];
            return NO;
        }else if(textField.text.length==2&& [string isEqualToString:@""]){
           txtfld_budget.text=[NSString stringWithFormat:@"%@",textFieldText];
            return YES;
        }
        int maxLength = 6;
        
        NSString *searchKeyword = @".";
        
        NSRange rangeOfYourString = [textField.text rangeOfString:searchKeyword];
        
        if(rangeOfYourString.location == NSNotFound)
        {
            // error condition — the text searchKeyword wasn't in 'string'
        }
        else if (rangeOfYourString.location == 0) {
            maxLength = 3;
        }
        else if (rangeOfYourString.location == 1) {
            maxLength = 4;
            }
        else if (rangeOfYourString.location == 2)
        {
            maxLength = 5;
        }
        
        if (textFieldText.length==0||[textFieldText length]<=maxLength)
        {
            return YES;
        }
        else
            return NO;
    }
    return YES;
}

# pragma mark UItextView Delegates 
- (BOOL)textView:(UITextView *)textView shouldChangeTextInRange:(NSRange)range replacementText:(NSString *)text
{
    if (textView==txtView_addcomment)
    {
        if (txtView_addcomment.text.length<1)
        {
            if ([text isEqualToString:@" "])
            {
                return NO;
            }
        }
        if ([text isEqualToString:@""])
        {
            return YES;
        }
        if ([text isEqualToString:@"\n"])
        {
            [textView resignFirstResponder];
            return YES;
        }
        if (txtView_addcomment.text.length<=300)
        {
            return YES;
        }
    }
    return YES;
}


-(void)textViewDidBeginEditing:(UITextView *)textView
{
    if (textView==txtView_addcomment)
    {
        if ([txtView_addcomment.text isEqualToString:@"Add comment"]) {
            txtView_addcomment.text=@"";
            txtView_addcomment.textColor=[UIColor darkGrayColor];
            
            
        }
    }
}


- (IBAction)btn_applyJoob:(id)sender
{
    
    if (accountDetialDict.count > 0)
    {
        [SVProgressHUD showInfoWithStatus:@"Loading..." maskType:SVProgressHUDMaskTypeClear];
        [self performSelector:@selector(applyJob) withObject:nil afterDelay:0.1f];
    }
    else
    {
        UIAlertView *alert=[[UIAlertView alloc]initWithTitle:@"Alert!" message:@"Sorry! Please add card before accept the job" delegate:nil cancelButtonTitle:@"OK" otherButtonTitles:nil];
        [alert show];
    }
    
}


-(void)viewDidDisappear:(BOOL)animated
{
    [super viewDidDisappear:YES];
    _status=@"";
}


-(void)applyJob
{
    [txtfld_budget resignFirstResponder];
    [txtView_addcomment resignFirstResponder];
    if([txtfld_budget.text hasSuffix:@"."])
    {
        txtfld_budget.text = [txtfld_budget.text substringToIndex:[txtfld_budget.text length] - 1];
    }
    NSLog(@"%@",txtfld_budget.text);
    NSString *strComment;
    if ([txtView_addcomment.text isEqualToString:@"Add comment"])
    {
        strComment=@"";
    }
    else
    {
        strComment=txtView_addcomment.text;
    }
    
    if ([_status isEqualToString:@"Myinvitation"])
    {
        NSString *token=[NSString stringWithFormat:@"%@",[defaults objectForKey:@"user_token"]];
        NSString *url=[NSString stringWithFormat:@"%@job-status?token=%@",serviceLink,token];
        NSMutableDictionary *jsonDict=[[NSMutableDictionary alloc]init];
        
        [jsonDict setValue:[[[self.dict_Detailinvitation objectForKey:@"job_application"] objectAtIndex:0] objectForKey:@"id"]forKey:@"job_application_id"];
        [jsonDict setValue:@"applied" forKey:@"applicant_job_status"];
         NSString *strf=[NSString stringWithFormat:@"%@",txtfld_budget.text];
        NSLog(@"%@",strf);
       
        NSLog(@"%@",self.dict_Detailinvitation);
        
        [jsonDict setValue:strf forKey:@"proposed_amount"];
        [jsonDict setValue:strComment forKey:@"comment"];
        
        
        
        //changes to notification
        [jsonDict setValue:[[[self.dict_Detailinvitation objectForKey:@"job_application"] objectAtIndex:0] objectForKey:@"job_id"] forKey:@"job_id"];
        
        [jsonDict setValue:[[self.dict_Detailinvitation objectForKey:@"user"] objectForKey:@"id"] forKey:@"job_user_id"];
        
        [jsonDict setValue:[defaults objectForKey:@"user_id"] forKey:@"sender_id"];
        
        [jsonDict setValue:[[self.dict_Detailinvitation objectForKey:@"user"] objectForKey:@"id"] forKey:@"receiver_id"];
        
        [jsonDict setValue:[defaults objectForKey:@"p_name"] forKey:@"sender_name"];

     
        
        
        if (starttime==nil||endtime==nil)
        {
            [SVProgressHUD dismiss];
            UIAlertView *alert=[[UIAlertView alloc]initWithTitle:@"" message:@"Please select time slot" delegate:nil cancelButtonTitle:@"OK" otherButtonTitles:nil];
            [alert show];
        }
        else if (strf.length ==0)
        {
            [SVProgressHUD dismiss];
            UIAlertView *alert=[[UIAlertView alloc]initWithTitle:@"" message:@"Please gives the proposed amount" delegate:nil cancelButtonTitle:@"OK" otherButtonTitles:nil];
            [alert show];
        }
        else
        {
            
          
            id json=[WebService signup:jsonDict :url];
            if ([[json objectForKey:@"message"]isEqualToString:@"Success"])
            {
                [SVProgressHUD dismiss];
                UIAlertView *alert=[[UIAlertView alloc]initWithTitle:@"" message:@"Congratulation! You have Accepted this job." delegate:self cancelButtonTitle:@"OK" otherButtonTitles: nil];
                [alert show];
                alert.tag=202;
//                [self.navigationController popToViewController:[[self.navigationController viewControllers] objectAtIndex:0] animated:YES];
                
                //[self.navigationController popViewControllerAnimated:YES];
            }
            
            else
            {
                [SVProgressHUD dismiss];
                UIAlertView *alert=[[UIAlertView alloc]initWithTitle:@"" message:@"Some error occured to Accept this job." delegate:nil cancelButtonTitle:@"OK" otherButtonTitles:nil];
                [alert show];
            }
        }
        
    }
    
    else
    {
        NSString *token=[NSString stringWithFormat:@"%@job-application?token=%@",serviceLink,[defaults objectForKey:@"user_token"]];
        
        NSMutableDictionary *jsonDict=[[NSMutableDictionary alloc]init];
        [jsonDict setValue:[self.jobDict objectForKey:@"id"] forKey:@"job_id"];
        [jsonDict setValue:[defaults objectForKey:@"user_id"] forKey:@"applicant_id"];
        [jsonDict setValue:@"applied" forKey:@"applicant_job_status"];
        [jsonDict setValue:starttime forKey:@"start_time"];
        [jsonDict setValue:endtime forKey:@"end_time"];
        NSString *strf=[NSString stringWithFormat:@"%@",txtfld_budget.text];
        [jsonDict setValue:strf forKey:@"proposed_amount"];
        [jsonDict setValue:strComment forKey:@"comment"];
        
        
        //changes to notification
        [jsonDict setValue:[self.jobDict objectForKey:@"user_id"] forKey:@"job_user_id"];
        
        [jsonDict setValue:[defaults objectForKey:@"user_id"] forKey:@"sender_id"];

        [jsonDict setValue:[self.jobDict objectForKey:@"user_id"] forKey:@"receiver_id"];

        [jsonDict setValue:[defaults objectForKey:@"p_name"] forKey:@"sender_name"];


        
        
        if (starttime==nil||endtime==nil)
        {
            [SVProgressHUD dismiss];
            UIAlertView *alert=[[UIAlertView alloc]initWithTitle:@"" message:@"Please select time slot" delegate:nil cancelButtonTitle:@"OK" otherButtonTitles:nil];
            [alert show];
        }
        else if (strf.length ==0)
        {
            [SVProgressHUD dismiss];
            UIAlertView *alert=[[UIAlertView alloc]initWithTitle:@"" message:@"Please gives the proposed amount" delegate:nil cancelButtonTitle:@"OK" otherButtonTitles:nil];
            [alert show];
            
        }
        else
        {
            id  json = [WebService signup:jsonDict:token];
            if ([[json objectForKey:@"message"]isEqualToString:@"Success"])
            {
                [SVProgressHUD dismiss];
                UIAlertView *alert=[[UIAlertView alloc]initWithTitle:@"" message:@"You have successfully applied this job." delegate:self cancelButtonTitle:@"OK" otherButtonTitles:nil];
                [alert show];
                alert.tag=201;
                
            }
            else if ([[json objectForKey:@"message"]isEqualToString:@"Error :: Already applied for this job."])
            {
                [SVProgressHUD dismiss];
                UIAlertView *alert=[[UIAlertView alloc]initWithTitle:@"" message:@"You have already applied this job." delegate:nil cancelButtonTitle:@"OK" otherButtonTitles:nil];
                [alert show];
            }
            
            else
            {
                [SVProgressHUD dismiss];
                UIAlertView *alert=[[UIAlertView alloc]initWithTitle:@"" message:@"Some error occured to apply job." delegate:nil cancelButtonTitle:@"OK" otherButtonTitles:nil];
                [alert show];
            }
        }
        
    }
    [SVProgressHUD dismiss];
}

//-(void)applyjob :(NSMutableDictionary*)dict andUrl:(NSString*)url
//
//{
//    id json=[WebService signup:dict :url];
//    if ([[json objectForKey:@"message"]isEqualToString:@"Success"])
//    {
//        UIAlertView *alert=[[UIAlertView alloc]initWithTitle:@"" message:@"Congratulation! You have Accepted this job." delegate:self cancelButtonTitle:@"OK" otherButtonTitles: nil];
//        [alert show];
//        alert.tag=202;
//        //                [self.navigationController popToViewController:[[self.navigationController viewControllers] objectAtIndex:0] animated:YES];
//        
//        //[self.navigationController popViewControllerAnimated:YES];
//    }
//    
//    else
//    {
//        UIAlertView *alert=[[UIAlertView alloc]initWithTitle:@"" message:@"Some error occured to Accept this job." delegate:nil cancelButtonTitle:@"OK" otherButtonTitles:nil];
//        [alert show];
//    }
//
//}

- (void)alertView:(UIAlertView *)alertView clickedButtonAtIndex:(NSInteger)buttonIndex
{
    if (buttonIndex==0)
    {
        if (alertView.tag == 201)
        {
            [self.navigationController popViewControllerAnimated:YES];
        }
        
        else if (alertView.tag == 202)
            
        {
            [self.navigationController popToViewController:[[self.navigationController viewControllers] objectAtIndex:0] animated:YES];
        }
    }
   
}

- (IBAction)btnSlotAction1:(id)sender
{
    UIButton *button=(UIButton*)sender;
    btn_slot1.backgroundColor=[UIColor orangeColor];
    btn_slot2.backgroundColor=[UIColor grayColor];
    btn_slot3.backgroundColor=[UIColor grayColor];
    slotTime=button.titleLabel.text;
    NSArray* arr = [slotTime componentsSeparatedByString: @"-"];
    starttime = [arr objectAtIndex: 0];
    endtime = [arr objectAtIndex: 1];
    NSLog(@"%@", starttime);
    
    NSLog(@"%@", endtime);
    
    NSLog(@"%@", [self changeformate_string24hr:starttime]);
    NSLog(@"%@", [self changeformate_string24hr:endtime]);
    starttime=[self changeformate_string24hr:starttime];
    endtime= [self changeformate_string24hr:endtime];
    
    
}

-(NSString *)changeformate_string24hr:(NSString *)date
{
    NSDateFormatter *dateFormatter = [[NSDateFormatter alloc] init];
    dateFormatter.dateFormat = @"hh:mm a";
    [dateFormatter setTimeZone:[NSTimeZone timeZoneWithName:@"GMT"]];
    [dateFormatter setLocale:[[NSLocale alloc] initWithLocaleIdentifier:@"en_US"]];
    NSDate *datepass = [dateFormatter dateFromString:date];
    
    dateFormatter.dateFormat = @"HH:mm";
    NSString *pmamDateString = [dateFormatter stringFromDate:datepass];
    return pmamDateString;
    
    
//    NSDateFormatter* df = [[NSDateFormatter alloc]init];
//    
//    //[df setTimeZone:[NSTimeZone systemTimeZone]];
//    
//    [df setDateFormat:@"hh:mm a"];
//    
//    NSDate* wakeTime = [df dateFromString:date];
//    
//    [df setDateFormat:@"HH:mm"];
//    
//    
//    return [df stringFromDate:wakeTime];
    
}


- (IBAction)btnSlotAction2:(id)sender {
    btn_slot2.backgroundColor=[UIColor orangeColor];
    btn_slot1.backgroundColor=[UIColor grayColor];
    btn_slot3.backgroundColor=[UIColor grayColor];
    UIButton *button=(UIButton*)sender;
    slotTime=button.titleLabel.text;
    NSArray* arr = [slotTime componentsSeparatedByString: @"-"];
    starttime = [arr objectAtIndex: 0];
    endtime = [arr objectAtIndex: 1];
    NSLog(@"%@", starttime);
    
    NSLog(@"%@", endtime);
    
    NSLog(@"%@", [self changeformate_string24hr:starttime]);
    NSLog(@"%@", [self changeformate_string24hr:endtime]);
    starttime=[self changeformate_string24hr:starttime];
    endtime= [self changeformate_string24hr:endtime];
    
}

- (IBAction)btnSlotAction3:(id)sender {
    btn_slot3.backgroundColor=[UIColor orangeColor];
    btn_slot1.backgroundColor=[UIColor grayColor];
    btn_slot2.backgroundColor=[UIColor grayColor];
    UIButton *button=(UIButton*)sender;
    slotTime=button.titleLabel.text;
    NSArray* arr = [slotTime componentsSeparatedByString: @"-"];
    starttime = [arr objectAtIndex: 0];
    endtime = [arr objectAtIndex: 1];
    NSLog(@"%@", starttime);
    
    NSLog(@"%@", endtime);
    
    NSLog(@"%@", [self changeformate_string24hr:starttime]);
    NSLog(@"%@", [self changeformate_string24hr:endtime]);
    starttime=[self changeformate_string24hr:starttime];
    endtime= [self changeformate_string24hr:endtime];
    
}



#pragma mark animation on clicked button
- (IBAction)btn1Action:(UIButton*)sender
{
    // Add photos
    photos=[[NSMutableArray alloc]init];
    for (int i=0; i<arrImageToP.count; i++)
    {
        [photos addObject:[MWPhoto photoWithURL:[NSURL URLWithString:[arrImageToP objectAtIndex:i]]]];
    }
    
    // Create browser (must be done each time photo browser is
    // displayed. Photo browser objects cannot be re-used)
    MWPhotoBrowser *browser = [[MWPhotoBrowser alloc] initWithDelegate:self];
    
    // Set options
    browser.displayActionButton = YES; // Show action button to allow sharing, copying, etc (defaults to YES)
    browser.displayNavArrows = NO; // Whether to display left and right nav arrows on toolbar (defaults to NO)
    browser.displaySelectionButtons = NO; // Whether selection buttons are shown on each image (defaults to NO)
    browser.zoomPhotosToFill = YES; // Images that almost fill the screen will be initially zoomed to fill (defaults to YES)
    browser.alwaysShowControls = NO; // Allows to control whether the bars and controls are always visible or whether they fade away to show the photo full (defaults to NO)
    browser.enableGrid = YES; // Whether to allow the viewing of all the photo thumbnails on a grid (defaults to YES)
    browser.startOnGrid = NO; // Whether to start on the grid of thumbnails instead of the first photo (defaults to NO)
    browser.autoPlayOnAppear = NO; // Auto-play first video
    
    // Customise selection images to change colours if required
    browser.customImageSelectedIconName = @"ImageSelected.png";
    browser.customImageSelectedSmallIconName = @"ImageSelectedSmall.png";
    
    // Optionally set the current visible photo before displaying
    [browser setCurrentPhotoIndex:sender.tag];
    NSLog(@"%ld",(long)sender.tag);
    
    // Present
    [self.navigationController pushViewController:browser animated:YES];
    
    // Manipulate
    [browser showNextPhotoAnimated:YES];
    [browser showPreviousPhotoAnimated:YES];
    [browser setCurrentPhotoIndex:sender.tag];
}

- (NSUInteger)numberOfPhotosInPhotoBrowser:(MWPhotoBrowser *)photoBrowser {
    return photos.count;
}

- (id <MWPhoto>)photoBrowser:(MWPhotoBrowser *)photoBrowser photoAtIndex:(NSUInteger)index {
    if (index < photos.count) {
        return [photos objectAtIndex:index];
    }
    return nil;
}

#pragma mark
#pragma mark-fetch complete Job details

-(void)fetchcompleteJobDetail{
    arrImg=[[NSMutableArray alloc]init];
    
    NSString *token=[NSString stringWithFormat:@"%@",[defaults objectForKey:@"user_token"]];
    NSString *url=[NSString stringWithFormat:@"%@job-detail/?token=%@",serviceLink,token];
    
    NSMutableDictionary *jsonDict=[[NSMutableDictionary alloc]init];
    [jsonDict setValue:[self.jobDict valueForKey:@"id"] forKey:@"id"];
    
    id json = [WebService signup:jsonDict:url];
    
    if ([[json objectForKey:@"message"]isEqualToString:@"Success"])
    {
        self.jobDict = [json valueForKey:@"response"];
        NSLog(@"%@",self.jobDict);
        arrImg=[self.jobDict objectForKey:@"job_images"];
        
        [self setJobImg];
        [ self setscroll ];
        //NSLog( self.jobDict valueForKey:@"job_time_slots");
        for (NSMutableDictionary *dict  in [self.jobDict valueForKey:@"job_time_slots"])
        {
            NSString *startTime=[dict objectForKey:@"start_time"];
            NSString *endTime=[dict objectForKey:@"end_time"];
            
            [Array_slot addObject:[NSString stringWithFormat:@"%@-%@",startTime,endTime]];
            
        }
        [self setTimeSlot];
        [SVProgressHUD dismiss];
        
        if (Array_slot.count>2)
        {
            [scrollViewSlotTime setContentSize:CGSizeMake(500,scrollViewSlotTime.frame.size.height)];
        }
        NSLog(@"time slot is %@",[self.jobDict valueForKey:@"job_time_slots"]);
        
    }
    else if ([[json objectForKey:@"error"]isEqualToString:@"token_expired"])
    {
        [SVProgressHUD dismiss];
        
        UIAlertView *alert=[[UIAlertView alloc]initWithTitle:@"" message:@"Session Expired Please login agian" delegate:nil cancelButtonTitle:@"OK" otherButtonTitles:nil];
        [alert show];
        
        AppDelegate *delegate;
        delegate=(AppDelegate*)[[UIApplication sharedApplication]delegate];
        
        [defaults setBool:NO forKey:@"logined"];
        [defaults synchronize];
        
        [[NSUserDefaults standardUserDefaults]synchronize];
        [delegate loginStatus];
    }
}

#pragma mark
#pragma setJobbImg
-(void)setJobImg
{
    if (arrImg.count)
    {
        for (int i=0; i<arrImg.count; i++)
        {
            NSString *str=[NSString stringWithFormat:@"%@%@",serviceLink,[[arrImg objectAtIndex:i] objectForKey:@"image_link"]];
            [arrImageToP addObject:str];
        }
    }
    NSLog(@"%@",arrImageToP);
    
    if (arrImg.count==1)
    {
        NSString *url=[NSString stringWithFormat:@"%@%@",serviceLink,[[arrImg objectAtIndex:0] objectForKey:@"image_link"]];
        
        img1Btn_Obj.hidden=NO;
        
        SDWebImageManager *manager = [SDWebImageManager sharedManager];
        [manager downloadImageWithURL:[NSURL URLWithString:url]
                              options:0
                             progress:^(NSInteger receivedSize, NSInteger expectedSize) {
                                 // progression tracking code
                             }
                            completed:^(UIImage *image, NSError *error, SDImageCacheType cacheType, BOOL finished, NSURL *imageURL) {
                                if (image) {
                                    // do something with image
                                [img1Btn_Obj setBackgroundImage:image forState:UIControlStateNormal];
                                }
                            }];
        
//        dispatch_queue_t q = dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_HIGH, 0ul);
//        dispatch_async(q, ^{
//            /* Fetch the image from the server... */
//            NSData * imageData = [NSData dataWithContentsOfURL:[NSURL URLWithString:url]];
//            UIImage *image = [UIImage imageWithData:imageData];            dispatch_async(dispatch_get_main_queue(), ^{
//                /* This is the main thread again, where we set the tableView's image to
//                 be what we just fetched. */
//                
//               [img1Btn_Obj setBackgroundImage:image forState:UIControlStateNormal];
//            });
//        });
        
        

        
    }
    
    
    if (arrImg.count==2)
    {
        NSString *url=[NSString stringWithFormat:@"%@%@",serviceLink,[[arrImg objectAtIndex:0] objectForKey:@"image_link"]];
        
        img1Btn_Obj.hidden=NO;
        img2Btn_Obj.hidden=NO;
        
        
        
        SDWebImageManager *manager = [SDWebImageManager sharedManager];
        [manager downloadImageWithURL:[NSURL URLWithString:url]
                              options:0
                             progress:^(NSInteger receivedSize, NSInteger expectedSize) {
                                 // progression tracking code
                             }
                            completed:^(UIImage *image, NSError *error, SDImageCacheType cacheType, BOOL finished, NSURL *imageURL) {
                                if (image) {
                                    // do something with image
                                    [img1Btn_Obj setBackgroundImage:image forState:UIControlStateNormal];
                                }
                            }];
        
        
        NSString *url1=[NSString stringWithFormat:@"%@%@",serviceLink,[[arrImg objectAtIndex:1] objectForKey:@"image_link"]];
        
                
        SDWebImageManager *manager1 = [SDWebImageManager sharedManager];
        [manager1 downloadImageWithURL:[NSURL URLWithString:url1]
                              options:0
                             progress:^(NSInteger receivedSize, NSInteger expectedSize) {
                                 // progression tracking code
                             }
                            completed:^(UIImage *image, NSError *error, SDImageCacheType cacheType, BOOL finished, NSURL *imageURL) {
                                if (image) {
                                    // do something with image
                                    [img2Btn_Obj setBackgroundImage:image forState:UIControlStateNormal];
                                }
                            }];
 
        
    }
    
    
    if (arrImg.count==3)
    {
        NSString *url=[NSString stringWithFormat:@"%@%@",serviceLink,[[arrImg objectAtIndex:0] objectForKey:@"image_link"]];
        
        img1Btn_Obj.hidden=NO;
        img2Btn_Obj.hidden=NO;
        img3Btn_Obj.hidden=NO;
        
        
        SDWebImageManager *manager = [SDWebImageManager sharedManager];
        [manager downloadImageWithURL:[NSURL URLWithString:url]
                              options:0
                             progress:^(NSInteger receivedSize, NSInteger expectedSize) {
                                 // progression tracking code
                             }
                            completed:^(UIImage *image, NSError *error, SDImageCacheType cacheType, BOOL finished, NSURL *imageURL) {
                                if (image) {
                                    // do something with image
                                    [img1Btn_Obj setBackgroundImage:image forState:UIControlStateNormal];
                                }
                            }];
        
        

        
        
        
        
       NSString *url1=[NSString stringWithFormat:@"%@%@",serviceLink,[[arrImg objectAtIndex:1] objectForKey:@"image_link"]];
       
       NSString *url2=[NSString stringWithFormat:@"%@%@",serviceLink,[[arrImg objectAtIndex:2] objectForKey:@"image_link"]];

        
        
        
        SDWebImageManager *manager1 = [SDWebImageManager sharedManager];
        [manager1 downloadImageWithURL:[NSURL URLWithString:url1]
                              options:0
                             progress:^(NSInteger receivedSize, NSInteger expectedSize) {
                                 // progression tracking code
                             }
                            completed:^(UIImage *image, NSError *error, SDImageCacheType cacheType, BOOL finished, NSURL *imageURL) {
                                if (image) {
                                    // do something with image
                                    [img2Btn_Obj setBackgroundImage:image forState:UIControlStateNormal];
                                }
                            }];
        
        
        SDWebImageManager *manager2 = [SDWebImageManager sharedManager];
        [manager2 downloadImageWithURL:[NSURL URLWithString:url2]
                               options:0
                              progress:^(NSInteger receivedSize, NSInteger expectedSize) {
                                  // progression tracking code
                              }
                             completed:^(UIImage *image, NSError *error, SDImageCacheType cacheType, BOOL finished, NSURL *imageURL) {
                                 if (image) {
                                     // do something with image
                                     [img3Btn_Obj setBackgroundImage:image forState:UIControlStateNormal];
                                 }
                             }];
        
    }
    
    
}
#pragma mark
#pragma -Set time Slot
-(void)setTimeSlot
{
    [btn_slot1.titleLabel setFont:[UIFont systemFontOfSize:14]];
    [btn_slot2.titleLabel setFont:[UIFont systemFontOfSize:14]];
    [btn_slot3.titleLabel setFont:[UIFont systemFontOfSize:14]];
    
    if ([Array_slot count]==1)
    {
        [btn_slot1 setTitle:[Array_slot objectAtIndex:0] forState:UIControlStateNormal];
        btn_slot2.hidden=YES;
        btn_slot3.hidden=YES;
    }
    if ([Array_slot count]==2)
    {
        [btn_slot1 setTitle:[Array_slot objectAtIndex:0] forState:UIControlStateNormal];
        [btn_slot2 setTitle:[Array_slot objectAtIndex:1] forState:UIControlStateNormal];
        btn_slot3.hidden=YES;
    }
    if ([Array_slot count]==3)
    {
        [btn_slot1 setTitle:[Array_slot objectAtIndex:0] forState:UIControlStateNormal];
        [btn_slot2 setTitle:[Array_slot objectAtIndex:1] forState:UIControlStateNormal];
        [btn_slot3 setTitle:[Array_slot objectAtIndex:2] forState:UIControlStateNormal];
    }
    if ([Array_slot count]>3) {
        [btn_slot1 setTitle:[Array_slot objectAtIndex:0] forState:UIControlStateNormal];
        [btn_slot2 setTitle:[Array_slot objectAtIndex:1] forState:UIControlStateNormal];
        [btn_slot3 setTitle:[Array_slot objectAtIndex:2] forState:UIControlStateNormal];
    }
    [SVProgressHUD dismiss];
}




#pragma  mark
#pragma  mark : - get Card details
-(void)getMerchantId
{
    
    dispatch_async(dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_BACKGROUND, 0), ^{
        
        NSMutableDictionary *jsonAccountDict=[[NSMutableDictionary alloc]init];
        NSLog(@"%@",[defaults objectForKey:@"merchant_id"]);
        
        [jsonAccountDict setValue:[defaults objectForKey:@"merchant_id"] forKey:@"id"];
        
        NSString *url=[NSString stringWithFormat:@"%@findMerchantAccount",serviceLink];
        
        id  json = [WebService signup:jsonAccountDict:url];
        
        NSLog(@"Account Detail %@",json);
        accountDetialDict=[[NSMutableDictionary alloc]init];
        
        
        if ([[json objectForKey:@"message"]isEqualToString:@"Success"])
        {
            accountDetialDict=[json objectForKey:@"response"];
            
            NSLog(@"Account Detail:%@",accountDetialDict);
            NSLog(@"Account Detail:%lu",(unsigned long)accountDetialDict.count);
            
        }
        
        else
        {
            UIAlertView *alert=[[UIAlertView alloc]initWithTitle:@"" message:@"Some error is occured to fetching account details" delegate:nil cancelButtonTitle:@"OK" otherButtonTitles: nil];
            
            [alert show];
        }
  
        
    });
    
    }


#pragma  mark
#pragma  mark : - get Card details
-(void)getcarddetails
{
    NSMutableDictionary *jsonDict=[[NSMutableDictionary alloc]init];
    
    [jsonDict setValue:[defaults objectForKey:@"customer_id"] forKey:@"id"];
    
    NSString *url=[NSString stringWithFormat:@"%@find-customer?token=%@",serviceLink,[defaults objectForKey:@"user_token"]];
    
    id  json = [WebService signup:jsonDict:url];
    
    NSLog(@"Card Detail %@",json);
     cardDetails=[NSMutableArray new];
    
    
    if ([[json objectForKey:@"message"]isEqualToString:@"Success"])
    {
        dictResponse=[[NSMutableDictionary alloc]init];
       
        
        dictResponse=[json objectForKey:@"response"];
        cardDetails=[dictResponse objectForKey:@"creditCards"];
        NSLog(@"card count:%lu",(unsigned long)cardDetails.count
              );
        
    }
    
    else if([[json objectForKey:@"error"]isEqualToString:@"token_expired"])
    {
        UIAlertView *alert=[[UIAlertView alloc]initWithTitle:@"" message:@"Session Expired Please login agian." delegate:nil cancelButtonTitle:@"OK" otherButtonTitles: nil];
        
        [alert show];
        
        [defaults setBool:NO forKey:@"logined"];
        
        [defaults synchronize];
        
        [[NSUserDefaults standardUserDefaults]synchronize];
        [delegate loginStatus];
    }
    else
    {
        UIAlertView *alert=[[UIAlertView alloc]initWithTitle:@"" message:@"Some error is occured to fetching card details" delegate:nil cancelButtonTitle:@"OK" otherButtonTitles: nil];
        
        [alert show];
    }
}
@end
