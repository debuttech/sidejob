//
//  ForgatePasswordViewController.h
//  sideJobs
//
//  Created by DebutMac4 on 13/08/15.
//  Copyright (c) 2015 DebutMac4. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface ForgatePasswordViewController : UIViewController<UITextFieldDelegate>
{
    UITapGestureRecognizer *tap;
    NSString *str_email;
    id json,json_changePassword;
    NSString *str_emailpopUP,*str_passwordPopUp,*str_confirmPasswordPopUp;
    NSString *id_forgotPassword;
    
    IBOutlet UITextField *txtFieldEmail_object;
    IBOutlet UIView *View_popUp;
    IBOutlet UITextField *txtField_confirmPassword;
    IBOutlet UITextField *txtField_password;
    IBOutlet UITextField *txtField_email;
    IBOutlet UIView *view_inPopView;
    IBOutlet UIView *view_sendCode;
    IBOutlet UITextField *txtfld_enterCode;
}

- (IBAction)btnSendMail_Action:(id)sender;
- (IBAction)btnSubmitFromPopup_Action:(id)sender;
- (IBAction)btn_CancelFromOPopUp:(id)sender;
- (IBAction)btnSubmitCode:(id)sender;
- (IBAction)btncancelFrom_entercode:(id)sender;


-(void)popUpview;

@end
