//
//  ContactUsViewController.h
//  sidejobs
//
//  Created by DebutMac4 on 03/11/15.
//  Copyright © 2015 DebutMac4. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface HelpViewController : UIViewController<UITableViewDataSource,UITableViewDelegate>
{
    
    IBOutlet UIWebView *webView;
   
    IBOutlet UITableView *tbleView;
    NSArray *array_HelpList;
}

@end
