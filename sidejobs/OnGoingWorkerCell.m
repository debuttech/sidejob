//
//  OnGoingWorkerCell.m
//  WorkerEarning
//
//  Created by Debut Mac 4 on 20/08/15.
//  Copyright (c) 2015 debut.infotech. All rights reserved.
//

#import "OnGoingWorkerCell.h"

@implementation OnGoingWorkerCell

- (void)awakeFromNib {
    // Initialization code
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];
    CGFloat borderWidth = 0.3f;
    
    _view_ongoing.layer.borderColor = [UIColor lightGrayColor].CGColor;
    _view_ongoing.layer.borderWidth = borderWidth;
    _btn_amount.layer.masksToBounds=YES;
    _btn_amount.layer.cornerRadius=_btn_amount.bounds.size.width/2;

    // Configure the view for the selected state
}

@end
