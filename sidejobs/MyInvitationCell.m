//
//  MyInvitationCell.m
//  SideJobsMileston2
//
//  Created by Debut Mac 4 on 17/09/15.
//  Copyright (c) 2015 debut.infotech. All rights reserved.
//

#import "MyInvitationCell.h"

@implementation MyInvitationCell

- (void)awakeFromNib {
    // Initialization code
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated
{
    [super setSelected:selected animated:animated];

    
    CGFloat borderWidth = 0.3f;
//    _view_invitationCell.frame = CGRectInset(_view_invitationCell.frame, -borderWidth, -borderWidth);
    _view_invitationCell.layer.borderColor = [UIColor lightGrayColor].CGColor;
    _view_invitationCell.layer.borderWidth = borderWidth;
    // Configure the view for the selected state
}

- (IBAction)btnAcceptInvite_Action:(id)sender {
}

- (IBAction)btnDeclineInvite_Action:(id)sender {
}
@end
