//
//  WorkerSearchTableViewCell.m
//  sideJobs
//
//  Created by DebutMac4 on 18/08/15.
//  Copyright (c) 2015 DebutMac4. All rights reserved.
//

#import "WorkerSearchTableViewCell.h"

@implementation WorkerSearchTableViewCell

- (void)awakeFromNib {
    // Initialization code
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];
    _view_tablebackground_object.layer.borderColor=[[UIColor grayColor]CGColor];
    _view_tablebackground_object.layer.borderWidth=0.28f;

    // Configure the view for the selected state
}

@end
