//
//  NotificationTVC.h
//  sidejobs
//
//  Created by DebutMac4 on 30/11/15.
//  Copyright © 2015 DebutMac4. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface NotificationTVC : UITableViewCell
@property (strong, nonatomic) IBOutlet UIImageView *img_user;
@property (strong, nonatomic) IBOutlet UILabel *lbl_notification;
@property (strong, nonatomic) IBOutlet UILabel *lbl_notidate;

@end
