//
//  SignupViewController.h
//  sideJobs
//
//  Created by DebutMac4 on 13/08/15.
//  Copyright (c) 2015 DebutMac4. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <Foundation/Foundation.h>

@interface SignupViewController : UIViewController<UIScrollViewDelegate,UIImagePickerControllerDelegate,UINavigationControllerDelegate,UITextFieldDelegate,UIPickerViewDelegate,UIPickerViewDataSource>
{
    NSUInteger indexOfCurrentPage;
    NSString *str_email,*str_phone,*str_name,*str_password,*str_address,*str_registrationtype,*str_fullName,*str_creditcardNO,*str_date,*str_cwc;
    bool value;
    UIImage *img_userProfile;
    NSString *user_ID;
    IBOutlet UIButton *btn_profilePic_Object;
    IBOutlet UIImageView *imgView_profileView;
    NSString *str_nameProfile,*str_emailProfile,*str_aboutProfile,*str_phoneprofile,*str_Id,*str_rating,*str_profilephoto;
    IBOutlet UIButton *btn_dltProfile_obj;
    
    NSString *paymentToken;
}
- (IBAction)btn_profileDelete:(id)sender;
- (IBAction)btn_back:(id)sender;

@property(weak)NSString *typeUser;
@property (strong, nonatomic) IBOutlet UIScrollView *scrollView_Object;
@property (strong, nonatomic) IBOutlet UIPageControl *pageControl_object;
@property (strong, nonatomic) IBOutlet UILabel *labelStep_object;
@property (strong, nonatomic) IBOutlet UITextField *txtField_Email;
@property (strong, nonatomic) IBOutlet UITextField *txtField_Phone;
@property (strong, nonatomic) IBOutlet UITextField *txtField_YourName;
@property (strong, nonatomic) IBOutlet UITextField *txtField_FullNmae;
@property (strong, nonatomic) IBOutlet UITextField *txtField_CreditCardNumber;
@property (strong, nonatomic) IBOutlet UITextField *txtField_date;
@property (strong, nonatomic) IBOutlet UITextField *txtField_Cwc;
@property (strong, nonatomic) IBOutlet UITextField *txtField_Password;
@property (strong, nonatomic) IBOutlet UIButton *btnNext_object;
@property (strong, nonatomic) IBOutlet UIImageView *imageViewProfileBorder_object;

- (IBAction)pageControl_Action:(id)sender;
- (IBAction)btnNext_Action:(id)sender;
- (IBAction)btnPicImage_Action:(id)sender;

-(void)upDateStep;
-(void)registration;
-(BOOL)isValidEmail:(NSString *)str;
-(void)clearField;
-(void)uploadProfileImages:(UIImage*)img;

@end
