//
//  SearchJobsViewController.h
//  sideJobs
//
//  Created by DebutMac4 on 17/08/15.
//  Copyright (c) 2015 DebutMac4. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <MapKit/MapKit.h>
#import <CoreLocation/CoreLocation.h>
#import "MapViewAnnotation.h"

@interface SearchJobsViewController : UIViewController<UITableViewDataSource,UITableViewDelegate,MKMapViewDelegate,UISearchBarDelegate,CLLocationManagerDelegate>
{
    
    IBOutlet UISearchBar *searchBar_object;
    IBOutlet UIButton *btnList_object;
    IBOutlet UIButton *btnMap_Object;
    IBOutlet UITableView *table_listJob;
    IBOutlet UIButton *btn_AutoObj;
    IBOutlet UIButton *btn_HomeObj;
    IBOutlet UIButton *btn_ElectObj;
    IBOutlet UILabel *lbl_NoJob;
    IBOutlet UILabel *lbl_start;
    IBOutlet UILabel *lbl_end;
    IBOutlet UILabel *lbl_MapRadius;
    IBOutlet UIButton *btn_AutoMap;
    IBOutlet UIButton *btn_HomeMap;
    IBOutlet UIButton *btn_electMap;
    CLLocationManager *locationManager;
    IBOutlet UITextField *txtFld_SelecteRange;

    UIVisualEffectView *visualEffectView;
    UIBlurEffect *blurEffect;
}
//@property (strong, nonatomic) IBOutlet UISearchBar *searchBar_object;

@property(strong,nonatomic)IBOutlet UIView *view_popUP;
@property (strong, nonatomic) IBOutlet UIView *View_MApViewList;
@property (strong, nonatomic) IBOutlet UIView *View_subPopup;
@property (strong, nonatomic) IBOutlet MKMapView *mapview_object;
@property (strong, nonatomic) IBOutlet UIView *View_ListviewWorker_Object;
@property (strong, nonatomic) IBOutlet UISlider *slider_custom;


- (IBAction)btnListView_Action:(id)sender;
- (IBAction)btnMapview_Action:(id)sender;
- (IBAction)btnFilter_Action:(id)sender;

- (IBAction)btn_autoFilter:(id)sender;
- (IBAction)btn_homefilter:(id)sender;
- (IBAction)btn_electfilter:(id)sender;
- (IBAction)btn_SubmitFilter:(id)sender;

- (IBAction)btn_autoMap:(id)sender;
- (IBAction)btn_HomeMap:(id)sender;
- (IBAction)btn_EltMap:(id)sender;

- (IBAction)slider_Action:(id)sender;

- (IBAction)btn_done:(id)sender;

- (IBAction)btnRefresh:(id)sender;



@end
