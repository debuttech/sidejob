//
//  MapViewAnnotation.m
//  Spottales
//
//  Created by Debut Mac 4 on 06/02/15.
//  Copyright (c) 2015 Debut Mac 4. All rights reserved.
//

#import "MapViewAnnotation.h"

@implementation MapViewAnnotation

-(id)initWithCoordinates:(CLLocationCoordinate2D)paramCoordinates withtitle:(NSString*)title  withTagValue:(NSString*)tag image:(NSMutableDictionary *)paramImage jobId:(NSMutableDictionary *)id_Job
{
    self = [super init];
    if(self != nil)
    {
        _coordinate = paramCoordinates;
        _title=title;
        _tag=tag;
        _image=paramImage;
        _job_Id=id_Job;
       // _subtitle=subtitle;
        
    }
    return (self);
}



@end