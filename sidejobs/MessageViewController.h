//
//  MessageViewController.h
//  sidejobs
//
//  Created by DebutMac4 on 24/11/15.
//  Copyright © 2015 DebutMac4. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <MessageUI/MessageUI.h>
#import <MessageUI/MFMailComposeViewController.h>


@interface MessageViewController : UIViewController<UITableViewDataSource,UITableViewDelegate,MFMessageComposeViewControllerDelegate,MFMailComposeViewControllerDelegate,UIActionSheetDelegate>
{
    
    IBOutlet UITableView *tbl_contactNo;
    IBOutlet UILabel *lbl_status;
}

@end
