#define ACCEPTABLE_CHARACTERS @"ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz "

#import "SignupViewController.h"
#import "WorkerDashboardViewController.h"
#import "DistributionViewController.h"
#import "ViewController.h"
#import "DEMORootViewController.h"
#import "Helper.h"
#import "Braintree.h"



#define kOFFSET_FOR_KEYBOARD 140.0

@interface SignupViewController ()
{
    DistributionViewController *distributionView;
    ViewController *view_controller;
    id json;
    UIImage *img_p;
    UIPickerView *myPickerView;
    NSMutableArray *monthsArray,*yearsArray;
    NSString *month,*year;
    NSString *yearString1;
    NSString  *selectedStringFromPickerView;
    NSUserDefaults *defaults;
    NSString *str_RegistrationStatus;
    UIAlertView *alertView1,*alertView2,*alertView3,*alert4;
    UIButton *rightButton;
    UIButton *backButton;
    NSString *nonceRecieved;
    
    
      Braintree *braintree;
}
@property(nonatomic,strong)NSNumber *selectedMonth,*selectedYear;
@property (nonatomic, strong) QBPopupMenu *popupMenu;

@end

@implementation SignupViewController

- (void)viewDidLoad {
    
    [super viewDidLoad];
    rightButton=[UIButton buttonWithType:UIButtonTypeCustom];
    [rightButton setFrame:CGRectMake(0, 0, 50, 21)];
    [rightButton setTitle:@"skip" forState:UIControlStateNormal];
    [rightButton addTarget:self action:@selector(rightBarButton:) forControlEvents:UIControlEventTouchUpInside];
    self.navigationItem.rightBarButtonItem=[[UIBarButtonItem alloc]initWithCustomView:rightButton];
    rightButton.hidden=YES;
    btn_profilePic_Object.enabled=YES;
    
    
    btn_dltProfile_obj.hidden=YES;
    value=NO;
    defaults=[NSUserDefaults standardUserDefaults];
    
    monthsArray=[[NSMutableArray alloc]initWithObjects:@"January",@"February",@"March",@"April",@"May",@"June",@"July",@"August",@"September",@"October",@"November",@"December",nil];
    NSDateFormatter *formatter = [[NSDateFormatter alloc] init];
    [formatter setDateFormat:@"mm-yyyy"];
    yearString1 = [formatter stringFromDate:[NSDate date]];
    yearsArray=[[NSMutableArray alloc]init];
    for (int i=0; i<100; i++)
    {
        [yearsArray addObject:[NSString stringWithFormat:@"%d",[yearString1 intValue]+i]];
    }
    myPickerView = [[UIPickerView alloc] init];
    myPickerView.dataSource=self;
    myPickerView.delegate = self;
    myPickerView.showsSelectionIndicator = YES;
       
    imgView_profileView.hidden=YES;
    btn_profilePic_Object.layer.masksToBounds=YES;
    btn_profilePic_Object.layer.cornerRadius = 5;
    btn_profilePic_Object.clipsToBounds=YES;
    [self.scrollView_Object setContentSize:CGSizeMake(self.scrollView_Object.frame.size.width, self.scrollView_Object.frame.size.height+500)];
    if(UI_USER_INTERFACE_IDIOM() == UIUserInterfaceIdiomPhone)
    {
        CGSize result = [[UIScreen mainScreen] bounds].size;
        if(result.height == 568)
        {
            // iPhone 5
        }
        else if(result.height == 667)
        {
            [self.scrollView_Object setFrame:CGRectMake(self.scrollView_Object.frame.origin.x, self.scrollView_Object.frame.origin.y+40, self.scrollView_Object.frame.size.width, self.scrollView_Object.frame.size.height)];
        }
        else if(result.height == 736)
        {
            [self.scrollView_Object setFrame:CGRectMake(self.scrollView_Object.frame.origin.x, self.scrollView_Object.frame.origin.y+40, self.scrollView_Object.frame.size.width, self.scrollView_Object.frame.size.height)];
        }
    }
    [self.navigationController.navigationBar setBackgroundImage:[UIImage imageNamed:@"atoz.png"] forBarMetrics:UIBarMetricsDefault];
    [[UINavigationBar appearance] setShadowImage:[[UIImage alloc] init]];
    self.navigationController.navigationBarHidden=NO;
    
    backButton=[UIButton buttonWithType:UIButtonTypeCustom];
    [backButton setFrame:CGRectMake(0, 0, 23, 21)];
    [backButton setImage:[UIImage imageNamed:@"arrow.png"] forState:UIControlStateNormal];
    [backButton addTarget:self action:@selector(leftBarButton:) forControlEvents:UIControlEventTouchUpInside];
    self.navigationItem.leftBarButtonItem=[[UIBarButtonItem alloc]initWithCustomView:backButton];
    backButton.hidden=NO;
    
    [self upDateStep];
    [self.navigationController.navigationBar
     setTitleTextAttributes:@{NSForegroundColorAttributeName : [UIColor whiteColor]}];
    [_scrollView_Object setContentSize:CGSizeMake(640, 400)];
    [_scrollView_Object setShowsVerticalScrollIndicator:NO];
    
    UITapGestureRecognizer *tap=[[UITapGestureRecognizer alloc]initWithTarget:self action:@selector(tapGesture:)];
    tap.numberOfTapsRequired=1;
    [self.view addGestureRecognizer:tap];
    _pageControl_object.backgroundColor=[UIColor grayColor];
    
}
-(void)viewWillDisappear:(BOOL)animated
{
    [super viewWillDisappear:YES];
    self.navigationController.navigationBarHidden=YES;
    str_RegistrationStatus=@"";
}
-(void)leftBarButton:(id)sender
{
    [self.navigationController popViewControllerAnimated:YES];
}

-(void)rightBarButton:(id)sender
{
    [self.navigationController popToRootViewControllerAnimated:YES];
    
    //    alertView2=[[UIAlertView alloc]initWithTitle:@"" message:@"Please verify your email to activate your account." delegate:self cancelButtonTitle:@"OK" otherButtonTitles: nil];
    //    [alertView2 show];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
}
-(void)tapGesture:(UITapGestureRecognizer *)sender
{
    [_txtField_YourName resignFirstResponder];
    [_txtField_Email resignFirstResponder];
    [_txtField_Password resignFirstResponder];
    [_txtField_Phone resignFirstResponder];
    [_txtField_FullNmae resignFirstResponder];
    [_txtField_CreditCardNumber resignFirstResponder];
    [_txtField_date resignFirstResponder];
    [_txtField_Cwc resignFirstResponder];
}

#pragma txtfield delegate method
#pragma mark
- (BOOL)textFieldShouldReturn:(UITextField *)textField
{
    if (textField == _txtField_YourName) {
        [_txtField_YourName resignFirstResponder];
        [_txtField_Email becomeFirstResponder];
    }
    else if (textField == _txtField_Email) {
        [_txtField_Email resignFirstResponder];
        [_txtField_Password becomeFirstResponder];
    }
    else if (textField == _txtField_Password) {
        [_txtField_Password resignFirstResponder];
        [_txtField_Phone becomeFirstResponder];
    }
    else if (textField == _txtField_Phone) {
        [_txtField_Phone resignFirstResponder];
    }
    else if (textField == _txtField_FullNmae) {
        [_txtField_FullNmae resignFirstResponder];
        [_txtField_CreditCardNumber becomeFirstResponder];
    }
    else if (textField == _txtField_CreditCardNumber) {
        [_txtField_CreditCardNumber resignFirstResponder];
        
    }
    
    return YES;
}
#pragma mark popup datepicker in keyboard...........start.......................
-(void)textFieldDidBeginEditing:(UITextField *)textField
{
    if (textField==_txtField_date)
    {
        UIToolbar *bar=[[UIToolbar alloc]initWithFrame:CGRectMake(0, 0, self.view.bounds.size.width,40)];
        UIBarButtonItem *item=[[UIBarButtonItem alloc]initWithTitle:@"Done" style:UIBarButtonItemStyleDone target:self action:@selector(Done:)];
        NSArray *arr=@[item];
        [bar setItems:arr];
        _txtField_date.inputView=myPickerView;
        _txtField_date.inputAccessoryView=bar;
    }
}
-(void)Done:(id)sender
{
    [_txtField_date resignFirstResponder];
}
#pragma mark
#pragma UIPicker for show date........................................
- (NSInteger)numberOfComponentsInPickerView:(UIPickerView *)pickerView
{
    return 2;
}
- (NSInteger)pickerView:(UIPickerView *)pickerView numberOfRowsInComponent:(NSInteger)component
{
    if (component==0)
    {
        return monthsArray.count;
    }else{
        return yearsArray.count;
    }
    return 0;
}

- (NSString *)pickerView:(UIPickerView *)pickerView titleForRow:(NSInteger)row forComponent:(NSInteger)component
{
    NSString *returningString;
    if (component == 0) {
        //Expiration month
        //  return monthArray[row];
        returningString=monthsArray[row];
    }
    else {
        //Expiration year
        NSDateFormatter *dateFormatter = [[NSDateFormatter alloc] init];
        [dateFormatter setDateFormat:@"yyyy"];
        NSInteger currentYear = [[dateFormatter stringFromDate:[NSDate date]] integerValue];
        returningString=[NSString stringWithFormat:@"%ld", currentYear + row];
    }
    
    return returningString;
    
}
- (void)pickerView:(UIPickerView *)pickerView didSelectRow:(NSInteger)row inComponent:(NSInteger)component {
    
    
    if (component == 0) {
        self.selectedMonth = @(row + 1);
        //   month=[monthsArray objectAtIndex:self.selectedMonth];
    }
    else {
        NSString *yearString = [self pickerView:pickerView titleForRow:row forComponent:1];
        self.selectedYear = @([yearString integerValue]);
    }
    if (!self.selectedMonth) {
        [pickerView selectRow:0 inComponent:0 animated:YES];
        self.selectedMonth = @(1); //Default to January if no selection
    }
    
    if (!self.selectedYear) {
        [pickerView selectRow:0 inComponent:1 animated:YES];
        NSString *yearString = [self pickerView:pickerView titleForRow:0 forComponent:1];
        self.selectedYear = @([yearString integerValue]); //Default to current year if no selection
    }
    selectedStringFromPickerView=[NSString stringWithFormat:@"%@/%@", self.selectedMonth, self.selectedYear];
    [self set];
}

-(void)set
{
    if ([self.selectedYear compare:[NSNumber numberWithInt:[yearString1 intValue]]] == NSOrderedDescending)
    {
        NSLog(@"date1 is later than date2");
    }
    _txtField_date.text=[NSString stringWithFormat:@"%@-%@",self.selectedMonth,self.selectedYear];
}

- (CGFloat)pickerView:(UIPickerView *)pickerView widthForComponent:(NSInteger)component
{
    if (component == 0)
    {
        return (self.view.frame.size.width * 55 ) / 100  ;
    }
    else
    {
        return (self.view.frame.size.width * 30 ) / 100  ;
    }
}



//....................................................................
#pragma mark popup datepicker in keyboard...........End.......................
-(void)textFieldDidEndEditing:(UITextField *)textField
{
}




-(BOOL)textField:(UITextField *)textField shouldChangeCharactersInRange:(NSRange)range replacementString:(NSString *)string {
    
    if (textField==_txtField_YourName )
    {
        if (_txtField_YourName.text.length<1)
        {
            if ([string isEqualToString:@" "]) {
                return NO;
            }
        }
        if (_txtField_YourName.text.length>50)
        {
            if ([string isEqualToString:@""]) {
                return YES;
            }
            else
            {
                return NO;
            }
        }
        NSCharacterSet *cs = [[NSCharacterSet characterSetWithCharactersInString:ACCEPTABLE_CHARACTERS] invertedSet];
        NSString *filtered = [[string componentsSeparatedByCharactersInSet:cs] componentsJoinedByString:@""];
        return [string isEqualToString:filtered];
    }
    if (textField==_txtField_Email)
    {
        if (_txtField_Email.text.length<1)
        {
            if ([string isEqualToString:@" "]) {
                return NO;
            }
        }
        NSString *textFieldText = [_txtField_Email.text stringByReplacingCharactersInRange:range withString:string];
        if (textFieldText.length==0||[textFieldText length]<=50)
        {
            return YES;
        }
        else
            return NO;
    }
    
    if (textField==_txtField_Password)
    {
        
        if (_txtField_Password.text.length<1)
        {
            if ([string isEqualToString:@" "]) {
                return NO;
            }
        }
        NSString *textFieldText = [_txtField_Password.text stringByReplacingCharactersInRange:range withString:string];
        if (textFieldText.length==0||[textFieldText length]<=20 )
        {
            return YES;
        }
        else
            return NO;
    }
    if (textField==_txtField_Phone)
    {
        NSString *textFieldText = [_txtField_Phone.text stringByReplacingCharactersInRange:range withString:string];
        if (textFieldText.length==0||[textFieldText length]<=15 )
        {
            return YES;
        }
        else
            return NO;
    }
    if (textField==_txtField_FullNmae)
    {
        if (_txtField_FullNmae.text.length<1)
        {
            if ([string isEqualToString:@" "]) {
                return NO;
            }
        }
        if (_txtField_FullNmae.text.length>50)
        {
            if ([string isEqualToString:@""]) {
                return YES;
            }
            else
            {
                return NO;
            }
        }
        NSCharacterSet *cs = [[NSCharacterSet characterSetWithCharactersInString:ACCEPTABLE_CHARACTERS] invertedSet];
        NSString *filtered = [[string componentsSeparatedByCharactersInSet:cs] componentsJoinedByString:@""];
        return [string isEqualToString:filtered];
    }
    if (textField==_txtField_CreditCardNumber)
    {
        NSString *textFieldText = [_txtField_CreditCardNumber.text stringByReplacingCharactersInRange:range withString:string];
        if (textFieldText.length==0||[textFieldText length]<=20)
        {
            return YES;
        }
        else
            return NO;
    }
    if (textField==_txtField_date)
    {
        return NO;
    }
    
    if (textField==_txtField_Cwc)
    {
        NSString *textFieldText = [_txtField_Cwc.text stringByReplacingCharactersInRange:range withString:string];
        if (textFieldText.length==0||[textFieldText length]<=4)
        {
            return YES;
        }
        else
            return NO;
    }
    return YES;
}
-(BOOL)validateAlphabets: (NSString *)alpha
{
    NSString *abnRegex = @"[A-Za-z]+";
    NSPredicate *abnTest = [NSPredicate predicateWithFormat:@"SELF MATCHES %@", abnRegex];
    BOOL isValid = [abnTest evaluateWithObject:alpha];
    return isValid;
}
-(BOOL)isValidEmail:(NSString *)checkString
{
    BOOL stricterFilter = NO; // Discussion http://blog.logichigh.com/2010/09/02/validating-an-e-mail-address/
    NSString *stricterFilterString = @"[A-Z0-9a-z\\._%+-]+@([A-Za-z0-9-]+\\.)+[A-Za-z]{2,4}";
    NSString *laxString = @".+@([A-Za-z0-9-]+\\.)+[A-Za-z]{2}[A-Za-z]*";
    NSString *emailRegex = stricterFilter ? stricterFilterString : laxString;
    NSPredicate *emailTest = [NSPredicate predicateWithFormat:@"SELF MATCHES %@", emailRegex];
    return [emailTest evaluateWithObject:checkString];
}
#pragma mark
#pragma mark -user Registration
-(void)registration
{
    
    if ([Helper isConnectedToInternet])
    {
    
    NSString *type=[defaults valueForKey:@"user_status"];
    str_address=@"not availble";
    str_registrationtype=@"sidejob_user";
    
    NSMutableDictionary *jsonDict=[[NSMutableDictionary alloc]init];
    [jsonDict setValue:str_name forKey:@"name"];
    [jsonDict setValue:str_email forKey:@"email"];
    [jsonDict setValue:type forKey:@"user_type"];
    [jsonDict setValue:str_registrationtype forKey:@"registration_type"];
    [jsonDict setValue:str_address forKey:@"address"];
    [jsonDict setValue:str_phone forKey:@"phone"];
    [jsonDict setValue:str_password forKey:@"password"];
    
    NSString *url=[NSString stringWithFormat:@"%@create-user",serviceLink];
    json = [WebService signup:jsonDict:url];
    if ([[json objectForKey:@"message" ]isEqualToString:@"Success"])
    {
        str_RegistrationStatus=@"yes";
        
        rightButton.hidden=NO;
        btn_profilePic_Object.enabled=NO;
        btn_dltProfile_obj.hidden=YES;
        // To get cutomerId
        
        
        alert4=[[UIAlertView alloc]initWithTitle:@"" message:@"Registration successful! Please verify your email to activate your account." delegate:self cancelButtonTitle:@"OK" otherButtonTitles: nil];
        [alert4 show];
        
        NSDictionary *dict=[NSDictionary new];
        dict=[json objectForKey:@"response"];
        user_ID=[json objectForKey:@"user_id"];
        NSLog(@"user id is...%@",user_ID);
        // [self performSelectorInBackground:@selector(saveCardDetailWithOutCard) withObject:nil];
        [self performSelector:@selector(saveCardDetailWithOutCard) withObject:nil afterDelay:0.1f];
        if (img_userProfile !=nil)
        {
            [self uploadProfileImages:img_userProfile];
        }
        else
        {
            [SVProgressHUD dismiss];
        }
    }
    else if([[json objectForKey:@"message" ]isEqualToString:@"Error :: Record Not verified."])
    {
        [SVProgressHUD dismiss];
        NSDictionary *dict=[NSDictionary new];
        dict=[json objectForKey:@"errors"];
        NSArray *arr=[dict objectForKey:@"email"];
        NSString *msg=[arr objectAtIndex:0];
        UIAlertView *alert=[[UIAlertView alloc]initWithTitle:@"" message:msg delegate:self cancelButtonTitle:@"OK" otherButtonTitles: nil];
        [alert show];
    }
    else if ([json objectForKey:@"errors"])
    {
        
    }
    else if (json==nil||json==[NSNull null])
    {
        UIAlertView *alert=[[UIAlertView alloc]initWithTitle:@"" message:@"No Response" delegate:self cancelButtonTitle:@"OK" otherButtonTitles: nil];
        [alert show];
    }
    else
    {
        UIAlertView *alert=[[UIAlertView alloc]initWithTitle:@"" message:@"Error to Register" delegate:self cancelButtonTitle:@"OK" otherButtonTitles: nil];
        [alert show];
    }
        
    }else
    {
        UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"No Internet" message:@"Please check your internet connection." delegate:nil cancelButtonTitle:@"OK" otherButtonTitles:nil];
        [alert show];
    }
    [SVProgressHUD dismiss];
        
        
}


-(void)uploadProfileImages:(UIImage*)img
{
    NSMutableURLRequest *request = [[NSMutableURLRequest alloc] initWithURL:[NSURL URLWithString:[NSString stringWithFormat:@"%@uploadprofileimage/%@",serviceLink,user_ID]]];
    NSData *imageData = UIImageJPEGRepresentation(img, 1.0);
    [request setCachePolicy:NSURLRequestReloadIgnoringLocalCacheData];
    [request setHTTPShouldHandleCookies:NO];
    [request setTimeoutInterval:60];
    [request setHTTPMethod:@"POST"];
    NSString *boundary = @"unique-consistent-string";
    // set Content-Type in HTTP header
    NSString *contentType = [NSString stringWithFormat:@"multipart/form-data; boundary=%@", boundary];
    [request setValue:contentType forHTTPHeaderField: @"Content-Type"];
    // post body
    NSMutableData *body = [NSMutableData data];
    // add params (all params are strings)
    [body appendData:[[NSString stringWithFormat:@"--%@\r\n", boundary] dataUsingEncoding:NSUTF8StringEncoding]];
    [body appendData:[[NSString stringWithFormat:@"Content-Disposition: form-data; name=%@\r\n\r\n", @"imageCaption"] dataUsingEncoding:NSUTF8StringEncoding]];
    [body appendData:[[NSString stringWithFormat:@"%@\r\n", @"Some Caption"] dataUsingEncoding:NSUTF8StringEncoding]];
    // add image data
    if (imageData) {
        [body appendData:[[NSString stringWithFormat:@"--%@\r\n", boundary] dataUsingEncoding:NSUTF8StringEncoding]];
        [body appendData:[[NSString stringWithFormat:@"Content-Disposition: form-data; name=%@; filename=imageName.jpg\r\n", @"photo"] dataUsingEncoding:NSUTF8StringEncoding]];
        [body appendData:[@"Content-Type: image/jpeg\r\n\r\n" dataUsingEncoding:NSUTF8StringEncoding]];
        [body appendData:imageData];
        [body appendData:[[NSString stringWithFormat:@"\r\n"] dataUsingEncoding:NSUTF8StringEncoding]];
    }
    [body appendData:[[NSString stringWithFormat:@"--%@--\r\n", boundary] dataUsingEncoding:NSUTF8StringEncoding]];
    // setting the body of the post to the reqeust
    [request setHTTPBody:body];
    // set the content-length
    NSString *postLength = [NSString stringWithFormat:@"%lu", (unsigned long)[body length]];
    [request setValue:postLength forHTTPHeaderField:@"Content-Length"];
    [NSURLConnection sendAsynchronousRequest:request queue:[NSOperationQueue currentQueue] completionHandler:^(NSURLResponse *response, NSData *data, NSError *error) {
        if(data.length > 0)
        {
            //success
            NSMutableString *responseStr = [[NSMutableString alloc] initWithData:data encoding:NSUTF8StringEncoding];
            NSMutableDictionary *dict = [[NSMutableDictionary alloc]init];
            dict = [responseStr JSONValue];
            NSLog(@"%@",dict);
            if ([[dict objectForKey:@"message"]isEqualToString:@"Success"])
            {
                [SVProgressHUD dismiss];
                
            }
            else if ([[dict objectForKey:@"message"]isEqualToString:@"Error :: User Image Not Found"])
            {
                [SVProgressHUD dismiss];
                UIAlertView *alert=[[UIAlertView alloc]initWithTitle:@"" message:@"Please select the profile image" delegate:self cancelButtonTitle:@"OK" otherButtonTitles: nil];
                [alert show];
            }
        }
    }];
}


-(void)reachedRootView
{
    [self.navigationController popToRootViewControllerAnimated:YES];
}



-(void)login_user;
{
    // NSUserDefaults *defaults=[NSUserDefaults standardUserDefaults];
    
    if ([[str_email stringByReplacingOccurrencesOfString:@" " withString:@""] length]<1 &&[[str_password stringByReplacingOccurrencesOfString:@" " withString:@""] length]<1)
    {
        UIAlertView *alert=[[UIAlertView alloc]initWithTitle:@"" message:@"Please enter valid email and password" delegate:self cancelButtonTitle:@"OK" otherButtonTitles: nil];
        [alert show];
    }
    else if ([[str_email stringByReplacingOccurrencesOfString:@" " withString:@""] length]<1)
    {
        UIAlertView *alert=[[UIAlertView alloc]initWithTitle:@"" message:@"Please enter valid email address" delegate:self cancelButtonTitle:@"OK" otherButtonTitles: nil];
        [alert show];
    }
    else if ([[str_password stringByReplacingOccurrencesOfString:@" " withString:@""] length]<1)
    {
        UIAlertView *alert=[[UIAlertView alloc]initWithTitle:@"" message:@"Please enter password" delegate:self cancelButtonTitle:@"OK" otherButtonTitles: nil];
        [alert show];
    }
    else
    {
        NSMutableDictionary *jsonDict=[[NSMutableDictionary alloc]init];
        [jsonDict setValue:[defaults valueForKey:@"status"] forKey:@"user_type"];
        [jsonDict setValue:str_email forKey:@"email"];
        [jsonDict setValue:str_password forKey:@"password"];
        NSString *url=[NSString stringWithFormat:@"%@api/authenticate",serviceLink];
        json = [WebService signup:jsonDict:url];
        if ([[json objectForKey:@"message" ]isEqualToString:@"Success"])
        {            NSMutableArray *Arr=[[NSMutableArray alloc]init];
            Arr=[json objectForKey:@"user"];
            NSMutableDictionary *dict=[NSMutableDictionary new];
            dict=[Arr objectAtIndex:0];
            str_nameProfile=[dict objectForKey:@"name"];
            str_phoneprofile=[dict objectForKey:@"phone"];
            str_emailProfile=[dict objectForKey:@"email"];
            str_aboutProfile=[dict objectForKey:@""];
            str_rating=[dict objectForKey:@"rating"];
            // str_profilephoto=[dict objectForKey:@"profile_image"];
            str_profilephoto=[NSString stringWithFormat:@"%@%@",serviceLink,[dict objectForKey:@"profile_image"]];
            //  str_ratingProfile=[dict objectForKey:@"rating"];
            str_Id=[dict objectForKey:@"id"];
            
            [defaults setValue:[json objectForKey:@"token"] forKey:@"user_token"];
            [defaults setValue:[dict objectForKey:@"id"] forKey:@"user_id"];
            [defaults setObject:str_nameProfile forKey:@"p_name"];
            [defaults setObject:str_emailProfile forKey:@"p_email"];
            [defaults setObject:str_aboutProfile forKey:@"p_about"];
            [defaults setObject:str_phoneprofile forKey:@"p_phone"];
            [defaults setObject:str_profilephoto forKey:@"p_photo"];
            [defaults setObject:str_rating forKey:@"rating_user"];
            [defaults setObject:[defaults valueForKey:@"user_type"] forKey:@"status"];
            [defaults synchronize];
            [[NSUserDefaults standardUserDefaults]synchronize];
            NSLog(@"%@",[defaults objectForKey:@"user_token"]);
            
            [defaults setBool:YES forKey:@"logined"];
            DEMORootViewController *viewController=[self.storyboard instantiateViewControllerWithIdentifier:@"rootController"];
            [self.navigationController pushViewController:viewController animated:YES];
        }
        else if([[json objectForKey:@"error" ]isEqualToString:@"invalid_credentials"])
        {
            UIAlertView *alert=[[UIAlertView alloc]initWithTitle:@"" message:@"Email and Password does not match" delegate:self cancelButtonTitle:@"OK" otherButtonTitles: nil];
            [alert show];
        }
    }
}
#pragma mark
#pragma page Control
- (IBAction)pageControl_Action:(id)sender
{
    [self testfield];
}


- (IBAction)btnNext_Action:(id)sender
{
    if ([str_RegistrationStatus isEqualToString:@"yes"])
    {
        
        if (_txtField_FullNmae.text.length<1)
        {
            
            UIAlertView *alertView=[[UIAlertView alloc]initWithTitle:@"Alert!" message:@"Please enter card holder name." delegate:nil cancelButtonTitle:@"OK" otherButtonTitles:nil];
            [alertView show];
        }
        else if(_txtField_CreditCardNumber.text.length<1)
        {
            UIAlertView *alertView=[[UIAlertView alloc]initWithTitle:@"Alert!" message:@"Please enter card number." delegate:nil cancelButtonTitle:@"OK" otherButtonTitles:nil];
            [alertView show];
        }
        
        else if(_txtField_date.text.length<1)
        {
            UIAlertView *alertView=[[UIAlertView alloc]initWithTitle:@"Alert!" message:@"Please enter expiry date." delegate:nil cancelButtonTitle:@"OK" otherButtonTitles:nil];
            [alertView show];
        }
        else if( _txtField_Cwc.text.length<3)
        {
            UIAlertView *alertView=[[UIAlertView alloc]initWithTitle:@"Alert!" message:@"Please enter valid cvc number." delegate:nil cancelButtonTitle:@"OK" otherButtonTitles:nil];
            [alertView show];
        }
        else
        {
            [SVProgressHUD showWithStatus:@"Saving..."];
            [self performSelector:@selector(setAuthentication) withObject:nil afterDelay:0.3];
            
            //[self setAuthentication];
        }
        
       
        //[self performSelector:@selector(varifyCardDtails) withObject:nil afterDelay:0.3f];
    }
    else
    {
    [self testfield];
    }
}


-(void)testfield;
{
    str_email=_txtField_Email.text;
    str_phone=_txtField_Phone.text;
    str_name=_txtField_YourName.text;
    str_password=_txtField_Password.text;
    
    if ([[str_name stringByReplacingOccurrencesOfString:@" " withString:@""] length]<3 ||str_name.length>50)
    {
        [SVProgressHUD dismiss];
        UIAlertView *alert=[[UIAlertView alloc]initWithTitle:@"" message:@"Please enter your name in between 3 to 50 characters." delegate:self cancelButtonTitle:@"OK" otherButtonTitles: nil];
        [alert show];
    }
    else if ([[str_email stringByReplacingOccurrencesOfString:@" " withString:@""] length]<1 || ![self isValidEmail:str_email])
    {
        [SVProgressHUD dismiss];
        UIAlertView *alert=[[UIAlertView alloc]initWithTitle:@"" message:@"Please enter a valid email." delegate:self cancelButtonTitle:@"OK" otherButtonTitles: nil];
        [alert show];
    }
    else if ([[str_password stringByReplacingOccurrencesOfString:@" " withString:@""] length]<6)
    {
        [SVProgressHUD dismiss];
        UIAlertView *alert=[[UIAlertView alloc]initWithTitle:@"" message:@"Please enter atleast  6 characters password." delegate:self cancelButtonTitle:@"OK" otherButtonTitles: nil];
        [alert show];
    }
    else if ([[str_phone stringByReplacingOccurrencesOfString:@" " withString:@""] length]<1 || str_phone.length<8 || str_phone.length>15)
    {
        [SVProgressHUD dismiss];
        UIAlertView *alert=[[UIAlertView alloc]initWithTitle:@"" message:@"Please enter a valid phone number." delegate:self cancelButtonTitle:@"OK" otherButtonTitles: nil];
        [alert show];
    }
    else
     
    {
        [SVProgressHUD showWithStatus:@"Loading..."];
        [self performSelector:@selector(registration) withObject:nil afterDelay:0.3f];
    }
}
#pragma mark
#pragma pick image for profile
- (IBAction)btnPicImage_Action:(id)sender
{
    QBPopupMenuItem *item = [QBPopupMenuItem itemWithTitle:@"Capture Image" target:self action:@selector(TakePictureWithCamera:)];
    QBPopupMenuItem *item2 = [QBPopupMenuItem itemWithTitle:@"Pick from library" target:self action:@selector(selectPhoto:)];
    //  QBPopupMenuItem *item3 = [QBPopupMenuItem itemWithTitle:@"Delete" target:self action:@selector(selectPhoto:)];
    
    NSArray *items;
    // if (isImageset) {
    //    items = @[item, item2,item3];
    // }else{
    items = @[item, item2];
    //  }
    QBPopupMenu *popupMenu = [[QBPopupMenu alloc] initWithItems:items];
    popupMenu.height=50;
    popupMenu.highlightedColor = [[UIColor colorWithRed:0 green:0.478 blue:1.0 alpha:1.0] colorWithAlphaComponent:0.8];
    self.popupMenu = popupMenu;
    
    UIButton *button = (UIButton *)sender;
    [self.popupMenu showInView:self.view targetRect:button.frame animated:YES];
}
-(void)TakePictureWithCamera:(id)sender
{
    if (![UIImagePickerController isSourceTypeAvailable:UIImagePickerControllerSourceTypeCamera]) {
        
        UIAlertView *myAlertView = [[UIAlertView alloc] initWithTitle:@"Error"
                                                              message:@"Device has no camera"
                                                             delegate:nil
                                                    cancelButtonTitle:@"OK"
                                                    otherButtonTitles: nil];
        [myAlertView show];
    }else
    {
        UIImagePickerController *picker = [[UIImagePickerController alloc] init];
        picker.allowsEditing = YES;
        picker.delegate = self;
        picker.sourceType = UIImagePickerControllerSourceTypeCamera;
        [self presentViewController:picker animated:YES completion:nil];
    }
}

- (void)selectPhoto:(UIButton *)sender {
    UIImagePickerController *picker = [[UIImagePickerController alloc] init];
    picker.delegate = self;
    picker.allowsEditing = YES;
    picker.sourceType = UIImagePickerControllerSourceTypePhotoLibrary;
    [self presentViewController:picker animated:YES completion:NULL];
}
- (void)imagePickerController:(UIImagePickerController *)picker didFinishPickingMediaWithInfo:(NSDictionary *)info {
    UIImage *chosenImage = info[UIImagePickerControllerEditedImage];
    [btn_profilePic_Object setImage:chosenImage forState:UIControlStateNormal];
    img_userProfile=chosenImage;
    imgView_profileView.hidden=NO;
    [picker dismissViewControllerAnimated:YES completion:nil];
    btn_dltProfile_obj.hidden=NO;
    self.navigationController.navigationBarHidden=NO;
}
-(void)imagePickerControllerDidCancel:(UIImagePickerController *)picker
{
    [SVProgressHUD dismiss];
    [picker dismissViewControllerAnimated:YES completion:nil];
}

-(void)upDateStep{
    
    if (_pageControl_object.currentPage==0)
    {
        _labelStep_object.text=[NSString stringWithFormat:@"STEP %ld OF 2",_pageControl_object.currentPage+1];
        self.navigationItem.title=[NSString stringWithFormat:@"STEP %ld OF 2",_pageControl_object.currentPage+1];
        [_btnNext_object setImage:[UIImage imageNamed:@"next_btn.png"] forState:UIControlStateNormal];
        value=NO;
    }
    if (_pageControl_object.currentPage==1)
    {
        NSString *str=[NSString stringWithFormat:@"STEP %ld OF 2",_pageControl_object.currentPage+1];
        [_btnNext_object setImage:[UIImage imageNamed:@"reg_btn.png"] forState:UIControlStateNormal];
        _labelStep_object.text=str;
        self.navigationItem.title=str;
        value=YES;
        _btnNext_object.tag=1;
    }
}
-(void)clearField
{
    _txtField_YourName.text=@"";
    _txtField_Email.text=@"";
    _txtField_Password.text=@"";
    _txtField_Phone.text=@"";
    _txtField_FullNmae.text=@"";
    _txtField_CreditCardNumber.text=@"";
    _txtField_date.text=@"";
    _txtField_Cwc.text=@"";
}
- (IBAction)btn_profileDelete:(id)sender {
    img_userProfile=nil;
    btn_dltProfile_obj.hidden=YES;
    [btn_profilePic_Object setImage:[UIImage imageNamed:@"camera_icon.png"] forState:UIControlStateNormal];
    imgView_profileView.hidden=YES;
}
- (IBAction)btn_back:(id)sender {
    [_scrollView_Object setContentOffset:CGPointMake(0,0) animated:YES];
    _pageControl_object.currentPage-=1;
    [self upDateStep];
}

#pragma mark
#pragma mark-varify card details
-(void)varifyCardDtails
{
    [defaults setObject:_txtField_FullNmae.text forKey:@"fullName_card"];
    [defaults setObject:_txtField_CreditCardNumber.text forKey:@"creditCardNumber_card"];
    [defaults setObject:_txtField_Cwc.text forKey:@"cvc_card"];
    [defaults setObject:_txtField_date.text forKey:@"expirydate_card"];
    [defaults setObject:_txtField_Phone.text forKey:@"phoneNo_card"];
    [defaults setObject:_txtField_YourName.text forKey:@"userName_card"];
    [defaults synchronize];
    
    
    if (_txtField_FullNmae.text.length>0 && _txtField_CreditCardNumber.text.length>0&&_txtField_date.text.length>0&&_txtField_Cwc.text.length>0)
    {
        
        NSString *str=_txtField_date.text;
        NSString* month1;
        NSString *year1;
        if (str.length>0)
        {
            NSArray* date = [str componentsSeparatedByString: @"-"];
            month1 = [date objectAtIndex: 0];
            year1=[date objectAtIndex:1];
        }
        
        
        NSMutableDictionary *jsonDict=[[NSMutableDictionary alloc]init];
        
        [jsonDict setObject:_txtField_CreditCardNumber.text forKey:@"number"];
        [jsonDict setObject:month1 forKey:@"expirationMonth"];
        [jsonDict setObject:year1 forKey:@"expirationYear"];
        [jsonDict setObject:_txtField_Cwc.text forKey:@"cvv"];
        [jsonDict setObject:[defaults objectForKey:@"customer_id"]forKey:@"id"];
        [jsonDict setObject:_txtField_FullNmae.text forKey:@"CardholderName"];
        
        
        NSString *urlstr=[NSString stringWithFormat:@"%@add-card",serviceLink];
        id json1 = [WebService signup:jsonDict:urlstr];
        
        if ([[json1 objectForKey:@"message"]isEqualToString:@"Success"])
        {
            [self setAuthentication];
        }
        else
        {
          if( [self.typeUser isEqualToString:@"customer"])
          {
            
            UIAlertView *alertView=[[UIAlertView alloc]initWithTitle:@"" message:@"Please enter correct card details" delegate:nil cancelButtonTitle:@"OK" otherButtonTitles:nil];
            [alertView show];
          }
          else
          {
              UIAlertView *alertView=[[UIAlertView alloc]initWithTitle:@"" message:@"Please enter correct account details" delegate:nil cancelButtonTitle:@"OK" otherButtonTitles:nil];
              [alertView show];
          }
             [SVProgressHUD dismiss];
        }
        
    }
    
    else
    {
        if( [self.typeUser isEqualToString:@"customer"])
        {
            
            UIAlertView *alertView=[[UIAlertView alloc]initWithTitle:@"" message:@"Please enter card details" delegate:nil cancelButtonTitle:@"OK" otherButtonTitles:nil];
            [alertView show];
        }
        else
        {
            UIAlertView *alertView=[[UIAlertView alloc]initWithTitle:@"" message:@"Please enter account details" delegate:nil cancelButtonTitle:@"OK" otherButtonTitles:nil];
            [alertView show];
        }

        
//        UIAlertView *alert=[[UIAlertView alloc]initWithTitle:@"" message:@"Please enter card details" delegate:nil cancelButtonTitle:@"OK" otherButtonTitles:nil];
//        [alert show];
        [SVProgressHUD dismiss];
    }
   
}

- (void)alertView:(UIAlertView *)alertView clickedButtonAtIndex:(NSInteger)buttonIndex
{
    NSInteger t=buttonIndex;
    
    if (alertView1)
    {
        switch (t) {
            case 0:
                [self.navigationController popToRootViewControllerAnimated:YES];
                break;
                
            default:
                break;
        }
    }
    if (alert4)
    {
        switch (t) {
            case 0:
                [_scrollView_Object setContentOffset:CGPointMake(_scrollView_Object.frame.origin.x+_scrollView_Object.frame.size.width, 0)animated:YES];
                _pageControl_object.currentPage+=1;
                [self upDateStep];
                break;
                
            default:
                break;
        }
    }
    
    if (alertView3)
    {
        switch (t) {
            case 0:
                [self.navigationController popToRootViewControllerAnimated:YES];
                break;
                
            default:
                break;
        }
    }
}
-(void)saveCardDetailWithOutCard
{
    NSMutableDictionary *jsonDict=[[NSMutableDictionary alloc]init];
    [jsonDict setObject:user_ID forKey:@"id"];
    [jsonDict setObject:str_email forKey:@"email"];
    [jsonDict setObject:_txtField_Phone.text forKey:@"phone"];
    [jsonDict setObject:_txtField_YourName.text forKey:@"name"];
    [jsonDict setObject:_txtField_FullNmae.text forKey:@"CardholderName"];
    
    NSString *url=[NSString stringWithFormat:@"%@create-customer",serviceLink];
    json = [WebService signup:jsonDict:url];
    if ([[json objectForKey:@"message"]isEqualToString:@"Success"])
    {
        [defaults setObject:[json objectForKey:@"customer_id"] forKey:@"customer_id"];
        
    }
}
-(void)getUserInfo
{
    //    http://112.196.27.243:8000/read-user/{id}
    NSString *urlString=[NSString stringWithFormat:@"%@read-user/%@",serviceLink,[defaults objectForKey:@"user_id"] ];
    id jsonuser=[WebService GetwithoutDict:urlString];
    if ([[jsonuser objectForKey:@"message"]isEqualToString:@"Success"])
    {
        NSString *customer=[jsonuser objectForKey:@"response"];
        [defaults setObject:customer forKey:@"userInfo"];
        [defaults synchronize];
    }
    
}

#pragma mark
#pragma mark-getClient Token from server

#pragma mark
#pragma mark-Payment
-(void)setAuthentication
{
    NSString *url=[NSString stringWithFormat:@"%@createClientToken",serviceLink];
    NSMutableDictionary *dict=[[NSMutableDictionary alloc]init];
    [dict setObject:[defaults valueForKey:@" "] forKey:@"customer_id"];
 
    if ([Helper isConnectedToInternet])
    {
        id json1=[WebService signup:dict :url];
        if ([[json1 objectForKey:@"message"]isEqualToString:@"Success"])
        {
            paymentToken=[[json1 objectForKey:@"response"] objectForKey:@"token"];
            
            
        }
        else
        {
            UIAlertView *alert=[[UIAlertView alloc]initWithTitle:@"Alert!" message:@"Server error." delegate:nil cancelButtonTitle:@"OK" otherButtonTitles:nil];
            [alert show];
        }
    }
    else
    {
        UIAlertView *alert=[[UIAlertView alloc]initWithTitle:@"No Internet" message:@"Please check intenet connection." delegate:nil cancelButtonTitle:@"OK" otherButtonTitles:nil];
        [alert show];
    }
    
    
    // Initialize `Braintree` once per checkout session
    
    if (paymentToken==nil||[paymentToken isEqualToString:@""])
    {
        UIAlertView *alert=[[UIAlertView alloc]initWithTitle:@"Alert!" message:@"Server error." delegate:nil cancelButtonTitle:@"OK" otherButtonTitles:nil];
        [alert show];
    }
    else
    {
        braintree = [Braintree braintreeWithClientToken:paymentToken];
        NSLog(@"%@",braintree);
        [self getnonce];
        
    }
    
    
}
#pragma mark
#pragma mark-generate nonce to payment......***********....................

-(void)getnonce
{
    // NSMutableArray *arr_lastcard=[cardDetails lastObject];
    //NSString *cardno=[[[cardDetails objectAtIndex:0]objectForKey:@"_attributes"] objectForKey:@"maskedNumber"];
//    if (cardDetails.count)
//    {
         NSString *str=_txtField_date.text;
    NSArray *arr=[str componentsSeparatedByString:@"-"];
    
    NSString *strM=[arr objectAtIndex:0];
    NSString *strY=[arr objectAtIndex:1];
    
    
        BTClientCardRequest *request = [[BTClientCardRequest alloc] init];
        request.number =_txtField_CreditCardNumber.text ;
        request.expirationMonth = strM;
        request.expirationYear =strY;
        //request.cvv=@"123";
        
        
        
        [self->braintree tokenizeCard:request completion:^(NSString *nonce, NSError *error)
         {
             if (error)
             {
                 //self.progressBlock([NSString stringWithFormat:@"Error: %@", error]);
                 [[[UIAlertView alloc] initWithTitle:@"Error"
                                             message:[error localizedDescription]
                                            delegate:nil
                                   cancelButtonTitle:@"OK"
                                   otherButtonTitles:nil] show];
             }
             
             
             if (nonce)
             {
                 nonceRecieved=nonce;
                 NSLog(@"Nonce %@",nonce);
                 [self passNounceToServer];
                 
                 // [self varifyCard];
                 //            self.progressBlock([NSString stringWithFormat:@"Card tokenized -> Nonce Received: %@", nonce]);
                 //             self.completionBlock(nonce);
             }
         }];
        
    }
    


#pragma mark
#pragma mark- Create payement Method......********....................
-(void)passNounceToServer
{
  //  NSString *token=[NSString stringWithFormat:@"%@",[defaults objectForKey:@"user_token"]];
    
    NSString *url=[NSString stringWithFormat:@"%@createPaymentMethod",serviceLink];
    NSMutableDictionary *dict=[[NSMutableDictionary alloc]init];
    
    [dict setObject:[defaults valueForKey:@"customer_id"] forKey:@"customerId"];
    [dict setObject:nonceRecieved forKey:@"nonce"];
    
     id json1=[WebService signup:dict :url];
    
    if ([[json1 objectForKey:@"message"]isEqualToString:@"Success"])
    {
        
        alertView3=[[UIAlertView alloc]initWithTitle:@"" message:@"Your card details are saved successfully!" delegate:self cancelButtonTitle:@"OK" otherButtonTitles:nil];
     
        [alertView3 show];
        NSLog(@"%@",json1);
    }
    else
    {
         UIAlertView *alert=[[UIAlertView alloc]initWithTitle:@"" message:@"Please enter the correct card details." delegate:nil cancelButtonTitle:@"OK" otherButtonTitles:nil];
        [alert show];
         NSLog(@"%@",json1);
    }

    [SVProgressHUD dismiss];
    
}

@end
