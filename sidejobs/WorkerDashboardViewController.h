//
//  WorkerDashboardViewController.h
//  sideJobs
//
//  Created by DebutMac4 on 17/08/15.
//  Copyright (c) 2015 DebutMac4. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface WorkerDashboardViewController : UIViewController
- (IBAction)btnBack_Action:(id)sender;

- (IBAction)btn_searchJobs_Action:(id)sender;
- (IBAction)btnMyEarning:(id)sender;
- (IBAction)btn_Message:(id)sender;


- (IBAction)btn_SideJobs:(id)sender;
@end
