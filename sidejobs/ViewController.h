//
//  ViewController.h
//  sideJobs
//
//  Created by DebutMac4 on 13/08/15.
//  Copyright (c) 2015 DebutMac4. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface ViewController : UIViewController
{
    id json;
    NSUserDefaults *defaults;
    NSString *str_Token;
    NSString *str_email,*str_password;
    NSString *str_nameProfile,*str_emailProfile,*str_aboutProfile,*str_phoneprofile,*str_Id,*str_rating,*str_approval,*str_facebookID, *str_merchant_id;
    NSString *str_profilephoto;
    NSString *str_FNmae,*str_Femail,*str_FPassword,*str_FprofilePics;
    NSString *user_ID;
    UIImage *Image_userProfile;
    IBOutlet UIButton *btnAsLogin_object;
    IBOutlet UIButton *btnLoginAsFacebook_object;
    IBOutlet UIButton *btn_checkObj;
}

@property (strong, nonatomic) IBOutlet UIButton *btnSignup_Object;
@property (strong, nonatomic) IBOutlet UITextField *txtField_email;
@property (strong, nonatomic) IBOutlet UITextField *txtField_passord;

- (IBAction)btnCheckAction:(id)sender;
- (IBAction)btn_TermConditionSee:(id)sender;

- (IBAction)btnForgot_Action:(id)sender;
- (IBAction)btnSign_Action:(id)sender;
- (IBAction)btnLogin_Action:(id)sender;
- (IBAction)Btnfb_Action:(id)sender;
-(void)facebookRegistration;
-(void)login_user;

@end

