//
//  MessageViewController.m
//  sidejobs
//
//  Created by DebutMac4 on 24/11/15.
//  Copyright © 2015 DebutMac4. All rights reserved.
//

#import "MessageViewController.h"
#import "MessageCellVC.h"
//#import <MessageUI/MessageUI.h>

@interface MessageViewController ()
{
    NSUserDefaults *defaults;
    NSMutableArray *arr_contactList;
    NSMutableDictionary *dict_Contact;
    AppDelegate *delegate;
}

@end

@implementation MessageViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    
    delegate=(AppDelegate*)[[UIApplication sharedApplication]delegate];
    defaults=[NSUserDefaults standardUserDefaults];
    [SVProgressHUD show];
    [self performSelector:@selector(getContactList) withObject:nil afterDelay:0.3f];
    
    self.navigationItem.title=@"Contact List";
    self.navigationItem.hidesBackButton=YES;
    UIButton *btn=[UIButton buttonWithType:UIButtonTypeCustom];
    [btn setFrame:CGRectMake(0, 0, 23, 21)];
    [btn setImage:[UIImage imageNamed:@"arrow.png"] forState:UIControlStateNormal];
    [btn addTarget:self action:@selector(back:) forControlEvents:UIControlEventTouchUpInside];
    self.navigationItem.leftBarButtonItem=[[UIBarButtonItem alloc]initWithCustomView:btn];
     
    // Do any additional setup after loading the view.
}
-(void)back:(UIButton*)sender
{
[self.navigationController popViewControllerAnimated:YES];
}
- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

-(void)getContactList
{
    
    NSMutableDictionary *jsonDict=[[NSMutableDictionary alloc]init];
    NSString *token=[NSString stringWithFormat:@"%@",[defaults objectForKey:@"user_token"]];
    [jsonDict setValue:[defaults objectForKey:@"user_id"] forKey:@"id"];
    [jsonDict setValue:@"1" forKey:@"contact"];
    [jsonDict setValue:[defaults objectForKey:@"user_status"] forKey:@"user_type"];

    
    NSString *url=[NSString stringWithFormat:@"%@workers-list?token=%@",serviceLink,token];
    id  json = [WebService signup:jsonDict:url];
    NSLog(@"%@",json);
    
    if ([[json objectForKey:@"message"]isEqualToString:@"Success"])
    {
        [SVProgressHUD dismiss];
        arr_contactList=[json objectForKey:@"response"];
        lbl_status.hidden=YES;
        [tbl_contactNo reloadData];
    }
    
    else if ([[json objectForKey:@"error"]isEqualToString:@"token_expired"])
    {
        [SVProgressHUD dismiss];
        UIAlertView *alert=[[UIAlertView alloc]initWithTitle:@"" message:@"Session Expired Please login agian" delegate:nil cancelButtonTitle:@"OK" otherButtonTitles: nil];
        [alert show];
        [defaults setBool:NO forKey:@"logined"];
        [defaults synchronize];
        [[NSUserDefaults standardUserDefaults]synchronize];
        [delegate loginStatus];
    }
    
    else if(json == nil)
    {
         [SVProgressHUD dismiss];
         lbl_status.hidden=NO;

    }
    else
    {
         [SVProgressHUD dismiss];
        lbl_status.hidden=NO;
//        UIAlertView *alert=[[UIAlertView alloc]initWithTitle:@"" message:@"Here no contact list!" delegate:nil cancelButtonTitle:@"OK" otherButtonTitles:nil];
//        [alert show];
    }
 [SVProgressHUD dismiss];
}

#pragma mark
#pragma mark-Tableview  delegate method
- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    return 1;
}
- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    return [arr_contactList count];
}

// Row display. Implementers should *always* try to reuse cells by setting each cell's reuseIdentifier and querying for available reusable cells with dequeueReusableCellWithIdentifier:
// Cell gets various attributes set automatically based on table (separators) and data source (accessory views, editing controls)

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
   static NSString *cellIdentifier=@"cell_msgchat";
    
    MessageCellVC *cell=(MessageCellVC *)[tableView dequeueReusableCellWithIdentifier:cellIdentifier];
    if (cell==nil) {
        
        cell=[[MessageCellVC alloc]initWithStyle:UITableViewCellStyleDefault reuseIdentifier:cellIdentifier];
    }
    
    cell.lbl_UserEmail.text=[[arr_contactList objectAtIndex:indexPath.row]objectForKey:@"email"];
    cell.lbl_UserName.text=[[arr_contactList objectAtIndex:indexPath.row]objectForKey:@"name"];
    cell.lbl_UserNumber.text=[[arr_contactList objectAtIndex:indexPath.row]objectForKey:@"phone"];
    if ([[[arr_contactList objectAtIndex:indexPath.row]objectForKey:@"phone"]isEqualToString:@""])
    {
        
    }
    else{
        NSString *profileImg=[[arr_contactList objectAtIndex:indexPath.row]objectForKey:@"profile_image"];
        NSURL *url=[NSURL URLWithString:[NSString stringWithFormat:@"%@%@",serviceLink,profileImg]];
        if (profileImg.length<1)
        {
            cell.img_User.image=[UIImage imageNamed:@"user.png"];
        }
        else
        {
            dispatch_async(dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_BACKGROUND, 0), ^{
                NSData * imageData = [NSData dataWithContentsOfURL:url];
                UIImage *image = [UIImage imageWithData:imageData];
                
                dispatch_sync(dispatch_get_main_queue(), ^{ //in main thread update the image
                    cell.img_User.image = image;
                    cell.textLabel.text = @""; //add this update will reflect the changes
                });
            });
        }
    }


    return cell;
}
-(void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    NSLog(@"%ld",(long)indexPath.row);
    
    
    UIActionSheet *actionSheet=[[UIActionSheet alloc]initWithTitle:@"Send" delegate:self cancelButtonTitle:@"Cancel" destructiveButtonTitle:nil otherButtonTitles:@"Message",@"Email",nil];
    
    [actionSheet showInView:self.view];
    
    dict_Contact=[arr_contactList objectAtIndex:indexPath.row];
}

- (void)actionSheet:(UIActionSheet *)actionSheet clickedButtonAtIndex:(NSInteger)buttonIndex
{
    switch (buttonIndex) {
        case 0:
            [self SendMSg];
            break;
        case 1:
            [self sendMail];
            break;
            
        default:
            break;
    }
}
-(void)SendMSg
{
    if(![MFMessageComposeViewController canSendText]) {
        UIAlertView *warningAlert = [[UIAlertView alloc] initWithTitle:@"Error" message:@"Your device doesn't support SMS!" delegate:nil cancelButtonTitle:@"OK" otherButtonTitles:nil];
        [warningAlert show];
        return;
    }
    
    NSArray *recipents = [NSArray arrayWithObjects:[dict_Contact objectForKey:@"phone"], nil];
    NSString *message =@"";
    
    MFMessageComposeViewController *messageController = [[MFMessageComposeViewController alloc] init];
    messageController.messageComposeDelegate = self;
    [messageController setRecipients:recipents];
    [messageController setBody:message];
    
    // Present message view controller on screen
    [self presentViewController:messageController animated:YES completion:nil];}



- (void)messageComposeViewController:(MFMessageComposeViewController *)controller didFinishWithResult:(MessageComposeResult) result
{
    switch (result) {
        case MessageComposeResultCancelled:
            break;
            
        case MessageComposeResultFailed:
        {
            UIAlertView *warningAlert = [[UIAlertView alloc] initWithTitle:@"Error" message:@"Failed to send SMS!" delegate:nil cancelButtonTitle:@"OK" otherButtonTitles:nil];
            [warningAlert show];
            break;
        }
            
        case MessageComposeResultSent:
        {
            UIAlertView *warningAlert = [[UIAlertView alloc] initWithTitle:@"" message:@"Message has been send." delegate:nil cancelButtonTitle:@"OK" otherButtonTitles:nil];
            [warningAlert show];
            break;
        }
            
        default:
            break;
    }
    
    [self dismissViewControllerAnimated:YES completion:nil];
}




-(void)sendMail
{
    if (![MFMailComposeViewController canSendMail])
    {
        UIAlertView *warningAlert = [[UIAlertView alloc] initWithTitle:@"NO mail account!" message:@"Please set up a Mail account from Mail account setting  to order to send a mail." delegate:nil cancelButtonTitle:@"OK" otherButtonTitles:nil];
        [warningAlert show];
        return;
    }
    else
    {
        
        // Email Subject
       // NSString *emailTitle = @"Test Email";
        // Email Content
       // NSString *messageBody = @"<h1>Learning iOS Programming!</h1>"; // Change the message body to HTML
        // To address
        
        
        NSArray *toRecipents = [NSArray arrayWithObject:[dict_Contact objectForKey:@"email"]];
        
        MFMailComposeViewController *mc = [[MFMailComposeViewController alloc] init];
        mc.mailComposeDelegate = self;
        [mc setSubject:@""];
        [mc setMessageBody:@"" isHTML:NO];
        [mc setToRecipients:toRecipents];
        
        [self presentViewController:mc animated:YES completion:NULL];
       

    }
}

- (void)mailComposeController:(MFMailComposeViewController *)controller didFinishWithResult:(MFMailComposeResult)result error:(NSError *)error
{
  
    switch (result) {
        case MFMailComposeResultSent:
        {
            UIAlertView *warningAlert = [[UIAlertView alloc] initWithTitle:@"" message:@"Email has been send." delegate:nil cancelButtonTitle:@"OK" otherButtonTitles:nil];
            [warningAlert show];
            NSLog(@"You sent the email.");
            break;
        }
        case MFMailComposeResultSaved:
            NSLog(@"You saved a draft of this email");
            
            break;
        case MFMailComposeResultCancelled:
            NSLog(@"You cancelled sending this email.");
            break;
        case MFMailComposeResultFailed:
            NSLog(@"Mail failed:  An error occurred when trying to compose this email");
            break;
        default:
            NSLog(@"An error occurred when trying to compose this email");
            break;
    }
    
    [self dismissViewControllerAnimated:YES completion:NULL];

}
/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
