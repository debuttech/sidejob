//
//  PaymentRecievedCell.m
//  WorkerEarning
//
//  Created by Debut Mac 4 on 18/08/15.
//  Copyright (c) 2015 debut.infotech. All rights reserved.
//

#import "PaymentRecievedCell.h"

@implementation PaymentRecievedCell


- (void)awakeFromNib
{

    // Initialization code
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated
{
    [super setSelected:selected animated:animated];
    _img_Category.layer.masksToBounds=YES;
    _img_Category.layer.cornerRadius=_img_Category.frame.size.width/2;
    _lbl_amout.layer.masksToBounds=YES;
    _lbl_amout.layer.cornerRadius=_lbl_amout.frame.size.width/2;
    _view_Background.layer.borderWidth=0.3f;
    _view_Background.layer.borderColor=[[UIColor grayColor]CGColor];


    // Configure the view for the selected state
}

@end
