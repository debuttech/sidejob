//
//  AccountDetailsViewController.m
//  sidejobs
//
//  Created by DebutMac4 on 29/03/16.
//  Copyright © 2016 DebutMac4. All rights reserved.
//

#import "AccountDetailsViewController.h"
#import "SubmitAccountDetailsViewController.h"
#import "WebService.h"
#import "AppDelegate.h"
#import "Helper.h"
#import "Carddetailcell.h"
@interface AccountDetailsViewController ()
{
     NSUserDefaults *defaults;
    AppDelegate *delegate;
     NSMutableDictionary *accountDetialDict;
    NSMutableArray *dictResponse;
     NSInteger activeAccountIndex;
    NSMutableDictionary *cardDetialDict;
    NSInteger cardNumber;
    BOOL changeColorFlag;
}
@end

@implementation AccountDetailsViewController

- (void)viewDidLoad
{
    [super viewDidLoad];
   
    delegate=(AppDelegate*)[[UIApplication sharedApplication]delegate];
    defaults=[NSUserDefaults standardUserDefaults];
    
    self.navigationItem.hidesBackButton=YES;
    self.navigationItem.title=@"Account Details";
    UIButton *backButton=[UIButton buttonWithType:UIButtonTypeCustom];
    [backButton setFrame:CGRectMake(0, 0, 23, 21)];
    [backButton setImage:[UIImage imageNamed:@"arrow.png"] forState:UIControlStateNormal];
    [backButton addTarget:self action:@selector(leftBarButton:) forControlEvents:UIControlEventTouchUpInside];
    self.navigationItem.leftBarButtonItem=[[UIBarButtonItem alloc]initWithCustomView:backButton];

}
-(void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:YES];
    [SVProgressHUD showWithStatus:@"Loading..."];
    [self performSelector:@selector(merchantAccountList) withObject:nil afterDelay:0.3f];
}



-(void)merchantAccountList
{
    if ([Helper isConnectedToInternet])
    {
        
        changeColorFlag = NO;
        NSMutableDictionary *jsonDict=[[NSMutableDictionary alloc]init];
        
        [jsonDict setValue:[defaults objectForKey:@"merchant_id"] forKey:@"id"];
        [jsonDict setObject:[defaults objectForKey:@"user_id"]forKey:@"id"];
        NSString *url=[NSString stringWithFormat:@"%@merchantAccountList", serviceLink];
        
        id  json = [WebService signup:jsonDict:url];
        //    NSLog(@"Account Detail %@",json);
        //    accountDetialDict=[NSMutableDictionary new];
        
        
        
        if ([[json objectForKey:@"message"]isEqualToString:@"Success"])
        {
            [SVProgressHUD dismiss];
            dictResponse = [NSMutableArray new];//= [json objectForKey:@"response"];
            //        accountDetialDict = [NSMutableDictionary new];
            dictResponse = [json objectForKey:@"response"];
            NSLog(@"Account Detail:%@",dictResponse);
            
        }
        else
        {
            [SVProgressHUD dismiss];
            UIAlertView *alert=[[UIAlertView alloc]initWithTitle:@"" message:@"Some error is occured to fetching account details" delegate:nil cancelButtonTitle:@"OK" otherButtonTitles: nil];
            [alert show];
        }
        
        if ([dictResponse count]==0)
        {
            label_NoAccountDetail.hidden=NO;
        }
        else
        {
            label_NoAccountDetail.hidden=YES;
            
        }
        [tableView_AccountDetailList reloadData];
    }
    
    else
    {
        UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"No Internet" message:@"Please check your internet connection." delegate:nil cancelButtonTitle:@"OK" otherButtonTitles:nil];
        [alert show];
    }

    
}


-(void)leftBarButton:(UIButton*)sender
{
    [self.navigationController popViewControllerAnimated:YES];
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}


- (IBAction)buttonTapped_AddAccountDetail:(UIButton *)sender
{
    SubmitAccountDetailsViewController *submitAccountDetails = [self.storyboard instantiateViewControllerWithIdentifier:@"submitAccountDetailsViewController"];
    [self.navigationController pushViewController:submitAccountDetails animated:YES];

}

#pragma mark - Table view data source and delegate

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
    
    //return cardDetails.count;
    return 1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    return [dictResponse count];
}
/*(
 {
 accdetail =     {
 funding =         {
 accountNumberLast4 = 8321;
 descriptor = "Jane Doe";
 destination = bank;
 email = "<null>";
 mobilePhone = "<null>";
 routingNumber = 071101307;
 };
 individual =         {
 address =             {
 locality = Chocago;
 postalCode = 60622;
 region = IL;
 streetAddress = "111 main St";
 };
 dateOfBirth = "1981-11-19";
 email = "jane@14laddres.com";
 firstName = Jane;
 lastName = Doe;
 phone = 5555555555;
 ssnLast4 = 4567;
 };
 };
 "created_at" = "2016-04-04 10:40:13";
 default = 1;
 id = 1;
 "merchant_id" = "jane_doe_instant_qpd3xvvk";
 "updated_at" = "2016-04-04 10:40:13";
 "user_id" = 115;
 },
 {
 accdetail =     {
 funding =         {
 accountNumberLast4 = 1321;
 descriptor = "Jane Doe";
 destination = bank;
 email = "<null>";
 mobilePhone = "<null>";
 routingNumber = 071101307;
 };
 individual =         {
 address =             {
 locality = Chicago;
 postalCode = 60622;
 region = IL;
 streetAddress = "111 Main St";
 };
 dateOfBirth = "1981-11-19";
 email = "jane@14ladders.com";
 firstName = Jane;
 lastName = Doe;
 phone = 5555555555;
 ssnLast4 = 4567;
 };
 };
 "created_at" = "2016-04-04 10:40:22";
 default = 1;
 id = 2;
 "merchant_id" = "jane_doe_instant_m866h692";
 "updated_at" = "2016-04-04 10:40:22";
 "user_id" = 115;
 }
 )
*/
- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    
    static NSString *customIdentifierListingWorkers = @"carddetailcell";
    Carddetailcell *cell = [tableView dequeueReusableCellWithIdentifier:customIdentifierListingWorkers];
    if (cell == nil)
    {
        cell = [[Carddetailcell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:customIdentifierListingWorkers];
    }
    NSString *firstname = [[[[dictResponse objectAtIndex:indexPath.row]valueForKey:@"accdetail"] valueForKey:@"individual"]valueForKey:@"firstName"] ;
    cell.label_accountNameWorker.text = firstname;

    NSString *str_fourDigit = [[[[dictResponse objectAtIndex:indexPath.row]  valueForKey:@"accdetail"] valueForKey:@"funding"]valueForKey:@"accountNumberLast4"];
    NSString *str_lastDigit=[NSString stringWithFormat:@"xxxx-xxxx-xxxx-%@",str_fourDigit];
    cell.carddetaill_lbl.text=str_lastDigit;
    cell.carddetail_view.layer.cornerRadius = 5;
    cell.cancelbtn.tag=indexPath.row;
    
    [cell.cancelbtn addTarget:self action:@selector(DeleteCard:) forControlEvents:UIControlEventTouchUpInside];

    
    // changeColorFlag for change color of selected account
    if (!changeColorFlag)
    {
        if ([[[dictResponse objectAtIndex:indexPath.row] valueForKey:@"default"] isEqualToString:@"1"])
        {
            cell.carddetail_view.backgroundColor=[UIColor orangeColor];
        }
        else
        {
            
            //    [cell.cancelbtn addTarget:self action:@selector(DeleteCard:) forControlEvents:UIControlEventTouchUpInside];
            cell.carddetail_view.backgroundColor=[UIColor grayColor];
        }
    }
    else
    {
        if (activeAccountIndex==indexPath.row)
        {
            cell.carddetail_view.backgroundColor=[UIColor orangeColor];
        }
        
    
        else
        {
            cell.carddetail_view.backgroundColor=[UIColor grayColor];
        }
    }

    return cell;
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    changeColorFlag = YES;
    [tableView_AccountDetailList reloadData];
    Carddetailcell *cell = [tableView cellForRowAtIndexPath:indexPath];
    cell.carddetail_view.backgroundColor=[UIColor orangeColor];
    activeAccountIndex=indexPath.row;
    // defaults setObject:<#(nullable id)#> forKey:<#(nonnull NSString *)#>
//    NSLog(@"%ld",(long)activeAccountIndex);

    NSString *merchantID = [[dictResponse objectAtIndex:indexPath.row]valueForKey:@"id"];
    NSString *userId = [[dictResponse objectAtIndex:indexPath.row]valueForKey:@"user_id"];
    [tableView reloadData];
        UIView *bgColorView = [[UIView alloc] init];
        cell.carddetail_view.backgroundColor=[UIColor orangeColor];
        [cell setSelectedBackgroundView:bgColorView];
        [tableView deselectRowAtIndexPath:indexPath animated:YES];
//        NSString *cellText = cell.textLabel.text;
    
    dispatch_async(dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_DEFAULT, 0), ^{
        //Do background work
        [self defaultMerchantAccountList:merchantID userId:userId];
    });
    
}

-(void)defaultMerchantAccountList:(NSString *)merchantId userId:(NSString *)user
{

    NSMutableDictionary *jsonDict=[[NSMutableDictionary alloc]init];
    [jsonDict setValue:merchantId forKey:@"id"];
    [jsonDict setObject:user forKey:@"user_id"];

    NSString *url=[NSString stringWithFormat:@"%@defaultMerchantAccount", serviceLink];
    
    id  json = [WebService signup:jsonDict:url];
    NSLog(@"Account Detail %@",json);
    //    accountDetialDict=[NSMutableDictionary new];
    
    
    
    if ([[json objectForKey:@"message"]isEqualToString:@"Success"])
    {
       // [self merchantAccountList];
    }
    else
    {
        UIAlertView *alert=[[UIAlertView alloc]initWithTitle:@"" message:@"Some error is occured to activate your account." delegate:nil cancelButtonTitle:@"OK" otherButtonTitles: nil];
        [alert show];
    }
    
}

#pragma mark
#pragma mark-delete Card Details
-(IBAction)DeleteCard:(UIButton*)sender
{
   
    UIAlertView *alert=[[UIAlertView alloc]initWithTitle:@"" message:@"Are you sure you want to delete this card?" delegate:self cancelButtonTitle:@"OK" otherButtonTitles:@"Cancel",nil];
    alert.tag = sender.tag;
    [alert show];
    
}
- (void)alertView:(UIAlertView *)alertView clickedButtonAtIndex:(NSInteger)buttonIndex
{
    
    switch (buttonIndex) {
        case 0:
        {
            NSString *merchantID = [[dictResponse objectAtIndex:alertView.tag]valueForKey:@"id"];
            NSString *userId = [[dictResponse objectAtIndex:alertView.tag]valueForKey:@"user_id"];
            NSMutableDictionary *infoDect=[[NSMutableDictionary alloc]init];
            [infoDect setValue:merchantID forKey:@"id"];
            [infoDect setValue:userId forKey:@"user_id"];
            
//            NSLog(@"id = %@ , user id = %@",merchantID,userId);
            [SVProgressHUD showWithMaskType:SVProgressHUDMaskTypeClear];
            [self performSelector:@selector(deletecard:) withObject:infoDect afterDelay:0.3f];
            break;
        }
        case 1:
            break;
            
        default:
            break;
    }
}

-(void)deletecard:(NSMutableDictionary *) infoDict
{
//    NSMutableDictionary *jsonDict=[[NSMutableDictionary alloc]init];
//    [jsonDict setValue:cardToken forKey:@"id"];
    NSString *url=[NSString stringWithFormat:@"%@deleteMerchantAccount",serviceLink];
    id  jsonDelete = [WebService signup:infoDict:url];
//    NSLog(@"Delete Account %@", jsonDelete);
    if ([[jsonDelete objectForKey:@"message"]isEqualToString:@"Success"])
    {
        [self performSelector:@selector(merchantAccountList) withObject:nil afterDelay:0.3f];
    }
    else
    {
        [SVProgressHUD dismiss];
        UIAlertView *alert=[[UIAlertView alloc]initWithTitle:@"" message:@"Error to delete this card!" delegate:nil cancelButtonTitle:@"OK" otherButtonTitles:nil];
        [alert show];
    }
    
}

@end
