//
//  CategoryViewController.m
//  sideJobs
//
//  Created by DebutMac4 on 17/08/15.
//  Copyright (c) 2015 DebutMac4. All rights reserved.
//

#import "CategoryViewController.h"
#import "SearchJobsViewController.h"

@interface CategoryViewController ()

@end

@implementation CategoryViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    self.navigationController.navigationBarHidden=NO;
    self.navigationItem.hidesBackButton = YES;
    
    // Do any additional setup after loading the view.
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

- (IBAction)btnSearchCategoryAction:(id)sender {
    SearchJobsViewController *searchJobs=[self.storyboard instantiateViewControllerWithIdentifier:@"SearchJobsViewController"];
    [self.navigationController pushViewController:searchJobs animated:YES];
}
@end
