//
//  SideJobsWorkersCell.h
//  WorkerEarning
//
//  Created by Debut Mac 4 on 19/08/15.
//  Copyright (c) 2015 debut.infotech. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface SideJobsWorkersSideCell : UITableViewCell
{
    
}
@property (strong, nonatomic) IBOutlet UILabel *lbl_HeadingName;
@property (strong, nonatomic) IBOutlet UILabel *lbl_CustomerName;
@property (strong, nonatomic) IBOutlet UILabel *lbl_CategoryOfWorker;
@property (strong, nonatomic) IBOutlet UILabel *lbl_PostedDateOfWorker;
@property (strong, nonatomic) IBOutlet UILabel *lbl_AppliedDateOfWorker;
@property (strong, nonatomic) IBOutlet UIImageView *image_OfCustomer;
@property (strong, nonatomic) IBOutlet UIView *view_AppliedJob;
@property (strong, nonatomic) IBOutlet UIButton *btn_amount;
@property (strong, nonatomic) IBOutlet UILabel *lbl_status;
@property (strong, nonatomic) IBOutlet UILabel *lbl_Location;

@end
