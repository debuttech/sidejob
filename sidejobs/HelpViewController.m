//
//  ContactUsViewController.m
//  sidejobs
//
//  Created by DebutMac4 on 03/11/15.
//  Copyright © 2015 DebutMac4. All rights reserved.
//

#import "HelpViewController.h"
#import "HelpViewTC.h"

#import "HowSideJobsWorkVCViewController.h"
#import "ReportAnIssueViewController.h"
#import "BeacomeServiceRepViewController.h"
#import "ContactViewController.h"
#import "TermConditionsViewController.h"

@interface HelpViewController ()
{
    UIActivityIndicatorView*  activity;
    BOOL loadFailedBool;
}

@end

@implementation HelpViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    
   NSUserDefaults* defaults =[NSUserDefaults standardUserDefaults];
    NSString *status=[defaults valueForKey:@"user_status"];
    if ([status isEqualToString:@"customer"])
    {
        array_HelpList = [[NSArray alloc]initWithObjects:@"How SideJob works",@"Report an issue",@"Become a Service Reps",@"Terms and Conditions",@"Contact Us",nil];
    }else if([status isEqualToString:@"worker"])
    {
       array_HelpList = [[NSArray alloc]initWithObjects:@"How SideJob works",@"Report an issue",@"Terms and Conditions",@"Contact Us",nil];
    }else{
    
   // array_HelpList = [[NSArray alloc]initWithObjects:@"How SideJob works",@"Report an issue",@"Become a Service Reps",@"Terms and Conditions",@"Contact Us",nil];
    }
    
    activity = [[UIActivityIndicatorView alloc] initWithFrame:CGRectMake(0.0f, 0.0f, 32.0f, 32.0f)];
    [activity setCenter:CGPointMake(160.0f, 208.0f)];
    [activity setActivityIndicatorViewStyle:UIActivityIndicatorViewStyleGray];
     activity.center=self.view.center;
    [self.view addSubview:activity];
    
//    [activity startAnimating];
//    [self api];
    
    [self.navigationController.navigationBar setBackgroundImage:[UIImage imageNamed:@"top_bar2.png"] forBarMetrics:UIBarMetricsDefault];
    self.navigationItem.backBarButtonItem = [[UIBarButtonItem alloc] initWithTitle:@"" style:self.navigationItem.backBarButtonItem.style target:nil action:nil];
    
    NSDictionary *size = [NSDictionary dictionaryWithObjectsAndKeys:[UIFont fontWithName:@"HelveticaNeue-Light" size:20.0],NSFontAttributeName, [UIColor whiteColor],NSForegroundColorAttributeName, nil];
    self.navigationController.navigationBar.titleTextAttributes = size;
    self.navigationItem.title=@"Help";
    
    
    UIButton *rightButton=[UIButton buttonWithType:UIButtonTypeCustom];
    [rightButton setFrame:CGRectMake(0, 0, 23, 23)];
   // [rightButton setImage:[UIImage imageNamed:@"icon.png"] forState:UIControlStateNormal];
    //rightButton.userInteractionEnabled = false;
    [rightButton setImage:[UIImage imageNamed:@"arrow1.png"] forState:UIControlStateNormal];
    [rightButton addTarget:self action:@selector(DashBarButton:) forControlEvents:UIControlEventTouchUpInside];
    self.navigationItem.rightBarButtonItem=[[UIBarButtonItem alloc]initWithCustomView:rightButton];
 
}
-(void)DashBarButton:(id)sender
{
    
    UIWindow *wnd = [[[UIApplication sharedApplication] delegate] window];
    
    
    [UIView transitionWithView:wnd
                      duration:0.5
                       options:UIViewAnimationOptionTransitionFlipFromLeft
                    animations:^{
                        
                        UIViewController *root = [[UIStoryboard storyboardWithName:@"Main" bundle:NULL] instantiateViewControllerWithIdentifier:@"rootController"];
                        [wnd makeKeyAndVisible];
                        wnd.rootViewController = root;
                    }
                    completion:nil];
    
}

-(void)api
{
    NSString *fullURL = @"http://sidejobapp.co";
    NSURL *url = [NSURL URLWithString:fullURL];
    NSURLRequest *requestObj = [NSURLRequest requestWithURL:url];
    [webView loadRequest:requestObj];
   }
- (void)webViewDidStartLoad:(UIWebView *)webView
{
    loadFailedBool = NO;
}
- (void)webView:(UIWebView *)webView didFailLoadWithError:(NSError *)error
{
    loadFailedBool = YES;
}
- (void)webViewDidFinishLoad:(UIWebView *)webView
{
    
    if (!loadFailedBool)
        [activity performSelector:@selector(stopAnimating) withObject:nil afterDelay:0.5];

}

#pragma mark - TableView delegate Method

-(NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    return  1;
    
}
-(NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    return  array_HelpList.count;
}
-(UITableViewCell*)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
   static NSString *Identifier = @"cell_Help";
    HelpViewTC *cell = [tableView dequeueReusableCellWithIdentifier:Identifier];
    
    if (cell == nil)
    {
        cell = [[HelpViewTC alloc]initWithStyle:UITableViewCellStyleDefault reuseIdentifier:Identifier];
    }
    cell.lbl.text = [array_HelpList objectAtIndex:indexPath.row];
    return  cell;
    
}

-(void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    
    [self callViewController:indexPath.row];
}

-(void)callViewController:(NSInteger)index
{
    
    NSUserDefaults* defaults =[NSUserDefaults standardUserDefaults];
    NSString *status=[defaults valueForKey:@"user_status"];
    if ([status isEqualToString:@"customer"])
    {
        if (index == 0)
        {
            HowSideJobsWorkVCViewController *Obj = [self.storyboard instantiateViewControllerWithIdentifier:@"HowSideJobsWorkVCViewController"];
            [self.navigationController pushViewController:Obj animated:true];
        }
        else if (index == 1)
        {
            ReportAnIssueViewController *Obj = [self.storyboard instantiateViewControllerWithIdentifier:@"ReportAnIssueViewController"];
            [self.navigationController pushViewController:Obj animated:true];
        }
        else if (index == 2)
        {
            BeacomeServiceRepViewController *Obj = [self.storyboard instantiateViewControllerWithIdentifier:@"BeacomeServiceRepViewController"];
            [self.navigationController pushViewController:Obj animated:true];
            
        }
        else if(index == 3)
        {
            TermConditionsViewController *Obj = [self.storyboard instantiateViewControllerWithIdentifier:@"TermConditionsViewController"];
            [self.navigationController pushViewController:Obj animated:true];
            
        }
        else if (index == 4)
        {
            ContactViewController  *Obj = [self.storyboard instantiateViewControllerWithIdentifier:@"ContactViewController"];
            [self.navigationController pushViewController:Obj animated:true];
            
        }

    }else if([status isEqualToString:@"worker"])
    {
        if (index == 0)
        {
            HowSideJobsWorkVCViewController *Obj = [self.storyboard instantiateViewControllerWithIdentifier:@"HowSideJobsWorkVCViewController"];
            [self.navigationController pushViewController:Obj animated:true];
        }
        else if (index == 1)
        {
            ReportAnIssueViewController *Obj = [self.storyboard instantiateViewControllerWithIdentifier:@"ReportAnIssueViewController"];
            [self.navigationController pushViewController:Obj animated:true];
        }
        
        else if(index == 2)
        {
            TermConditionsViewController *Obj = [self.storyboard instantiateViewControllerWithIdentifier:@"TermConditionsViewController"];
            [self.navigationController pushViewController:Obj animated:true];
            
        }
        else if (index == 3)
        {
            ContactViewController  *Obj = [self.storyboard instantiateViewControllerWithIdentifier:@"ContactViewController"];
            [self.navigationController pushViewController:Obj animated:true];
            
        }

    }
    
    
    
}


- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
