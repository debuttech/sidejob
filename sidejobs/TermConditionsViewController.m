//
//  TermConditionsViewController.m
//  sidejobs
//
//  Created by DebutMac4 on 07/12/15.
//  Copyright © 2015 DebutMac4. All rights reserved.
//
#define login @"https://spottales.com/webservices/spotale_local/spotale.php"

#import "TermConditionsViewController.h"
#import <Helper.h>

@interface TermConditionsViewController ()
{
    UIActivityIndicatorView *indicator;
      BOOL loadFailedBool;
    NSString *strint_TC;
}

@end

@implementation TermConditionsViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    
    indicator = [[UIActivityIndicatorView alloc] initWithFrame:CGRectMake(0.0f, 0.0f, 32.0f, 32.0f)];
    [indicator setCenter:CGPointMake(160.0f, 208.0f)];
    [indicator setActivityIndicatorViewStyle:UIActivityIndicatorViewStyleGray];
    indicator.center=self.view.center;
    [self.view addSubview:indicator];
    
    //[indicator startAnimating];
    [SVProgressHUD show];
    [self performSelector:@selector(showTermCondition) withObject:nil afterDelay:0.3f];
    
  //  [self api];
    
    self.navigationController.navigationBarHidden=NO;
    [self.navigationController.navigationBar setBackgroundImage:[UIImage imageNamed:@"top_bar2.png"] forBarMetrics:UIBarMetricsDefault];
    
    self.navigationItem.hidesBackButton=YES;
    
    UIButton *button=[UIButton buttonWithType:UIButtonTypeCustom];
    [button setFrame:CGRectMake(0, 0, 23, 21)];
    [button addTarget:self action:@selector(backBarButtonItem:) forControlEvents:UIControlEventTouchUpInside];
    [button setImage:[UIImage imageNamed:@"arrow.png"] forState:UIControlStateNormal];
    self.navigationItem.leftBarButtonItem=[[UIBarButtonItem alloc]initWithCustomView:button];
    
    
    NSDictionary *size = [NSDictionary dictionaryWithObjectsAndKeys:[UIFont fontWithName:@"HelveticaNeue-Light" size:18.0],NSFontAttributeName, [UIColor whiteColor],NSForegroundColorAttributeName, nil];
    self.navigationController.navigationBar.titleTextAttributes = size;
    self.navigationItem.title=@"Terms&Conditions";
    
   // [self performSelector:@selector(showTermCondition) withObject:nil afterDelay:0.3f];

}

-(void)backBarButtonItem:(id)sender
{
    [self.navigationController popViewControllerAnimated:YES];
}
- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

-(void)showTermCondition
{
    id json;
    NSString *url=[NSString stringWithFormat:@"%@pagesContent",serviceLink];
    
    if ([Helper isConnectedToInternet])
    {
        json = [WebService GetwithoutDict:url];
        if ([[json objectForKey:@"message" ]isEqualToString:@"Success"])
        {
            NSArray *arr = [json objectForKey:@"response"];
            [SVProgressHUD dismiss];
            
            for (int i=0; i < arr.count; i++)
            {
                NSString *ID = [NSString stringWithFormat:@"%@",[[arr objectAtIndex:i]objectForKey:@"id"] ];
                if ([ID isEqualToString:@"4"])
                {
                    strint_TC = [[arr objectAtIndex:i] objectForKey:@"content"];
                    NSAttributedString *attributedString = [[NSAttributedString alloc] initWithData:[strint_TC dataUsingEncoding:NSUnicodeStringEncoding] options:@{ NSDocumentTypeDocumentAttribute: NSHTMLTextDocumentType } documentAttributes:nil error:nil];
                    textView_tC.attributedText = attributedString;
                }
            }
        
        }
       
        
    }
    else
    {
        UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"No Internet" message:@"Please check your internet connection." delegate:nil cancelButtonTitle:@"OK" otherButtonTitles:nil];
        [alert show];
        [SVProgressHUD dismiss];
        
    }
   

    [SVProgressHUD dismiss];
}

-(void)api
{
    NSString *fullURL = @"http://sidejobapp.co";
    NSURL *url = [NSURL URLWithString:fullURL];
    NSURLRequest *requestObj = [NSURLRequest requestWithURL:url];
    [webView loadRequest:requestObj];
}
- (void)webViewDidStartLoad:(UIWebView *)webView
{
    loadFailedBool = NO;
}
- (void)webView:(UIWebView *)webView didFailLoadWithError:(NSError *)error
{
    loadFailedBool = YES;
    [indicator performSelector:@selector(stopAnimating) withObject:nil afterDelay:0.5];
}
- (void)webViewDidFinishLoad:(UIWebView *)webView
{
    
    if (!loadFailedBool)
        [indicator performSelector:@selector(stopAnimating) withObject:nil afterDelay:0.5];
    
}


/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
