//
//  WorkerDashboardViewController.m
//  sideJobs
//
//  Created by DebutMac4 on 17/08/15.
//  Copyright (c) 2015 DebutMac4. All rights reserved.
//

#import "WorkerDashboardViewController.h"
#import "CategoryViewController.h"
#import "SearchJobsViewController.h"
#import "MySideJobsFromWorkerSides.h"
#import "PaymentViewViewController.h"
#import "MessageViewController.h"
#import "NotificationVCViewController.h"

@interface WorkerDashboardViewController ()
{
    NSUserDefaults *defaults;
    AppDelegate *delegate;
}

@end

@implementation WorkerDashboardViewController

- (void)viewDidLoad
{
    [super viewDidLoad];
    delegate = (AppDelegate*)[[UIApplication sharedApplication]delegate];
   
 
    self.navigationController.navigationBar.topItem.title = @"";
    NSDictionary *size = [NSDictionary dictionaryWithObjectsAndKeys:[UIFont fontWithName:@"HelveticaNeue-Light" size:20.0],NSFontAttributeName,[UIColor whiteColor],NSForegroundColorAttributeName, nil];
    self.navigationController.navigationBar.titleTextAttributes = size;
    
    
  /*  UIButton *noti_Bell=[UIButton buttonWithType:UIButtonTypeCustom];
    [noti_Bell setFrame:CGRectMake(0, 0, 20, 22)];
    [noti_Bell setImage:[UIImage imageNamed:@"notificationbelln"] forState:UIControlStateNormal];
    [noti_Bell addTarget:self action:@selector(notification:) forControlEvents:UIControlEventTouchUpInside];
    self.navigationItem.rightBarButtonItem=[[UIBarButtonItem alloc]initWithCustomView:noti_Bell];*/
    
    UIButton *noti_Bell=[UIButton buttonWithType:UIButtonTypeCustom];
    [noti_Bell setFrame:CGRectMake(0, 0, 26, 26)];
    [noti_Bell setImage:[UIImage imageNamed:@"notificationbelln"] forState:UIControlStateNormal];
    [noti_Bell addTarget:self action:@selector(notification:) forControlEvents:UIControlEventTouchUpInside];
    
    UILabel *lbl_Noti=[[UILabel alloc]initWithFrame:CGRectMake(12, 0, 16, 16)];
    lbl_Noti.layer.cornerRadius = lbl_Noti.frame.size.width/2;
    lbl_Noti.layer.masksToBounds = YES;
    lbl_Noti.font = [UIFont systemFontOfSize:10.0];
    lbl_Noti.textAlignment = NSTextAlignmentCenter;
    lbl_Noti.backgroundColor = [UIColor redColor];
    lbl_Noti.textColor = [UIColor whiteColor];
    lbl_Noti.text = [NSString stringWithFormat:@"%ld", (long)[[UIApplication sharedApplication] applicationIconBadgeNumber]];
    
    NSLog(@"%@",lbl_Noti.text);
    NSLog(@"%@",[NSString stringWithFormat:@"%ld", (long)[[UIApplication sharedApplication] applicationIconBadgeNumber]]);
    
     UIView *view_Noti = [[UIView alloc]initWithFrame:CGRectMake(0, 0, 26, 26)];
     [view_Noti addSubview:noti_Bell];
    
     if ([[UIApplication sharedApplication] applicationIconBadgeNumber] > 0)
     {

         [view_Noti addSubview:lbl_Noti];
     }
     else
     {
         
     }
    self.navigationItem.rightBarButtonItem=[[UIBarButtonItem alloc]initWithCustomView:view_Noti];
}

-(void)notification:(id)sender
{
    NotificationVCViewController *obj = [self.storyboard instantiateViewControllerWithIdentifier:@"NotificationVCViewController"];
//    [UIApplication sharedApplication].applicationIconBadgeNumber = 0;

    UINavigationController *nav = [[UINavigationController alloc]initWithRootViewController:obj];
    [self.navigationController presentViewController:nav animated:YES completion:nil];
    
}



- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

-(void)viewWillAppear:(BOOL)animated{
    [super viewWillAppear:YES];
    defaults=[NSUserDefaults standardUserDefaults];
    [defaults removeObjectForKey:@"pushjd1"];
    [defaults removeObjectForKey:@"pushjd"];
    [NSUserDefaults standardUserDefaults];
    [defaults synchronize];
    
}


- (IBAction)btnBack_Action:(id)sender
{
    [self.navigationController popViewControllerAnimated:YES];
}

- (IBAction)btn_searchJobs_Action:(id)sender
{
    SearchJobsViewController *searchJobs=[self.storyboard instantiateViewControllerWithIdentifier:@"SearchJobsViewController"];
    [self.navigationController pushViewController:searchJobs animated:YES];
}

- (IBAction)btnMyEarning:(id)sender
{
   // PaymentViewViewController *pv=[self.storyboard instantiateViewControllerWithIdentifier:@"secondViewController"];
    [self.sideMenuViewController setContentViewController:[[UINavigationController alloc] initWithRootViewController:[self.storyboard instantiateViewControllerWithIdentifier:@"secondViewController"]]
                                                 animated:YES];
   // [self presentViewController:pv animated:YES completion:NULL];
   // [self.navigationController pushViewController:pv animated:YES];
}

- (IBAction)btn_Message:(id)sender {
    MessageViewController *MVC=[self.storyboard instantiateViewControllerWithIdentifier:@"MessageViewController"];
    [self.navigationController pushViewController:MVC animated:YES];
}



- (IBAction)btn_SideJobs:(id)sender
{
    MySideJobsFromWorkerSides *sideView=[self.storyboard instantiateViewControllerWithIdentifier:@"MySideJobsFromWorkerSides"];
    [self.navigationController pushViewController:sideView animated:YES];
}
@end
