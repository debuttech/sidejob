//
//  AppDelegate.m
//  sideJobs
//
//  Created by DebutMac4 on 13/08/15.
//  Copyright (c) 2015 DebutMac4. All rights reserved.
//

#import "AppDelegate.h"
#import <Fabric/Fabric.h>
#import <Crashlytics/Crashlytics.h>
#import "DEMORootViewController.h"
#import "ViewController.h"
#import <Helper.h>
#import "TPKeyboardAvoidingScrollView.h"
#import <FBSDKCoreKit/FBSDKCoreKit.h>


@interface AppDelegate ()
{
    
}

@end

@implementation AppDelegate
@synthesize nav;


- (BOOL)application:(UIApplication *)application didFinishLaunchingWithOptions:(NSDictionary *)launchOptions {
    
      [Fabric with:@[CrashlyticsKit]];
    [[FBSDKApplicationDelegate sharedInstance] application:application
                             didFinishLaunchingWithOptions:launchOptions];
    
    
    NSUserDefaults *prefs = [NSUserDefaults standardUserDefaults];
    [NSUserDefaults resetStandardUserDefaults];
    [prefs synchronize];
    
    defaults=[NSUserDefaults standardUserDefaults];
//    [TPKeyboardAvoidingScrollView class];

   [[UIApplication sharedApplication] registerUserNotificationSettings:[UIUserNotificationSettings settingsForTypes:(UIUserNotificationTypeSound | UIUserNotificationTypeAlert | UIUserNotificationTypeBadge) categories:nil]];
    
    [UIApplication sharedApplication].applicationIconBadgeNumber=0;
//    application.applicationIconBadgeNumber=0;
    [[UIApplication sharedApplication] registerForRemoteNotifications];
    
   
    
    [self readNotification];
//    NSString *msg = [defaults objectForKey:@"noti"];
    
    
    
//    UIAlertView *alertView = [[UIAlertView alloc] initWithTitle:@"Push Notification" message:msg delegate:nil cancelButtonTitle:@"OK" otherButtonTitles:nil];
//    
//    [alertView show];

    _applicantCount=@"";
    
    [self loginStatus];
    return YES;
}


- (BOOL)application:(UIApplication *)application openURL:(NSURL *)url sourceApplication:(NSString *)sourceApplication annotation:(id)annotation {
    return [[FBSDKApplicationDelegate sharedInstance] application:application
                                                          openURL:url
                                                sourceApplication:sourceApplication
                                                       annotation:annotation
            ];
}
#pragma mark
#pragma login status
-(void)loginStatus
{
  //  NSUserDefaults *defaults=[NSUserDefaults standardUserDefaults];
    BOOL value=[defaults boolForKey:@"logined"];
    if (value)
    {
        UIViewController *root = [[UIStoryboard storyboardWithName:@"Main" bundle:NULL] instantiateViewControllerWithIdentifier:@"rootController"];
        UIWindow *wnd = [[[UIApplication sharedApplication] delegate] window];
        wnd.rootViewController = root;
        [wnd makeKeyAndVisible];
        
    }
    else
    {
        ViewController *root = [[UIStoryboard storyboardWithName:@"Main" bundle:NULL] instantiateViewControllerWithIdentifier:@"ViewController"];
        UINavigationController *navController = [[UINavigationController alloc] initWithRootViewController:root];
        UIWindow *wnd = [[[UIApplication sharedApplication] delegate] window];
        wnd.rootViewController = navController;
        [wnd makeKeyAndVisible];
    }
}

-(void)readNotification
{
//    defaults=[NSUserDefaults standardUserDefaults];
    
    NSMutableDictionary *jsonDict=[[NSMutableDictionary alloc]init];
    [jsonDict setValue:[defaults objectForKey:@"user_id"] forKey:@"user_id"];
   
   
    NSString *token=[NSString stringWithFormat:@"%@",[defaults objectForKey:@"user_token"]];
    NSString *url=[NSString stringWithFormat:@"%@markNotificationStatus?token=%@",serviceLink,token];
    
    if ([Helper isConnectedToInternet])
    {
        
        id  json = [WebService signup:jsonDict:url];
        
        NSLog(@"%@",json);
        if ([[json objectForKey:@"message"]isEqualToString:@"Success"])
        {

      }
        
    }
    
    else
    {
        
        
        UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"No Internet" message:@"Please check your internet connection." delegate:nil cancelButtonTitle:@"OK" otherButtonTitles:nil];
        [alert show];
        
    }
    
}

//- (BOOL)application: (UIApplication *)application
//            openURL: (NSURL *)url
//  sourceApplication: (NSString *)sourceApplication
//         annotation: (id)annotation
//{
//    return [FBAppCall handleOpenURL:url sourceApplication:sourceApplication];
//}

- (void)applicationWillResignActive:(UIApplication *)application {
    // Sent when the application is about to move from active to inactive state. This can occur for certain types of temporary interruptions (such as an incoming phone call or SMS message) or when the user quits the application and it begins the transition to the background state.
    // Use this method to pause ongoing tasks, disable timers, and throttle down OpenGL ES frame rates. Games should use this method to pause the game.
}

- (void)applicationDidEnterBackground:(UIApplication *)application {
    // Use this meth500od to release shared resources, save user data, invalidate timers, and store enough application state information to restore your application to its current state in case it is terminated later.
    // If your application supports background execution, this method is called instead of applicationWillTerminate: when the user quits.
}

- (void)applicationWillEnterForeground:(UIApplication *)application {
    // Called as part of the transition from the background to the inactive state; here you can undo many of the changes made on entering the background.
}

- (void)applicationDidBecomeActive:(UIApplication *)application {
    // Restart any tasks that were paused (or not yet started) while the application was inactive. If the application was previously in the background, optionally refresh the user interface.
}

- (void)applicationWillTerminate:(UIApplication *)application {
    // Called when the application is about to terminate. Save data if appropriate. See also applicationDidEnterBackground:.
}

- (void)application:(UIApplication*)application didRegisterForRemoteNotificationsWithDeviceToken:(NSData*)deviceToken
{
    NSLog(@"My token is: %@", deviceToken);
    NSString *str = [NSString stringWithFormat:@"%@",deviceToken];
    // //NSLog(@"My token is: %@", str);
    NSString *deviceToken1 = [str stringByTrimmingCharactersInSet:[NSCharacterSet characterSetWithCharactersInString:@"<>"]];
    deviceToken1 = [deviceToken1 stringByReplacingOccurrencesOfString:@" " withString:@""];
    
    NSLog(@"My token is: '%@'", deviceToken1);
    //    [CustomAlertHelper showAlert:@"eviceToken aa" withMessage:deviceToken1];
    
  //  NSUserDefaults *userdefaults=[NSUserDefaults standardUserDefaults];
    [defaults setObject:deviceToken1 forKey:@"device_token"];
    [defaults synchronize];
    UIPasteboard *pb = [UIPasteboard generalPasteboard];
    [pb setString:deviceToken1];
    NSMutableDictionary *currentUserData =[[NSMutableDictionary alloc] init];
    currentUserData = [defaults objectForKey:@"userInfo"];
    NSString *user_id=[currentUserData valueForKey:@"user_id"];
    
}

-(void)application:(UIApplication *)application didReceiveRemoteNotification:(NSDictionary *)userInfo fetchCompletionHandler:(void (^)(UIBackgroundFetchResult))completionHandler
{
    
    if(application.applicationState == UIApplicationStateActive)
    {
        NSLog(@"userInfo For Notification %@", userInfo);
        
        [UIApplication sharedApplication].applicationIconBadgeNumber = [[[userInfo objectForKey:@"aps"] objectForKey: @"badgecount"] intValue];
        
        [defaults setObject:[[userInfo objectForKey:@"aps"] objectForKey: @"badgecount" ] forKey:@"noti"];
        [defaults synchronize];

        //app is currently active, can update badges count here
        
    }else if(application.applicationState == UIApplicationStateBackground)
    {
        
        //app is in background, if content-available key of your notification is set to 1, poll to your backend to retrieve data and update your interface here
        
    }else if(application.applicationState == UIApplicationStateInactive)
    {
        
        //app is transitioning from background to foreground (user taps notification), do what you need when user taps here
        
    }
}
- (void)application:(UIApplication *)application didReceiveRemoteNotification:(NSDictionary *)userInfo
{
    NSLog(@"userInfo For Notification %@", userInfo);
    
    [UIApplication sharedApplication].applicationIconBadgeNumber = [[[userInfo objectForKey:@"aps"] objectForKey: @"badgecount"] intValue];
    
    [defaults setObject:[[userInfo objectForKey:@"aps"] objectForKey: @"badgecount" ] forKey:@"noti"];
    [defaults synchronize];
    
    
//    if (application.applicationState == UIApplicationStateActive) {
//        UIAlertView *alertView = [[UIAlertView alloc] initWithTitle:@"Push Notification" message:userInfo[@"aps"][@"alert"] delegate:nil cancelButtonTitle:@"OK" otherButtonTitles:nil];
//        
//        [alertView show];
//    }
//    else if (application.applicationState == UIApplicationStateBackground || application.applicationState == UIApplicationStateInactive)
//    { 
//        // Do something else rather than showing an alert view, because it won't be displayed.
//    }
//    
//   // NSInteger badge = [UIApplication sharedApplication].applicationIconBadgeNumber;
//    
//    //  NSInteger badge  =  [[userInfo objectForKey:@"badge"] integerValue];
//    
//   NSInteger badge = [UIApplication sharedApplication].applicationIconBadgeNumber = [[[userInfo objectForKey:@"aps"] objectForKey: @"badgecount"] intValue];
//    
//     _Noti_badgeNumber = [NSString stringWithFormat:@"%ld",(long)badge];

}

/**
 * Failed to Register for Remote Notifications
 */
- (void)application:(UIApplication *)application didFailToRegisterForRemoteNotificationsWithError:(NSError *)error {
#if !TARGET_IPHONE_SIMULATOR
    
    NSLog(@"Error in registration. Error: %@", error);
#endif
}

@end
