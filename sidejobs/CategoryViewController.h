//
//  CategoryViewController.h
//  sideJobs
//
//  Created by DebutMac4 on 17/08/15.
//  Copyright (c) 2015 DebutMac4. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface CategoryViewController : UIViewController
- (IBAction)btnSearchCategoryAction:(id)sender;

@end
