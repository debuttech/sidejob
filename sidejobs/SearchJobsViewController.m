//
//  SearchJobsViewController.m
//  sideJobs
//
//  Created by DebutMac4 on 17/08/15.
//  Copyright (c) 2015 DebutMac4. All rights reserved.
//

#import "SearchJobsViewController.h"
#import "WorkerSearchTableViewCell.h"
#import "AppDelegate.h"
#import <MapKit/MapKit.h>
#import <CoreImage/CoreImage.h>
#import <QuartzCore/QuartzCore.h>
#import "WebService.h"
#import "ApplyJobFromListJobViewController.h"
#import <CoreLocation/CoreLocation.h>
#import "MyInvitationCell.h"
#define METERS_PER_MILE 1609.344

@interface SearchJobsViewController ()
{
    NSUserDefaults *defaults;
    NSMutableArray *jobsArray,*fromSrchArr,*fromFiltrArr;
    NSString *categoryFilterStr;
    int i_img;
    CLLocation *currentLocation;
    NSMutableArray *arr_milesFilter;
    NSString *str_latitude,*str_longitude;
    BOOL removePin;
    NSString *str_selectRange;
    NSString *str_checkFilter;
    NSString *str_categoryId;
    NSMutableArray *jobFromMapPinClicked;
    NSMutableArray *arr_invitationJob;
    
}
@property (weak, nonatomic) IBOutlet UILabel *rangeLbl;
@property (weak, nonatomic) IBOutlet UISlider *sliderRange;

@end

@implementation SearchJobsViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    searchBar_object.layer.borderWidth = 0.3f;
    searchBar_object.layer.borderColor = [[UIColor orangeColor] CGColor];
    
    removePin=NO;
    i_img=0;
    categoryFilterStr=[[NSString alloc]init];
    str_latitude=[[NSString alloc]init];
    str_longitude=[[NSString alloc]init];
    str_selectRange=[[NSString alloc]init];
    
    defaults=[NSUserDefaults standardUserDefaults];
    locationManager = [[CLLocationManager alloc]init];
    
    locationManager.delegate = self;
    locationManager.distanceFilter = kCLDistanceFilterNone;
    locationManager.desiredAccuracy = kCLLocationAccuracyBest;
    if ([locationManager respondsToSelector:@selector(requestAlwaysAuthorization)]) {
        [locationManager requestAlwaysAuthorization];
        [locationManager startUpdatingLocation];
        //_mapview_object.showsUserLocation=true;
        _mapview_object.delegate=self;
        [_mapview_object setUserTrackingMode:MKUserTrackingModeFollow animated:YES];
    }
//    [SVProgressHUD showWithStatus:@"Loading"];
//    [self performSelector:@selector(FetchJobsList) withObject:nil afterDelay:0.3f];

    self.navigationItem.hidesBackButton=YES;
    
    
    UIButton *backButton=[UIButton buttonWithType:UIButtonTypeCustom];
    [backButton setFrame:CGRectMake(0, 0, 23, 21)];
    [backButton addTarget:self action:@selector(backButton:) forControlEvents:UIControlEventTouchUpInside];
    [backButton setImage:[UIImage imageNamed:@"arrow.png"] forState:UIControlStateNormal];
    self.navigationItem.leftBarButtonItem=[[UIBarButtonItem alloc]initWithCustomView:backButton];
    self.navigationItem.title=@"Search Jobs";
    
    [[UINavigationBar appearance] setShadowImage:[[UIImage alloc] init]];
    [_View_ListviewWorker_Object setFrame:CGRectMake(0, self.View_ListviewWorker_Object.frame.origin.y, self.View_ListviewWorker_Object.frame.size.width, self.View_ListviewWorker_Object.frame.size.height)];
    
    [_slider_custom setThumbImage: [UIImage imageNamed:@"range_btn1.png"] forState:UIControlStateNormal];
    _slider_custom.transform = CGAffineTransformScale(CGAffineTransformIdentity, 1.0, 2.0);
      _slider_custom.minimumValue = 0.0;
    _slider_custom.maximumValue = 50.0;
    _slider_custom.continuous = NO;
    
    _sliderRange.continuous = NO;
    _sliderRange.minimumValue = 0.0;
    _sliderRange.maximumValue = 50.0;


    
}
- (void) dismissKeyboard
{
    [searchBar_object resignFirstResponder];
}
-(void)viewWillDisappear:(BOOL)animated
{
    [super viewWillDisappear:YES];
    [searchBar_object resignFirstResponder];
    
    
}
-(void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:YES];
    [SVProgressHUD showWithStatus:@"Loading"];
    [self performSelector:@selector(getInvitationData) withObject:nil afterDelay:0.1f];

    [self performSelector:@selector(FetchJobsList) withObject:nil afterDelay:0.3f];
}
- (BOOL)searchBarShouldBeginEditing:(UISearchBar *)searchBar
{
    // [searchBar setShowsCancelButton:YES];
    UITextField *searchBarTextField = nil;
    for (UIView *subView in searchBar_object.subviews)
    {
        for (UIView *sndSubView in subView.subviews)
        {
            if ([sndSubView isKindOfClass:[UITextField class]])
            {
                searchBarTextField = (UITextField *)sndSubView;
                break;
            }
        }
    }
    searchBarTextField.enablesReturnKeyAutomatically = NO;
    return YES;
}
-(void)searchBarSearchButtonClicked:(UISearchBar *)searchBar
{
    [self.view endEditing:YES];
}

-(void)backButton:(id)sender
{
    [self.navigationController popViewControllerAnimated:YES];
}

#pragma mark
#pragma mark - Fetch jobs List
-(void)FetchJobsList
{
    NSString *userId=[NSString stringWithFormat:@"%@",[defaults objectForKey:@"user_id"]];
    NSMutableDictionary *dict=[[NSMutableDictionary alloc]init];
    [dict setValue:userId forKey:@"id"];
    
    NSString *token=[NSString stringWithFormat:@"%@",[defaults objectForKey:@"user_token"]];
    NSString *url=[NSString stringWithFormat:@"%@publish-jobs?token=%@",serviceLink,token];
    
    id jsons=[WebService signup:dict :url];
    NSLog(@"%@",[dict JSONRepresentation]);
    jobsArray= [[NSMutableArray alloc]init];
    if ([[jsons objectForKey:@"message"]isEqualToString:@"Success"])
    {
        
        
        if (arr_invitationJob.count)
        {
            jobsArray= [[NSMutableArray alloc]init];

            jobsArray = [[arr_invitationJob arrayByAddingObjectsFromArray:[jsons valueForKey:@"response"]] mutableCopy];
            NSLog(@"%@",jobsArray);
        }else{
            jobsArray= [[NSMutableArray alloc]init];

            jobsArray=[jsons valueForKey:@"response"];

        }
      
        NSArray *arrNotations=[self createAnnotations];
        [_mapview_object addAnnotations:arrNotations];
        //[self ZoomMapView];
        
        [SVProgressHUD dismiss];
        if (jobsArray.count)
        {
            lbl_NoJob.hidden=YES;
            table_listJob.dataSource=self;
            table_listJob.delegate=self;
            [table_listJob reloadData];
        }
        else
        {
            lbl_NoJob.hidden=NO;
        }
    }
    else if ([[jsons objectForKey:@"error"]isEqualToString:@"token_expired"])
    {
        [SVProgressHUD dismiss];
        
        UIAlertView *alert=[[UIAlertView alloc]initWithTitle:@"" message:@"Session Expired please login again" delegate:nil cancelButtonTitle:@"OK" otherButtonTitles: nil];
        [alert show];
        AppDelegate *delegate;
        delegate=(AppDelegate*)[[UIApplication sharedApplication]delegate];
        [defaults setBool:NO forKey:@"logined"];
        [defaults synchronize];
        [[NSUserDefaults standardUserDefaults]synchronize];
        [delegate loginStatus];
    }
    else
    {
        [SVProgressHUD dismiss];
    }
    [SVProgressHUD dismiss];
    
}


-(void)getInvitationData
{
    arr_invitationJob=[[NSMutableArray alloc]init];
    NSMutableDictionary *jsonDict=[[NSMutableDictionary alloc]init];
    NSString *token=[NSString stringWithFormat:@"%@",[defaults objectForKey:@"user_token"]];
    [jsonDict setValue:[defaults objectForKey:@"user_id"] forKey:@"id"];
    
    NSString *url=[NSString stringWithFormat:@"%@invites-list?token=%@",serviceLink,token];
    id  json = [WebService signup:jsonDict:url];
    if ([[json objectForKey:@"message"]isEqualToString:@"Success"])
    {
        [SVProgressHUD dismiss];
        arr_invitationJob=[json objectForKey:@"response"];
//        NSLog(@"%@",arr_invitationJob);
    }else{
        [SVProgressHUD dismiss];
    }
    if (arr_invitationJob.count)
    {
        [table_listJob reloadData];
    }
    else
    {
    }
}





#pragma mark
#pragma mark-Create Annotation
- (NSMutableArray *)createAnnotations
{
    NSMutableArray *arrMapPin;
    if ([str_checkFilter isEqualToString:@"done"])
    {
        arrMapPin=[NSMutableArray arrayWithArray:arr_milesFilter];
    }
    else
    {
        arrMapPin=[NSMutableArray arrayWithArray:jobsArray];
    }
    NSMutableArray *annotations = [[NSMutableArray alloc] init];
    
    [_mapview_object setDelegate:self];
    for (int i=0;i<[arrMapPin count];i++)
    {
        
        CLLocationCoordinate2D coord;
        coord.latitude = [[[arrMapPin objectAtIndex:i] valueForKey:@"latitude"]doubleValue];
        coord.longitude =[[[arrMapPin objectAtIndex:i] valueForKey:@"longitude"]doubleValue];
        
        NSString *str=[[arrMapPin objectAtIndex:i] objectForKey:@"location"];
        NSString *tagId=[[arrMapPin objectAtIndex:i] objectForKey:@"category_id"];
        NSString *strjobid=[[arrMapPin objectAtIndex:i]objectForKey:@"id"];
        
        NSMutableDictionary *imgDict=[[NSMutableDictionary alloc]init];
        [imgDict setObject:tagId forKey:@"img_id"];
        
        NSMutableDictionary *JobIdDict=[[NSMutableDictionary alloc]init];
        [JobIdDict setObject:strjobid forKey:@"id"];
        
        MapViewAnnotation *annotation1 = [[MapViewAnnotation alloc]initWithCoordinates:coord withtitle:str withTagValue:tagId image:imgDict jobId:JobIdDict];
        
        [annotations addObject:annotation1];
    }
    
    return annotations;
}
-(void)filterJobList
{
    NSString *radius;
    NSMutableDictionary *dict=[[NSMutableDictionary alloc]init];
    if ([lbl_MapRadius.text integerValue]>0.000)
    {
        radius=lbl_MapRadius.text;
        [dict setValue:radius forKey:@"radius"];
        [dict setValue:str_longitude forKey:@"longitude"];
        [dict setValue:str_latitude forKey:@"latitude"];
    }
    if (categoryFilterStr>0||categoryFilterStr !=nil)
    {
        [dict setValue:categoryFilterStr forKey:@"category_id"];
    }
    if (lbl_MapRadius.text==nil)
    {
        [self ZoomMapView];
    }
    // Filter from list view.......................................
    if ([str_selectRange integerValue]>0)
    {
        [dict setValue:str_selectRange forKey:@"radius"];
        [dict setValue:str_longitude forKey:@"longitude"];
        [dict setValue:str_latitude forKey:@"latitude"];
    }
    
    NSString *userId=[NSString stringWithFormat:@"%@",[defaults objectForKey:@"user_id"]];
    [dict setValue:userId forKey:@"id"];
    NSString *token=[NSString stringWithFormat:@"%@",[defaults objectForKey:@"user_token"]];
    NSString *url=[NSString stringWithFormat:@"%@publish-jobs?token=%@",serviceLink,token];
    
    id jsons=[WebService signup:dict :url];
    
    arr_milesFilter= [[NSMutableArray alloc]init];
    fromFiltrArr=[[NSMutableArray alloc] init];
    if ([[jsons objectForKey:@"message"]isEqualToString:@"Success"])
    {
        [SVProgressHUD dismiss];
        if ([str_checkFilter isEqualToString:@"popUp"])
        {
            fromFiltrArr=[jsons valueForKey:@"response"];
            [table_listJob reloadData];
            if (fromFiltrArr.count)
            {
                lbl_NoJob.hidden=YES;
                
            }
            else
            {
                lbl_NoJob.hidden=NO;
            }
        }
        if ([str_checkFilter isEqualToString:@"done"])
        {
            arr_milesFilter=[jsons valueForKey:@"response"];
            if (arr_milesFilter.count)
            {
                lbl_NoJob.hidden=YES;
            }
            else
            {
                // [lbl_NoJob setFrame:CGRectMake(lbl_NoJob.frame.origin.x, lbl_NoJob.frame.origin.y-45, lbl_NoJob.frame.size.width, lbl_NoJob.frame.size.height+45)];
                // lbl_NoJob.hidden=NO;
            }
            
            NSArray *existingpoints = _mapview_object.annotations;
            if ([existingpoints count])
                [_mapview_object removeAnnotations:existingpoints];
            
            NSArray *arrNotations=[self createAnnotations];
            [_mapview_object addAnnotations:arrNotations];
            //  [_mapview_object setUserTrackingMode:MKUserTrackingModeFollow animated:YES];
            [self ZoomMapView];
            
            
        }
    }
    else if ([[jsons objectForKey:@"error"]isEqualToString:@"token_expired"])
    {
        [SVProgressHUD dismiss];
        
        UIAlertView *alert=[[UIAlertView alloc]initWithTitle:@"" message:@"Session Expired please login again" delegate:nil cancelButtonTitle:@"OK" otherButtonTitles: nil];
        [alert show];
        AppDelegate *delegate;
        delegate=(AppDelegate*)[[UIApplication sharedApplication]delegate];
        [defaults setBool:NO forKey:@"logined"];
        [defaults synchronize];
        [[NSUserDefaults standardUserDefaults]synchronize];
        [delegate loginStatus];
    }
    else
    {
        [SVProgressHUD dismiss];
    }
    [SVProgressHUD dismiss];
}
-(void)ZoomMapView
{
    
    // double miles = 5.0;
    double miles=[lbl_MapRadius.text doubleValue];
    double scalingFactor = ABS( (cos(2 * M_PI * currentLocation.coordinate.latitude / 360.0) ));
    
    MKCoordinateSpan span;
    
    span.latitudeDelta = miles/69.0;
    span.longitudeDelta = miles/(scalingFactor * 69.0);
    
    MKCoordinateRegion region;
    region.span = span;
    region.center = currentLocation.coordinate;
    [_mapview_object setRegion:region animated:YES];
}
#pragma mark
#pragma searchbar delegate method
- (void)searchBar:(UISearchBar *)searchBar textDidChange:(NSString *)searchText;
{
    fromSrchArr=[[NSMutableArray alloc] init];
    
    if(searchText.length > 0)
    {
        for (int i=0; i<jobsArray.count; i++) {
            
            if ([[[jobsArray objectAtIndex:i] valueForKey:@"title"] rangeOfString:searchText options:NSCaseInsensitiveSearch].location != NSNotFound)
            {
                [fromSrchArr addObject:[jobsArray objectAtIndex:i]];
                
            }
        }
    }
    if (!fromSrchArr.count && searchBar.text.length)
    {
        lbl_NoJob.hidden=NO;
        [table_listJob setHidden:YES];
    }
    else if (fromSrchArr.count == 0 && searchBar.text.length == 0)
    {
        
        lbl_NoJob.hidden=YES;
        [table_listJob setHidden:NO];
    }
    else
    {
        [table_listJob setHidden:NO];
        lbl_NoJob.hidden=YES;
    }
    
    [table_listJob reloadData];
}
- (void)searchBarTextDidEndEditing:(UISearchBar *)searchBar
{
    if ([searchBar.text isEqualToString:@""] || !searchBar.text.length)
    {
        fromSrchArr=[[NSMutableArray alloc] init];
        
    }
    
    [table_listJob reloadData];
}
#pragma mark -
#pragma mark - View For annotation MKMapView delegate
- (MKAnnotationView *)mapView:(MKMapView *)mapView viewForAnnotation:(id <MKAnnotation>)annotation
{
    static NSString *identifier = @"MyLocation";

    if ([annotation isKindOfClass:[MKUserLocation class]])
        return nil;
    if ([annotation isKindOfClass:[MapViewAnnotation class]])
    {
        MKAnnotationView *annotationView = (MKAnnotationView *)[_mapview_object dequeueReusableAnnotationViewWithIdentifier:identifier];
        
        UIButton *advertButton = [UIButton buttonWithType:UIButtonTypeDetailDisclosure];
        annotationView = [[MKAnnotationView alloc] initWithAnnotation:annotation reuseIdentifier:identifier];
        annotationView.enabled = YES;
        annotationView.canShowCallout = YES;
        annotationView.rightCalloutAccessoryView=advertButton;
        annotationView.image=nil;
        
        NSMutableDictionary *imgDict=[(MapViewAnnotation *)annotationView.annotation image];
        NSMutableDictionary *JobDict=[(MapViewAnnotation *)annotationView.annotation job_Id];
        NSString *jobId=[JobDict valueForKey:@"id"];
        advertButton.tag=[jobId integerValue];
        [advertButton addTarget:self action:@selector(clickedOnMapPinCallout:) forControlEvents:UIControlEventTouchUpInside];
        
        NSString *img_id=[NSString stringWithFormat:@"%@",[imgDict valueForKey:@"img_id"]];
        
        if ([img_id isEqualToString:@"1"])
        {
            annotationView.image=[UIImage imageNamed:@"loc2h.png"];
        }
        if ([img_id isEqualToString:@"2"])
        {
            annotationView.image=[UIImage imageNamed:@"loc1a.png"];
        }
        if ([img_id isEqualToString:@"3"])
        {
            annotationView.image=[UIImage imageNamed:@"loce.png"];
        }
        // [self ZoomMapView];
        
        return annotationView;
    }
    return nil;
}
-(void)clickedOnMapPinCallout:(id)sender
{
    UIButton *button=(UIButton*)sender;
    NSInteger Jobtag=button.tag;
    NSString *jbtag=[NSString stringWithFormat:@"%ld",(long)Jobtag];
    jobFromMapPinClicked=[[NSMutableArray alloc]init];
    for (int i=0; i<jobsArray.count; i++)
    {
        NSString *idjob=[NSString stringWithFormat:@"%@",[[jobsArray objectAtIndex:i]objectForKey:@"id"]];
        
        if ([jbtag isEqualToString:idjob])
        {
            // jobFromMapPinClicked =[jobsArray objectAtIndex:i];
            [jobFromMapPinClicked addObject:[jobsArray objectAtIndex:i]];
        }
    }
    [_View_ListviewWorker_Object setFrame:CGRectMake(0, self.View_ListviewWorker_Object.frame.origin.y, self.View_ListviewWorker_Object.frame.size.width, self.View_ListviewWorker_Object.frame.size.height)];
    [table_listJob reloadData];
}
-(void)detailBtn:(id)sender
{
    NSLog(@"show locatio detail");
}
#pragma mark
#pragma mark - CLLocationManagerDelegate
#pragma mark CLLocationManager Delegate
- (void)locationManager:(CLLocationManager *)manager didUpdateLocations:(NSArray *)locations
{
    currentLocation= [locations objectAtIndex:0];
    [locationManager stopUpdatingLocation];
    [self->locationManager stopUpdatingLocation];
    NSLog(@"my latitude :%f",currentLocation.coordinate.latitude);
    NSLog(@"my longitude :%f",currentLocation.coordinate.longitude);
    str_latitude=[NSString stringWithFormat:@"%f",currentLocation.coordinate.latitude ];
    str_longitude=[NSString stringWithFormat:@"%f",currentLocation.coordinate.longitude ];
}
- (void)locationManager:(CLLocationManager *)manager didFailWithError:(NSError *)error
{
    str_longitude=@"";
    str_latitude=@"";
    NSLog(@"didFailWithError: %@", error);
    UIAlertView *errorAlert = [[UIAlertView alloc]
                               initWithTitle:@"Error" message:@"Failed to Get Your Location.Please allow locations>Settings" delegate:nil cancelButtonTitle:@"OK" otherButtonTitles:nil];
    [errorAlert show];
}
- (void)locationManager:(CLLocationManager *)manager didUpdateToLocation:(CLLocation *)newLocation fromLocation:(CLLocation *)oldLocation
{
    NSLog(@"didUpdateToLocation: %@", newLocation);
    CLLocation *currentLocation = newLocation;
    
    if (currentLocation != nil)
    {
        
    }
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
}

#pragma mark
#pragma mark- tableView delegate method
#pragma mark
#pragma mark- tableView delegate method
- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    return 1;
}
- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    if ([[[[[jobsArray objectAtIndex:indexPath.row]objectForKey:@"job_application"] objectAtIndex:0] valueForKey:@"applicant_job_status"] isEqualToString:@"invited"])
    {
        return 193;
    }
    return 166;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    if (jobFromMapPinClicked.count)
    {
        return jobFromMapPinClicked.count;
    }
    else if (fromSrchArr.count)
    {
        return [fromSrchArr count];
    }
    else if (fromFiltrArr.count)
    {
        return [fromFiltrArr count];
        
    }
    else
        return jobsArray.count ;
}
- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    
    UITableViewCell *globlCell;
    
    if ([[[[[jobsArray objectAtIndex:indexPath.row]objectForKey:@"job_application"] objectAtIndex:0] valueForKey:@"applicant_job_status"] isEqualToString:@"invited"])
//    if (jobsArray.count>0 && indexPath.row<jobsArray.count)
    {
        static NSString *cellIdentifier = @"myInvitationCell";
        
        MyInvitationCell *cell=(MyInvitationCell *)[tableView dequeueReusableCellWithIdentifier:cellIdentifier];
        if (cell == nil)
        {
            cell = [[MyInvitationCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:cellIdentifier];
        }
        cell.lbl_Amount.layer.cornerRadius = 25;
        cell.lbl_Amount.clipsToBounds = YES;
        cell.userIcon_img.layer.cornerRadius = 12;
        cell.userIcon_img.clipsToBounds = YES;
        cell.lbl_Heading.text=[[jobsArray objectAtIndex:indexPath.row]objectForKey:@"title"];
        cell.lbl_UserName.text=[[[jobsArray objectAtIndex:indexPath.row]objectForKey:@"user"]objectForKey:@"name"];
        cell.lbl_CatName.text=[NSString stringWithFormat:@"%@",[[jobsArray objectAtIndex:indexPath.row]objectForKey:@"category_id"] ];
        cell.lbl_InviteOn.text=[[jobsArray objectAtIndex:indexPath.row]valueForKeyPath:@"created_at"];
        
        cell.btnAccept.tag=indexPath.row;
        cell.btnDecline.tag=indexPath.row;
        [cell.btnAccept addTarget:self action:@selector(AcceptJob:) forControlEvents:UIControlEventTouchUpInside];
        [cell.btnDecline addTarget:self action:@selector(DeclineJob:) forControlEvents:UIControlEventTouchUpInside];
        
        NSString *image=[[[jobsArray objectAtIndex:indexPath.row]objectForKey:@"user"]objectForKey:@"profile_image"];
        NSString *imgUrl=[NSString stringWithFormat:@"%@%@",serviceLink,image];
        
        
        if (image.length<1)
        {
            cell.userIcon_img.image=[UIImage imageNamed:@"user.png"];
        }
        else
        {
            [cell.userIcon_img sd_setImageWithURL:[NSURL URLWithString:imgUrl] placeholderImage:[UIImage imageNamed:@"defuser.png"]];
            
//            dispatch_async(dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_BACKGROUND, 0), ^{
//                NSData * imageData = [NSData dataWithContentsOfURL:[NSURL URLWithString:imgUrl]];
//                UIImage *image = [UIImage imageWithData:imageData];
//                
//                dispatch_sync(dispatch_get_main_queue(), ^{ //in main thread update the image
//                    cell.userIcon_img.image = image;
//                    cell.textLabel.text = @""; //add this update will reflect the changes
//                });
//            });
            
        }
        
        NSString *catId=[NSString stringWithFormat:@"%@",[[jobsArray objectAtIndex:indexPath.row]objectForKey:@"category_id"]];
        
        if ([catId isEqualToString:@"1"]) {
            cell.lbl_CatName.text = @"Home" ;
            cell.imgCategory.image=[UIImage imageNamed:@"cat_home"];
            
        }else if([catId isEqualToString:@"2"])
        {
            cell.lbl_CatName.text = @"Auto" ;
            cell.imgCategory.image=[UIImage imageNamed:@"car_icon"];
            
        }else if([catId isEqualToString:@"3"])
        {
            cell.lbl_CatName.text =@"Electronic" ;
            cell.imgCategory.image=[UIImage imageNamed:@"cat_elc"];
        }
        
       globlCell = cell;
    }
    else{
    
    static NSString *cellidentifier=@"cell_WorkerSearch";
    WorkerSearchTableViewCell *cell=(WorkerSearchTableViewCell *)[tableView dequeueReusableCellWithIdentifier:cellidentifier];
    if (cell == nil)
    {
        cell = [[WorkerSearchTableViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:cellidentifier];
    }
    
    UIBezierPath *shadowPath = [UIBezierPath bezierPathWithRect:cell.view_tablebackground_object.bounds];
    cell.view_tablebackground_object.layer.masksToBounds = NO;
    cell.view_tablebackground_object.layer.shadowColor = [UIColor blackColor].CGColor;
    cell.view_tablebackground_object.layer.shadowOffset = CGSizeMake(0.0f, 0.0f);
    cell.view_tablebackground_object.layer.shadowOpacity = 0.1f;
    cell.view_tablebackground_object.layer.shadowPath = shadowPath.CGPath;
    cell.view_tablebackground_object.alpha=1.0;
    
    if (fromSrchArr.count)
    {
        NSMutableDictionary *dict=[NSMutableDictionary new];
        dict=[fromSrchArr objectAtIndex:indexPath.row];
        
        cell.label_CategoryTitle.text=[dict valueForKey:@"title"];
        cell.lbl_date.text=[dict valueForKey:@"created_at"];
        cell.lbl_address.text=[dict valueForKey:@"location"];
        cell.lbl_time.text=[dict valueForKey:@"date_of_completion"];
        
        NSDictionary *dict1=[[fromSrchArr objectAtIndex:indexPath.row]objectForKey:@"job_applications"];
        NSArray *str=[dict1 valueForKeyPath:@"applicant_job_status"];
        if (str.count>0)
        {
            cell.btnStatusJobApplied.hidden=NO;
        }
        else
        {
            cell.btnStatusJobApplied.hidden=YES;
        }
        
        NSString *catId=[NSString stringWithFormat:@"%@",[dict valueForKey:@"category_id"]];
        
        if ([catId isEqualToString:@"1"])
        {
            cell.lbl_category.text=@"Home";
            cell.imageView_category.image=[UIImage imageNamed:@"cat_home"];
            
        } else if ([catId isEqualToString:@"2"] )
        {
            cell.lbl_category.text=@"Auto";
            cell.imageView_category.image=[UIImage imageNamed:@"car_icon"];
        } else if ([catId isEqualToString:@"3"] )
        {
            cell.lbl_category.text=@"Electronics";
            cell.imageView_category.image=[UIImage imageNamed:@"cat_elc"];
        }
        
        globlCell = cell;
    }
    else if (fromFiltrArr.count)
    {
        NSMutableDictionary *dict=[NSMutableDictionary new];
        dict=[fromFiltrArr objectAtIndex:indexPath.row];
        cell.label_CategoryTitle.text=[dict valueForKey:@"title"];
        cell.lbl_date.text=[dict valueForKey:@"created_at"];
        cell.lbl_address.text=[dict valueForKey:@"location"];
        cell.lbl_time.text=[dict valueForKey:@"date_of_completion"];
        
        NSDictionary *dict1=[[fromFiltrArr objectAtIndex:indexPath.row]objectForKey:@"job_applications"];
        NSArray *str=[dict1 valueForKeyPath:@"applicant_job_status"];
        
        if (str.count>0)
        {
            cell.btnStatusJobApplied.hidden=NO;
        }
        else
        {
            cell.btnStatusJobApplied.hidden=YES;
        }
        
        NSString *catId=[NSString stringWithFormat:@"%@",[dict valueForKey:@"category_id"]];

        
        if ([catId isEqualToString:@"1"])
        {
            cell.lbl_category.text=@"Home";
            cell.imageView_category.image=[UIImage imageNamed:@"cat_home"];
        } else if ([catId isEqualToString:@"2"] )
        {
            cell.lbl_category.text=@"Auto";
            cell.imageView_category.image=[UIImage imageNamed:@"car_icon"];
        } else if ([catId isEqualToString:@"3"] )
        {
            cell.lbl_category.text=@"Electronics";
            cell.imageView_category.image=[UIImage imageNamed:@"cat_elc"];
        }
        
        globlCell = cell;
    }
    
    else if (jobFromMapPinClicked.count)
        
    {
        NSMutableDictionary *dict=[NSMutableDictionary new];
        dict=[jobFromMapPinClicked objectAtIndex:indexPath.row] ;
        cell.label_CategoryTitle.text=[dict valueForKey:@"title"];
        cell.lbl_date.text=[dict valueForKey:@"created_at"];
        cell.lbl_address.text=[dict valueForKey:@"location"];
        cell.lbl_time.text=[dict valueForKey:@"date_of_completion"];
        
        NSDictionary *dict1=[[jobFromMapPinClicked objectAtIndex:indexPath.row]objectForKey:@"job_applications"];
        NSArray *str=[dict1 valueForKeyPath:@"applicant_job_status"];
        if (str.count>0)
        {
            cell.btnStatusJobApplied.hidden=NO;
        }
        else
        {
            cell.btnStatusJobApplied.hidden=YES;
        }
        
        NSString *catId=[NSString stringWithFormat:@"%@",[dict valueForKey:@"category_id"]];
        
        if ([catId isEqualToString:@"1"])
        {
            cell.lbl_category.text=@"Home";
            cell.imageView_category.image=[UIImage imageNamed:@"cat_home"];
        } else if ([catId isEqualToString:@"2"] )
        {
            cell.lbl_category.text=@"Auto";
            cell.imageView_category.image=[UIImage imageNamed:@"car_icon"];
        } else if ([catId isEqualToString:@"3"] )
        {
            cell.lbl_category.text=@"Electronics";
            cell.imageView_category.image=[UIImage imageNamed:@"cat_elc"];
        }
        
        globlCell = cell;
        
    }
    else{
        NSMutableDictionary *dict=[NSMutableDictionary new];
        dict=[jobsArray objectAtIndex:indexPath.row];
        cell.label_CategoryTitle.text=[dict valueForKey:@"title"];
        cell.lbl_date.text=[dict valueForKey:@"created_at"];
        cell.lbl_address.text=[dict valueForKey:@"location"];
        cell.lbl_time.text=[dict valueForKey:@"date_of_completion"];
        NSDictionary *dict1=[[jobsArray objectAtIndex:indexPath.row]objectForKey:@"job_applications"];
        NSArray *str=[dict1 valueForKeyPath:@"applicant_job_status"];
        NSArray *arrApplicant=[[jobsArray objectAtIndex:indexPath.row]objectForKey:@"job_applications"];
        if (arrApplicant.count)
        {
            cell.lbl_Status.text=[[[[jobsArray objectAtIndex:indexPath.row]objectForKey:@"job_applications"]objectAtIndex:0]objectForKey:@"applicant_job_status"];
            
        }
        else
        {
            
        }
        
        if (str.count>0)
        {
            cell.btnStatusJobApplied.hidden=NO;
        }
        else
        {
            cell.btnStatusJobApplied.hidden=YES;
        }
        
        NSString *catID=[NSString stringWithFormat:@"%@",[dict valueForKey:@"category_id"] ];
        
        if ([catID isEqualToString:@"1"])
        {
            cell.lbl_category.text=@"Home";
            cell.imageView_category.image=[UIImage imageNamed:@"cat_home"];
            
        } else if ([catID isEqualToString:@"2"] )
        {
            cell.lbl_category.text=@"Auto";
            cell.imageView_category.image=[UIImage imageNamed:@"car_icon"];
        } else if ([catID isEqualToString:@"3"] )
        {
            cell.lbl_category.text=@"Electronics";
            cell.imageView_category.image=[UIImage imageNamed:@"cat_elc"];
        }
        
        globlCell = cell;
     }
    }
    return globlCell;
}


-(void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    
    
    if ([[[[[jobsArray objectAtIndex:indexPath.row]objectForKey:@"job_application"] objectAtIndex:0] valueForKey:@"applicant_job_status"] isEqualToString:@"invited"])
    {

        return;
    }
    
    NSMutableDictionary *dict=[NSMutableDictionary new];
    
    ApplyJobFromListJobViewController *applyjobview=[self.storyboard instantiateViewControllerWithIdentifier:@"ApplyJobFromListJobViewController"];
    if (fromSrchArr.count)
    {
        dict=[fromSrchArr objectAtIndex:indexPath.row];
        applyjobview.jobDict=dict;
        
    }
    else if (jobFromMapPinClicked.count)
    {
        dict=[jobFromMapPinClicked objectAtIndex:indexPath.row];
        applyjobview.jobDict=dict;
    }
    else
    {
        dict=[jobsArray objectAtIndex:indexPath.row];
        applyjobview.jobDict=dict;
        
    }
    [self.navigationController pushViewController:applyjobview animated:YES];
}
#pragma  mark
#pragma mark-List View and map View Segment
- (IBAction)btnListView_Action:(UIButton *)sender
{
    lbl_NoJob.hidden=YES;
    jobFromMapPinClicked=nil;
    [self performSelectorInBackground:@selector(FetchJobsList) withObject:nil];
   // [self FetchJobsList];
    
    
    [_View_MApViewList setFrame:CGRectMake(500,_View_MApViewList.frame.origin.y,_View_MApViewList.frame.size.width, _View_MApViewList.frame.size.height)];
    [_View_ListviewWorker_Object setFrame:CGRectMake(0, self.View_ListviewWorker_Object.frame.origin.y, self.View_ListviewWorker_Object.frame.size.width, self.View_ListviewWorker_Object.frame.size.height)];
    if (!sender.selected)
    {
        [btnMap_Object setBackgroundImage:[UIImage imageNamed:@"map_view"] forState:UIControlStateNormal];
        [btnList_object setBackgroundImage:[UIImage imageNamed:@"list_view_s"] forState:UIControlStateNormal];
    }
    else
    {
    }
    
}

- (IBAction)btnMapview_Action:(UIButton *)sender
{
    [searchBar_object resignFirstResponder];
    lbl_NoJob.hidden=YES;
    [_View_ListviewWorker_Object setFrame:CGRectMake(500, self.View_ListviewWorker_Object.frame.origin.y, self.View_ListviewWorker_Object.frame.size.width, self.View_ListviewWorker_Object.frame.size.height)];
    [_View_MApViewList setFrame:CGRectMake(0,_View_MApViewList.frame.origin.y,_View_MApViewList.frame.size.width,_View_MApViewList.frame.size.height)];
    if (!sender.selected)
    {
        [btnMap_Object setBackgroundImage:[UIImage imageNamed:@"map_view_s"] forState:UIControlStateNormal];
        [btnList_object setBackgroundImage:[UIImage imageNamed:@"list_view"] forState:UIControlStateNormal];
    }
    else
    {
    }
    
}
- (IBAction)btnFilter_Action:(id)sender
{
    fromSrchArr=nil;
    
    [UIView animateWithDuration:0.3f
                     animations:^{
                         blurEffect = [UIBlurEffect effectWithStyle:UIBlurEffectStyleExtraLight];
                         visualEffectView = [[UIVisualEffectView alloc] initWithEffect:blurEffect];
                         visualEffectView.alpha=1.0f;
                         visualEffectView.frame = self.view.bounds;
                         _view_popUP.center=self.view.center;
                         [self.view.window addSubview:visualEffectView];
                     }
                     completion:^(BOOL finished)
     {
         [visualEffectView addSubview:_view_popUP];
     }
     ];
    
    
}
#pragma mark
#pragma mark-textField delegate
-(BOOL)textField:(UITextField *)textField shouldChangeCharactersInRange:(NSRange)range replacementString:(NSString *)string {
    
    if (textField==txtFld_SelecteRange)
    {
        NSUInteger newLength = [txtFld_SelecteRange.text length] + [string length] - range.length;
        NSCharacterSet *cs = [[NSCharacterSet characterSetWithCharactersInString:@"0123456789"] invertedSet];
        
        NSString *filtered = [[string componentsSeparatedByCharactersInSet:cs] componentsJoinedByString:@""];
        
        NSString *myTextField = [textField.text stringByAppendingString:filtered];
        BOOL ret = ([string isEqualToString:filtered])&&(newLength <= 2)&&(myTextField.integerValue<=10);
        return ret;
    }
    return YES;
}


#pragma mark
#pragma mark- popup view action btn
- (IBAction)btn_autoFilter:(id)sender
{
    categoryFilterStr=@"2";
    str_checkFilter=@"popUp";
    str_selectRange=txtFld_SelecteRange.text;
    
    [btn_AutoObj setImage:[UIImage imageNamed:@"icon_pop_up"] forState:UIControlStateNormal];
    [ btn_HomeObj setImage:[UIImage imageNamed:@"home1"] forState:UIControlStateNormal];
    [btn_ElectObj setImage:[UIImage imageNamed:@"e_icon1"] forState:UIControlStateNormal];
}
- (IBAction)btn_homefilter:(id)sender
{
    categoryFilterStr=@"1";
    str_checkFilter=@"popUp";
    str_selectRange=txtFld_SelecteRange.text;
    
    [btn_AutoObj setImage:[UIImage imageNamed:@"icon1_pop_up"] forState:UIControlStateNormal];
    [ btn_HomeObj setImage:[UIImage imageNamed:@"home2"] forState:UIControlStateNormal];
    [btn_ElectObj setImage:[UIImage imageNamed:@"e_icon1"] forState:UIControlStateNormal];
}
- (IBAction)btn_electfilter:(id)sender
{
    categoryFilterStr=@"3";
    str_checkFilter=@"popUp";
    str_selectRange=txtFld_SelecteRange.text;
    
    [btn_AutoObj setImage:[UIImage imageNamed:@"icon1_pop_up"] forState:UIControlStateNormal];
    [ btn_HomeObj setImage:[UIImage imageNamed:@"home1"] forState:UIControlStateNormal];
    [btn_ElectObj setImage:[UIImage imageNamed:@"e_icon2"] forState:UIControlStateNormal];
}

- (IBAction)btn_SubmitFilter:(id)sender
{
    str_selectRange=[NSString stringWithFormat:@"%d",(int)self.sliderRange.value];
    [SVProgressHUD showWithStatus:@"loading..."];
    [self performSelector:@selector(filterJobList) withObject:nil afterDelay:0.3f];
    [visualEffectView removeFromSuperview];
    // [_view_popUP setFrame:CGRectMake(1000, self.view_popUP.frame.origin.y, self.view_popUP.frame.size.width, self.view_popUP.frame.size.height)];
}

-(void)fetchDataByFilter
{
    fromFiltrArr=[[NSMutableArray alloc] init];
    for (int i=0; i<jobsArray.count; i++)
    {
        if ([categoryFilterStr isEqualToString:[[jobsArray objectAtIndex:i] valueForKey:@"category_id"]])
        {
            [fromFiltrArr addObject:[jobsArray objectAtIndex:i]];
        }
    }
    [table_listJob reloadData];
}



#pragma mark
#pragma mark-Accept job and Decline Job
-(void)AcceptJob:(UIButton*)sender
{
    NSString *tag=[NSString stringWithFormat:@"%ld",(long)sender.tag];
    ApplyJobFromListJobViewController *applyView=[self.storyboard instantiateViewControllerWithIdentifier:@"ApplyJobFromListJobViewController"];
    applyView.jobDict=[arr_invitationJob objectAtIndex:[tag integerValue]];
    applyView.status=@"Myinvitation";
    applyView.dict_Detailinvitation=[arr_invitationJob objectAtIndex:[tag integerValue]];
    [self.navigationController pushViewController:applyView animated:YES];
}
-(void)DeclineJob:(UIButton*)sender
{
    NSString *tag=[NSString stringWithFormat:@"%ld",(long)sender.tag];
    
    NSString *token=[NSString stringWithFormat:@"%@",[defaults objectForKey:@"user_token"]];
    NSString *url=[NSString stringWithFormat:@"%@job-status?token=%@",serviceLink,token];
    
    NSMutableDictionary *jsonDict=[[NSMutableDictionary alloc]init];
    [jsonDict setValue:[[[[arr_invitationJob objectAtIndex:[tag integerValue]]objectForKey:@"job_application"]objectAtIndex:0] objectForKey:@"id"] forKey:@"job_application_id"];
    [jsonDict setValue:@"declined" forKey:@"applicant_job_status"];
    [jsonDict setValue:@"Not available"forKey:@"proposed_amount"];
    
    
    //change to notification
    [jsonDict setValue:[[[[arr_invitationJob objectAtIndex:[tag integerValue]] objectForKey:@"job_application"] objectAtIndex:0] objectForKey:@"job_id"] forKey:@"job_id"];
    
    [jsonDict setValue:[[[arr_invitationJob objectAtIndex:[tag integerValue]] objectForKey:@"user"] objectForKey:@"id"] forKey:@"job_user_id"];
    
    [jsonDict setValue:[defaults objectForKey:@"user_id"] forKey:@"sender_id"];
    
    [jsonDict setValue:[[[arr_invitationJob objectAtIndex:[tag integerValue]] objectForKey:@"user"] objectForKey:@"id"] forKey:@"receiver_id"];
    
    [jsonDict setValue:[defaults objectForKey:@"p_name"] forKey:@"sender_name"];
    
    
    
    
    id json=[WebService signup:jsonDict :url];
    if ([[json objectForKey:@"message"]isEqualToString:@"Success"])
    {
        UIAlertView *alert=[[UIAlertView alloc]initWithTitle:@"" message:@"You have Decline this job." delegate:nil cancelButtonTitle:@"OK" otherButtonTitles: nil];
        [alert show];
//        [self.navigationController popViewControllerAnimated:YES];
        
    }
    else
    {
        UIAlertView *alert=[[UIAlertView alloc]initWithTitle:@"" message:@"Some error to decline this job." delegate:nil cancelButtonTitle:@"OK" otherButtonTitles: nil];
        [alert show];
    }
}




#pragma mark
#pragma mark- mapview action btn

- (IBAction)btn_autoMap:(id)sender {
    btn_AutoMap.selected=!btn_AutoMap.selected;
    if (btn_AutoMap.selected)
    {
        [btn_AutoMap setImage:[UIImage imageNamed:@"icon_pop_up"] forState:UIControlStateNormal];
        categoryFilterStr=@"2";
        
        str_checkFilter=@"done";
        [self performSelectorInBackground:@selector(filterJobList) withObject:nil];
       // [self performSelector:@selector(filterJobList) withObject:nil afterDelay:0.3f];
    }
    else
    {
        [btn_AutoMap setImage:[UIImage imageNamed:@"icon1_pop_up"] forState:UIControlStateNormal];
        categoryFilterStr=nil;
        
        str_checkFilter=@"done";
        [self performSelectorInBackground:@selector(filterJobList) withObject:nil];
        //[self performSelector:@selector(filterJobList) withObject:nil afterDelay:0.3f];
    }
    [btn_HomeMap setImage:[UIImage imageNamed:@"home1"] forState:UIControlStateNormal];
    [btn_electMap setImage:[UIImage imageNamed:@"e_icon1"] forState:UIControlStateNormal];
    btn_HomeMap.selected=NO;
    btn_electMap.selected=NO;
    
}
- (IBAction)btn_HomeMap:(id)sender {
    btn_HomeMap.selected=!btn_HomeMap.selected;
    if (btn_HomeMap.selected)
    {
        [btn_HomeMap setImage:[UIImage imageNamed:@"home2"] forState:UIControlStateNormal];
        categoryFilterStr=@"1";
        
        str_checkFilter=@"done";
        [self performSelectorInBackground:@selector(filterJobList) withObject:nil];

        
       // [self performSelector:@selector(filterJobList) withObject:nil afterDelay:0.3f];
        
    }
    else
    {
        [btn_HomeMap setImage:[UIImage imageNamed:@"home1"] forState:UIControlStateNormal];
        categoryFilterStr=nil;
        
        str_checkFilter=@"done";
        [self performSelector:@selector(filterJobList) withObject:nil afterDelay:0.3f];
    }
    
    
    [btn_AutoMap setImage:[UIImage imageNamed:@"icon1_pop_up"] forState:UIControlStateNormal];
    [btn_electMap setImage:[UIImage imageNamed:@"e_icon1"] forState:UIControlStateNormal];
    btn_AutoMap.selected=NO;
    btn_electMap.selected=NO;
    
    
}
- (IBAction)btn_EltMap:(id)sender {
    
    btn_electMap.selected=!btn_electMap.selected;
    if (btn_electMap.selected)
    {
        [btn_electMap setImage:[UIImage imageNamed:@"e_icon2"] forState:UIControlStateNormal];
        categoryFilterStr=@"3";
        
        str_checkFilter=@"done";
        [self performSelector:@selector(filterJobList) withObject:nil afterDelay:0.3f];
        
    }
    else
    {
        [btn_electMap setImage:[UIImage imageNamed:@"e_icon1"] forState:UIControlStateNormal];
        categoryFilterStr=nil;
        
        str_checkFilter=@"done";
        [self performSelector:@selector(filterJobList) withObject:nil afterDelay:0.3f];
    }
    
    [btn_AutoMap setImage:[UIImage imageNamed:@"icon1_pop_up"] forState:UIControlStateNormal];
    [btn_HomeMap setImage:[UIImage imageNamed:@"home1"] forState:UIControlStateNormal];
    btn_AutoMap.selected=NO;
    btn_HomeMap.selected=NO;
    
}
- (IBAction)slider_Action:(id)sender
{
    lbl_MapRadius.text=[NSString stringWithFormat:@"%d",(int)_slider_custom.value];
    str_checkFilter=@"done";
    [self performSelector:@selector(filterJobList) withObject:nil afterDelay:0.3f];
    // [self ZoomMapView];
    
    
}
- (IBAction)SliderRangeAction:(id)sender
{
     _rangeLbl.text=[NSString stringWithFormat:@"%d Miles",(int)self.sliderRange.value];
    txtFld_SelecteRange.text = _rangeLbl.text;
}


   
- (IBAction)btn_done:(id)sender {
    removePin=YES;
    //  str_checkFilter=@"done";
    // [self performSelector:@selector(filterJobList) withObject:nil afterDelay:0.3f];
    
}

- (IBAction)btnRefresh:(id)sender {
    [SVProgressHUD showWithStatus:@"Loading..."];
    [self performSelector:@selector(FetchJobsList) withObject:nil afterDelay:0.3f];
  
    fromFiltrArr=nil;
    [table_listJob reloadData];
    //   [_view_popUP setFrame:CGRectMake(1000, self.view_popUP.frame.origin.y, self.view_popUP.frame.size.width, self.view_popUP.frame.size.height)];
    [visualEffectView removeFromSuperview];
}
@end
