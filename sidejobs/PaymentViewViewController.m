//
//  PaymentViewViewController.m
//  sidejobs
//
//  Created by DebutMac4 on 29/10/15.
//  Copyright © 2015 DebutMac4. All rights reserved.
//

#import "PaymentViewViewController.h"
#import "PaymentRecievedCell.h"
#import "AppDelegate.h"

@interface PaymentViewViewController ()
{
    NSUserDefaults *defaults;
    NSMutableArray *arrDuePayment,*arrRecievePayment;
    AppDelegate *delegate;
    NSString *DueRecieveStatus;
    NSString *status;
    
    
    NSMutableArray *arr_DuePaymentWorkerLogin,*arr_RecievePaymenWorkerLogin;
}

@end

@implementation PaymentViewViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    delegate=(AppDelegate*)[[UIApplication sharedApplication]delegate];
    defaults=[NSUserDefaults standardUserDefaults];
    
    UIButton *rightButton=[UIButton buttonWithType:UIButtonTypeCustom];
    [rightButton setFrame:CGRectMake(0, 0, 23, 23)];
    //arrow1
    [rightButton setImage:[UIImage imageNamed:@"arrow1.png"] forState:UIControlStateNormal];
    [rightButton addTarget:self action:@selector(DashBarButton:) forControlEvents:UIControlEventTouchUpInside];
    self.navigationItem.rightBarButtonItem=[[UIBarButtonItem alloc]initWithCustomView:rightButton];
    
    [self.navigationController.navigationBar setBackgroundImage:[UIImage imageNamed:@"top_bar2.png"] forBarMetrics:UIBarMetricsDefault];
    self.navigationItem.backBarButtonItem = [[UIBarButtonItem alloc] initWithTitle:@"" style:self.navigationItem.backBarButtonItem.style target:nil action:nil];
    
    NSDictionary *size = [NSDictionary dictionaryWithObjectsAndKeys:[UIFont fontWithName:@"HelveticaNeue-Light" size:20.0],NSFontAttributeName, [UIColor whiteColor],NSForegroundColorAttributeName, nil];
    self.navigationController.navigationBar.titleTextAttributes = size;
    
    self.navigationItem.title=@"Payment Detail";
    [[UINavigationBar appearance]setBackgroundColor:[UIColor colorWithRed:71.0f green:71.0f blue:71.0f alpha:1.0]];
    
     DueRecieveStatus=@"recieve";
    [SVProgressHUD showWithMaskType:SVProgressHUDMaskTypeClear];
    
    status=[defaults valueForKey:@"user_status"];
    if ([status isEqualToString:@"customer"])
    {
        [self performSelector:@selector(duePayment) withObject:nil afterDelay:0.3f];
    }
    
    else if ([status isEqualToString:@"worker"])
    {
    [self performSelector:@selector(getpaymentDetailsFromWorkerLogin) withObject:nil afterDelay:0.3f];
    }
}

-(void)DashBarButton:(id)sender
{
    
     UIWindow *wnd = [[[UIApplication sharedApplication] delegate] window];
    
    
    [UIView transitionWithView:wnd
                      duration:0.5
                       options:UIViewAnimationOptionTransitionFlipFromLeft
                    animations:^{
                        
                        UIViewController *root = [[UIStoryboard storyboardWithName:@"Main" bundle:NULL] instantiateViewControllerWithIdentifier:@"rootController"];
                        [wnd makeKeyAndVisible];
                        wnd.rootViewController = root;
                    }
                    completion:nil];
    
}

-(void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:YES];
    status=[defaults valueForKey:@"user_status"];
}


//when user are customer than call this...........*****************......................................
#pragma mark
#pragma mark-get payment details
-(void)duePayment
{
    NSString *token=[NSString stringWithFormat:@"%@",[defaults objectForKey:@"user_token"]];
    NSString *url=[NSString stringWithFormat:@"%@customer-ongoing?token=%@",serviceLink,token];
    NSMutableDictionary *dict=[[NSMutableDictionary alloc]init];
    [dict setValue:[defaults objectForKey:@"user_id"] forKey:@"id"];
    id json = [WebService signup:dict :url];
    
    if ([[json objectForKey:@"message"]isEqualToString:@"Success"])
    {
        [SVProgressHUD dismiss];
        arrDuePayment=[[NSMutableArray alloc]init];
        arrRecievePayment=[[NSMutableArray alloc]init];
        
        NSMutableArray *arrResponse=[[NSMutableArray alloc]init];
        arrResponse=[json objectForKey:@"response"];
        

        for (int i=0; i<arrResponse.count; i++)
        {
            NSMutableArray *arr=[[arrResponse objectAtIndex:i]objectForKey:@"job_applications"];
            if (arr.count)
            {
                NSLog(@"%@", [[arrResponse objectAtIndex:i]objectForKey:@"job_applications"]);
                
                if ([DueRecieveStatus isEqualToString:@"due"])
                {
                    if ([[[[[arrResponse objectAtIndex:i]objectForKey:@"job_applications"]objectAtIndex:0]objectForKey:@"applicant_job_status"]isEqualToString:@"completed"])
                    {
                        [arrDuePayment addObject:[arrResponse objectAtIndex:i]];
                    }
                }
                
                else
                {
                    if ([[[[[arrResponse objectAtIndex:i]objectForKey:@"job_applications"]objectAtIndex:0]objectForKey:@"applicant_job_status"]isEqualToString:@"confirmed"])
                    {
                        [arrRecievePayment addObject:[arrResponse objectAtIndex:i]];
                    }
                }
               
            }
        }
        
        _table_Payment.dataSource=self;
        _table_Payment.delegate=self;
        [_table_Payment reloadData];
    }
    else if ([[json objectForKey:@"error"]isEqualToString:@"token_expired"])
    {
        [SVProgressHUD dismiss];
        UIAlertView *alert=[[UIAlertView alloc]initWithTitle:@"" message:@"Session Expired Please login agian" delegate:nil cancelButtonTitle:@"OK" otherButtonTitles: nil];
        [alert show];
        
        [defaults setBool:NO forKey:@"logined"];
        [defaults synchronize];
        [[NSUserDefaults standardUserDefaults]synchronize];
        [delegate loginStatus];
    }
    
    if (arrDuePayment.count||arrRecievePayment.count)
    {
        lbl_noREcord.hidden = YES;
    }else{
        lbl_noREcord.hidden = NO;
    }
    NSLog(@"%@",arrDuePayment);
    [SVProgressHUD dismiss];

}

//when user are worker than call this. . . . . . . .*************** . . . . . . . ..  . . . . . . . . . . . . . . . . . . . . . . . .

-(void)getpaymentDetailsFromWorkerLogin
{
    NSString *token=[NSString stringWithFormat:@"%@",[defaults objectForKey:@"user_token"]];
    NSString *url=[NSString stringWithFormat:@"%@applied-workers?token=%@",serviceLink,token];
    NSMutableDictionary *dict=[[NSMutableDictionary alloc]init];
    [dict setValue:[defaults objectForKey:@"user_id"] forKey:@"id"];
    id json = [WebService signup:dict :url];
    if ([[json objectForKey:@"message"]isEqualToString:@"Success"])
    {
        arrDuePayment=[[NSMutableArray alloc]init];
        arrRecievePayment=[[NSMutableArray alloc]init];
        NSMutableArray *arrResponse=[[NSMutableArray alloc]init];
        
        arrResponse=[json objectForKey:@"response"];
        for (int i=0; i<[arrResponse count]; i++)
        {
            
            if ([DueRecieveStatus isEqualToString:@"due"])
            {
              if ([[[[[arrResponse objectAtIndex:i]objectForKey:@"job_applications"]objectAtIndex:0]objectForKey:@"applicant_job_status"]isEqualToString:@"completed"])
              {
                [arrDuePayment addObject:[arrResponse objectAtIndex:i]];
              }
            }
            else
            {
                if ([[[[[arrResponse objectAtIndex:i]objectForKey:@"job_applications"]objectAtIndex:0]objectForKey:@"applicant_job_status"]isEqualToString:@"confirmed"])
                {
                    [arrRecievePayment addObject:[arrResponse objectAtIndex:i]];
                }
            }
        }
        _table_Payment.dataSource=self;
        _table_Payment.delegate=self;
        [_table_Payment reloadData];
    }
    else if ([[json objectForKey:@"error"]isEqualToString:@"token_expired"])
    {
        [SVProgressHUD dismiss];
        UIAlertView *alert=[[UIAlertView alloc]initWithTitle:@"" message:@"Session Expired Please login agian" delegate:nil cancelButtonTitle:@"OK" otherButtonTitles: nil];
        [alert show];
       
        [defaults setBool:NO forKey:@"logined"];
        [defaults synchronize];
        [[NSUserDefaults standardUserDefaults]synchronize];
        [delegate loginStatus];
    }
    
    if (arrDuePayment.count||arrRecievePayment.count)
    {
        lbl_noREcord.hidden = YES;
    }else{
        lbl_noREcord.hidden = NO;
    }
    NSLog(@"%@",arrDuePayment);
    [SVProgressHUD dismiss];

}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

#pragma mark
#pragma mark-TableView Delegate method....................******.......................................

-(NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    
    return 1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section;
{
    if ([DueRecieveStatus isEqualToString:@"due"])
    {
        return arrDuePayment.count;
    }
    else
      return arrRecievePayment.count;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    static NSString *customCellIdentifier = @"paymentRecievedCell";
    PaymentRecievedCell *cell = [tableView dequeueReusableCellWithIdentifier:customCellIdentifier];
    
    if (cell == nil)
    {
        cell = [[PaymentRecievedCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:customCellIdentifier];
    }
    NSMutableArray *arrData=[[NSMutableArray alloc]init];
    if ([DueRecieveStatus isEqualToString:@"due"])
    {
        arrData=arrDuePayment;
       // cell.lbl_pendingPayStatus.hidden=NO;
    }
    else
    {
        arrData=arrRecievePayment;
        //cell.lbl_pendingPayStatus.hidden=YES;
    }
    NSString *catId=[NSString stringWithFormat:@"%@",[[arrData objectAtIndex:indexPath.row]objectForKey:@"category_id"]];
    cell.lbl_Title.text=[[arrData objectAtIndex:indexPath.row] objectForKey:@"title"];
    
    //complete job time
    NSString *completeTime=[[[[arrData objectAtIndex:indexPath.row]objectForKey:@"job_applications"] objectAtIndex:0]objectForKey:@"updated_at"];
    NSArray *Arrtime=[completeTime componentsSeparatedByString:@" "];
    NSString* seprateCompleteTime=[Arrtime objectAtIndex:0];
    cell.lbl_CompletedOn.text=seprateCompleteTime;
    
    //start job time
    NSString *startTime=[[[[arrData objectAtIndex:indexPath.row]objectForKey:@"job_awarded"] objectAtIndex:0]objectForKey:@"created_at"];
    NSArray *ArrStart=[startTime componentsSeparatedByString:@" "];
    NSString* seprateStarttime=[ArrStart objectAtIndex:0];
    cell.lbl_StartedOn.text=seprateStarttime;
    cell.lbl_amout.text=[NSString stringWithFormat:@"$%@",[[[[arrData objectAtIndex:indexPath.row]objectForKey:@"job_awarded"]objectAtIndex:0]objectForKey:@"proposed_amount"]];

    
    if ([catId isEqualToString:@"1"]) {
        cell.lbl_CategoryName.text = @"Home" ;
        cell.img_Category.image=[UIImage imageNamed:@"cat_home"];
        
    }else if([catId isEqualToString:@"2"])
    {
        cell.lbl_CategoryName.text = @"Auto" ;
        cell.img_Category.image=[UIImage imageNamed:@"car_icon"];
        
    }else if([catId isEqualToString:@"3"])
    {
        cell.lbl_CategoryName.text =@"Electronic" ;
        cell.img_Category.image=[UIImage imageNamed:@"cat_elc"];
    }

    return cell;
}


- (IBAction)btnPayRecievedAction:(id)sender
{
    DueRecieveStatus=@"recieve";
    [SVProgressHUD showWithMaskType:SVProgressHUDMaskTypeClear];
    if ([status isEqualToString:@"customer"])
    {
        [self performSelector:@selector(duePayment) withObject:nil afterDelay:0.3f];
    }
    
    else if ([status isEqualToString:@"worker"])
    {
        [self performSelector:@selector(getpaymentDetailsFromWorkerLogin) withObject:nil afterDelay:0.3f];
    }
    _btn_paymentRecieved.backgroundColor=[UIColor orangeColor];
    _btn_PaymentDue.backgroundColor=[UIColor grayColor];
}

- (IBAction)btn_PayDueAction:(id)sender
{
    DueRecieveStatus=@"due";
   [SVProgressHUD showWithMaskType:SVProgressHUDMaskTypeClear];
    if ([status isEqualToString:@"customer"])
    {
        [self performSelector:@selector(duePayment) withObject:nil afterDelay:0.3f];
    }
    
    else if ([status isEqualToString:@"worker"])
    {
        [self performSelector:@selector(getpaymentDetailsFromWorkerLogin) withObject:nil afterDelay:0.3f];
    }
    _btn_paymentRecieved.backgroundColor=[UIColor grayColor];
    _btn_PaymentDue.backgroundColor=[UIColor orangeColor];

}


@end
