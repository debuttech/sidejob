//
//  MessageCellVC.h
//  sidejobs
//
//  Created by DebutMac4 on 24/11/15.
//  Copyright © 2015 DebutMac4. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface MessageCellVC : UITableViewCell
@property (strong, nonatomic) IBOutlet UIImageView *img_User;
@property (strong, nonatomic) IBOutlet UILabel *lbl_UserName;
@property (strong, nonatomic) IBOutlet UILabel *lbl_UserEmail;
@property (strong, nonatomic) IBOutlet UILabel *lbl_UserNumber;

@end
