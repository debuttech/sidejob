//
//  SubmitAccountDetailsViewController.h
//  sidejobs
//
//  Created by DebutMac4 on 29/03/16.
//  Copyright © 2016 DebutMac4. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface SubmitAccountDetailsViewController : UIViewController <UITextFieldDelegate,UIPickerViewDataSource,UIPickerViewDelegate>
{
    
    IBOutlet UIScrollView *scrollView_SubmitAccountDetail;
    IBOutlet UITextField *textfield_FirstName;
    IBOutlet UITextField *textfield_lastName;
    IBOutlet UITextField *textfield_DateOfBirth;
    //IBOutlet UITextField *textfield_FirstName;
    IBOutlet UITextField *textfield_EmailAddress;
    IBOutlet UITextField *textfield_AccountNumber;
    IBOutlet UITextField *textfield_PhoneNumber;
    IBOutlet UITextField *textfield_SSNNumber;
    //IBOutlet UITextField *textfield_CVCNumber;
    IBOutlet UITextField *textfield_RoutingNumber;
    IBOutlet UITextField *textfield_Region;
    IBOutlet UITextField *textfield_Locality;
    IBOutlet UITextField *textfield_StreetAddress;
    IBOutlet UITextField *textfield_PostalCode;
    
    IBOutlet UIButton *button_SubmitAccountDetail;
}
- (IBAction)buttonTapped_SubmitAccountDetail:(UIButton *)sender;
@end
