//
//  SubmitAccountDetailsViewController.m
//  sidejobs
//
//  Created by DebutMac4 on 29/03/16.
//  Copyright © 2016 DebutMac4. All rights reserved.
//
#define ACCEPTABLE_CHARACTERS @"ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz "



#import "SubmitAccountDetailsViewController.h"
#import "WebService.h"
#import "AppDelegate.h"
#import "Helper.h"

@interface SubmitAccountDetailsViewController ()
{
    NSUserDefaults *defaults;
    UIDatePicker *datePicker ;
    NSMutableArray *monthsArray,*yearsArray, *dayArray;
    NSString *yearString1;
    NSString *str_email;
   
}

@end

@implementation SubmitAccountDetailsViewController

- (void)viewDidLoad
{
    [super viewDidLoad];
    
    UIToolbar* numberToolbar = [[UIToolbar alloc]initWithFrame:CGRectMake(0, 0, 320, 50)];
    numberToolbar.barStyle = UIBarStyleBlackTranslucent;
    numberToolbar.items = @[[[UIBarButtonItem alloc]initWithTitle:@"Cancel" style:UIBarButtonItemStyleBordered target:self action:@selector(cancelNumberPad)],
                            [[UIBarButtonItem alloc]initWithBarButtonSystemItem:UIBarButtonSystemItemFlexibleSpace target:nil action:nil],
                            [[UIBarButtonItem alloc]initWithTitle:@"Done" style:UIBarButtonItemStyleDone target:self action:@selector(doneWithNumberPad)]];
    [numberToolbar sizeToFit];
    textfield_PostalCode.inputAccessoryView = numberToolbar;
    defaults=[NSUserDefaults standardUserDefaults];
//    NSString *merchant_id = [defaults objectForKey:@"merchant_id"];
    self.navigationItem.hidesBackButton=YES;
    self.navigationItem.title=@"Add Account Details";
    UIButton *backButton=[UIButton buttonWithType:UIButtonTypeCustom];
    [backButton setFrame:CGRectMake(0, 0, 23, 21)];
    [backButton setImage:[UIImage imageNamed:@"arrow.png"] forState:UIControlStateNormal];
    [backButton addTarget:self action:@selector(leftBarButton:) forControlEvents:UIControlEventTouchUpInside];
    self.navigationItem.leftBarButtonItem=[[UIBarButtonItem alloc]initWithCustomView:backButton];
    
    
    textfield_DateOfBirth.delegate = self;
    datePicker = [[UIDatePicker alloc] init];
    
    NSDateFormatter *dateFormatter = [[NSDateFormatter alloc] init];
    [dateFormatter setDateFormat:@"dd-MM-yyyy"];
    
    yearString1 = [dateFormatter stringFromDate:[NSDate date]];
    
    // converting string to date
    NSDate *theMaximumDate = [dateFormatter dateFromString: yearString1];
    
    
    // here you can assign the max and min dates to your datePicker
    [datePicker setMaximumDate:theMaximumDate]; //the min age restriction
//    [datePicker setMinimumDate:theMinimumDate]; //the max age restriction (if needed, or else dont use this line)
    [datePicker setDatePickerMode:UIDatePickerModeDate];
    [datePicker addTarget:self action:@selector(updateTextField:) forControlEvents:UIControlEventValueChanged];
    
    // and finally set the datePicker as the input mode of your textfield
    [textfield_DateOfBirth setInputView:datePicker];
    [datePicker setFrame:CGRectMake(0, 400, self.view.frame.size.width, self.view.bounds.size.height/3.0)];
    textfield_DateOfBirth.inputAccessoryView.backgroundColor=[UIColor orangeColor];
    datePicker.backgroundColor=[UIColor whiteColor];

    
    
    UITapGestureRecognizer *tap=[[UITapGestureRecognizer alloc]initWithTarget:self action:@selector(tapGesture:)];
    tap.numberOfTapsRequired=1;
    [self.view addGestureRecognizer:tap];
    UITapGestureRecognizer * tapGesture = [[UITapGestureRecognizer alloc]
                                           initWithTarget:self
                                           action:@selector(hideKeyBoard)];
    
    [self.view addGestureRecognizer:tapGesture];

    }




-(void) viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:YES];
    scrollView_SubmitAccountDetail.contentSize = CGSizeMake(320, 950);
}

//- (void)touchesBegan:(NSSet *)touches withEvent:(UIEvent *)event
//{
//    [self.view endEditing:YES];
//}

-(void)cancelNumberPad{
    [textfield_PostalCode resignFirstResponder];
    textfield_PostalCode.text = @"";
}

-(void)doneWithNumberPad{
    NSString *numberFromTheKeyboard = textfield_PostalCode.text;
    [textfield_PostalCode resignFirstResponder];
}
-(void)hideKeyBoard
{
    [self.view endEditing:YES];
}

-(void)updateTextField:(id)sender {
    UIDatePicker *picker = (UIDatePicker*)textfield_DateOfBirth.inputView;
    textfield_DateOfBirth.text = [self formatDate:picker.date];
}

- (NSString *)formatDate:(NSDate *)date
{
    NSDateFormatter *dateFormatter = [[NSDateFormatter alloc] init];
    [dateFormatter setDateStyle:NSDateFormatterShortStyle];
    [dateFormatter setDateFormat:@"dd-MM-yyyy"];
     NSString *formattedDate = [dateFormatter stringFromDate:date];
     return formattedDate;
     }

-(void)tapGesture:(UITapGestureRecognizer *)sender
{
//    [_txtFldName resignFirstResponder];
//    [_txtFldCardNumber resignFirstResponder];
    [textfield_DateOfBirth resignFirstResponder];
//    [_txtFldCVc resignFirstResponder];
    
}


- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}




-(void)leftBarButton:(UIButton*)sender
{
    [self.navigationController popViewControllerAnimated:YES];
}

- (IBAction)buttonTapped_SubmitAccountDetail:(UIButton *)sender
{
    str_email = textfield_EmailAddress.text;
    
    if (textfield_FirstName.text.length<1)
    {
        
        UIAlertView *alertView=[[UIAlertView alloc]initWithTitle:@"Alert!" message:@"Please enter first name name." delegate:nil cancelButtonTitle:@"OK" otherButtonTitles:nil];
        [alertView show];
    }
    else if (textfield_lastName.text.length<1)
    {
        
        UIAlertView *alertView=[[UIAlertView alloc]initWithTitle:@"Alert!" message:@"Please enter last name name." delegate:nil cancelButtonTitle:@"OK" otherButtonTitles:nil];
        [alertView show];
    }

    else if(textfield_EmailAddress.text.length<1 || ![self isValidEmail:str_email])
    {
        UIAlertView *alertView=[[UIAlertView alloc]initWithTitle:@"Alert!" message:@"Please enter valid email address." delegate:nil cancelButtonTitle:@"OK" otherButtonTitles:nil];
        [alertView show];
    }
//    else if (![self isValidEmail:str_email])
//    {
//        UIAlertView *alert=[[UIAlertView alloc]initWithTitle:@"" message:@"Please enter a valid email." delegate:nil cancelButtonTitle:@"OK" otherButtonTitles: nil];
//        [alert show];
//        
//    }
    
    else if(textfield_AccountNumber.text.length<1)
    {
        UIAlertView *alertView=[[UIAlertView alloc]initWithTitle:@"Alert!" message:@"Please enter your account number." delegate:nil cancelButtonTitle:@"OK" otherButtonTitles:nil];
        [alertView show];
    }
    else if( textfield_PhoneNumber.text.length<1)
    {
        UIAlertView *alertView=[[UIAlertView alloc]initWithTitle:@"Alert!" message:@"Please enter valid phone number number." delegate:nil cancelButtonTitle:@"OK" otherButtonTitles:nil];
        [alertView show];
    }
    else if( textfield_SSNNumber.text.length<1)
    {
        UIAlertView *alertView=[[UIAlertView alloc]initWithTitle:@"Alert!" message:@"Please enter valid ssn number." delegate:nil cancelButtonTitle:@"OK" otherButtonTitles:nil];
        [alertView show];
    }
//    else if( textfield_CVCNumber.text.length<1)
//    {
//        UIAlertView *alertView=[[UIAlertView alloc]initWithTitle:@"Alert!" message:@"Please enter valid cvc number." delegate:nil cancelButtonTitle:@"OK" otherButtonTitles:nil];
//        [alertView show];
//    }
    else if( textfield_RoutingNumber.text.length<1)
    {
        UIAlertView *alertView=[[UIAlertView alloc]initWithTitle:@"Alert!" message:@"Please enter routing number." delegate:nil cancelButtonTitle:@"OK" otherButtonTitles:nil];
        [alertView show];
    }
    else if( textfield_DateOfBirth.text.length<1)
    {
        UIAlertView *alertView=[[UIAlertView alloc]initWithTitle:@"Alert!" message:@"Please enter your date of birth." delegate:nil cancelButtonTitle:@"OK" otherButtonTitles:nil];
        [alertView show];
    }
    else if( textfield_Region.text.length<1)
    {
        UIAlertView *alertView=[[UIAlertView alloc]initWithTitle:@"Alert!" message:@"Please enter your region." delegate:nil cancelButtonTitle:@"OK" otherButtonTitles:nil];
        [alertView show];
    }
    else if( textfield_Locality.text.length<1)
    {
        UIAlertView *alertView=[[UIAlertView alloc]initWithTitle:@"Alert!" message:@"Please enter your locality." delegate:nil cancelButtonTitle:@"OK" otherButtonTitles:nil];
        [alertView show];
    }
    else if( textfield_StreetAddress.text.length<1)
    {
        UIAlertView *alertView=[[UIAlertView alloc]initWithTitle:@"Alert!" message:@"Please enter your street address." delegate:nil cancelButtonTitle:@"OK" otherButtonTitles:nil];
        [alertView show];
    }
    else if( textfield_PostalCode.text.length<1)
    {
        UIAlertView *alertView=[[UIAlertView alloc]initWithTitle:@"Alert!" message:@"Please enter your postal code." delegate:nil cancelButtonTitle:@"OK" otherButtonTitles:nil];
        [alertView show];
    }

    else
    {
        if ([Helper isConnectedToInternet])
        {
           // [SVProgressHUD showWithStatus:@"Saving..."];
            [self performSelector:@selector(submitAccountDetailMethodCall) withObject:nil afterDelay:0.3];
            
        }
        else
        {
            UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"No Internet" message:@"Please check your internet connection." delegate:nil cancelButtonTitle:@"OK" otherButtonTitles:nil];
            [alert show];
            
        }

    }

}

-(void)submitAccountDetailMethodCall
{
  /*  'email'     => 'required|email',
    'id'     => 'required',
    'name'     => 'required',
    'lastName'     => 'required',
    'firstName'     => 'required',
    'phone'     => 'required',
    'dateOfBirth'     => 'required',
    'phone'     => 'required',
    'ssn'  => 'required',
    'accountNumber'  => 'required',
    'cvv'     => 'required',
    'routingNumber'  => 'required',
    'region'  => 'required',
    'locality'  => 'required',
    'streetAddress'  => 'required',
    'postalCode'  => 'required',*/
    
    
   /* [defaults setValue:[json objectForKey:@"token"] forKey:@"user_token"];
    [defaults setValue:[dict objectForKey:@"id"] forKey:@"user_id"];
    [defaults setObject:str_nameProfile forKey:@"p_name"];
    [defaults setObject:str_emailProfile forKey:@"p_email"];
    [defaults setObject:str_aboutProfile forKey:@"p_about"];
    [defaults setObject:str_phoneprofile forKey:@"p_phone"];
    [defaults setObject:str_profilephoto forKey:@"p_photo"];
    [defaults setObject:str_rating forKey:@"rating_user"];
    [defaults setObject:[defaults valueForKey:@"user_type"] forKey:@"status"];*/
    [SVProgressHUD setDefaultMaskType:SVProgressHUDMaskTypeClear];
    [SVProgressHUD show];
    
    NSMutableDictionary *jsonDict=[[NSMutableDictionary alloc]init];
    
    [jsonDict setObject:[defaults objectForKey:@"p_name"] forKey:@"name"];
    [jsonDict setObject:textfield_AccountNumber.text forKey:@"accountNumber"];
    [jsonDict setObject:textfield_EmailAddress.text forKey:@"email"];
    [jsonDict setObject:textfield_PhoneNumber.text forKey:@"phone"];
    [jsonDict setObject:[defaults objectForKey:@"user_id"]forKey:@"id"];
    [jsonDict setObject:textfield_SSNNumber.text forKey:@"ssn"];
    //[jsonDict setObject:textfield_CVCNumber.text forKey:@"cvv"];
    [jsonDict setObject:textfield_FirstName.text forKey:@"firstName"];
    [jsonDict setObject:textfield_lastName.text forKey:@"lastName"];
    [jsonDict setObject:textfield_RoutingNumber.text forKey:@"routingNumber"];
    [jsonDict setObject:textfield_DateOfBirth.text forKey:@"dateOfBirth"];
    [jsonDict setObject:textfield_Region.text forKey:@"region"];
    [jsonDict setObject:textfield_Locality.text forKey:@"locality"];
    [jsonDict setObject:textfield_StreetAddress.text forKey:@"streetAddress"];
    [jsonDict setObject:textfield_PostalCode.text forKey:@"postalCode"];

    
    NSLog(@"%@dict",jsonDict);
    NSLog(@"%@JSONRepresentation dict",[jsonDict JSONRepresentation]);
    NSString *urlstr=[NSString stringWithFormat:@"%@createMerchantAccount",serviceLink];
     NSLog(@"URL with token %@",urlstr);
    id json1 = [WebService signup:jsonDict:urlstr];
    
    
    NSLog(@"Responce %@", json1);

  
    if ([[json1 objectForKey:@"message"]isEqualToString:@"Success"])
    {
        //[self setAuthentication];
        [SVProgressHUD dismiss];
        UIAlertView *alertView=[[UIAlertView alloc]initWithTitle:@"Alert!" message:@"Your Account Details Successfuly Saved." delegate:self cancelButtonTitle:@"OK" otherButtonTitles:nil];
        alertView.tag = 100;
        [alertView show];
       
    }
    else
    {
        [SVProgressHUD dismiss];
        UIAlertView *alertView=[[UIAlertView alloc]initWithTitle:@"Alert!" message:@"Your Card Details are not valid. Please enter valid card details." delegate:nil cancelButtonTitle:@"OK" otherButtonTitles:nil];
//        alertView.tag = 101;
        [alertView show];
        //[SVProgressHUD dismiss];
    }

}


- (void)alertView:(UIAlertView *)alertView clickedButtonAtIndex:(NSInteger)buttonIndex
{
    if (alertView.tag == 100)
    {
        [self.navigationController popViewControllerAnimated:YES];
    }
    else if (alertView.tag == 101)
    {
        
    }
}



#pragma txtfield delegate method
#pragma mark
- (BOOL)textFieldShouldReturn:(UITextField *)textField
{
    if (textField == textfield_FirstName)
    {
        [textfield_lastName becomeFirstResponder];
    }
    else if (textField == textfield_lastName) {
        [textfield_EmailAddress becomeFirstResponder];
    }
    
    else if (textField == textfield_EmailAddress) {
        [textfield_DateOfBirth becomeFirstResponder];
    }
    else if (textField == textfield_DateOfBirth) {
        [textfield_AccountNumber becomeFirstResponder];
    }
    else if (textField == textfield_AccountNumber) {
        [textfield_PhoneNumber becomeFirstResponder];
    }
    else if (textField == textfield_PhoneNumber) {
        [textfield_SSNNumber becomeFirstResponder];
    }
//    else if (textField == textfield_SSNNumber) {
//        
//        [textfield_CVCNumber becomeFirstResponder];
//    }
//    else if (textField == textfield_CVCNumber) {
//        
//        [textfield_RoutingNumber becomeFirstResponder];
//    }
    else if (textField == textfield_RoutingNumber) {
        [textfield_Region becomeFirstResponder];
    }
    else if (textField == textfield_Locality) {
        [textfield_Region becomeFirstResponder];
    }
    else if (textField == textfield_Region) {
        [textfield_StreetAddress becomeFirstResponder];
    }
    
    else if (textField == textfield_StreetAddress) {
        [textfield_PostalCode becomeFirstResponder];
    }
    else if (textField == textfield_PostalCode) {
        [textfield_PostalCode resignFirstResponder];
    }
    return YES;
}

-(void)textFieldDidBeginEditing:(UITextField *)textField
{
    if (textField==textfield_DateOfBirth)
    {
        UIToolbar *bar=[[UIToolbar alloc]initWithFrame:CGRectMake(0, 0, self.view.bounds.size.width,40)];
        UIBarButtonItem *item=[[UIBarButtonItem alloc]initWithTitle:@"Done" style:UIBarButtonItemStyleDone target:self action:@selector(Done:)];
        NSArray *arr=@[item];
        [bar setItems:arr];
        textfield_DateOfBirth.inputView=datePicker;
        textfield_DateOfBirth.inputAccessoryView=bar;
    }
}

-(void)Done:(id)sender
{
    NSDateFormatter *formatter = [[NSDateFormatter alloc] init];
    [formatter setDateFormat:@"dd-MM-yyyy"];
    
    textfield_DateOfBirth.text = [formatter stringFromDate:datePicker.date];
    //textfield_DateOfBirth.text = [NSString stringWithFormat:@"%@",datePicker.date];
    [textfield_DateOfBirth resignFirstResponder];
}


-(BOOL)textField:(UITextField *)textField shouldChangeCharactersInRange:(NSRange)range replacementString:(NSString *)string {
    
    if (textField==textfield_FirstName )
    {
        if (textfield_FirstName.text.length<1)
        {
            if ([string isEqualToString:@" "])
            {
                return NO;
            }
        }
        if (textfield_FirstName.text.length>50)
        {
            if ([string isEqualToString:@""]) {
                return YES;
            }
            else
            {
                return NO;
            }
        }
        NSCharacterSet *cs = [[NSCharacterSet characterSetWithCharactersInString:ACCEPTABLE_CHARACTERS] invertedSet];
        NSString *filtered = [[string componentsSeparatedByCharactersInSet:cs] componentsJoinedByString:@""];
        return [string isEqualToString:filtered];
    }
    if (textField==textfield_EmailAddress)
    {
        if (textfield_EmailAddress.text.length<1)
        {
            if ([string isEqualToString:@" "])
            {
                return NO;
            }
        }
       
        NSString *textFieldText = [textfield_EmailAddress.text stringByReplacingCharactersInRange:range withString:string];
        if (textFieldText.length==0||[textFieldText length]<=50)
        {
            return YES;
        }
        else
            return NO;
    }
    
    if (textField==textfield_DateOfBirth)
    {
        return NO;
    }
    
    if (textField==textfield_PhoneNumber)
    {
        NSString *textFieldText = [textfield_PhoneNumber.text stringByReplacingCharactersInRange:range withString:string];
        if (textFieldText.length==0||[textFieldText length]<=15 )
        {
            return YES;
        }
        else
            return NO;
    }
    if (textField==textfield_AccountNumber)
    {
        NSString *textFieldText = [textfield_AccountNumber.text stringByReplacingCharactersInRange:range withString:string];
        if (textFieldText.length==0||[textFieldText length]<=20)
        {
            return YES;
        }
        else
            return NO;
    }
    
//    if (textField==textfield_CVCNumber)
//    {
//        NSString *textFieldText = [textfield_CVCNumber.text stringByReplacingCharactersInRange:range withString:string];
//        if (textFieldText.length==0||[textFieldText length]<=4)
//        {
//            return YES;
//        }
//        else
//            return NO;
//    }
    return YES;
}


-(BOOL)isValidEmail:(NSString *)checkString
{
    BOOL stricterFilter = NO; // Discussion http://blog.logichigh.com/2010/09/02/validating-an-e-mail-address/
    NSString *stricterFilterString = @"[A-Z0-9a-z\\._%+-]+@([A-Za-z0-9-]+\\.)+[A-Za-z]{2,4}";
    NSString *laxString = @".+@([A-Za-z0-9-]+\\.)+[A-Za-z]{2}[A-Za-z]*";
    NSString *emailRegex = stricterFilter ? stricterFilterString : laxString;
    NSPredicate *emailTest = [NSPredicate predicateWithFormat:@"SELF MATCHES %@", emailRegex];
    return [emailTest evaluateWithObject:checkString];
}


@end
