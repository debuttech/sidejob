//
//  SignUpAsViewController.m
//  sideJobs
//
//  Created by DebutMac4 on 13/08/15.
//  Copyright (c) 2015 DebutMac4. All rights reserved.
//

#import "SignUpAsViewController.h"
#import "SignupViewController.h"


@interface SignUpAsViewController (){
    NSUserDefaults *defaults;
}

@end

@implementation SignUpAsViewController

- (void)viewDidLoad
{
    [super viewDidLoad];
    self.navigationController.navigationBarHidden=YES;
    defaults=[NSUserDefaults standardUserDefaults];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
   
}
-(void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:YES];
  //  self.navigationController.navigationBarHidden=YES;
}

- (IBAction)btnBack_Action:(id)sender {
    [self.navigationController popViewControllerAnimated:YES];
}

- (IBAction)btnSignUp_Action:(id)sender
{
    UIButton *selectedButton = (UIButton *)sender;
    
    NSLog(@"Selected button tag is %ld", (long)selectedButton.tag);
    if (selectedButton.tag==1)
    {
        [defaults setObject:@"customer" forKey:@"user_status"];
        [defaults synchronize];
        signupView=[self.storyboard instantiateViewControllerWithIdentifier:@"SignupViewController"];
        signupView.typeUser = @"customer";
        [self.navigationController pushViewController:signupView animated:YES];
    }
    else
    {
         [defaults setObject:@"worker" forKey:@"user_status"];
        [defaults synchronize];
    signupView=[self.storyboard instantiateViewControllerWithIdentifier:@"SignupViewController"];
        signupView.typeUser = @"worker";
    [self.navigationController pushViewController:signupView animated:YES];
    }
}

@end
