   //
//  WebService.m
//  Barter
//
//  Created by Debut Infotech on 13/12/13.
//  Copyright (c) 2013 Debut Infotech. All rights reserved.
//

#import "WebService.h"
#import "JSON.h"


#define kAFRabbitAPIBaseURLString @""
@interface WebService ()
@end

@implementation WebService

//Web Services Hitting Method called from Various Classes
#pragma mark - Web Service Method

//+ (NSMutableURLRequest *) webServices :(NSMutableDictionary *) webDict : (NSString *) 
//{
//    NSString *strUrl = [NSString stringWithFormat:@"%@", addUrl];
//    NSURL *url = [NSURL URLWithString:strUrl];
//    NSMutableURLRequest *request = [NSMutableURLRequest requestWithURL:url];
//    
//    NSError *error = NULL;
//    NSString *jsonString = [webDict JSONRepresentation];
//    
//    NSLog(@"Json string is : %@", jsonString);
//    
//    [request setHTTPMethod:@"POST"];
//    [request setValue:@"application/json" forHTTPHeaderField:@"Content-type"];
//    [request setValue:@"application/json" forHTTPHeaderField:@"Accept"];
//    [request setValue:@"" forHTTPHeaderField:@"Authorization"];
//    
//    [request setHTTPBody:[NSData dataWithBytes:[jsonString UTF8String] length:jsonString.length]];
//      return request;
//}

+(NSMutableArray *)signup :(NSMutableDictionary *) webDict : (NSString *)serviceType
{
    NSString *strUrl = [NSString stringWithFormat:@"%@", serviceType];
    NSURL *url = [NSURL URLWithString:strUrl];
    NSMutableURLRequest *request = [NSMutableURLRequest requestWithURL:url];
    NSError *error = NULL;
    
    NSString *jsonString = [webDict JSONRepresentation];
    
    NSLog(@"%@", jsonString);
    
    [request setHTTPMethod:@"POST"];
    [request setValue:@"application/json" forHTTPHeaderField:@"Content-type"];
    [request setValue:@"application/json" forHTTPHeaderField:@"Accept"];
    [request setHTTPBody:[NSData dataWithBytes:[jsonString UTF8String] length:jsonString.length]];
    
    NSHTTPURLResponse *response = nil;
    NSData *responseData = [NSURLConnection sendSynchronousRequest:request returningResponse:&response error:&error];
    NSMutableString *responseStr = [[NSMutableString alloc] initWithData:responseData encoding:NSUTF8StringEncoding];
    NSMutableArray *arr = [[NSMutableArray alloc]init];
    arr = [responseStr JSONValue];
    return arr;
    
}

+(NSMutableArray *)GetwithoutDict : (NSString *)serviceType
{
    NSString *strUrl = [NSString stringWithFormat:@"%@", serviceType];
    NSURL *url = [NSURL URLWithString:strUrl];
    NSMutableURLRequest *request = [NSMutableURLRequest requestWithURL:url];
    NSError *error = NULL;
        
    [request setHTTPMethod:@"GET"];
    [request setValue:@"application/json" forHTTPHeaderField:@"Content-type"];
    [request setValue:@"application/json" forHTTPHeaderField:@"Accept"];
    
    NSHTTPURLResponse *response = nil;
    NSData *responseData = [NSURLConnection sendSynchronousRequest:request returningResponse:&response error:&error];
    NSMutableString *responseStr = [[NSMutableString alloc] initWithData:responseData encoding:NSUTF8StringEncoding];
    NSMutableArray *arr = [[NSMutableArray alloc]init];
    arr = [responseStr JSONValue];
    
    return arr;
}
+(NSMutableArray *)POSTwithoutDict : (NSString *)serviceType
{
    NSString *strUrl = [NSString stringWithFormat:@"%@", serviceType];
    NSURL *url = [NSURL URLWithString:strUrl];
    NSMutableURLRequest *request = [NSMutableURLRequest requestWithURL:url];
    NSError *error = NULL;
    
    [request setHTTPMethod:@"POST"];
    [request setValue:@"application/json" forHTTPHeaderField:@"Content-type"];
    [request setValue:@"application/json" forHTTPHeaderField:@"Accept"];
    
    NSHTTPURLResponse *response = nil;
    NSData *responseData = [NSURLConnection sendSynchronousRequest:request returningResponse:&response error:&error];
    NSMutableString *responseStr = [[NSMutableString alloc] initWithData:responseData encoding:NSUTF8StringEncoding];
    NSMutableArray *arr = [[NSMutableArray alloc]init];
    arr = [responseStr JSONValue];
    
    return arr;
}


+(NSMutableArray *)deleteData : (NSString *)serviceType
{
    NSString *strUrl = [NSString stringWithFormat:@"%@", serviceType];
    NSURL *url = [NSURL URLWithString:strUrl];
    NSMutableURLRequest *request = [NSMutableURLRequest requestWithURL:url];
    NSError *error = NULL;
    
    [request setHTTPMethod:@"DELETE"];
    [request setValue:@"application/json" forHTTPHeaderField:@"Content-type"];
    [request setValue:@"application/json" forHTTPHeaderField:@"Accept"];
    
    NSHTTPURLResponse *response = nil;
    NSData *responseData = [NSURLConnection sendSynchronousRequest:request returningResponse:&response error:&error];
    NSMutableString *responseStr = [[NSMutableString alloc] initWithData:responseData encoding:NSUTF8StringEncoding];
    NSMutableArray *arr = [[NSMutableArray alloc]init];
    arr = [responseStr JSONValue];
    
    return arr;
}
//+ (NSMutableArray *)signupT :(NSMutableDictionary *)  webDict :(NSString *) addUrl
//{
//    NSString *strUrl = [NSString stringWithFormat:@"%@", addUrl];
//    NSURL *url = [NSURL URLWithString:strUrl];
//    NSMutableURLRequest *request = [NSMutableURLRequest requestWithURL:url];
//    NSError *error = NULL;
//    
//    [request setHTTPMethod:@"DELETE"];
//    [request setValue:@"application/json" forHTTPHeaderField:@"Content-type"];
//    [request setValue:@"application/json" forHTTPHeaderField:@"Accept"];
//    //[request setValue:@"application/json" forHTTPHeaderField:@"Bearer"];
//    
//    NSHTTPURLResponse *response = nil;
//    NSData *responseData = [NSURLConnection sendSynchronousRequest:request returningResponse:&response error:&error];
//    NSMutableString *responseStr = [[NSMutableString alloc] initWithData:responseData encoding:NSUTF8StringEncoding];
//    NSMutableArray *arr = [[NSMutableArray alloc]init];
//    arr = [responseStr JSONValue];
//    
//    return arr;
//  
//}

@end
