//
//  MakePaymentCell.h
//  SideJobsMileston2
//
//  Created by Debut Mac 4 on 07/09/15.
//  Copyright (c) 2015 debut.infotech. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface MakePaymentCell : UITableViewCell

{
    IBOutlet UIView *view_makepayment;
}
@property (strong, nonatomic) IBOutlet UILabel *lbl_Title;
@property (strong, nonatomic) IBOutlet UILabel *lbl_userName;
@property (strong, nonatomic) IBOutlet UILabel *lbl_StartTime;
@property (strong, nonatomic) IBOutlet UILabel *lbl_CompleteTime;
@property (strong, nonatomic) IBOutlet UIImageView *img_user;

@property (strong, nonatomic) IBOutlet UILabel *lbl_Amount;
@property (strong, nonatomic) IBOutlet UIButton *btn_makePayment;


@end
