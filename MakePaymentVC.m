//
//  MakePaymentVC.m
//  SideJobsMileston2
//
//  Created by Debut Mac 4 on 07/09/15.
//  Copyright (c) 2015 debut.infotech. All rights reserved.
//

#import "MakePaymentVC.h"
#import "MakePaymentCell.h"

@interface MakePaymentVC ()

@end

@implementation MakePaymentVC

- (void)viewDidLoad {
    [super viewDidLoad];
    NSLog(@"%@",_dict);
    self.navigationItem.hidesBackButton=YES;
    self.navigationItem.title=@"Make Payment";
    UIButton *button=[UIButton buttonWithType:UIButtonTypeCustom];
    [button setFrame:CGRectMake(0, 0, 23, 21)];
    [button addTarget:self action:@selector(leftbutton:) forControlEvents:UIControlEventTouchUpInside];
    [button setBackgroundImage:[UIImage imageNamed:@"arrow"] forState:UIControlStateNormal];
    self.navigationItem.leftBarButtonItem=[[UIBarButtonItem alloc]initWithCustomView:button];
    
  
    
}
-(void)leftbutton:(UIButton*)sender
{
    [self.navigationController popViewControllerAnimated:YES];

}




-(NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    return 1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section;
{
    return 1;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    static NSString *customIdentifierListingWorkers = @"makePaymentCell";
    MakePaymentCell *cell = [tableView dequeueReusableCellWithIdentifier:customIdentifierListingWorkers];
    if (cell == nil)
    {
        cell = [[MakePaymentCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:customIdentifierListingWorkers];
    }
    NSString *img=[[[[self.dict objectForKey:@"job_awarded"]objectAtIndex:0]objectForKey:@"user"]objectForKey:@"profile_image"];
    NSString *profileImg;
    if ([img isEqualToString:@""])
    {
        
    }
    else
    {
        profileImg=[NSString stringWithFormat:@"%@%@",serviceLink,img];
        dispatch_async(dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_BACKGROUND, 0), ^{
            NSData * imageData = [NSData dataWithContentsOfURL:[NSURL URLWithString:profileImg]];
            UIImage *image = [UIImage imageWithData:imageData];
            
            dispatch_sync(dispatch_get_main_queue(), ^{
                cell.img_user.image = image;
                
            });
        });
    }
    
    cell.lbl_Title.text=[self.dict objectForKey:@"title"];
    cell.lbl_userName.text=[[[[self.dict objectForKey:@"job_awarded"]objectAtIndex:0]objectForKey:@"user"]objectForKey:@"name"];
    cell.lbl_Amount.text=[NSString stringWithFormat:@"$%@",[[[self.dict objectForKey:@"job_awarded"]objectAtIndex:0]objectForKey:@"proposed_amount"] ];
    
    
    //complete job time
    NSString *completeTime=[[[self.dict objectForKey:@"job_applications"] objectAtIndex:0]objectForKey:@"updated_at"];
    NSArray *Arrtime=[completeTime componentsSeparatedByString:@" "];
    NSString* seprateCompleteTime=[Arrtime objectAtIndex:0];
    cell.lbl_CompleteTime.text=seprateCompleteTime;
    
    //start job time
    NSString *startTime=[[[self.dict objectForKey:@"job_awarded"] objectAtIndex:0]objectForKey:@"created_at"];
    NSArray *ArrStart=[startTime componentsSeparatedByString:@" "];
    NSString* seprateStarttime=[ArrStart objectAtIndex:0];
    cell.lbl_StartTime.text=seprateStarttime;

    
    
    
//    cell.lbl_CompleteTime.text=[[[self.dict objectForKey:@"job_applications"]objectAtIndex:0]objectForKey:@"updated_at"];
//    cell.lbl_StartTime.text=[[[self.dict objectForKey:@"job_awarded"]objectAtIndex:0]objectForKey:@"created_at"];

    return cell;
}



- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

@end
